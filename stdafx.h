#if !defined (STDAFX_H)
#define STDAFX_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2001.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: StdAfx.h
//
//     Description: MFC Pre-Compiled Header Support
//
//          Author: Marc Gustafson
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 7 %
//           %name: stdafx.h %
//        %version: 11.1.10 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#if _MSC_VER > 1000
#pragma once
#endif

#include "TargetVer.h"

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS

#define _AFX_ALL_WARNINGS

#include <afxwin.h>            // MFC core and standard components
#include <afxext.h>            // MFC extensions
#include <afxmt.h>             // MFC multithreading suport
#include <afxdisp.h>           // MFC dispatch support
#include <afxsock.h>           // MFC socket support
#include <afxtempl.h>          // MFC template support

#include <shlobj.h>            // Windows Shell API Support

#include <afxeditbrowsectrl.h> // MFC edit browse control support

#include <comdef.h>            // Native C++ compiler COM support - main definitions header
#include <comutil.h>           // Native C++ compiler COM support - BSTR, VARIANT wrappers header
#pragma warning (push)
#pragma warning (disable:4091)
#include <dbghelp.h>           // Windows mini-dump diagnostics support
#pragma warning (pop)
#include <msxml6.h>            // XML serialization support
#include <dpapi.h>             // Data Protection API

#include <array>               // STL array support
#include <atomic>              // STL atomic support
#include <cctype>              // STL character type support
#include <deque>               // STL deque container class support
#include <memory>              // STL memory management
#include <mutex>               // STL mutex support
#include <regex>               // STL regular expression support
#include <set>                 // STL set container class support
#include <vector>              // STL vector container class support

#import <msxml6.dll>           // XML serialization support

#include <openssl\blowfish.h>  // OpenSSL Blowfish symmetric cipher support

#if defined (min)
#undef min                     // undefine macro version of min function
#endif

#if defined (max)
#undef max                     // undefine macro version of max function
#endif

#include <strsafe.h>           // Safer C library string routine replacements

#ifndef _SDL_BANNED_RECOMMENDED
#define _SDL_BANNED_RECOMMENDED
#endif

#include "Banned.h"            // Microsoft SDL banned APIs
#include "Global.h"            // Application level support
#include "Utility.h"           // Common utility support

#if defined (UNICODE)
using CMatch              = std::wcmatch;
using CRegexIterator      = std::wcregex_iterator;
using CRegexTokenIterator = std::wcregex_token_iterator;
using CSubMatch           = std::wcsub_match;
using CRegex              = std::wregex;
#else
using CMatch              = std::cmatch;
using CRegexIterator      = std::cregex_iterator;
using CRegexTokenIterator = std::cregex_token_iterator;
using CSubMatch           = std::csub_match;
using CRegex              = std::regex;
#endif

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  04/14/2006  MEG     initial revision
//  12/05/2006  MCC     modified for QAC++ compliance
//  05/06/2009  MCC     implemented support for HASP adapter
//  12/01/2009  TAN     baselined Windows target version header file
//  12/09/2009  MCC     modified to reference MSXML6 explicitly
//  12/18/2009  MCC     baselined and refactored common utility support
//  03/25/2010  MCC     moved plate definitions to PDCLib project
//  06/18/2010  MCC     modified to generate dump file on abnormal termination
//  06/21/2010  MCC     added Microsoft SDL include files
//  09/16/2010  MCC     moved TIFF library class into PDCLib project
//  09/21/2011  MCC     baselined message handler class
//  06/03/2013  MCC     generalized ring buffer implementation
//  09/19/2013  MCC     added explicit HASP feature check
//  12/17/2014  MCC     implemented critical sections with C++11 concurrency
//  01/09/2015  MCC     templatized number of elements macro
//  07/02/2015  MCC     added method editor to remote control interface
//  07/15/2015  MCC     default browse controls to unique MRU directory
//  07/21/2015  MCC     corrected compiler warnings
//  12/01/2017  MCC     updated worker thread implementation
//  07/11/2018  MCC     implemented support for encrypted calibrations
//
// ============================================================================

#endif
