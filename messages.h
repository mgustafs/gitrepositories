// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================
// ============================================================================
//
//            Name: Messages.h
//
//     Description: Message Text declarations
//
//          Author: Mike Conner
//
// ============================================================================
// ============================================================================
//
//      %subsystem: 1 %
//           %name: messages.mc %
//        %version: 4 %
//          %state: %
//         %cvtype: ascii %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================
//
//  Values are 32 bit values laid out as follows:
//
//   3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1
//   1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//  +---+-+-+-----------------------+-------------------------------+
//  |Sev|C|R|     Facility          |               Code            |
//  +---+-+-+-----------------------+-------------------------------+
//
//  where
//
//      Sev - is the severity code
//
//          00 - Success
//          01 - Informational
//          10 - Warning
//          11 - Error
//
//      C - is the Customer code flag
//
//      R - is a reserved bit
//
//      Facility - is the facility code
//
//      Code - is the facility's status code
//
//
// Define the facility codes
//


//
// Define the severity codes
//


//
// MessageId: CATEGORY_NONE
//
// MessageText:
//
// None
//
#define CATEGORY_NONE                    ((WORD)0x00000001L)

//
// MessageId: MSG_EVENTLOG_ERROR_TYPE
//
// MessageText:
//
// %1
//
#define MSG_EVENTLOG_ERROR_TYPE          ((DWORD)0x00000003L)

//
// MessageId: MSG_EVENTLOG_WARNING_TYPE
//
// MessageText:
//
// %1
//
#define MSG_EVENTLOG_WARNING_TYPE        ((DWORD)0x00000004L)

//
// MessageId: MSG_EVENTLOG_INFORMATION_TYPE
//
// MessageText:
//
// %1
//
#define MSG_EVENTLOG_INFORMATION_TYPE    ((DWORD)0x00000005L)

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  09/28/2010  MCC     initial revision
//  09/29/2010  MCC     changed service email address
//  09/16/2011  MCC     generalized event logging interface
//  08/16/2013  MCC     implemented workaround for event id error message
//
// ============================================================================
