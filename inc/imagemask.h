#if !defined (IMAGEMASK_H)
#define IMAGEMASK_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: ImageMask.h
//
//     Description: Image Mask class declaration
//
//          Author: Marc Gustafson
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: imagemask.h %
//        %version: 5 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mgustafs %
//  %date_modified: %
//
// ============================================================================

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CImageMask : public CObject
#else
class __declspec (dllimport) CImageMask : public CObject
#endif
{
  DECLARE_SERIAL (CImageMask)

public:
  explicit CImageMask (CString const & identifier);
  explicit CImageMask (int pixelsPerWell, CPoint const & plateOrigin, CPoint const & maskOrigin, double pixelsPerMM);
  virtual ~CImageMask ();

  inline CString GetIdentifier (void) const { return m_identifier; }

  inline int GetPixelsPerWell (void) const { return m_pixelsPerWell; }
  inline void SetPixelsPerWell (int pixelsPerWell) { m_pixelsPerWell = pixelsPerWell; }

  inline CPoint GetPlateOrigin (void) const { return m_plateOrigin; }
  inline void SetPlateOrigin (CPoint const & plateOrigin) { m_plateOrigin = plateOrigin; }

  inline CPoint GetMaskOrigin (void) const { return m_maskOrigin; }
  inline void SetMaskOrigin (CPoint const & maskOrigin) { m_maskOrigin = maskOrigin; }

  inline double GetPixelsPerMM (void) const { return m_pixelsPerMM; }
  inline void SetPixelsPerMM (double pixelsPerMM) { m_pixelsPerMM = pixelsPerMM; }

  inline CString GetComment (void) const { return m_comment; }
  inline void SetComment (CString const & comment) { m_comment = comment; }

  inline CTime GetTimeStamp (void) const { return m_timeStamp; }
  inline void SetTimeStamp (void) { m_timeStamp = CTime::GetCurrentTime (); }

  void SetImageMask (std::vector <WORD> & mask,
                     int                & horizontalSize,
                     int                & verticalSize,
                     int                  numRows,
                     int                  numColumns,
                     double               rowSpacing,
                     double               columnSpacing) const;

  virtual void Serialize (CArchive & ar);

protected:
  explicit CImageMask (void);

private:
  CString m_identifier;
  int m_pixelsPerWell;
  CPoint m_plateOrigin;
  CPoint m_maskOrigin;
  double m_pixelsPerMM;
  CString m_comment;
  CTime m_timeStamp;

  // copy construction and assignment not allowed for this class

  CImageMask (const CImageMask &);
  CImageMask & operator = (const CImageMask &);
};

#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CImageMaskException
#else
class __declspec (dllimport) CImageMaskException
#endif
{
public:
  explicit CImageMaskException (void);
  explicit CImageMaskException (CException * e, bool autoDelete = false);
  explicit CImageMaskException (CString const & errorMessage);
  CImageMaskException (CImageMaskException const & other);
  virtual ~CImageMaskException ();

  CString GetErrorMessage (void) const;

private:
  CString const m_errorMessage;

  // assignment not allowed for this class

  CImageMaskException & operator = (CImageMaskException const &);
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  09/09/2010  MEG     initial revision
//  09/15/2010  MEG     simplified origin accessor/mutator
//  09/16/2010  MEG     added horizontal and vertical size setting to mask
//  09/20/2010  MCC     implemented support for image mask visualization
//  03/22/2016  MEG     added plate origin to mask to support wider image view
//
// ============================================================================

#endif
