#if !defined (CELDYNE_H)
#define CELDYNE_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: Celdyne.h
//
//     Description: Celdyne Beckhoff class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: celdyne.h %
//        %version: 3 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "Device.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CCeldyne : public CDevice
#else
class __declspec (dllimport) CCeldyne : public CDevice
#endif
{
  DECLARE_SERIAL (CCeldyne)

public:
  explicit CCeldyne (void);
  virtual ~CCeldyne ();

  inline int GetADSControllerID (void) const { return m_adsControllerID; }
  inline void SetADSControllerID (int adsControllerID) { m_adsControllerID = adsControllerID; }

  inline WORD GetAnalogPortNumber (void) const { return m_analogPortNumber; }
  inline void SetAnalogPortNumber (WORD analogPortNumber) { m_analogPortNumber = analogPortNumber; }

  inline WORD GetDiscretePortNumber (void) const { return m_discretePortNumber; }
  inline void SetDiscretePortNumber (WORD discretePortNumber) { m_discretePortNumber = discretePortNumber; }

  inline bool GetCeldynePresent (void) const { return m_celdynePresent; }
  inline void SetCeldynePresent (bool celdynePresent) { m_celdynePresent = celdynePresent; }

  inline bool GetGripperPresent (void) const { return m_gripperPresent; }
  inline void SetGripperPresent (bool gripperPresent) { m_gripperPresent = gripperPresent; }

  inline bool GetShakerPresent (void) const { return m_shakerPresent; }
  inline void SetShakerPresent (bool shakerPresent) { m_shakerPresent = shakerPresent; }

  inline int GetPortNumber (void) const { return m_shakerPortNumber; }
  inline void SetPortNumber (int shakerPortNumber) { m_shakerPortNumber = shakerPortNumber; }

  inline bool GetShakerSimulation (void) const { return m_shakerSimulation; }
  inline void SetShakerSimulation (bool shakerSimulation) { m_shakerSimulation = shakerSimulation; }

  inline CString GetLocationsFileName (void) const { return m_locationsFileName; }
  inline void SetLocationsFileName (CString const & locationsFileName) { m_locationsFileName = locationsFileName; }

  inline CString GetPlateDefinitionsFileName (void) const { return m_plateDefinitionsFileName; }
  inline void SetPlateDefinitionsFileName (CString const & plateDefinitionsFileName) { m_plateDefinitionsFileName = plateDefinitionsFileName; }

  virtual bool IsFeaturePresent (void) const;

  virtual void Accept (CDeviceVisitor & deviceVisitor);
  virtual void Serialize (CArchive & ar);

private:
  int m_adsControllerID;
  WORD m_analogPortNumber;
  WORD m_discretePortNumber;
  bool m_celdynePresent;
  bool m_gripperPresent;
  bool m_shakerPresent;
  int m_shakerPortNumber;
  bool m_shakerSimulation;
  CString m_locationsFileName;
  CString m_plateDefinitionsFileName;

  // copy construction and assignment not allowed for this class

  CCeldyne (const CCeldyne &);
  CCeldyne & operator = (const CCeldyne &);
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  05/06/2014  MCC     initial revision
//  05/08/2014  MCC     implemented Celdyne gripper and shaker options
//  10/20/2017  MCC     implemented support for optional Celdyne presence
//
// ============================================================================

#endif
