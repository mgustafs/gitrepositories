#if !defined (GLOBAL_H)
#define GLOBAL_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2006.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: Global.h
//
//     Description: Global declarations
//
//          Author: Marc Gustafson
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 4 %
//           %name: global.h %
//        %version: 39.1.2 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "ControllerType.h"
#include "DeviceType.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
// G L O B A L   C O N S T A N T   D E F I N T I O N S

const UINT METHOD_SCHEMA_VERSION_0    (static_cast <UINT> (-1));
const UINT METHOD_SCHEMA_VERSION_1    (1);
const UINT METHOD_SCHEMA_VERSION_2    (2);
const UINT METHOD_SCHEMA_VERSION_3    (3);
const UINT METHOD_SCHEMA_VERSION_4    (4);
const UINT METHOD_SCHEMA_VERSION_5    (5);
const UINT METHOD_SCHEMA_VERSION_6    (6);
const UINT METHOD_SCHEMA_VERSION_7    (7);
const UINT METHOD_SCHEMA_VERSION_8    (8);
const UINT METHOD_SCHEMA_VERSION_9    (9);
const UINT METHOD_SCHEMA_VERSION_10   (10);
const UINT METHOD_SCHEMA_VERSION_11   (11);
const UINT METHOD_SCHEMA_VERSION_12   (12);
const UINT METHOD_SCHEMA_VERSION_13   (13);

const UINT TOPOLOGY_SCHEMA_VERSION_0  (static_cast <UINT> (-1));
const UINT TOPOLOGY_SCHEMA_VERSION_1  (1);
const UINT TOPOLOGY_SCHEMA_VERSION_2  (2);
const UINT TOPOLOGY_SCHEMA_VERSION_3  (3);
const UINT TOPOLOGY_SCHEMA_VERSION_4  (4);
const UINT TOPOLOGY_SCHEMA_VERSION_5  (5);
const UINT TOPOLOGY_SCHEMA_VERSION_6  (6);
const UINT TOPOLOGY_SCHEMA_VERSION_7  (7);
const UINT TOPOLOGY_SCHEMA_VERSION_8  (8);
const UINT TOPOLOGY_SCHEMA_VERSION_9  (9);
const UINT TOPOLOGY_SCHEMA_VERSION_10 (10);
const UINT TOPOLOGY_SCHEMA_VERSION_11 (11);
const UINT TOPOLOGY_SCHEMA_VERSION_12 (12);
const UINT TOPOLOGY_SCHEMA_VERSION_13 (13);
const UINT TOPOLOGY_SCHEMA_VERSION_14 (14);
const UINT TOPOLOGY_SCHEMA_VERSION_15 (15);
const UINT TOPOLOGY_SCHEMA_VERSION_16 (16);
const UINT TOPOLOGY_SCHEMA_VERSION_17 (17);
const UINT TOPOLOGY_SCHEMA_VERSION_18 (18);
const UINT TOPOLOGY_SCHEMA_VERSION_19 (19);
const UINT TOPOLOGY_SCHEMA_VERSION_20 (20);
const UINT TOPOLOGY_SCHEMA_VERSION_21 (21);
const UINT TOPOLOGY_SCHEMA_VERSION_22 (22);
const UINT TOPOLOGY_SCHEMA_VERSION_23 (23);
const UINT TOPOLOGY_SCHEMA_VERSION_24 (24);
const UINT TOPOLOGY_SCHEMA_VERSION_25 (25);
const UINT TOPOLOGY_SCHEMA_VERSION_26 (26);

// G L O B A L   F U N C T I O N   D E C L A R A T I O N S

CArchive & operator>> (CArchive & ar, EControllerType & rhs);
CArchive & operator<< (CArchive & ar, EControllerType const & rhs);

CArchive & operator>> (CArchive & ar, EDeviceType & rhs);
CArchive & operator<< (CArchive & ar, EDeviceType const & rhs);
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  04/14/2006  MEG     initial revision
//  11/28/2006  MCC     baselined serializable topology class hierarchy
//  12/06/2006  MCC     decoupled method and topology version schema constants
//  09/06/2007  MCC     implemented purge valve feature
//  10/30/2007  MCC     implemented pintool barcode reader support
//  01/11/2008  TAN     added TOPOLOGY_SCHEMA_VERSION_4
//  04/04/2008  TAN     added TOPOLOGY_SCHEMA_VERSION_5
//  07/03/2006  MCC     implemented skip step feature
//  08/22/2008  MCC     added method editor class to project
//  05/06/2009  MCC     implemented support for HASP adapter
//  05/06/2009  MCC     added template file name to topology
//  06/23/2009  MEG     added TOPOLOGY_SCHEMA_VERSION_7
//  12/18/2009  MCC     baselined and refactored common utility support
//  01/15/2010  MCC     implemented support for application data folder
//  03/18/2010  MCC     implemented support for plate definition variable type
//  03/29/2010  MCC     implemented support for user defined plate types
//  05/24/2010  MCC     implemented support for conditional reference teaching
//  08/09/2010  MCC     enhanced functionality of step iterator
//  08/10/2010  MCC     implemented support for variable grouping
//  09/13/2010  MCC     implemented infrastructure for head definition access
//  10/11/2010  MEG     implemented support for Raven barcode reader
//  11/05/2010  MCC     implemented support for limited application access
//  02/18/2011  MCC     implemented support for method variable logging
//  03/22/2011  MCC     implemented support modifying enumeration keys
//  06/03/2011  MEG     implemented support for Libra6 plate definitions
//  07/06/2011  TAN     implemented support for G2 Dispenser barcode reader
//  09/22/2011  MCC     added Summit message handler to Device Editor
//  10/10/2011  MEG     added shaker simulation
//  03/13/2012  TAN     implemented support for stacker and balance for G2 Dispenser
//  05/22/2012  MCC     implemented support for pin-tool plate definition
//  07/03/2012  MCC     added support for variable specific plate definitions
//  07/20/2012  MCC     implemented support for iterator comment
//  07/30/2012  MCC     implemented support for left/right handed dispensers
//  10/04/2012  MCC     implemented support for optional high speed centrifuge
//  03/27/2014  MEG     implemented optional barcode reader for lumi reader
//  07/17/2014  MCC     implemented support for executing shell commands
//  11/24/2014  MEG     added second generation lumi reader
//  01/27/2015  TAN     removed hasStacker boolean on G2Dispenser
//  11/04/2015  MCC     implemented support for simulation mode at device level
//  10/20/2017  MCC     implemented support for optional Celdyne presence
//  07/12/2018  MCC     implemented support for encrypted calibrations
//
// ============================================================================

#endif
