#if !defined (DISPENSERGALIL_H)
#define DISPENSERGALIL_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: DispenserGalil.h
//
//     Description: DispenserGalil class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: dispensergalil.h %
//        %version: 7 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "Device.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CDispenserGalil : public CDevice
#else
class __declspec (dllimport) CDispenserGalil : public CDevice
#endif
{
  DECLARE_SERIAL (CDispenserGalil)

public:
  explicit CDispenserGalil (void);
  virtual ~CDispenserGalil ();

  inline CString GetLocationsFileName (void) const
    { return m_locationsFileName; }
  inline void SetLocationsFileName (CString const & locationsFileName)
    { m_locationsFileName = locationsFileName; }
  inline CString GetPlateDefinitionsFileName (void) const
    { return m_plateDefinitionsFileName; }
  inline void SetPlateDefinitionsFileName (CString const & plateDefinitionsFileName)
    { m_plateDefinitionsFileName = plateDefinitionsFileName; }
  inline bool GetHasLoadingDock (void) const
    { return m_hasLoadingDock; }
  inline void SetHasLoadingDock (bool hasLoadingDock)
    { m_hasLoadingDock = hasLoadingDock; }
  inline bool GetHasAutomationInterface (void) const
    { return m_hasAutomationInterface; }
  inline void SetHasAutomationInterface (bool hasAutomationInterface)
    { m_hasAutomationInterface = hasAutomationInterface; }
  inline bool GetHasPurgeValve (void) const
    { return m_hasPurgeValve; }
  inline void SetHasPurgeValve (bool hasPurgeValve)
    { m_hasPurgeValve = hasPurgeValve; }
  inline int GetPortNumber (void) const
    { return m_portNumber; }
  inline void SetPortNumber (int portNumber)
    { m_portNumber = portNumber; }
  inline int GetNumTipHeads (void) const
    { return m_numTipHeads; }
  inline void SetNumTipHeads (int numTipHeads)
    { m_numTipHeads = numTipHeads; }
  inline WORD GetAnalogPortNumber (void) const
    { return m_analogPortNumber; }
  inline void SetAnalogPortNumber (WORD analogPortNumber)
    { m_analogPortNumber = analogPortNumber; }
  inline WORD GetDiscretePortNumber (void) const
    { return m_discretePortNumber; }
  inline void SetDiscretePortNumber (WORD discretePortNumber)
    { m_discretePortNumber = discretePortNumber; }
  inline int GetControllerID (int index) const
    { return m_controllerID[index]; }
  inline void SetControllerID (int index, int controllerID)
    { m_controllerID[index] = controllerID; }

  virtual bool IsFeaturePresent (void) const;

  virtual void Accept (CDeviceVisitor & deviceVisitor);
  virtual void Serialize (CArchive & ar);

private:
  CString m_locationsFileName;
  CString m_plateDefinitionsFileName;
  bool m_hasLoadingDock;
  bool m_hasAutomationInterface;
  bool m_hasPurgeValve;
  int m_portNumber;
  int m_numTipHeads;
  WORD m_analogPortNumber;
  WORD m_discretePortNumber;
  CArray <int> m_controllerID;

  // copy construction and assignment not allowed for this class

  CDispenserGalil (const CDispenserGalil &);
  CDispenserGalil & operator = (const CDispenserGalil &);
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  11/28/2006  MCC     initial revision
//  11/30/2006  MCC     corrected analog/discrete port number data types
//  09/06/2007  MCC     implemented purge valve feature
//  03/29/2010  MCC     implemented support for user defined plate types
//  05/24/2010  MCC     implemented support for conditional reference teaching
//  11/05/2010  MCC     implemented support for limited application access
//  01/10/2012  MCC     implemented support for device specific feature IDs
//
// ============================================================================

#endif
