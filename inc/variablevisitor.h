#if !defined (VARIABLEVISITOR_H)
#define VARIABLEVISITOR_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: VariableVisitor.h
//
//     Description: Variable Visitor class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: variablevisitor.h %
//        %version: 1 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
class CVariableByteArray;
class CVariableDouble;
class CVariableEnum;
class CVariableInt;
class CVariableString;

#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CVariableVisitor : public CObject
#else
class __declspec (dllimport) CVariableVisitor : public CObject
#endif
{
  DECLARE_DYNAMIC (CVariableVisitor)

public:
  virtual ~CVariableVisitor ();

  virtual void Visit (CVariableByteArray * variable) = 0;
  virtual void Visit (CVariableDouble * variable) = 0;
  virtual void Visit (CVariableEnum * variable) = 0;
  virtual void Visit (CVariableInt * variable) = 0;
  virtual void Visit (CVariableString * variable) = 0;

protected:
  explicit CVariableVisitor (void);

  // copy assignment not allowed for this class

  CVariableVisitor & operator = (const CVariableVisitor &);

private:

  // copy construction not allowed for this class

  CVariableVisitor (const CVariableVisitor &);
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  05/11/2009  MCC     initial revision
//
// ============================================================================

#endif
