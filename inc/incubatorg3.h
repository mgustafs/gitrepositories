#if !defined (INCUBATORG3_H)
#define INCUBATORG3_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: IncubatorG3.h
//
//     Description: G3 Incubator class declaration
//
//          Author: Tommy Nguyen
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: incubatorg3.h %
//        %version: 2 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "Device.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CIncubatorG3 : public CDevice
#else
class __declspec (dllimport) CIncubatorG3 : public CDevice
#endif
{
  DECLARE_SERIAL (CIncubatorG3)

public:
  explicit CIncubatorG3 (void);
  virtual ~CIncubatorG3 ();

  inline CString GetHostAddress (void) const { return m_hostAddress; }
  inline void SetHostAddress (CString const & hostAddress) { m_hostAddress = hostAddress; }

  inline WORD GetHostPortNumber (void) const { return m_hostPortNumber; }
  inline void SetHostPortNumber (WORD hostPortNumber) { m_hostPortNumber = hostPortNumber; }

  virtual bool IsFeaturePresent (void) const;

  virtual void Accept (CDeviceVisitor & deviceVisitor);
  virtual void Serialize (CArchive & ar);

private:
  CString m_hostAddress;
  WORD m_hostPortNumber;

  // copy construction and assignment not allowed for this class

  CIncubatorG3 (const CIncubatorG3 &);
  CIncubatorG3 & operator = (const CIncubatorG3 &);
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  07/01/2015  TAN     initial revision
//  10/30/2015  MCC     added support for Central Station device type
//
// ============================================================================

#endif
