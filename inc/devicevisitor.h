#if !defined (DEVICEVISITOR_H)
#define DEVICEVISITOR_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: DeviceVisitor.h
//
//     Description: Device visitor design pattern declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: devicevisitor.h %
//        %version: 20 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
class CDispenserBeckhoff;
class CDispenserGalil;
class CDispenserParker;
class CPinToolBeckhoff;
class CPinToolGalil;
class CPinToolParker;
class CProteinPurificationSystem;
class CTCDispenser;
class CCCDispenser;
class CLibra6;
class CMicrofluidizer;
class CACStation;
class CACStationG3;
class CTCStation;
class CTCStationG3;
class CFCDispenser;
class CCellGenerator;
class CCentrifuge;
class CCCStation;
class CLoadingDock;
class CPipettor;
class CRaven;
class CISMAC;
class CFACSCyan;
class CSSStation;
class CSingleTipDispenser;
class CLumiReader;
class CCeldyne;
class CFFStation;
class CIncubatorG3;
class CCentralStation;

#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CDeviceVisitor : public CObject
#else
class __declspec (dllimport) CDeviceVisitor : public CObject
#endif
{
  DECLARE_DYNAMIC (CDeviceVisitor)

public:
  virtual ~CDeviceVisitor ();

  virtual void Visit (CDispenserBeckhoff * device) = 0;
  virtual void Visit (CDispenserGalil * device) = 0;
  virtual void Visit (CDispenserParker * device) = 0;
  virtual void Visit (CPinToolBeckhoff * device) = 0;
  virtual void Visit (CPinToolGalil * device) = 0;
  virtual void Visit (CPinToolParker * device) = 0;
  virtual void Visit (CTCDispenser * device) = 0;
  virtual void Visit (CCCDispenser * device) = 0;
  virtual void Visit (CLibra6 * device) = 0;
  virtual void Visit (CMicrofluidizer * device) = 0;
  virtual void Visit (CACStation * device) = 0;
  virtual void Visit (CACStationG3 * device) = 0;
  virtual void Visit (CTCStation * device) = 0;
  virtual void Visit (CTCStationG3 * device) = 0;
  virtual void Visit (CFCDispenser * device) = 0;
  virtual void Visit (CCellGenerator * device) = 0;
  virtual void Visit (CCentrifuge * device) = 0;
  virtual void Visit (CProteinPurificationSystem * device) = 0;
  virtual void Visit (CCCStation * device) = 0;
  virtual void Visit (CLoadingDock * device) = 0;
  virtual void Visit (CPipettor * device) = 0;
  virtual void Visit (CRaven * device) = 0;
  virtual void Visit (CISMAC * device) = 0;
  virtual void Visit (CFACSCyan * device) = 0;
  virtual void Visit (CSSStation * device) = 0;
  virtual void Visit (CSingleTipDispenser * device) = 0;
  virtual void Visit (CLumiReader * device) = 0;
  virtual void Visit (CCeldyne * device) = 0;
  virtual void Visit (CFFStation * device) = 0;
  virtual void Visit (CIncubatorG3 * device) = 0;
  virtual void Visit (CCentralStation * device) = 0;

protected:
  explicit CDeviceVisitor (void);

  // copy assignment not allowed for this class

  CDeviceVisitor & operator = (const CDeviceVisitor &);

private:

  // copy construction not allowed for this class

  CDeviceVisitor (const CDeviceVisitor &);
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  11/28/2006  MCC     initial revision
//  01/25/2007  MCC     implemented support for G2 PinTool device type
//  05/10/2007  MEG     added support for PPS Beckhoff device type
//  05/21/2007  MCC     added support for G3 AC-Station device type
//  07/18/2007  MCC     added support for CC-Station device type
//  07/24/2007  TAN     added support for G3 TC-Station device type
//  08/29/2007  MCC     added support for Loading Dock device type
//  02/16/2009  MEG     added support for Pipettor device type
//  06/29/2009  MCC     added support for Libra6 device type
//  03/10/2010  MEG     added support for Raven device type
//  03/26/2010  TAN     added support for Dispenser Beckhoff device type
//  07/19/2011  MCC     added support for ISMAC device type
//  08/24/2011  TAN     added support for FACSCyan device type
//  03/23/2012  MCC     added support for SSStation device type
//  05/08/2013  MCC     added support for single tip dispenser device type
//  07/25/2013  MEG     added support for lumi reader device type
//  05/06/2014  MCC     added support for Celdyne device type
//  07/29/2014  MCC     added support for FFStation device type
//  07/01/2015  TAN     added support for G3 Incubator device type
//  10/30/2015  MCC     added support for Central Station device type
//
// ============================================================================

#endif
