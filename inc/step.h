#if !defined (STEP_H)
#define STEP_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: Step.h
//
//     Description: Step class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: step.h %
//        %version: 8 %
//          %state: working %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: Monday, October 07, 2002 12:59:48 PM %
//
// ============================================================================

#include "HaspAdapter.h"
#include "VariableList.h"

#if _MSC_VER > 1000
#pragma once
#endif

#define ASSERT_VALID_VARIABLE_LIST(x) ASSERT (dynamic_cast <CStep const *> (x) == NULL)

namespace PDCLib
{

#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CStep : public CVariableList
#else
class __declspec (dllimport) CStep : public CVariableList
#endif
{
  DECLARE_SERIAL (CStep)

public:
  explicit CStep (void);
  virtual ~CStep ();

  inline void SetAlias (const CString & alias)
    { m_alias = alias; }
  inline CString GetAlias (void) const
    { return m_alias; }

  inline void SetSkip (bool skip)
    { m_skip = skip; }
  inline bool GetSkip (void) const
    { return m_skip; }

  inline void SetSkippable (bool skippable)
    { m_skippable = skippable; }
  inline bool GetSkippable (void) const
    { return m_skippable; }

  inline CHaspAdapter::EHaspFeature GetFeature (void) const
    { return m_feature; }
  inline void SetFeature (CHaspAdapter::EHaspFeature feature)
    { m_feature = feature; }

  inline POSITION GetFirstVariableList (void) const
    { return m_varList.GetHeadPosition (); }
  inline CVariableList * GetNextVariableList (POSITION & pos) const
    { return const_cast <CVariableList *> (dynamic_cast <CVariableList const *> (m_varList.GetNext (pos))); }
  inline CVariableList * GetVariableList (POSITION pos) const
    { return const_cast <CVariableList *> (dynamic_cast <CVariableList const *> (m_varList.GetAt (pos))); }
  inline POSITION AddVariableList (CVariableList * varList)
    { return m_varList.AddTail (varList); }
  inline POSITION InsertVariableListAfter (POSITION pos, CVariableList * varList)
    { return m_varList.InsertAfter (pos, varList); }
  inline POSITION InsertVariableListBefore (POSITION pos, CVariableList * varList)
    { return m_varList.InsertBefore (pos, varList); }
  CVariable * GetVariable (const CString & alias) const;
  void RemoveVariableList (POSITION pos, bool deleteVarList);

  CStep * Duplicate (bool copyVariables) const;

  virtual void Serialize (CArchive & ar);

private:
  CString m_alias;
  bool m_skip;
  bool m_skippable;
  CHaspAdapter::EHaspFeature m_feature;
  CObList m_varList;

  void CreateVariableList (void);

  // copy construction and assignment not allowed for this class

  CStep (const CStep &);
  CStep & operator = (const CStep &);
};
}  // end namespace PDCLib

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  08/05/2002  MCC     initial revision
//  04/01/2004  MCC     implemented support for cut/copy/paste operations
//  07/19/2005  MCC     replaced all double-underscores
//  04/14/2006  MEG     modified to use namespace PDCLib
//  07/03/2006  MCC     implemented skip step feature
//  08/10/2010  MCC     implemented support for variable grouping
//  07/17/2014  MCC     implemented support for executing shell commands
//
// ============================================================================

#endif
