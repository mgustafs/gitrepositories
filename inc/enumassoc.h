#if !defined (ENUMASSOC_H)
#define ENUMASSOC_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: EnumAssoc.h
//
//     Description: Enumeration association class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: enumassoc.h %
//        %version: 5 %
//          %state: working %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: Monday, October 07, 2002 12:59:40 PM %
//
// ============================================================================

namespace PDCLib
{

class CEnumAssoc : public CObject
{
  DECLARE_SERIAL (CEnumAssoc)

public:
  explicit CEnumAssoc (void);
  explicit CEnumAssoc (const CString & key, VARIANT const & value);
  virtual ~CEnumAssoc ();

  inline CString GetKey (void) const
    { return m_key; }
  inline VARIANT GetValue (void) const
    { return m_value; }

  virtual void Serialize (CArchive & ar);

private:
  CString m_key;
  VARIANT m_value;

  friend CArchive & operator>> (CArchive & ar, VARIANT & rhs);
  friend CArchive & operator<< (CArchive & ar, VARIANT const & rhs);

  // copy construction and assignment not allowed for this class

  CEnumAssoc (const CEnumAssoc &);
  CEnumAssoc & operator = (const CEnumAssoc &);
};

}  // end namespace PDCLib

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  08/14/2002  MCC     initial revision
//  07/19/2005  MCC     replaced all double-underscores
//  04/14/2006  MEG     modified to use namespace PDCLib
//  11/28/2006  MCC     baselined serializable topology class hierarchy
//  12/05/2006  MCC     modified for QAC++ compliance
//
// ============================================================================

#endif
