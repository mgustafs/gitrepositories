#if !defined (TIFFLIB_H)
#define TIFFLIB_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: TIFFLib.h
//
//     Description: TIFF library wrapper class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: tifflib.h %
//        %version: 20 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#if _MSC_VER > 1000
#pragma once
#endif

typedef struct tiff TIFF;

namespace PDCLib
{
#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CTIFFLib final
#else
class __declspec (dllimport) CTIFFLib final
#endif
{
public:
  enum class ERotation
  {
    None,
    Rotate90,
    Rotate180,
    Rotate270
  };

  enum class EMedianFilter
  {
    MedianFilterNone,
    MedianFilter2x2,
    MedianFilter3x3,
    MedianFilter5x5
  };

  explicit CTIFFLib (void) = default;
  virtual ~CTIFFLib () = default;

  bool RawToTIFF (CString const & fileName,
                  void    const * rawImage,
                  DWORD           cxImage,
                  DWORD           cyImage,
                  DWORD           cxOffset,
                  DWORD           cyOffset,
                  DWORD           cxMask,
                  DWORD           cyMask,
                  ERotation       rotation,
                  bool            yFlip);

  bool TIFFToRaw (CString            const & fileName,
                  std::vector <WORD>       & rawImage,
                  int                      & cxImage,
                  int                      & cyImage);

  bool DarkFrameSubtract (CString       const & inputFileName,
                          void                * rawImage,
                          int                   cxImage,
                          int                   cyImage,
                          EMedianFilter         medianFilter  = EMedianFilter::MedianFilter5x5,
                          int                   medianElement = 12);

  static void Transform (CRect           & imageMask,
                         CSize     const & frame,
                         ERotation         rotation,
                         bool              yFlip);
  static void MedianFilter (void          * rawImage,
                            int             cxImage,
                            int             cyImage,
                            EMedianFilter   medianFilter  = EMedianFilter::MedianFilter5x5,
                            int             medianElement = 12);

  inline CString GetErrorMessage (void) const { return m_errorMessage; }

private:
  CString m_errorMessage;

  static CMutex m_apiGate;

  static void MedianFilter2x2 (void * rawImage, int cxImage, int cyImage, int medianElement);
  static void MedianFilter3x3 (void * rawImage, int cxImage, int cyImage, int medianElement);
  static void MedianFilter5x5 (void * rawImage, int cxImage, int cyImage, int medianElement);

  bool CropImage (WORD  * rawImage,
                  DWORD   cxImage,
                  DWORD   cyImage,
                  DWORD   cxOffset,
                  DWORD   cyOffset,
                  DWORD   cxMask,
                  DWORD   cyMask);

  bool RawToTIFF0 (TIFF * tiff, WORD * rawImage, DWORD cxImage, DWORD cyImage, bool yFlip);
  bool RawToTIFF1 (TIFF * tiff, WORD * rawImage, DWORD cxImage, DWORD cyImage, bool yFlip);
  bool RawToTIFF2 (TIFF * tiff, WORD * rawImage, DWORD cxImage, DWORD cyImage, bool yFlip);
  bool RawToTIFF3 (TIFF * tiff, WORD * rawImage, DWORD cxImage, DWORD cyImage, bool yFlip);

public:

  // copy construction and assignment not allowed for this class

  CTIFFLib (const CTIFFLib &) = delete;
  CTIFFLib & operator = (const CTIFFLib &) = delete;
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  05/14/2010  MCC     initial revision
//  08/20/2010  MCC     implemented support for image analysis
//  09/16/2010  MCC     moved TIFF library class into PDCLib project
//  09/21/2010  MCC     implemented correct image mask drawing
//  09/21/2010  MCC     modified to support image rotation
//  09/21/2010  MCC     implemented support for image mask tracking cursor
//  12/13/2010  MCC     relaxed constraints on image mask size validation
//  02/23/2011  MCC     implemented support for flipping image
//  02/26/2011  MCC     corrected problem with image cropping
//  03/02/2011  MCC     refactored image mask transformation code
//  12/17/2014  MCC     implemented critical sections with C++11 concurrency
//  10/12/2015  MCC     added median filter to TIFF library class
//  10/13/2015  MCC     added dark field subtraction to TIFF library class
//  10/15/2015  MCC     added 2x2 median filter to TIFF library class
//  10/20/2015  MCC     generalized median filter in TIFF library class
//  10/22/2015  MEG     removed output file from dark frame subtract
//  11/09/2015  MCC     added 5x5 median filter to TIFF library class
//  12/09/2015  MEG     made RawToTIFF into non-destructive operation
//  04/21/2016  MEG     added median filter none option
//  03/31/2017  MCC     added reset method to remote control interface
//
// ============================================================================

#endif
