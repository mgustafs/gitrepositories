#if !defined (VARIABLELIST_H)
#define VARIABLELIST_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: VariableList.h
//
//     Description: Variable list class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: variablelist.h %
//        %version: 13 %
//          %state: working %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: Monday, October 07, 2002 12:59:58 PM %
//
// ============================================================================

#include "Variable.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{

#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CVariableList : public CObject
#else
class __declspec (dllimport) CVariableList : public CObject
#endif
{
  DECLARE_SERIAL (CVariableList)

public:
  explicit CVariableList (void);
  explicit CVariableList (const CString & identifier);
  virtual ~CVariableList ();

  inline void SetIdentifier (const CString & identifier)
    { m_identifier = identifier; }
  inline CString GetIdentifier (void) const
    { return m_identifier; }

  inline void SetComment (const CString & comment)
    { m_comment = comment; }
  inline CString GetComment (void) const
    { return m_comment; }

  inline void SetIsCollapsed (bool isCollapsed)
    { m_isCollapsed = isCollapsed; }
  inline bool GetIsCollapsed (void) const
    { return m_isCollapsed; }

  inline POSITION GetFirstVariable (void) const
    { return m_varList.GetHeadPosition (); }
  inline CVariable * GetNextVariable (POSITION & pos) const
    { return const_cast <CVariable *> (dynamic_cast <CVariable const *> (m_varList.GetNext (pos))); }
  inline CVariable * GetVariable (POSITION pos) const
    { return const_cast <CVariable *> (dynamic_cast <CVariable const *> (m_varList.GetAt (pos))); }
  CVariable * GetVariable (const CString & alias) const;
  POSITION AddVariable (CVariable * variable);
  POSITION InsertVariableAfter (POSITION pos, CVariable * variable);
  POSITION InsertVariableBefore (POSITION pos, CVariable * variable);
  void RemoveVariable (POSITION pos);

  CVariableList * Duplicate (bool copyVariables) const;

  virtual void Serialize (CArchive & ar);

protected:
  CVariableList (const CVariableList &);

  inline UINT GetObjectSchema (void) const
    { return m_objectSchema; }

private:
  UINT m_objectSchema;
  bool m_isCollapsed;
  CString m_identifier;
  CString m_comment;
  CObList m_varList;

  // assignment not allowed for this class

  CVariableList & operator = (const CVariableList &);
};
}  // end namespace PDCLib

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  08/16/2002  MCC     initial revision
//  12/11/2002  MCC     removed const qualifier from accessors
//  07/21/2003  MCC     updated version schema
//  04/01/2004  MCC     implemented support for cut/copy/paste operations
//  07/21/2004  MCC     added method for getting variable by identifier
//  08/19/2004  MCC     modified to search for variables by alias
//  07/19/2005  MCC     replaced all double-underscores
//  04/14/2006  MEG     modified to use namespace PDCLib
//  08/10/2010  MCC     implemented support for variable grouping
//  04/30/2013  MCC     removed save before run method constraint
//  05/03/2013  MCC     corrected removal of save before run method constraint
//  06/05/2014  MCC     modified to propagate collapsed property on duplicate
//
// ============================================================================

#endif
