#if !defined (PINTOOLPARKER_H)
#define PINTOOLPARKER_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: PinToolParker.h
//
//     Description: PinToolParker class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: pintoolparker.h %
//        %version: 2 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "Device.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CPinToolParker : public CDevice
#else
class __declspec (dllimport) CPinToolParker : public CDevice
#endif
{
  DECLARE_SERIAL (CPinToolParker)

public:
  explicit CPinToolParker (void);
  virtual ~CPinToolParker ();

  inline int GetPortNumber (void) const
    { return m_portNumber; }
  inline void SetPortNumber (int portNumber)
    { m_portNumber = portNumber; }

  virtual bool IsFeaturePresent (void) const;

  virtual void Accept (CDeviceVisitor & deviceVisitor);
  virtual void Serialize (CArchive & ar);

private:
  int m_portNumber;

  // copy construction and assignment not allowed for this class

  CPinToolParker (const CPinToolParker &);
  CPinToolParker & operator = (const CPinToolParker &);
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  11/28/2006  MCC     initial revision
//  01/10/2012  MCC     implemented support for device specific feature IDs
//
// ============================================================================

#endif
