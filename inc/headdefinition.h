#if !defined (HEADDEFINITION_H)
#define HEADDEFINITION_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: HeadDefinition.h
//
//     Description: Head Definition class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: headdefinition.h %
//        %version: 2 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mgustafs %
//  %date_modified: %
//
// ============================================================================

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CHeadDefinition : public CObject
#else
class __declspec (dllimport) CHeadDefinition : public CObject
#endif
{
  DECLARE_SERIAL (CHeadDefinition)

public:
  explicit CHeadDefinition (CString const & identifier);
  virtual ~CHeadDefinition ();

  inline CString GetIdentifier (void) const { return m_identifier; }

  inline int GetNumWells (void) const { return m_numWells; }
  inline void SetNumWells (int numWells) { m_numWells = numWells; }

  inline double GetOffset (void) const { return m_offset; }
  inline void SetOffset (double offset) { m_offset = offset; }

  inline double GetStroke (void) const { return m_stroke; }
  inline void SetStroke (double stroke) { m_stroke = stroke; }

  inline double GetHome (void) const { return m_home; }
  inline void SetHome (double home) { m_home = home; }

  inline double GetDryer (void) const { return m_dryer; }
  inline void SetDryer (double dryer) { m_dryer = dryer; }

  inline double GetPurge (void) const { return m_purge; }
  inline void SetPurge (double purge) { m_purge = purge; }

  inline double GetDiameter (void) const { return m_diameter; }
  inline void SetDiameter (double diameter) { m_diameter = diameter; }

  inline double GetLoadPressure (void) const { return m_loadPressure; }
  inline void SetLoadPressure (double loadPressure) { m_loadPressure = loadPressure; }

  inline double GetUnloadPressure (void) const { return m_unloadPressure; }
  inline void SetUnloadPressure (double unloadPressure) { m_unloadPressure = unloadPressure; }

  inline CString GetComment (void) const { return m_comment; }
  inline void SetComment (CString const & comment) { m_comment = comment; }

  inline CTime GetTimeStamp (void) const { return m_timeStamp; }
  inline void SetTimeStamp (void) { m_timeStamp = CTime::GetCurrentTime (); }

  virtual void Serialize (CArchive & ar);

protected:
  explicit CHeadDefinition (void);

private:
  CString m_identifier;
  int m_numWells;
  double m_offset;
  double m_stroke;
  double m_home;
  double m_dryer;
  double m_purge;
  double m_diameter;
  double m_loadPressure;
  double m_unloadPressure;
  CString m_comment;
  CTime m_timeStamp;

  // copy construction and assignment not allowed for this class

  CHeadDefinition (const CHeadDefinition &);
  CHeadDefinition & operator = (const CHeadDefinition &);
};

#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CHeadDefinitionException
#else
class __declspec (dllimport) CHeadDefinitionException
#endif
{
public:
  explicit CHeadDefinitionException (void);
  explicit CHeadDefinitionException (CException * e, bool autoDelete = false);
  explicit CHeadDefinitionException (CString const & errorMessage);
  CHeadDefinitionException (CHeadDefinitionException const & other);
  virtual ~CHeadDefinitionException ();

  CString GetErrorMessage (void) const;

private:
  CString const m_errorMessage;

  // assignment not allowed for this class

  CHeadDefinitionException & operator = (CHeadDefinitionException const &);
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  09/13/2010  MCC     initial revision
//  10/14/2010  MEG     updated head definition with more positioning values
//
// ============================================================================

#endif
