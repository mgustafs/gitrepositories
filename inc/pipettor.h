#if !defined (PIPETTOR_H)
#define PIPETTOR_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: Pipettor.h
//
//     Description: Pipettor class declaration
//
//          Author: Marc Gustafson
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: pipettor.h %
//        %version: 3 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "Device.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CPipettor : public CDevice
#else
class __declspec (dllimport) CPipettor : public CDevice
#endif
{
  DECLARE_SERIAL (CPipettor)

public:
  explicit CPipettor (void);
  virtual ~CPipettor ();

  inline int GetADSControllerID (void) const
    { return m_adsControllerID; }
  inline void SetADSControllerID (int adsControllerID)
    { m_adsControllerID = adsControllerID; }
  inline WORD GetAnalogPortNumber (void) const
    { return m_analogPortNumber; }
  inline void SetAnalogPortNumber (WORD analogPortNumber)
    { m_analogPortNumber = analogPortNumber; }
  inline WORD GetDiscretePortNumber (void) const
    { return m_discretePortNumber; }
  inline void SetDiscretePortNumber (WORD discretePortNumber)
    { m_discretePortNumber = discretePortNumber; }
  inline CString GetLocationsFileName (void) const
    { return m_locationsFileName; }
  inline void SetLocationsFileName (CString const & locationsFileName)
    { m_locationsFileName = locationsFileName; }
  inline bool GetHasStaticNest (void) const
    { return m_hasStaticNest; }
  inline void SetHasStaticNest (bool hasStaticNest)
    { m_hasStaticNest = hasStaticNest; }
  inline bool GetHasPlatePositioner (int index) const
    { return m_hasPlatePositioner[index]; }
  inline void SetHasPlatePositioner (int index, bool hasPlatePositioner)
    { m_hasPlatePositioner[index] = hasPlatePositioner; }

  virtual bool IsFeaturePresent (void) const;

  virtual void Accept (CDeviceVisitor & deviceVisitor);
  virtual void Serialize (CArchive & ar);

private:
  int m_adsControllerID;
  WORD m_analogPortNumber;
  WORD m_discretePortNumber;
  CString m_locationsFileName;
  bool m_hasStaticNest;
  CArray <bool> m_hasPlatePositioner;

  // copy construction and assignment not allowed for this class

  CPipettor (const CPipettor &);
  CPipettor & operator = (const CPipettor &);
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  02/13/2009  MEG     initial revision
//  06/23/2009  MEG     added configuration of plate positioners and nest
//  01/11/2012  MCC     implemented support for device specific feature IDs
//
// ============================================================================

#endif
