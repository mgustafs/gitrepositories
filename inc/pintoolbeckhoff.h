#if !defined (PINTOOLBECKHOFF_H)
#define PINTOOLBECKHOFF_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: PinToolBeckhoff.h
//
//     Description: PinToolBeckhoff class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: pintoolbeckhoff.h %
//        %version: 4 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "Device.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CPinToolBeckhoff : public CDevice
#else
class __declspec (dllimport) CPinToolBeckhoff : public CDevice
#endif
{
  DECLARE_SERIAL (CPinToolBeckhoff)

public:
  explicit CPinToolBeckhoff (void);
  virtual ~CPinToolBeckhoff ();

  inline int GetADSControllerID (void) const
    { return m_adsControllerID; }
  inline void SetADSControllerID (int adsControllerID)
    { m_adsControllerID = adsControllerID; }
  inline int GetPortNumber (void) const
    { return m_portNumber; }
  inline void SetPortNumber (int portNumber)
    { m_portNumber = portNumber; }
  inline WORD GetAnalogPortNumber (void) const
    { return m_analogPortNumber; }
  inline void SetAnalogPortNumber (WORD analogPortNumber)
    { m_analogPortNumber = analogPortNumber; }
  inline WORD GetDiscretePortNumber (void) const
    { return m_discretePortNumber; }
  inline void SetDiscretePortNumber (WORD discretePortNumber)
    { m_discretePortNumber = discretePortNumber; }
  inline CString GetLocationsFileName (void) const
    { return m_locationsFileName; }
  inline void SetLocationsFileName (CString const & locationsFileName)
    { m_locationsFileName = locationsFileName; }
  inline CString GetPlateDefinitionsFileName (void) const
    { return m_plateDefinitionsFileName; }
  inline void SetPlateDefinitionsFileName (CString const & plateDefinitionsFileName)
    { m_plateDefinitionsFileName = plateDefinitionsFileName; }
  inline CString GetBarcodeLogFileName (void) const
    { return m_barcodeLogFileName; }
  inline void SetBarcodeLogFileName (CString const & barcodeLogFileName)
    { m_barcodeLogFileName = barcodeLogFileName; }
  inline bool GetHasBarcodeReader (void) const
    { return m_hasBarcodeReader; }
  inline void SetHasBarcodeReader (bool hasBarcodeReader)
    { m_hasBarcodeReader = hasBarcodeReader; }
  inline bool GetHasThirdNest (void) const
    { return m_hasThirdNest; }
  inline void SetHasThirdNest (bool hasThirdNest)
    { m_hasThirdNest = hasThirdNest; }
  inline bool GetHasStaticNest (void) const
    { return m_hasStaticNest; }
  inline void SetHasStaticNest (bool hasStaticNest)
    { m_hasStaticNest = hasStaticNest; }
  inline bool GetHasPlatePositioner (int index) const
    { return m_hasPlatePositioner[index]; }
  inline void SetHasPlatePositioner (int index, bool hasPlatePositioner)
    { m_hasPlatePositioner[index] = hasPlatePositioner; }

  virtual bool IsFeaturePresent (void) const;

  virtual void Accept (CDeviceVisitor & deviceVisitor);
  virtual void Serialize (CArchive & ar);

private:
  int m_adsControllerID;
  int m_portNumber;
  WORD m_analogPortNumber;
  WORD m_discretePortNumber;
  CString m_locationsFileName;
  CString m_barcodeLogFileName;
  CString m_plateDefinitionsFileName;
  bool m_hasBarcodeReader;
  bool m_hasThirdNest;
  bool m_hasStaticNest;
  CArray <bool> m_hasPlatePositioner;

  // copy construction and assignment not allowed for this class

  CPinToolBeckhoff (const CPinToolBeckhoff &);
  CPinToolBeckhoff & operator = (const CPinToolBeckhoff &);
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  01/25/2007  MCC     initial revision
//  10/30/2007  MCC     implemented pintool barcode reader support
//  01/10/2012  MCC     implemented support for device specific feature IDs
//  05/22/2012  MCC     implemented support for pin-tool plate definition
//
// ============================================================================

#endif
