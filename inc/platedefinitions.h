#if !defined (PLATEDEFINITIONS_H)
#define PLATEDEFINITIONS_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: PlateDefinitions.h
//
//     Description: Plate Definitions class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: platedefinitions.h %
//        %version: 13 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "PlateDefinition.h"
#include "IPlateDefinitions.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CPlateDefinitions : public IPlateDefinitions
#else
class __declspec (dllimport) CPlateDefinitions : public IPlateDefinitions
#endif
{
  DECLARE_SERIAL (CPlateDefinitions)

public:
  virtual ~CPlateDefinitions ();

  static IPlateDefinitions * Load (CString const & fileName);

  virtual void Reload (void);
  virtual void Store (void);

  virtual POSITION GetHeadPosition (void) const;
  virtual IPlateDefinition * GetNext (POSITION & pos);
  virtual IPlateDefinition * GetAt (POSITION pos);

  virtual IPlateDefinition * Find (CString const & identifier);

  virtual POSITION AddTail (CString const & identifier, int numTipHeads);
  virtual void RemoveAt (POSITION pos);

  virtual void Sort (ESortCriterion sortCriterion);

  virtual ESortCriterion GetSortCriterion (void) const { return m_sortCriterion; }

protected:
  explicit CPlateDefinitions (void);
  explicit CPlateDefinitions (CString const & fileName);
  virtual void Serialize (CArchive & ar);

private:
  typedef CTypedPtrList <CObList, CPlateDefinition *> CPlateDefinitionList;
  typedef std::shared_ptr <CPlateDefinitionList> CPlateDefinitionListPtr;

  CString m_fileName;
  ESortCriterion m_sortCriterion;
  CPlateDefinitionListPtr m_plateDefinitions;

  static void Destroy (CPlateDefinitionListPtr const & plateDefinitionList);

  // copy construction and assignment not allowed for this class

  CPlateDefinitions (const CPlateDefinitions &);
  CPlateDefinitions & operator = (const CPlateDefinitions &);
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  03/17/2010  MCC     initial revision
//  03/18/2010  MCC     implemented infrastructure for plate definition archive
//  03/18/2010  MCC     implemented support for empty plate definition archive
//  03/18/2010  MCC     implemented support for plate definition variable type
//  03/19/2010  MCC     implemented additional integrity checks
//  03/24/2010  MCC     modified to check for taught plate definitions
//  03/25/2010  MCC     moved plate definitions to PDCLib project
//  04/01/2010  MCC     implemented support for user defined plate types
//  06/29/2010  MCC     generalized plate definition for all tip heads
//  07/29/2010  MCC     implemented support for sorting plate definitions
//  07/30/2010  MCC     refined support for sorting plate definitions
//  06/22/2011  MCC     added buttons to edit definitions dialogs
//  11/04/2013  MCC     refactored plate definitions implementation
//
// ============================================================================

#endif
