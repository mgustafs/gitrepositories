#if !defined (VARIABLESTRING_H)
#define VARIABLESTRING_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: VariableString.h
//
//     Description: Variable string class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: variablestring.h %
//        %version: 16 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "Position.h"
#include "Variable.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{

#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CVariableString : public CVariable
#else
class __declspec (dllimport) CVariableString : public CVariable
#endif
{
  DECLARE_SERIAL (CVariableString)

public:
  enum ESubtype
    {
      String,
      FileOpen,
      PlateDefinition,
      ImageMask,
      DirectoryName,
      FileSaveAs,
      Application
    };

  explicit CVariableString (ESubtype subtype = String);
  virtual ~CVariableString ();

  inline void SetValue (const CString & value) { m_value = value; }
  inline CString GetValue (void) const { return m_value; }

  inline void SetSubtype (ESubtype subtype) { m_subtype = subtype; }
  inline ESubtype GetSubtype (void) const { return m_subtype; }

  inline void SetInhibitEmpty (bool inhibitEmpty) { m_inhibitEmpty = inhibitEmpty; }
  inline bool GetInhibitEmpty (void) const { return m_inhibitEmpty; }

  inline EPosition GetReferencePosition (void) const { return m_referencePosition; }
  inline void SetReferencePosition (EPosition referencePosition) { m_referencePosition = referencePosition; }

  virtual CString GetFormattedVariable (void) const;
  virtual CString GetPrintFormattedVariable (bool isTemplateView) const;

  virtual CVariable * Duplicate (void) const { return new CVariableString (*this); }
  virtual void Serialize (CArchive & ar);

  virtual void Accept (CVariableVisitor & variableVisitor);

private:
  CString m_value;
  ESubtype m_subtype;
  EPosition m_referencePosition;
  bool m_inhibitEmpty;

  friend CArchive & operator>> (CArchive & ar, ESubtype & rhs);
  friend CArchive & operator<< (CArchive & ar, ESubtype const & rhs);

  // copy construction and assignment not allowed for this class

  CVariableString (const CVariableString &);
  CVariableString & operator = (const CVariableString &);
};

}  // end namespace PDCLib

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  04/29/2004  MCC     initial revision
//  07/19/2005  MCC     replaced all double-underscores
//  01/30/2006  MCC     commonized document and print views
//  04/14/2006  MEG     modified to use namespace PDCLib
//  05/11/2009  MCC     implemented support for new document creation strategy
//  03/18/2010  MCC     implemented support for plate definition variable type
//  03/19/2010  MCC     corrected problem with display of units
//  08/04/2010  MCC     implemented support for tree view icons
//  09/14/2010  MCC     implemented infrastructure for head definition access
//  09/29/2010  MCC     implemented support for directory name subtype
//  11/05/2010  MCC     implemented support for limited application access
//  07/26/2011  MCC     corrected numerous problems with printing
//  07/03/2012  MCC     added support for variable specific plate definitions
//  05/09/2013  MCC     refactored variable processing
//  09/02/2014  MCC     added application string subtype for shell commands
//
// ============================================================================

#endif
