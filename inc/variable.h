#if !defined (VARIABLE_H)
#define VARIABLE_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: Variable.h
//
//     Description: Variable class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: variable.h %
//        %version: 17 %
//          %state: working %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: Monday, October 07, 2002 12:59:50 PM %
//
// ============================================================================

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
class CVariableVisitor;

#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CVariable : public CObject
#else
class __declspec (dllimport) CVariable : public CObject
#endif
{
  DECLARE_SERIAL (CVariable)

public:
  explicit CVariable (void);
  virtual ~CVariable ();

  inline void AddRef (void)
    { ++m_refCount; }
  void Release (void);

  inline bool HasDependents (void) const
    { return (m_refCount > 1) ? true : false; }

  inline void SetIdentifier (const CString & identifier)
    { m_identifier = identifier; }
  inline CString GetIdentifier (void) const
    { return m_identifier; }

  inline void SetAlias (const CString & alias)
    { m_alias = alias; }
  inline CString GetAlias (void) const
    { return m_alias; }

  inline void SetUnits (const CString & units)
    { m_units = units; }
  inline CString GetUnits (void) const
    { return m_units; }

  inline void SetIsConstant (bool isConstant)
    { m_isConstant = isConstant; }
  inline bool GetIsConstant (void) const
    { return m_isConstant; }

  inline void SetIsLoggingEnabled (bool isLoggingEnabled)
    { m_isLoggingEnabled = isLoggingEnabled; }
  inline bool GetIsLoggingEnabled (void) const
    { return m_isLoggingEnabled; }

  inline void SetComment (const CString & comment)
    { m_comment = comment; }
  inline CString GetComment (void) const
    { return m_comment; }

  virtual CString GetAnnotation (void) const { return CString (); }

  virtual CString GetFormattedVariable (void) const;
  virtual CString GetPrintFormattedVariable (bool isTemplateView) const;

  virtual CVariable * Duplicate (void) const;
  virtual void Serialize (CArchive & ar);

  virtual void Accept (CVariableVisitor & variableVisitor);

protected:
  inline UINT GetObjectSchema (void) const
    { return m_objectSchema; }

  CString GetPrintFormattedLVal (bool isTemplateView) const;
  CString GetPrintFormattedModifiers (const CString   & minValue,
                                      const CString   & maxValue,
                                            bool        isTemplateView) const;
  CString GetPrintFormattedModifiers (bool isTemplateView) const;

  // copy construction not allowed for this class

  CVariable (const CVariable &);

private:
  UINT m_objectSchema;
  CString m_identifier;
  CString m_alias;
  CString m_units;
  bool m_isConstant;
  bool m_isLoggingEnabled;
  CString m_comment;
  LONG m_refCount;

  // assignment not allowed for this class

  CVariable & operator = (const CVariable &);
};

}  // end namespace PDCLib

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  08/05/2002  MCC     initial revision
//  07/11/2003  MCC     replaced post-increments with preferred pre-increments
//  07/21/2003  MCC     updated version schema
//  04/01/2004  MCC     implemented support for cut/copy/paste operations
//  04/15/2004  MCC     modified paste variable behavior
//  07/19/2005  MCC     replaced all double-underscores
//  01/30/2006  MCC     commonized document and print views
//  04/14/2006  MEG     modified to use namespace PDCLib
//  05/11/2009  MCC     implemented support for new document creation strategy
//  08/12/2009  MCC     modified to display reference attribute
//  08/04/2010  MCC     implemented support for tree view icons
//  11/05/2010  MCC     implemented support for limited application access
//  02/18/2011  MCC     implemented support for method variable logging
//  07/26/2011  MCC     corrected numerous problems with printing
//  11/29/2011  MCC     implemented additional features for report generation
//  05/09/2013  MCC     refactored variable processing
//
// ============================================================================

#endif
