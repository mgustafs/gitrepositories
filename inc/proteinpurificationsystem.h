#if !defined (PROTEINPURIFICATIONSYSTEM_H)
#define PROTEINPURIFICATIONSYSTEM_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: ProteinPurificationSystem.h
//
//     Description: Protein Purification System Beckhoff class declaration
//
//          Author: Marc Gustafson
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: proteinpurificationsystem.h %
//        %version: 4 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mgustafs %
//  %date_modified: %
//
// ============================================================================

#include "Device.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CProteinPurificationSystem : public CDevice
#else
class __declspec (dllimport) CProteinPurificationSystem : public CDevice
#endif
{
  DECLARE_SERIAL (CProteinPurificationSystem)

public:
  explicit CProteinPurificationSystem (void);
  virtual ~CProteinPurificationSystem ();

  inline int GetADSControllerID (void) const
    { return m_adsControllerID; }
  inline void SetADSControllerID (int adsControllerID)
    { m_adsControllerID = adsControllerID; }
  inline WORD GetAnalogPortNumber (void) const
    { return m_analogPortNumber; }
  inline void SetAnalogPortNumber (WORD analogPortNumber)
    { m_analogPortNumber = analogPortNumber; }
  inline WORD GetDiscretePortNumber (void) const
    { return m_discretePortNumber; }
  inline void SetDiscretePortNumber (WORD discretePortNumber)
    { m_discretePortNumber = discretePortNumber; }
  inline CString GetLocationsFileName (void) const
    { return m_locationsFileName; }
  inline void SetLocationsFileName (CString const & locationsFileName)
    { m_locationsFileName = locationsFileName; }

  virtual bool IsFeaturePresent (void) const;

  virtual void Accept (CDeviceVisitor & deviceVisitor);
  virtual void Serialize (CArchive & ar);

private:
  int m_adsControllerID;
  WORD m_analogPortNumber;
  WORD m_discretePortNumber;
  CString m_locationsFileName;

  // copy construction and assignment not allowed for this class

  CProteinPurificationSystem (const CProteinPurificationSystem &);
  CProteinPurificationSystem & operator = (const CProteinPurificationSystem &);
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  05/10/2007  MEG     initial revision
//  01/10/2012  MCC     implemented support for device specific feature IDs
//  10/04/2012  MCC     implemented support for optional high speed centrifuge
//  06/18/2014  MEG     removed high speed centrifuge
//
// ============================================================================

#endif
