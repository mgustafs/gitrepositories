#if !defined (METHODEDITOR_H)
#define METHODEDITOR_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2006.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: MethodEditor.h
//
//     Description: Method Editor class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: methodeditor.h %
//        %version: 7 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
class CMethod;
class CVariable;

#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CMethodEditor
#else
class __declspec (dllimport) CMethodEditor
#endif
{
public:
  explicit CMethodEditor (CString const & fileName);
  explicit CMethodEditor (std::shared_ptr <CMethod> method);
  virtual ~CMethodEditor ();

  void Save (void);
  void SaveAs (CString const & fileName);

  void SkipStep (CString const & stepAlias, bool skip);

  void SetVariable (CString const & stepAlias,
                    CString const & variableAlias,
                    CString const & value);
  void SetVariable (CString const & stepAlias,
                    CString const & variableAlias,
                    int             value);
  void SetVariable (CString const & stepAlias,
                    CString const & variableAlias,
                    double          value);

  void GetVariable (CString const & stepAlias,
                    CString const & variableAlias,
                    CString       & value) const;
  void GetVariable (CString const & stepAlias,
                    CString const & variableAlias,
                    int           & value) const;
  void GetVariable (CString const & stepAlias,
                    CString const & variableAlias,
                    double        & value) const;

private:
  CString const m_fileName;
  std::shared_ptr <CMethod> m_method;

  void SaveAs (CString const & fileName, UINT nOpenFlags);

  template <typename T, typename U>
  void SetVariable (CString const & stepAlias,
                    CString const & variableAlias,
                    U       const & value);
  template <typename T, typename U>
  void GetVariable (CString const & stepAlias,
                    CString const & variableAlias,
                    U             & value) const;

  CVariable * GetVariable (CString const & stepAlias,
                           CString const & variableAlias) const;

  // copy construction and assignment not allowed for this class

  CMethodEditor (const CMethodEditor &);
  CMethodEditor & operator = (const CMethodEditor &);
};

#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) XMethodEditor
#else
class __declspec (dllimport) XMethodEditor
#endif
{
public:
  explicit XMethodEditor (CString const & fileName);
  virtual ~XMethodEditor ();

  bool Open (void);
  bool Save (void);
  bool SaveAs (CString const & fileName);

  bool SkipStep (CString const & stepAlias, bool skip);

  bool SetVariable (CString const & stepAlias,
                    CString const & variableAlias,
                    CString const & value);
  bool SetVariable (CString const & stepAlias,
                    CString const & variableAlias,
                    int             value);
  bool SetVariable (CString const & stepAlias,
                    CString const & variableAlias,
                    double          value);

  bool GetVariable (CString const & stepAlias,
                    CString const & variableAlias,
                    CString       & value) const;
  bool GetVariable (CString const & stepAlias,
                    CString const & variableAlias,
                    int           & value) const;
  bool GetVariable (CString const & stepAlias,
                    CString const & variableAlias,
                    double        & value) const;

  CString GetErrorMessage (void) const;

private:
  CString const m_fileName;
  std::shared_ptr <CMethodEditor> m_methodEditor;
  CString mutable m_errorMessage;

  // copy construction and assignment not allowed for this class

  XMethodEditor (const XMethodEditor &);
  XMethodEditor & operator = (const XMethodEditor &);
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  08/22/2008  MCC     initial revision
//  09/03/2008  MCC     added interface for accessing variables
//  09/09/2008  MCC     corrected problem with save as implementation
//  09/10/2008  MCC     corrected problem with exception handling
//  10/09/2008  MCC     implemented interface for skipping steps
//  12/17/2014  MCC     replaced auto pointers with C++11 unique pointers
//  07/02/2015  MCC     added method editor to remote control interface
//
// ============================================================================

#endif
