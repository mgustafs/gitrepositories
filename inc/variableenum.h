#if !defined (VARIABLEENUM_H)
#define VARIABLEENUM_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: VariableEnum.h
//
//     Description: Variable enumeration class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: variableenum.h %
//        %version: 15 %
//          %state: working %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: Monday, October 07, 2002 12:59:54 PM %
//
// ============================================================================

#include "Variable.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{

#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CVariableEnum : public CVariable
#else
class __declspec (dllimport) CVariableEnum : public CVariable
#endif
{
  DECLARE_SERIAL (CVariableEnum)

public:
  explicit CVariableEnum (void);
  virtual ~CVariableEnum ();

  inline void SetInhibitKeyModify (bool inhibitKeyModify) { m_inhibitKeyModify = inhibitKeyModify; }
  inline bool GetInhibitKeyModify (void) const { return m_inhibitKeyModify; }

  inline void SetValue (const CString & value) { m_value = value; }
  inline CString GetValue (void) const { return m_value; }

  void RegisterAssoc (const CString & key, VARIANT const & value);
  inline POSITION GetFirstAssoc (void) const { return m_assocList.GetHeadPosition (); }
  void GetNextAssoc (POSITION & pos, CString & key, VARIANT & value) const;
  void RemoveAllAssoc (void);

  virtual CString GetAnnotation (void) const { return _T ("ENUM=") + m_value; }

  virtual CString GetFormattedVariable (void) const;
  virtual CString GetPrintFormattedVariable (bool isTemplateView) const;

  _variant_t GetValue_ (void) const;

  virtual CVariable * Duplicate (void) const { return new CVariableEnum (*this); }
  virtual void Serialize (CArchive & ar);

  virtual void Accept (CVariableVisitor & variableVisitor);

private:
  bool m_inhibitKeyModify;
  CString m_value;
  CObList m_assocList;

  bool Lookup (const CString & key, VARIANT & value) const;

  // copy construction and assignment not allowed for this class

  CVariableEnum (const CVariableEnum &);
  CVariableEnum & operator = (const CVariableEnum &);
};

}  // end namespace PDCLib

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  08/05/2002  MCC     initial revision
//  04/01/2004  MCC     implemented support for cut/copy/paste operations
//  04/15/2004  MCC     modified paste variable behavior
//  07/19/2005  MCC     replaced all double-underscores
//  01/30/2006  MCC     commonized document and print views
//  04/14/2006  MEG     modified to use namespace PDCLib
//  12/05/2006  MCC     modified for QAC++ compliance
//  05/11/2009  MCC     implemented support for new document creation strategy
//  08/04/2010  MCC     implemented support for tree view icons
//  11/05/2010  MCC     implemented support for limited application access
//  03/22/2011  MCC     implemented support modifying enumeration keys
//  07/26/2011  MCC     corrected numerous problems with printing
//  11/30/2011  MCC     implemented additional features for report generation
//  05/09/2013  MCC     refactored variable processing
//
// ============================================================================

#endif
