#if !defined (IMAGEMASKS_H)
#define IMAGEMASKS_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: ImageMasks.h
//
//     Description: Image Masks class declaration
//
//          Author: Marc Gustafson
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: imagemasks.h %
//        %version: 2 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "ImageMask.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CImageMasks : public CObject
#else
class __declspec (dllimport) CImageMasks : public CObject
#endif
{
  DECLARE_SERIAL (CImageMasks)

public:
  virtual ~CImageMasks ();

  static CImageMasks * Load (CString const & fileName);

  void Reload (void);
  void Store (void);

  POSITION GetHeadPosition (void) const;
  CImageMask * GetNext (POSITION & pos);
  CImageMask * GetAt (POSITION pos);

  CImageMask * Find (CString const & identifier);

  POSITION AddTail (CString const & identifier);
  void RemoveAt (POSITION pos);

protected:
  explicit CImageMasks (void);
  explicit CImageMasks (CString const & fileName);
  virtual void Serialize (CArchive & ar);

private:
  typedef CTypedPtrList <CObList, CImageMask *> CImageMaskList;
  typedef std::shared_ptr <CImageMaskList> CImageMaskListPtr;

  CString m_fileName;
  CImageMaskListPtr m_imageMasks;

  static void Destroy (CImageMaskListPtr const & imageMaskList);

  // copy construction and assignment not allowed for this class

  CImageMasks (const CImageMasks &);
  CImageMasks & operator = (const CImageMasks &);
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  09/14/2010  MEG     initial revision
//  06/22/2011  MCC     added buttons to edit definitions dialogs
//
// ============================================================================

#endif
