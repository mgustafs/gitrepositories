#if !defined (LIBRA6_H)
#define LIBRA6_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: Libra6.h
//
//     Description: Libra6 class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: libra6.h %
//        %version: 3 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "Device.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CLibra6 : public CDevice
#else
class __declspec (dllimport) CLibra6 : public CDevice
#endif
{
  DECLARE_SERIAL (CLibra6)

public:
  explicit CLibra6 (void);
  virtual ~CLibra6 ();

  inline CString GetLocationsFileName (void) const
    { return m_locationsFileName; }
  inline void SetLocationsFileName (CString const & locationsFileName)
    { m_locationsFileName = locationsFileName; }
  inline CString GetPlateDefinitionsFileName (void) const
    { return m_plateDefinitionsFileName; }
  inline void SetPlateDefinitionsFileName (CString const & plateDefinitionsFileName)
    { m_plateDefinitionsFileName = plateDefinitionsFileName; }
  inline int GetControllerID (void) const
    { return m_controllerID; }
  inline void SetControllerID (int controllerID)
    { m_controllerID = controllerID; }

  virtual bool IsFeaturePresent (void) const;

  virtual void Accept (CDeviceVisitor & deviceVisitor);
  virtual void Serialize (CArchive & ar);

private:
  CString m_locationsFileName;
  CString m_plateDefinitionsFileName;
  int m_controllerID;

  // copy construction and assignment not allowed for this class

  CLibra6 (const CLibra6 &);
  CLibra6 & operator = (const CLibra6 &);
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  06/29/2009  MCC     initial revision
//  06/03/2011  MEG     implemented support for Libra6 plate definitions
//  01/10/2012  MCC     implemented support for device specific feature IDs
//
// ============================================================================

#endif
