#if !defined (HEADDEFINITIONS_H)
#define HEADDEFINITIONS_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: HeadDefinitions.h
//
//     Description: Head Definitions class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: headdefinitions.h %
//        %version: 2 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "HeadDefinition.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CHeadDefinitions : public CObject
#else
class __declspec (dllimport) CHeadDefinitions : public CObject
#endif
{
  DECLARE_SERIAL (CHeadDefinitions)

public:
  virtual ~CHeadDefinitions ();

  static CHeadDefinitions * Load (CString const & fileName);

  void Reload (void);
  void Store (void);

  POSITION GetHeadPosition (void) const;
  CHeadDefinition * GetNext (POSITION & pos);
  CHeadDefinition * GetAt (POSITION pos);

  CHeadDefinition * Find (CString const & identifier);

  POSITION AddTail (CString const & identifier);
  void RemoveAt (POSITION pos);

protected:
  explicit CHeadDefinitions (void);
  explicit CHeadDefinitions (CString const & fileName);
  virtual void Serialize (CArchive & ar);

private:
  typedef CTypedPtrList <CObList, CHeadDefinition *> CHeadDefinitionList;
  typedef std::shared_ptr <CHeadDefinitionList> CHeadDefinitionListPtr;

  CString m_fileName;
  CHeadDefinitionListPtr m_headDefinitions;

  static void Destroy (CHeadDefinitionListPtr const & headDefinitionList);

  // copy construction and assignment not allowed for this class

  CHeadDefinitions (const CHeadDefinitions &);
  CHeadDefinitions & operator = (const CHeadDefinitions &);
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  09/13/2010  MCC     initial revision
//  06/22/2011  MCC     added buttons to edit definitions dialogs
//
// ============================================================================

#endif
