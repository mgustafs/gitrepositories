#if !defined (TOPOLOGY_H)
#define TOPOLOGY_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: Topology.h
//
//     Description: Topology class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: topology.h %
//        %version: 2 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
class CDevice;

#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CTopology : public CObject
#else
class __declspec (dllimport) CTopology : public CObject
#endif
{
  DECLARE_SERIAL (CTopology)

public:
  explicit CTopology (void);
  virtual ~CTopology ();

  void Load (CString const & fileName);
  void Store (CString const & fileName);

  bool IsEmpty (void) const;
  POSITION GetFirstDevice (void) const;
  CDevice * GetNextDevice (POSITION & pos) const;
  CDevice * GetDevice (POSITION pos) const;
  POSITION AppendDevice (CDevice * device);
  POSITION PrependDevice (CDevice * device);
  POSITION InsertDeviceAfter (POSITION pos, CDevice * device);
  POSITION InsertDeviceBefore (POSITION pos, CDevice * device);
  void RemoveDevice (POSITION pos);

  virtual void Serialize (CArchive & ar);

private:
  CTypedPtrList <CObList, CDevice *> m_device;

  // copy construction and assignment not allowed for this class

  CTopology (const CTopology &);
  CTopology & operator = (const CTopology &);
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  11/29/2006  MCC     initial revision
//  12/01/2006  MCC     exposed serialize interface
//
// ============================================================================

#endif
