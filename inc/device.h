#if !defined (DEVICE_H)
#define DEVICE_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: Device.h
//
//     Description: Device class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: device.h %
//        %version: 7 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "ControllerType.h"
#include "DeviceType.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
class CDeviceVisitor;

#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CDevice : public CObject
#else
class __declspec (dllimport) CDevice : public CObject
#endif
{
  DECLARE_SERIAL (CDevice)

public:
  virtual ~CDevice ();

  inline void SetCalibrationFileName (CString const & calibrationFileName) { m_calibrationFileName = calibrationFileName; }
  inline CString GetCalibrationFileName (void) const { return m_calibrationFileName; }
  inline void SetTemplateFileName (CString const & templateFileName) { m_templateFileName = templateFileName; }
  inline CString GetTemplateFileName (void) const { return m_templateFileName; }
  inline void SetEncryptedCalibrationFileName (CString const & encryptedCalibrationFileName) { m_encryptedCalibrationFileName = encryptedCalibrationFileName; }
  inline CString GetEncryptedCalibrationFileName (void) const { return m_encryptedCalibrationFileName; }
  inline void SetResetTimeout (DWORD resetTimeout) { m_resetTimeout = resetTimeout; }
  inline DWORD GetResetTimeout (void) const { return m_resetTimeout; }
  inline void SetResetDelay (DWORD resetDelay) { m_resetDelay = resetDelay; }
  inline DWORD GetResetDelay (void) const { return m_resetDelay; }
  inline void SetLoopRate (DWORD loopRate) { m_loopRate = loopRate; }
  inline DWORD GetLoopRate (void) const { return m_loopRate; }
  inline void SetLimitedAccess (bool limitedAccess) { m_limitedAccess = limitedAccess; }
  inline bool GetLimitedAccess (void) const { return m_limitedAccess; }
  inline void SetSimulationMode (bool simulationMode) { m_simulationMode = simulationMode; }
  inline bool GetSimulationMode (void) const { return m_simulationMode; }

  virtual bool IsFeaturePresent (void) const { return false; }

  virtual void Accept (CDeviceVisitor & deviceVisitor);
  virtual void Serialize (CArchive & ar);

protected:
  explicit CDevice (CString const & calibrationFileName          = CString (),
                    CString const & templateFileName             = CString (),
                    CString const & encryptedCalibrationFileName = CString ());

  inline UINT GetObjectSchema (void) const { return m_objectSchema; }

  static CString GetAppDataFileName (CString const & fileName);
  static CString GetAppProgFileName (CString const & fileName);

private:
  UINT m_objectSchema;
  CString m_calibrationFileName;
  CString m_templateFileName;
  CString m_encryptedCalibrationFileName;
  bool m_limitedAccess;
  bool m_simulationMode;
  DWORD m_resetTimeout;
  DWORD m_resetDelay;
  DWORD m_loopRate;

public:
  CDevice (CDevice const &) = delete;
  CDevice & operator = (CDevice const &) = delete;
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  11/28/2006  MCC     initial revision
//  12/05/2006  MCC     configured location of calibration and location files
//  05/06/2009  MCC     added template file name to topology
//  11/05/2010  MCC     implemented support for limited application access
//  01/10/2012  MCC     implemented support for device specific feature IDs
//  11/04/2015  MCC     implemented support for simulation mode at device level
//  07/12/2018  MCC     implemented support for encrypted calibrations
//
// ============================================================================

#endif
