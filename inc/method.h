#if !defined (METHOD_H)
#define METHOD_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: Method.h
//
//     Description: Method class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: method.h %
//        %version: 22 %
//          %state: working %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: Tuesday, October 08, 2002 8:34:23 AM %
//
// ============================================================================

#include "ControllerType.h"
#include "DeviceType.h"
#include "Iterator.h"
#include "VariableList.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CMethod : public CVariableList
#else
class __declspec (dllimport) CMethod : public CVariableList
#endif
{
  DECLARE_SERIAL (CMethod)

public:
  explicit CMethod (void);
  virtual ~CMethod ();

  inline void SetDeviceType (EDeviceType deviceType)
    { m_deviceType = deviceType; }
  inline EDeviceType GetDeviceType (void) const
    { return m_deviceType; }

  inline void SetControllerType (EControllerType controllerType)
    { m_controllerType = controllerType; }
  inline EControllerType GetControllerType (void) const
    { return m_controllerType; }

  inline void SetDeviceId (int deviceId)
    { m_deviceId = deviceId; }
  inline int GetDeviceId (void) const
    { return m_deviceId; }

  inline void SetCalibrationFile (const CString & calibrationFile)
    { m_calibrationFile = calibrationFile; }
  inline CString GetCalibrationFile (void) const
    { return m_calibrationFile; }

  inline void SetIsTemplate (bool isTemplate)
    { m_isTemplate = isTemplate; }
  inline bool GetIsTemplate (void) const
    { return m_isTemplate; }

  inline POSITION GetFirstIterator (void) const
    { return m_iteratorList.GetHeadPosition (); }
  inline CIterator * GetNextIterator (POSITION & pos) const
    { return const_cast <CIterator *> (dynamic_cast <CIterator const *> (m_iteratorList.GetNext (pos))); }
  inline CIterator * GetIterator (POSITION pos) const
    { return const_cast <CIterator *> (dynamic_cast <CIterator const *> (m_iteratorList.GetAt (pos))); }
  inline POSITION AddIterator (CIterator * iterator)
    { return m_iteratorList.AddTail (iterator); }
  inline POSITION InsertIteratorAfter (POSITION pos, CIterator * iterator)
    { return m_iteratorList.InsertAfter (pos, iterator); }
  inline POSITION InsertIteratorBefore (POSITION pos, CIterator * iterator)
    { return m_iteratorList.InsertBefore (pos, iterator); }
  void RemoveIterator (POSITION pos, bool deleteIterator);

  CStep * GetStep (const CString & stepAlias) const;

  CVariable * GetVariable (const CString & stepAlias, const CString & variableAlias) const;

  inline void AddRef (void)
    { ::InterlockedIncrement (&m_refCount); }
  void Release (void);

  CVariableList * Duplicate (void) const { return new CMethod (*this); }

  virtual void Serialize (CArchive & ar);

  static std::unique_ptr <CMethod> ReadObject (CArchive & ar);
  static std::unique_ptr <CMethod> ReadObject (CString const & fileName);

  static void WriteObject (CArchive & ar, CMethod & method);
  static void WriteObject (CString const & fileName, CMethod & method);

private:
  EDeviceType m_deviceType;
  EControllerType m_controllerType;
  int m_deviceId;
  CString m_calibrationFile;
  bool m_isTemplate;
  CObList m_iteratorList;
  LONG m_refCount;

  void SerializeIteratorList (CArchive & ar);

  // copy construction and assignment not allowed for this class

  CMethod (const CMethod &);
  CMethod & operator = (const CMethod &);
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  08/05/2002  MCC     initial revision
//  10/08/2002  MCC     added reference counting
//  12/11/2002  MCC     removed const qualifier from accessors
//  07/21/2003  MCC     updated version schema
//  07/30/2003  MCC     improved memory mangement with STL smart pointers
//  10/29/2003  MCC     implemented step interator feature
//  01/27/2004  MEG     removed DeviceType enumeration
//  04/02/2004  MCC     implemented support for cut/copy/paste operations
//  07/19/2005  MCC     replaced all double-underscores
//  07/21/2005  MCC     standardized convention for enum type definitions
//  04/14/2006  MEG     modified to use namespace PDCLib
//  04/27/2006  MEG     added PDCLib qualifier for EDeviceType use
//  11/14/2006  MCC     added fully qualified variable accessor
//  11/28/2006  MCC     baselined serializable topology class hierarchy
//  10/09/2008  MCC     implemented interface for skipping steps
//  04/28/2009  MCC     implemented support for drag and drop template editing
//  08/11/2010  MCC     implemented support for variable grouping
//  04/30/2013  MCC     removed save before run method constraint
//  05/07/2013  MCC     corrected removal of save before run method constraint
//  12/17/2014  MCC     replaced auto pointers with C++11 unique pointers
//  07/02/2015  MCC     added method editor to remote control interface
//
// ============================================================================

#endif
