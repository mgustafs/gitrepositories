#if !defined (ITERATOR_H)
#define ITERATOR_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: Iterator.h
//
//     Description: Iterator class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: iterator.h %
//        %version: 10 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "Step.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{

#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CIterator : public CObject
#else
class __declspec (dllimport) CIterator : public CObject
#endif
{
  DECLARE_SERIAL (CIterator)

public:
  explicit CIterator (void);
  virtual ~CIterator ();

  inline void SetLowerBound (int lowerBound)
    { m_lowerBound = lowerBound; }
  inline int GetLowerBound (void) const
    { return m_lowerBound; }
  inline void SetUpperBound (int upperBound)
    { m_upperBound = upperBound; }
  inline int GetUpperBound (void) const
    { return m_upperBound; }
  inline void SetIncrement (int increment)
    { m_increment = increment; }
  inline int GetIncrement (void) const
    { return m_increment; }
  inline void SetComment (const CString & comment)
    { m_comment = comment; }
  inline CString GetComment (void) const
    { return m_comment; }

  inline POSITION GetFirstStep (void) const
    { return m_stepList.GetHeadPosition (); }
  inline CStep * GetNextStep (POSITION & pos) const
    { return const_cast <CStep *> (dynamic_cast <CStep const *> (m_stepList.GetNext (pos))); }
  inline CStep * GetStep (POSITION pos) const
    { return const_cast <CStep *> (dynamic_cast <CStep const *> (m_stepList.GetAt (pos))); }
  inline POSITION AddStep (CStep * step)
    { return m_stepList.AddTail (step); }
  inline POSITION InsertStepAfter (POSITION pos, CStep * step)
    { return m_stepList.InsertAfter (pos, step); }
  inline POSITION InsertStepBefore (POSITION pos, CStep * step)
    { return m_stepList.InsertBefore (pos, step); }
  void RemoveStep (POSITION pos, bool deleteStep);

  CStep * GetStep (const CString & stepAlias) const;

  CVariable * GetVariable (const CString & stepAlias, const CString & variableAlias) const;

  CIterator * Duplicate (bool copyVariables) const;

  virtual void Serialize (CArchive & ar);

private:
  int m_lowerBound;
  int m_upperBound;
  int m_increment;
  CString m_comment;
  CObList m_stepList;

  // copy construction and assignment not allowed for this class

  CIterator (const CIterator &);
  CIterator & operator = (const CIterator &);
};

}  // end namespace PDCLib

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  10/28/2003  MCC     initial revision
//  04/01/2004  MCC     implemented support for cut/copy/paste operations
//  07/19/2005  MCC     replaced all double-underscores
//  04/14/2006  MEG     modified to use namespace PDCLib
//  11/14/2006  MCC     added fully qualified variable accessor
//  10/09/2008  MCC     implemented interface for skipping steps
//  04/28/2009  MCC     implemented support for drag and drop template editing
//  08/09/2010  MCC     enhanced functionality of step iterator
//  07/20/2012  MCC     implemented support for iterator comment
//
// ============================================================================

#endif
