#if !defined (FACSCYAN_H)
#define FACSCYAN_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: FACSCyan.h
//
//     Description: FACS Cyan Beckhoff class declaration
//
//          Author: Tommy Nguyen
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: facscyan.h %
//        %version: 1.1.4 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "Device.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CFACSCyan : public CDevice
#else
class __declspec (dllimport) CFACSCyan : public CDevice
#endif
{
  DECLARE_SERIAL (CFACSCyan)

public:
  explicit CFACSCyan (void);
  virtual ~CFACSCyan ();

  inline int GetADSControllerID (void) const
    { return m_adsControllerID; }
  inline void SetADSControllerID (int adsControllerID)
    { m_adsControllerID = adsControllerID; }
  inline int GetPortNumber (void) const
    { return m_portNumber; }
  inline void SetPortNumber (int portNumber)
    { m_portNumber = portNumber; }
  inline WORD GetAnalogPortNumber (void) const
    { return m_analogPortNumber; }
  inline void SetAnalogPortNumber (WORD analogPortNumber)
    { m_analogPortNumber = analogPortNumber; }
  inline WORD GetDiscretePortNumber (void) const
    { return m_discretePortNumber; }
  inline void SetDiscretePortNumber (WORD discretePortNumber)
    { m_discretePortNumber = discretePortNumber; }
  inline WORD GetHostPortNumber (void) const
    { return m_hostPortNumber; }
  inline void SetHostPortNumber (WORD hostPortNumber)
    { m_hostPortNumber = hostPortNumber; }
  inline CString GetLocationsFileName (void) const
    { return m_locationsFileName; }
  inline void SetLocationsFileName (CString const & locationsFileName)
    { m_locationsFileName = locationsFileName; }
  inline CString GetPlateDefinitionsFileName (void) const
    { return m_plateDefinitionsFileName; }
  inline void SetPlateDefinitionsFileName (CString const & plateDefinitionsFileName)
    { m_plateDefinitionsFileName = plateDefinitionsFileName; }
  inline CString GetHostAddress (void) const
    { return m_hostAddress; }
  inline void SetHostAddress (CString const & hostAddress)
    { m_hostAddress = hostAddress; }
  inline bool GetShakerSimulation (void) const
    { return m_shakerSimulation; }
  inline void SetShakerSimulation (bool shakerSimulation)
    { m_shakerSimulation = shakerSimulation; }

  virtual bool IsFeaturePresent (void) const;

  virtual void Accept (CDeviceVisitor & deviceVisitor);
  virtual void Serialize (CArchive & ar);

private:
  int m_adsControllerID;
  int m_portNumber;
  WORD m_analogPortNumber;
  WORD m_discretePortNumber;
  WORD m_hostPortNumber;
  CString m_locationsFileName;
  CString m_plateDefinitionsFileName;
  CString m_hostAddress;
  bool m_shakerSimulation;

  // copy construction and assignment not allowed for this class

  CFACSCyan (const CFACSCyan &);
  CFACSCyan & operator = (const CFACSCyan &);
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  08/24/2011  TAN     initial revision
//  09/21/2011  MCC     added Summit message handler to Device Editor
//  10/10/2011  MEG     added shaker simulation
//  01/10/2012  MCC     implemented support for device specific feature IDs
//  05/06/2014  MCC     added support for Celdyne device type
//
// ============================================================================

#endif
