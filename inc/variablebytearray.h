#if !defined (VARIABLEBYTEARRAY_H)
#define VARIABLEBYTEARRAY_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: VariableByteArray.h
//
//     Description: Variable byte array class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: variablebytearray.h %
//        %version: 17 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "Variable.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{

#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CVariableByteArray : public CVariable
#else
class __declspec (dllimport) CVariableByteArray : public CVariable
#endif
{
  DECLARE_SERIAL (CVariableByteArray)

public:
  enum class EMaskType
    {
      Column4x6   = 8, //   24 well
      Column8x12  = 0, //   96 well
      Column16x24 = 1, //  384 well
      Column32x48 = 2, // 1536 well
      Grid3x4     = 3, //   12 well (deprecated)
      Grid4x6     = 7, //   24 well
      Grid8x12    = 4, //   96 well
      Grid16x24   = 5, //  384 well
      Grid32x48   = 6  // 1536 well
    };

  explicit CVariableByteArray (EMaskType maskType = EMaskType::Column32x48);
  virtual ~CVariableByteArray ();

  void SetType (EMaskType maskType);
  inline EMaskType GetType (void) const { return m_maskType; }

  void SetValue (const CByteArray & value);
  void GetValue (CByteArray & value) const;

  virtual CString GetFormattedVariable (void) const;
  virtual CString GetPrintFormattedVariable (bool isTemplateView) const;

  virtual CVariable * Duplicate (void) const { return new CVariableByteArray (*this); }
  virtual void Serialize (CArchive & ar);

  virtual void Accept (CVariableVisitor & variableVisitor);

  static int GetSize (EMaskType maskType);

private:
  EMaskType m_maskType;
  CByteArray m_value;

  friend CArchive & operator>> (CArchive & ar, EMaskType & rhs);
  friend CArchive & operator<< (CArchive & ar, EMaskType const & rhs);

  // copy construction and assignment not allowed for this class

  CVariableByteArray (const CVariableByteArray &);
  CVariableByteArray & operator = (const CVariableByteArray &);
};

}  // end namespace PDCLib

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  05/30/2003  MCC     initial revision
//  04/01/2004  MCC     implemented support for cut/copy/paste operations
//  04/15/2004  MCC     modified paste variable behavior
//  07/19/2005  MCC     replaced all double-underscores
//  09/22/2005  MEG     added size parameter to default constructor
//  01/30/2006  MCC     commonized document and print views
//  03/21/2006  MEG     modified interface to support more generic mask types
//  04/14/2006  MEG     modified to use namespace PDCLib
//  11/28/2006  MCC     baselined serializable topology class hierarchy
//  05/11/2009  MCC     implemented support for new document creation strategy
//  08/04/2010  MCC     implemented support for tree view icons
//  11/05/2010  MCC     implemented support for limited application access
//  07/26/2011  MCC     corrected numerous problems with printing
//  05/09/2013  MCC     refactored variable processing
//  01/09/2015  MCC     templatized number of elements macro
//  10/27/2015  MCC     implemented support for 24 well grid and column masks
//
// ============================================================================

#endif
