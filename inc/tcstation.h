#if !defined (TCSTATION_H)
#define TCSTATION_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: TCStation.h
//
//     Description: TCStation class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: tcstation.h %
//        %version: 3 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "Device.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CTCStation : public CDevice
#else
class __declspec (dllimport) CTCStation : public CDevice
#endif
{
  DECLARE_SERIAL (CTCStation)

public:
  explicit CTCStation (void);
  virtual ~CTCStation ();

  inline CString GetLocationsFileName (void) const
    { return m_locationsFileName; }
  inline void SetLocationsFileName (CString const & locationsFileName)
    { m_locationsFileName = locationsFileName; }
  inline int GetADSControllerID (void) const
    { return m_adsControllerID; }
  inline void SetADSControllerID (int adsControllerID)
    { m_adsControllerID = adsControllerID; }
  inline WORD GetAnalogPortNumber (void) const
    { return m_analogPortNumber; }
  inline void SetAnalogPortNumber (WORD analogPortNumber)
    { m_analogPortNumber = analogPortNumber; }
  inline WORD GetDiscretePortNumber (void) const
    { return m_discretePortNumber; }
  inline void SetDiscretePortNumber (WORD discretePortNumber)
    { m_discretePortNumber = discretePortNumber; }
  inline int GetControllerID (void) const
    { return m_controllerID; }
  inline void SetControllerID (int controllerID)
    { m_controllerID = controllerID; }

  virtual bool IsFeaturePresent (void) const;

  virtual void Accept (CDeviceVisitor & deviceVisitor);
  virtual void Serialize (CArchive & ar);

private:
  CString m_locationsFileName;
  int m_adsControllerID;
  WORD m_analogPortNumber;
  WORD m_discretePortNumber;
  int m_controllerID;

  // copy construction and assignment not allowed for this class

  CTCStation (const CTCStation &);
  CTCStation & operator = (const CTCStation &);
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  11/29/2006  MCC     initial revision
//  11/30/2006  MCC     corrected analog/discrete port number data types
//  01/10/2012  MCC     implemented support for device specific feature IDs
//
// ============================================================================

#endif
