#if !defined (DISPENSERBECKHOFF_H)
#define DISPENSERBECKHOFF_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: DispenserBeckhoff.h
//
//     Description: Dispenser Beckhoff class declaration
//
//          Author: Tommy Nguyen
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: dispenserbeckhoff.h %
//        %version: 8.1.1 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "Device.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CDispenserBeckhoff : public CDevice
#else
class __declspec (dllimport) CDispenserBeckhoff : public CDevice
#endif
{
  DECLARE_SERIAL (CDispenserBeckhoff)

public:
  explicit CDispenserBeckhoff (void);
  virtual ~CDispenserBeckhoff ();

  enum EOrientation { LeftHanded, RightHanded };

  inline CString GetLocationsFileName (void) const
    { return m_locationsFileName; }
  inline void SetLocationsFileName (CString const & locationsFileName)
    { m_locationsFileName = locationsFileName; }
  inline CString GetPlateDefinitionsFileName (void) const
    { return m_plateDefinitionsFileName; }
  inline void SetPlateDefinitionsFileName (CString const & plateDefinitionsFileName)
    { m_plateDefinitionsFileName = plateDefinitionsFileName; }
  inline int GetPortNumber (int index) const
    { return m_portNumber[index]; }
  inline void SetPortNumber (int index, int portNumber)
    { m_portNumber[index] = portNumber; }
  inline int GetNumTipHeads (void) const
    { return m_numTipHeads; }
  inline void SetNumTipHeads (int numTipHeads)
    { m_numTipHeads = numTipHeads; }
  inline int GetADSControllerID (void) const
    { return m_adsControllerID; }
  inline void SetADSControllerID (int adsControllerID)
    { m_adsControllerID = adsControllerID; }
  inline WORD GetAnalogPortNumber (void) const
    { return m_analogPortNumber; }
  inline void SetAnalogPortNumber (WORD analogPortNumber)
    { m_analogPortNumber = analogPortNumber; }
  inline WORD GetDiscretePortNumber (void) const
    { return m_discretePortNumber; }
  inline void SetDiscretePortNumber (WORD discretePortNumber)
    { m_discretePortNumber = discretePortNumber; }
  inline bool GetHasBalance (void) const
    { return m_hasBalance; }
  inline void SetHasBalance (bool hasBalance)
    { m_hasBalance = hasBalance; }
  inline bool GetHasVentedEnclosure (void) const
    { return m_hasVentedEnclosure; }
  inline void SetHasVentedEnclosure (bool hasVentedEnclosure)
    { m_hasVentedEnclosure = hasVentedEnclosure; }
  inline EOrientation GetOrientation (void) const
    { return m_orientation; }
  inline void SetOrientation (EOrientation orientation)
    { m_orientation = orientation; }

  virtual bool IsFeaturePresent (void) const;

  virtual void Accept (CDeviceVisitor & deviceVisitor);
  virtual void Serialize (CArchive & ar);

private:
  CString m_locationsFileName;
  CString m_plateDefinitionsFileName;
  CArray <int> m_portNumber;
  int m_numTipHeads;
  int m_adsControllerID;
  WORD m_analogPortNumber;
  WORD m_discretePortNumber;
  bool m_hasBalance;
  bool m_hasVentedEnclosure;
  EOrientation m_orientation;

  friend CArchive & operator>> (CArchive & ar, EOrientation & rhs);
  friend CArchive & operator<< (CArchive & ar, EOrientation const & rhs);

  // copy construction and assignment not allowed for this class

  CDispenserBeckhoff (const CDispenserBeckhoff &);
  CDispenserBeckhoff & operator = (const CDispenserBeckhoff &);
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  03/26/2010  TAN     initial revision
//  05/24/2010  MCC     implemented support for conditional reference teaching
//  11/05/2010  MCC     implemented support for limited application access
//  07/06/2011  TAN     implemented support for G2 Dispenser barcode reader
//  01/10/2012  MCC     implemented support for device specific feature IDs
//  03/13/2012  TAN     implemented support for stacker and balance on G2 Dispenser
//  07/30/2012  MCC     implemented support for left/right handed dispensers
//  01/27/2015  TAN     removed hasStacker boolean on G2Dispenser
//  10/20/2017  MCC     implemented support for optional Celdyne presence
//
// ============================================================================

#endif
