#if !defined (FCDISPENSER_H)
#define FCDISPENSER_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: FCDispenser.h
//
//     Description: FCDispenser class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: fcdispenser.h %
//        %version: 2 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "Device.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CFCDispenser : public CDevice
#else
class __declspec (dllimport) CFCDispenser : public CDevice
#endif
{
  DECLARE_SERIAL (CFCDispenser)

public:
  explicit CFCDispenser (void);
  virtual ~CFCDispenser ();

  inline CString GetLocationsFileName (void) const
    { return m_locationsFileName; }
  inline void SetLocationsFileName (CString const & locationsFileName)
    { m_locationsFileName = locationsFileName; }
  inline int GetControllerID (void) const
    { return m_controllerID; }
  inline void SetControllerID (int controllerID)
    { m_controllerID = controllerID; }

  virtual bool IsFeaturePresent (void) const;

  virtual void Accept (CDeviceVisitor & deviceVisitor);
  virtual void Serialize (CArchive & ar);

private:
  CString m_locationsFileName;
  int m_controllerID;

  // copy construction and assignment not allowed for this class

  CFCDispenser (const CFCDispenser &);
  CFCDispenser & operator = (const CFCDispenser &);
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  11/29/2006  MCC     initial revision
//  01/10/2012  MCC     implemented support for device specific feature IDs
//
// ============================================================================

#endif
