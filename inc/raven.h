#if !defined (RAVEN_H)
#define RAVEN_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: Raven.h
//
//     Description: Raven Beckhoff class declaration
//
//          Author: Marc Gustafson
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: raven.h %
//        %version: 8 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "Device.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CRaven : public CDevice
#else
class __declspec (dllimport) CRaven : public CDevice
#endif
{
  DECLARE_SERIAL (CRaven)

public:
  explicit CRaven (void);
  virtual ~CRaven ();

  inline int GetADSControllerID (void) const
    { return m_adsControllerID; }
  inline void SetADSControllerID (int adsControllerID)
    { m_adsControllerID = adsControllerID; }
  inline int GetPortNumber (void) const
    { return m_portNumber; }
  inline void SetPortNumber (int portNumber)
    { m_portNumber = portNumber; }
  inline WORD GetAnalogPortNumber (void) const
    { return m_analogPortNumber; }
  inline void SetAnalogPortNumber (WORD analogPortNumber)
    { m_analogPortNumber = analogPortNumber; }
  inline WORD GetDiscretePortNumber (void) const
    { return m_discretePortNumber; }
  inline void SetDiscretePortNumber (WORD discretePortNumber)
    { m_discretePortNumber = discretePortNumber; }
  inline CString GetLocationsFileName (void) const
    { return m_locationsFileName; }
  inline void SetLocationsFileName (CString const & locationsFileName)
    { m_locationsFileName = locationsFileName; }
  inline CString GetImageMasksFileName (void) const
    { return m_imageMasksFileName; }
  inline void SetImageMasksFileName (CString const & imageMasksFileName)
    { m_imageMasksFileName = imageMasksFileName; }
  inline CString GetHeadDefinitionsFileName (void) const
    { return m_headDefinitionsFileName; }
  inline void SetHeadDefinitionsFileName (CString const & headDefinitionsFileName)
    { m_headDefinitionsFileName = headDefinitionsFileName; }
  inline CString GetPlateDefinitionsFileName (void) const
    { return m_plateDefinitionsFileName; }
  inline void SetPlateDefinitionsFileName (CString const & plateDefinitionsFileName)
    { m_plateDefinitionsFileName = plateDefinitionsFileName; }

  virtual bool IsFeaturePresent (void) const;

  virtual void Accept (CDeviceVisitor & deviceVisitor);
  virtual void Serialize (CArchive & ar);

private:
  int m_adsControllerID;
  int m_portNumber;
  WORD m_analogPortNumber;
  WORD m_discretePortNumber;
  CString m_locationsFileName;
  CString m_imageMasksFileName;
  CString m_headDefinitionsFileName;
  CString m_plateDefinitionsFileName;

  // copy construction and assignment not allowed for this class

  CRaven (const CRaven &);
  CRaven & operator = (const CRaven &);
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  03/10/2010  MEG     initial revision
//  03/17/2010  MEG     added plate definitions file name
//  05/24/2010  MCC     implemented support for conditional reference teaching
//  09/13/2010  MCC     implemented infrastructure for head definition access
//  09/14/2010  MCC     implemented support for new Raven attributes
//  10/11/2010  MEG     added port number for barcode reader
//  11/05/2010  MCC     implemented support for limited application access
//  01/10/2012  MCC     implemented support for device specific feature IDs
//
// ============================================================================

#endif
