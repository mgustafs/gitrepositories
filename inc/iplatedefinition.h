#if !defined (IPLATEDEFINITION_H)
#define IPLATEDEFINITION_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: IPlateDefinition.h
//
//     Description: Plate Definition Interface class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: iplatedefinition.h %
//        %version: 5 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "Position.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) IPlateDefinition : public CObject
#else
class __declspec (dllimport) IPlateDefinition : public CObject
#endif
{
  DECLARE_SERIAL (IPlateDefinition)

public:
  virtual ~IPlateDefinition ();

  virtual CString GetIdentifier (void) const = 0;
  virtual int GetNumTipHeads (void) const = 0;

  virtual double GetXAxis (int tipHead) const = 0;
  virtual void SetXAxis (int tipHead, double xAxis) = 0;

  virtual double GetYAxis (int tipHead) const = 0;
  virtual void SetYAxis (int tipHead, double yAxis) = 0;

  virtual double GetZAxis (int tipHead) const = 0;
  virtual void SetZAxis (int tipHead, double zAxis) = 0;

  virtual double GetRowSpacing (void) const = 0;
  virtual void SetRowSpacing (double rowSpacing) = 0;

  virtual double GetColumnSpacing (void) const = 0;
  virtual void SetColumnSpacing (double columnSpacing) = 0;

  virtual double GetWellDepth (void) const = 0;
  virtual void SetWellDepth (double wellDepth) = 0;

  virtual double GetMinPlateHeight (void) const = 0;
  virtual void SetMinPlateHeight (double minPlateHeight) = 0;

  virtual double GetMaxPlateHeight (void) const = 0;
  virtual void SetMaxPlateHeight (double maxPlateHeight) = 0;

  virtual int GetNumRows (void) const = 0;
  virtual void SetNumRows (int numRows) = 0;

  virtual int GetNumColumns (void) const = 0;
  virtual void SetNumColumns (int numColumns) = 0;

  virtual CString GetComment (void) const = 0;
  virtual void SetComment (CString const & comment) = 0;

  virtual CString GetBarcodeRegex (void) const = 0;
  virtual void SetBarcodeRegex (CString const & barcodeRegex) = 0;

  virtual PDCLib::EPosition GetReferencePosition (void) const = 0;
  virtual void SetReferencePosition (PDCLib::EPosition referencePosition) = 0;
  virtual bool IsReferencePositionMatch (PDCLib::EPosition referencePosition) const = 0;

  virtual bool GetIsTaught (void) const = 0;
  virtual bool GetIsTaught (int tipHead) const = 0;
  virtual void SetIsTaught (int tipHead, bool isTaught) = 0;

  virtual CTime GetTimeStamp (void) const = 0;
  virtual void SetTimeStamp (void) = 0;

protected:
  explicit IPlateDefinition (void);

  virtual void Serialize (CArchive & ar);

  // copy assignment not allowed for this class

  IPlateDefinition & operator = (const IPlateDefinition &);

private:

  // copy construction not allowed for this class

  IPlateDefinition (const IPlateDefinition &);
};

#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CPlateDefinitionException
#else
class __declspec (dllimport) CPlateDefinitionException
#endif
{
public:
  explicit CPlateDefinitionException (void);
  explicit CPlateDefinitionException (CException * e, bool autoDelete = false);
  explicit CPlateDefinitionException (LPCTSTR format, ...);
  CPlateDefinitionException (CPlateDefinitionException const & other);
  virtual ~CPlateDefinitionException ();

  CString GetErrorMessage (void) const;

private:
  CString m_errorMessage;

  // assignment not allowed for this class

  CPlateDefinitionException & operator = (CPlateDefinitionException const &);
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  11/01/2013  MCC     initial revision
//  11/04/2013  MCC     refactored plate definitions implementation
//  11/05/2013  MCC     added destroy interface to plate definition
//  11/05/2013  MCC     generalized plate definition exception constructor
//  12/11/2013  MEG     added well depth
//  01/18/2018  MCC     implemented further support for interoperability
//
// ============================================================================

#endif
