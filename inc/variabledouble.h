#if !defined (VARIABLEDOUBLE_H)
#define VARIABLEDOUBLE_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: VariableDouble.h
//
//     Description: Variable double class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: variabledouble.h %
//        %version: 13 %
//          %state: working %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: Monday, October 07, 2002 12:59:52 PM %
//
// ============================================================================

#include "Variable.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{

#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CVariableDouble : public CVariable
#else
class __declspec (dllimport) CVariableDouble : public CVariable
#endif
{
  DECLARE_SERIAL (CVariableDouble)

public:
  explicit CVariableDouble (double value = 0.0);
  virtual ~CVariableDouble ();

  inline void SetValue (double value) { m_value = value; }
  inline double GetValue (void) const { return m_value; }

  void SetMinValue (CVariableDouble * minValue);
  CVariableDouble * GetMinValue (void) const { return m_minValue; }

  void SetMaxValue (CVariableDouble * maxValue);
  CVariableDouble * GetMaxValue (void) const { return m_maxValue; }

  virtual CString GetAnnotation (void) const { return _T ("UNIT=") + GetUnits (); }

  virtual CString GetFormattedVariable (void) const;
  virtual CString GetPrintFormattedVariable (bool isTemplateView) const;

  virtual CVariable * Duplicate (void) const { return new CVariableDouble (*this); }
  virtual void Serialize (CArchive & ar);

  virtual void Accept (CVariableVisitor & variableVisitor);

private:
  double m_value;
  CVariableDouble * m_minValue;
  CVariableDouble * m_maxValue;

  // copy construction and assignment not allowed for this class

  CVariableDouble (const CVariableDouble &);
  CVariableDouble & operator = (const CVariableDouble &);
};

}  // end namespace PDCLib

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  08/05/2002  MCC     initial revision
//  04/01/2004  MCC     implemented support for cut/copy/paste operations
//  04/15/2004  MCC     modified paste variable behavior
//  07/19/2005  MCC     replaced all double-underscores
//  01/30/2006  MCC     commonized document and print views
//  04/14/2006  MEG     modified to use namespace PDCLib
//  05/11/2009  MCC     implemented support for new document creation strategy
//  08/04/2010  MCC     implemented support for tree view icons
//  11/05/2010  MCC     implemented support for limited application access
//  07/26/2011  MCC     corrected numerous problems with printing
//  11/30/2011  MCC     implemented additional features for report generation
//  05/09/2013  MCC     refactored variable processing
//
// ============================================================================

#endif
