#if !defined (POSITION_H)
#define POSITION_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: Position.h
//
//     Description: Position enumeration declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: position.h %
//        %version: 2.1.65 %
//        %version: 2.1.65 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
enum class EPosition
  {
    PositionNone,                     // unused location
    Idle,                             // common location (must start at one)
    TipA1536,                         // dispenser location (Parker specific)
    TipA384,                          // dispenser location (Parker specific)
    TipB1536,                         // dispenser location (Parker specific)
    TipB384,                          // dispenser location (Parker specific)
    TipAClean,                        // dispenser location
    TipBClean,                        // dispenser location
    TipAPrime,                        // dispenser location
    TipBPrime,                        // dispenser location
    Maintenance,                      // dispenser location
    DispenserSafe,                    // dispenser location
    TipAReference,                    // dispenser location
    TipBReference,                    // dispenser location
    TipCReference,                    // dispenser location
    TipCClean,                        // dispenser location
    TipCPrime,                        // dispenser location
    TipABeadRecovery1,                // dispenser location
    TipABeadRecovery2,                // dispenser location
    TipATipCleaning,                  // dispenser location
    TipASonicate,                     // dispenser location
    TipBSonicate,                     // dispenser location
    TipCSonicate,                     // dispenser location
    TipAPrimeAux,                     // dispenser location
    TipBPrimeAux,                     // dispenser location
    TipCPrimeAux,                     // dispenser location
    LDIdle,                           // loading dock location
    LDMaintenance,                    // loading dock location
    LDAspirateSample,                 // loading dock location
    LDAspirateTipClean,               // loading dock location
    LDAspirateTipDrying,              // loading dock location
    LDTiltAspirateSample,             // loading dock location
    CCMaintenance,                    // cell counter location
    CCTransferSample,                 // cell counter location
    CCTipAPrime,                      // cell counter location
    CCTipAClean,                      // cell counter location
    CCAspirateSample,                 // cell counter location
    CCAspirateTipClean,               // cell counter location
    CCAspirateTipDrying,              // cell counter location
    Dryer,                            // pintool location
    ReferenceQuadrant1,               // pintool location
    ReferenceQuadrant2,               // pintool location
    ReferenceQuadrant3,               // pintool location
    ReferenceQuadrant4,               // pintool location
    PinToolSafe,                      // pintool location
    Wash1,                            // pintool location
    Wash2,                            // pintool location
    Wash3,                            // pintool location
    PinToolSrcRef,                    // pintool location
    PinToolDstRef,                    // pintool location
    PinToolNstRef,                    // pintool location
    PinToolStaticNstRef,              // pintool location (Beckhoff specific)
    FlaskLocator1Idle,                // tc-dispenser location
    FlaskLocator2Idle,                // tc-dispenser location
    FlaskLocator1RetractedIdle,       // tc-dispenser location
    FlaskLocator2RetractedIdle,       // tc-dispenser location
    CleanStationIdle,                 // tc-dispenser location
    SystemMaintenance,                // tc-dispenser location
    TipHead1Idle,                     // tc-dispenser location
    TipHead1Prime,                    // tc-dispenser location
    TipHead1CleanStation,             // tc-dispenser location
    TipHead1TipDrying,                // tc-dispenser location
    TipHead1FlaskLocator1,            // tc-dispenser location
    TipHead1FlaskLocator2,            // tc-dispenser location
    TipHead2Idle,                     // tc-dispenser location
    TipHead2Prime,                    // tc-dispenser location
    TipHead2CleanStation,             // tc-dispenser location
    TipHead2TipDrying,                // tc-dispenser location
    TipHead2Waste,                    // tc-dispenser location
    TipHead2FlaskLocator1,            // tc-dispenser location
    TipHead2FlaskLocator2,            // tc-dispenser location
    TipHead3Idle,                     // tc-dispenser location
    TipHead3Prime,                    // tc-dispenser location
    TipHead3CleanStation,             // tc-dispenser location
    TipHead3TipDrying,                // tc-dispenser location
    TipHead3FlaskLocator1,            // tc-dispenser location
    TipHead3FlaskLocator2,            // tc-dispenser location
    MicrofluidizerSafe,               // micro-fluidizer location
    CleaningStation,                  // micro-fluidizer location
    Rack12SampleA1,                   // micro-fluidizer location
    Rack12SampleC4,                   // micro-fluidizer location
    Rack96SampleA1,                   // micro-fluidizer location
    Rack96SampleH12,                  // micro-fluidizer location
    Rack384SampleA1,                  // micro-fluidizer location
    Rack384SampleP24,                 // micro-fluidizer location
    Rack1536SampleA1,                 // micro-fluidizer location
    Rack1536SampleAF48,               // micro-fluidizer location
    ACTCSystemMaintenance,            // ac/tc location
    TransferTipFlaskPos,              // ac/tc location
    ReagentTipFlaskPos,               // ac/tc location
    ReagentRemovalTipFlaskPos,        // ac/tc location
    CleanStation,                     // ac/tc location
    CleanStation2,                    // ac/tc location (TC-Station specific)
    Prime,                            // ac/tc location
    TransferTipTiltFlaskPos,          // ac/tc location (TC-Station specific)
    CleanSeptumPos,                   // ac/tc location
    ReagentRemovalTiltFlaskPos,       // ac station location
    CellCounterDispenserIdle,         // ac station location
    ReagentRemovalTipSampleWellPos,   // ac station location
    ReagentTipSampleWellPos,          // ac station location
    CellCounterCleanStation,          // ac station location
    CellCounterPrime,                 // ac station location
    FCSystemMaintenance,              // fc dispenser
    FCWashPos,                        // fc dispenser
    FCPrimePos,                       // fc dispenser
    FCInFlaskAspiratePos,             // fc dispenser
    FCPostTiltAspiratePos,            // fc dispenser
    FCInFlaskDispensePos,             // fc dispenser
    Valve1Port1,                      // cell generator location
    Valve2Port1,                      // cell generator location
    FCIdle,                           // pps fraction collector location
    FCMaintenance,                    // pps fraction collector location
    FCWash,                           // pps fraction collector location
    FCPrime,                          // pps fraction collector location
    FCCalibrate,                      // pps fraction collector location
    FCDispenseHigh,                   // pps fraction collector location
    FCDispenseMedium,                 // pps fraction collector location
    FCDispenseLow,                    // pps fraction collector location
    PPSLDIdle,                        // pps loading dock location
    PPSLDMaintenance,                 // pps loading dock location
    PPSLDWash,                        // pps loading dock location
    PPSLDPrime,                       // pps loading dock location
    PPSLDCalibrate,                   // pps loading dock location
    PPSLDDispense,                    // pps loading dock location
    PPSLDAspirate,                    // pps loading dock location
    PPSLDAspirateTilt,                // pps loading dock location
    PPSHSCIdle,                       // pps high speed centrifuge location
    PPSHSCMaintenance,                // pps high speed centrifuge location
    PPSHSCCalibrate,                  // pps high speed centrifuge location
    PPSHSCAspirate,                   // pps high speed centrifuge location
    PPSHSCTriturate,                  // pps high speed centrifuge location
    PPSHSCSonicate,                   // pps high speed centrifuge location
    PPSHSCLDDispense,                 // pps high speed centrifuge location
    PPSHSCBufferDispense,             // pps high speed centrifuge location
    PPSHSCLDPrime,                    // pps high speed centrifuge location
    PPSHSCBufferPrime,                // pps high speed centrifuge location
    PPSHSCAspirateWash,               // pps high speed centrifuge location
    PPSHSCSonicateWash,               // pps high speed centrifuge location
    PPSHSCRotorIdle,                  // pps high speed centrifuge location
    ACSSystemMaintenance,             // g3 ac/cc-station location
    ACSWashPos,                       // g3 ac/cc-station location
    ACSPrimePos,                      // g3 ac/cc-station location
    ACSInFlaskAspiratePos,            // g3 ac/cc-station location
    ACSPostTiltAspiratePos,           // g3 ac/cc-station location
    ACSInFlaskDispensePos,            // g3 ac/cc-station location
    ACSValve1Port1,                   // g3 ac/cc-station location
    ACSValve2Port1,                   // g3 ac/cc-station location
    ACSValve3Port1,                   // g3 ac/cc-station location
    ACSTriturateDispensePos,          // g3 ac/cc-station location
    ACSTipDrying,                     // g3 ac/cc-station location
    CCSIdle,                          // cc-station location
    CCSSystemMaintenance,             // cc-station location
    CCSAspirateSample,                // cc-station location
    CCSDispenseSample,                // cc-station location
    CCSPrime,                         // cc-station location
    CCSCalibrate,                     // cc-station location
    CCSTipDrying,                     // cc-station location
    CCSQuickCleanWells,               // cc-station location
    TCSSystemMaintenance,             // g3 tc-station location
    TCSHead1WashPos,                  // g3 tc-station location
    TCSHead1PrimePos,                 // g3 tc-station location
    TCSHead1DropRemovePos,            // g3 tc-station location
    TCSConicalPos,                    // g3 tc-station location
    TCSInSrcFlaskHead1DispensePos,    // g3 tc-station location
    TCSInSrcFlaskHead1AspiratePos,    // g3 tc-station location
    TCSPostTiltSrcHead1FlaskAspPos,   // g3 tc-station location
    TCSTipHead2Idle,                  // g3 tc-station location
    TCSHead2WashPos,                  // g3 tc-station location
    TCSHead2PrimePos,                 // g3 tc-station location
    TCSHead2DropRemovePos,            // g3 tc-station location
    TCSInDestFlaskDispensePos,        // g3 tc-station location
    TCSInDestFlaskAspiratePos,        // g3 tc-station location
    TCSPostTiltDestFlaskAspPos,       // g3 tc-station location
    TCSInSrcFlaskHead2DispensePos,    // g3 tc-station location
    TCSInSrcFlaskHead2AspiratePos,    // g3 tc-station location
    TCSPostTiltSrcFlaskHead2AspPos,   // g3 tc-station location
    TCSValve1Port1,                   // g3 tc-station location
    TCSValve2Port1,                   // g3 tc-station location
    TCSValve3Port1,                   // g3 tc-station location
    TCSInSrcTriturateDispensePos,     // g3 tc-station location
    TCSInDestTriturateDispensePos,    // g3 tc-station location
    F2PLDMaintenance,                 // F2P loading dock location
    F2PLDWash,                        // F2P loading dock location
    F2PLDCalibrate,                   // F2P loading dock location
    F2PLDAspirate,                    // F2P loading dock location
    F2PLDAspirateTilt,                // F2P loading dock location
    F2PLDValvePort1,                  // F2P loading dock location
    F2PLDTriturateDispense,           // F2P loading dock location
    CFlaskHome,                       // Centrifuge location
    CPlateHome,                       // Centrifuge location
    PipettorDryer,                    // Pipettor location
    PipettorSafe,                     // Pipettor location
    PipettorGreinerSrc1_1536,         // Pipettor location
    PipettorUserDefinedSrc1_1536,     // Pipettor location
    PipettorGreinerDst1_1536,         // Pipettor location
    PipettorUserDefinedDst1_1536,     // Pipettor location
    PipettorGreinerDst2_1536,         // Pipettor location
    PipettorUserDefinedDst2_1536,     // Pipettor location
    PipettorWash1,                    // Pipettor location
    PipettorWash2,                    // Pipettor location
    PipettorNestSrc_1536,             // Pipettor location
    PipettorWaste,                    // Pipettor location
    PipettorAssemble,                 // Pipettor location
    PipettorGuideLock,                // Pipettor location
    PipettorBlot,                     // Pipettor location
    PipettorHome,                     // Pipettor location
    Libra6Dryer,                      // Libra6 location
    Libra6Safe,                       // Libra6 location
    Libra6SrcReference,               // Libra6 location
    Libra6DstReference,               // Libra6 location
    Libra6Wash1,                      // Libra6 location
    Libra6Wash2,                      // Libra6 location
    Libra6NestSrc_1536,               // Libra6 location
    Libra6NestSrc_384,                // Libra6 location
    Libra6TipLoad,                    // Libra6 location
    Libra6TipApproach,                // Libra6 location
    Libra6TipEject,                   // Libra6 location
    Libra6Home,                       // Libra6 location
    Libra6TipStrokeLimited,           // Libra6 location
    Libra6ReferenceQuadrant1,         // Libra6 location
    Libra6ReferenceQuadrant2,         // Libra6 location
    Libra6ReferenceQuadrant3,         // Libra6 location
    Libra6ReferenceQuadrant4,         // Libra6 location
    RavenMaintenance,                 // Raven location
    RavenSafe,                        // Raven location
    RavenPipetteHeadReference,        // Raven location
    RavenReferenceQuadrant1Head1,     // Raven location
    RavenReferenceQuadrant2Head1,     // Raven location
    RavenReferenceQuadrant3Head1,     // Raven location
    RavenReferenceQuadrant4Head1,     // Raven location
    RavenReferenceQuadrant1Head2,     // Raven location
    RavenReferenceQuadrant2Head2,     // Raven location
    RavenReferenceQuadrant3Head2,     // Raven location
    RavenReferenceQuadrant4Head2,     // Raven location
    RavenGripperRobotAccess,          // Raven location
    RavenGripperCameraAccess,         // Raven location
    RavenFilterWheelRef,              // Raven location
    RavenSrcRefHead1,                 // Raven location
    RavenDstRefHead1,                 // Raven location
    RavenSrcRefHead2,                 // Raven location
    RavenDstRefHead2,                 // Raven location
    RavenBoatHead1,                   // Raven location
    RavenTroughHead1,                 // Raven location
    RavenBoatHead2,                   // Raven location
    RavenTroughHead2,                 // Raven location
    RavenWash1Head1,                  // Raven location
    RavenWash2Head1,                  // Raven location
    RavenWash3Head1,                  // Raven location
    RavenDryerHead1,                  // Raven location
    RavenBlotHead1,                   // Raven location
    RavenQuadrantSafeHead1,           // Raven location
    RavenWash1Head2,                  // Raven location
    RavenWash2Head2,                  // Raven location
    RavenWash3Head2,                  // Raven location
    RavenDryerHead2,                  // Raven location
    RavenBlotHead2,                   // Raven location
    RavenQuadrantSafeHead2,           // Raven location
    RavenTipApproach,                 // Raven location
    RavenTipLoad,                     // Raven location
    DBSafe,                           // Dispenser Beckhoff location
    DBMaintenance,                    // Dispenser Beckhoff location
    DBTipAPlateReference,             // Dispenser Beckhoff location
    DBTipAClean,                      // Dispenser Beckhoff location
    DBTipAManualClean,                // Dispenser Beckhoff location
    DBTipBPlateReference,             // Dispenser Beckhoff location
    DBTipBVacuum,                     // Dispenser Beckhoff location
    DBTipBPrime,                      // Dispenser Beckhoff location
    DBTipBPrimeAux,                   // Dispenser Beckhoff location
    DBTipBSonicate,                   // Dispenser Beckhoff location
    DBTipCPlateReference,             // Dispenser Beckhoff location
    DBTipCVacuum,                     // Dispenser Beckhoff location
    DBTipCPrime,                      // Dispenser Beckhoff location
    DBTipCPrimeAux,                   // Dispenser Beckhoff location
    DBTipCSonicate,                   // Dispenser Beckhoff location
    ISMACSyringe1Idle,                // ISMAC location
    ISMACSyringe2Idle,                // ISMAC location
    ISMACSyringe3Idle,                // ISMAC location
    ISMACSyringe4Idle,                // ISMAC location
    ISMACSyringe5Idle,                // ISMAC location
    ISMACSyringe6Idle,                // ISMAC location
    ISMACSyringe7Idle,                // ISMAC location
    ISMACSuspensionPumpIdle,          // ISMAC location
    ISMACValveIdle,                   // ISMAC location
    FACSCyanSafeHeight,               // FACS Cyan location
    FACSCyanMaintenance,              // FACS Cyan location
    FACSCyanReferenceTopLeft1536,     // FACS Cyan location
    FACSCyanReferenceTopRight1536,    // FACS Cyan location
    FACSCyanReferenceBottomRight1536, // FACS Cyan location
    FACSCyanPlateClearanceHeight,     // FACS Cyan location
    FACSCyanTube1,                    // FACS Cyan location
    FACSCyanTube2,                    // FACS Cyan location
    FACSCyanWash1,                    // FACS Cyan location
    FACSCyanWash2,                    // FACS Cyan location
    FACSCyanWash3,                    // FACS Cyan location
    FACSCyanWash4,                    // FACS Cyan location
    FACSCyanReferenceTopLeft384,      // FACS Cyan location
    FACSCyanReferenceTopRight384,     // FACS Cyan location
    FACSCyanReferenceBottomRight384,  // FACS Cyan location
    FACSCyanReferenceTopLeft96,       // FACS Cyan location
    FACSCyanReferenceTopRight96,      // FACS Cyan location
    FACSCyanReferenceBottomRight96,   // FACS Cyan location
    FACSCyanReference,                // FACS Cyan location
    FACSCyanX2Safe,                   // FACS Cyan location
    STDMaintenance,                   // Single Tip Dispenser location
    STDReference,                     // Single Tip Dispenser location
    LRInternalIdle,                   // Lumi Reader location
    LRMaintenance,                    // Lumi Reader location
    LRSafe,                           // Lumi Reader location
    LRPlateReference,                 // Lumi Reader location
    LRCameraIdle,                     // Lumi Reader location
    LRCameraMaintenance,              // Lumi Reader location
    LRCameraReference,                // Lumi Reader location
    LRDoorOpen,                       // Lumi Reader location
    LRDoorPartOpen,                   // Lumi Reader location
    LRDoorClosed,                     // Lumi Reader location
    CeldyneX2Safe,                    // Celdyne location
    CeldyneSafeHeight,                // Celdyne location
    CeldyneMaintenance,               // Celdyne location
    CeldyneReferenceTopLeft1536,      // Celdyne location
    CeldyneReferenceTopRight1536,     // Celdyne location
    CeldyneReferenceBottomRight1536,  // Celdyne location
    CeldyneReferenceTopLeft384,       // Celdyne location
    CeldyneReferenceTopRight384,      // Celdyne location
    CeldyneReferenceBottomRight384,   // Celdyne location
    CeldyneReferenceTopLeft96,        // Celdyne location
    CeldyneReferenceTopRight96,       // Celdyne location
    CeldyneReferenceBottomRight96,    // Celdyne location
    CeldyneReference,                 // Celdyne location
    CeldyneTube1,                     // Celdyne location
    CeldyneTube2,                     // Celdyne location
    CeldyneWash1,                     // Celdyne location
    CeldyneWash2,                     // Celdyne location
    CeldyneWash3,                     // Celdyne location
    CeldyneWash4,                     // Celdyne location
    FFSSystemMaintenance,             // FF-Station location
    FFSCleanTipPos,                   // FF-Station location
    FFSPrimePos,                      // FF-Station location
    FFSInFlaskAspiratePos,            // FF-Station location
    FFSPostTiltAspiratePos,           // FF-Station location
    FFSInFlaskDispensePos,            // FF-Station location
    CSNest,                           // Central Station location
    CSTray,                           // Central Station location
    CSMaintenance,                    // Central Station location
    PositionLast                      // unused location
  };

#if defined (_PDCLIB_EXPORT)
__declspec (dllexport) CString GetPosition (EPosition position);

__declspec (dllexport) EPosition GetFirstPosition (void);
__declspec (dllexport) void GetNextPosition (EPosition & position);
__declspec (dllexport) EPosition GetLastPosition (void);

CArchive & operator>> (CArchive & ar, EPosition & rhs);
CArchive & operator<< (CArchive & ar, EPosition & rhs);
#else
__declspec (dllimport) CString GetPosition (EPosition position);

__declspec (dllimport) EPosition GetFirstPosition (void);
__declspec (dllimport) void GetNextPosition (EPosition & position);
__declspec (dllimport) EPosition GetLastPosition (void);
#endif
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  01/12/2007  MCC     initial revision
//  01/26/2007  MCC     added support for new G2 PinTool taught locations
//  02/19/2007  MCC     baselined G2 PinTool top-level transfer state machine
//  02/23/2007  TAN     added tilt location for AC/TC station
//  02/26/2007  TAN     modified to add new TC station location to the end
//  02/28/2007  TAN     added tilt location for loading dock
//  04/06/2007  TAN     added new tilt location for TC station
//  05/11/2007  MEG     add support PPS Fraction Collector locations
//  05/21/2007  MCC     added G3 AC-Station taught locations
//  06/12/2007  TAN     added clean septum location for AC/TC station
//  06/25/2007  MEG     added PPS loading dock locations
//  07/19/2007  MCC     added CC-Station taught locations
//  07/20/2007  TAN     removed CleanFlask feature
//  08/10/2007  TAN     added G3 TC-Station taught locations
//  08/29/2007  MCC     added support for Loading Dock device type
//  08/30/2007  TAN     added new wash locations for G3 TC-Station
//  08/30/2007  MCC     added support for additional Loading Dock axes
//  09/05/2007  MCC     added support for additional Loading Dock locations
//  09/11/2007  MCC     removed unused Loading Dock prime location
//  09/17/2007  TAN     added prime locations for G3 TC-Station
//  09/28/2007  MEG     added PPS high speed centrifuge locations
//  10/02/2007  TAN     added dropremove locations for G3 TC-Station
//  10/12/2007  MCC     improved diagnostics for taught location faults
//  01/04/2008  TAN     added Centrifuge locations
//  05/05/2008  MEG     added TriturateDispense locations
//  06/26/2008  MCC     implemented transfer sample quick clean feature
//  07/01/2008  MCC     added AC-Station tip drying location
//  10/06/2008  MCC     implemented support for sonicator locations
//  01/06/2009  MEG     added new prime positions for dispenser
//  03/03/2009  MEG     added Pipettor locations
//  03/27/2009  MEG     added Pipettor Home position
//  06/30/2009  MCC     added Libra6 locations
//  07/23/2009  MEG     removed unused Libra6 locations
//  08/18/2009  MEG     added Libra6 stroke limited location
//  10/07/2009  MEG     added Libra6 384 locations
//  10/15/2009  MEG     added Libra6 reference quadrant locations
//  03/23/2010  MEG     added initial Raven locations
//  03/25/2010  MCC     implemented support for user defined plate types
//  04/02/2010  TAN     added support for Dispenser Beckhoff device type
//  06/07/2010  TAN     added Dispenser Beckhoff positions
//  06/09/2010  MCC     baselined Raven filter wheel state machine class
//  06/14/2010  TAN     merged parallel version
//  06/25/2010  MEG     updated with current Raven positions
//  06/28/2010  MEG     added new Raven positions for wash and birdbath
//  10/13/2010  MEG     added reference quadrant positions for both heads
//  01/11/2011  MEG     added new taught positions for tip handling
//  06/06/2011  MEG     modified libra6 positions for user defined plate types
//  07/20/2011  MCC     added support for ISMAC device type
//  07/21/2011  MCC     baselined ISMAC I/O class
//  07/22/2011  MCC     implemented ISMAC device configuration dialog
//  07/27/2011  MEG     added taught positions for quadrant safe moves
//  08/25/2011  TAN     added support for FACSCyan device type
//  11/09/2011  MEG     added plate reference positions for plate types
//  05/18/2012  MCC     implemented support for pin-tool plate definition
//  06/20/2012  TAN     added FACSCyanX2Safe position for G2 FACSCyan
//  07/03/2012  MCC     added support for variable specific plate definitions
//  07/23/2013  MCC     baselined single tip dispenser device config dialog
//  08/27/2013  MEG     added basic lumi reader positions
//  04/16/2014  TAN     refactored single tip dispenser device config dialog
//  05/07/2014  MCC     added support for Celdyne device type
//  05/08/2014  MCC     implemented Celdyne gripper and shaker options
//  05/21/2014  MCC     implemented Celdyne plate sample
//  07/29/2014  MCC     added support for FFStation device type
//  07/30/2014  MCC     removed unused FFStation locations
//  08/04/2014  MCC     updated FFStation wash position
//  11/05/2015  MCC     added Central Station placeholder position
//  01/28/2016  TAN     added Central Station locations
//  01/18/2018  MCC     implemented further support for interoperability
//
// ============================================================================

#endif
