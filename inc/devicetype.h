#if !defined (DEVICETYPE_H)
#define DEVICETYPE_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2006.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: DeviceType.h
//
//     Description: EDeviceType enumeration
//
//          Author: Marc Gustafson
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: devicetype.h %
//        %version: 20 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
enum EDeviceType
  {
    Dispenser,
    PinTool,
    TCDispenser,
    CCDispenser,
    Microfluidizer,
    TCStation,
    ACStation,
    FCDispenser,
    CellGenerator,
    Centrifuge,
    ProteinPurificationSystem,
    CCStation,
    LoadingDock,
    Pipettor,
    Libra6,
    Raven,
    ISMAC,
    FACSCyan,
    SSStation,
    SingleTipDispenser,
    LumiReader,
    Celdyne,
    FFStation,
    Incubator,
    CentralStation,
    InvalidDevice
  };

#if defined (_PDCLIB_EXPORT)
CString __declspec (dllexport) GetDeviceType (EDeviceType deviceType);
#else
CString __declspec (dllimport) GetDeviceType (EDeviceType deviceType);
#endif
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  04/27/2006  MEG     initial revision
//  09/27/2006  TAN     add FCDispenser device type
//  10/03/2006  MCC     implemented support for cell generator device type
//  10/04/2006  MEG     implemented support for centrifuge device type
//  11/28/2006  MCC     baselined serializable topology class hierarchy
//  05/10/2007  MEG     implemented protein purification system device type
//  07/18/2007  MCC     added support for CC-Station device type
//  08/29/2007  MCC     added support for Loading Dock device type
//  02/16/2009  MEG     added support for Pipettor device type
//  06/29/2009  MCC     added support for Libra6 device type
//  03/10/2010  MEG     added support for Raven device type
//  07/19/2011  MCC     added support for ISMAC device type
//  08/24/2011  TAN     added support for FACSCyan device type
//  03/23/2012  MCC     added support for SSStation device type
//  05/08/2013  MCC     added support for single tip dispenser device type
//  07/25/2013  MEG     added support for lumi reader device type
//  05/06/2014  MCC     added support for Celdyne device type
//  07/29/2014  MCC     added support for FFStation device type
//  07/01/2015  TAN     added support for G3 Incubator device type
//  10/30/2015  MCC     added support for Central Station device type
//
// ============================================================================

#endif
