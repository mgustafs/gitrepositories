#if !defined (PLATEDEFINITION_H)
#define PLATEDEFINITION_H

// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: PlateDefinition.h
//
//     Description: Plate Definition class declaration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: platedefinition.h %
//        %version: 16 %
//          %state: %
//         %cvtype: incl %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "Position.h"
#include "IPlateDefinition.h"

#if _MSC_VER > 1000
#pragma once
#endif

namespace PDCLib
{
#if defined (_PDCLIB_EXPORT)
class __declspec (dllexport) CPlateDefinition : public IPlateDefinition
#else
class __declspec (dllimport) CPlateDefinition : public IPlateDefinition
#endif
{
  DECLARE_SERIAL (CPlateDefinition)

public:
  explicit CPlateDefinition (CString const & identifier, int numTipHeads);
  virtual ~CPlateDefinition ();

  virtual CString GetIdentifier (void) const { return m_identifier; }
  virtual int GetNumTipHeads (void) const { return m_isTaught.GetCount (); }

  virtual double GetXAxis (int tipHead) const;
  virtual void SetXAxis (int tipHead, double xAxis) { m_xAxis.SetAtGrow (tipHead, xAxis); }

  virtual double GetYAxis (int tipHead) const;
  virtual void SetYAxis (int tipHead, double yAxis) { m_yAxis.SetAtGrow (tipHead, yAxis); }

  virtual double GetZAxis (int tipHead) const;
  virtual void SetZAxis (int tipHead, double zAxis) { m_zAxis.SetAtGrow (tipHead, zAxis); }

  virtual double GetRowSpacing (void) const { return m_rowSpacing; }
  virtual void SetRowSpacing (double rowSpacing) { m_rowSpacing = rowSpacing; }

  virtual double GetColumnSpacing (void) const { return m_columnSpacing; }
  virtual void SetColumnSpacing (double columnSpacing) { m_columnSpacing = columnSpacing; }

  virtual double GetWellDepth (void) const { return m_wellDepth; }
  virtual void SetWellDepth (double wellDepth) { m_wellDepth = wellDepth; }

  virtual double GetMinPlateHeight (void) const { return m_minPlateHeight; }
  virtual void SetMinPlateHeight (double minPlateHeight) { m_minPlateHeight = minPlateHeight; }

  virtual double GetMaxPlateHeight (void) const { return m_maxPlateHeight; }
  virtual void SetMaxPlateHeight (double maxPlateHeight) { m_maxPlateHeight = maxPlateHeight; }

  virtual int GetNumRows (void) const { return m_numRows; }
  virtual void SetNumRows (int numRows) { m_numRows = numRows; }

  virtual int GetNumColumns (void) const { return m_numColumns; }
  virtual void SetNumColumns (int numColumns) { m_numColumns = numColumns; }

  virtual CString GetComment (void) const { return m_comment; }
  virtual void SetComment (CString const & comment) { m_comment = comment; }

  virtual CString GetBarcodeRegex (void) const { return m_barcodeRegex; }
  virtual void SetBarcodeRegex (CString const & barcodeRegex) { m_barcodeRegex = barcodeRegex; }

  virtual EPosition GetReferencePosition (void) const { return m_referencePosition; }
  virtual void SetReferencePosition (EPosition referencePosition) { m_referencePosition = referencePosition; }
  virtual bool IsReferencePositionMatch (EPosition referencePosition) const
    { return (  referencePosition == EPosition::PositionNone) ||
             (m_referencePosition == EPosition::PositionNone) ||
             (m_referencePosition == referencePosition); }

  virtual bool GetIsTaught (void) const;
  virtual bool GetIsTaught (int tipHead) const;
  virtual void SetIsTaught (int tipHead, bool isTaught) { m_isTaught.SetAtGrow (tipHead, isTaught); }

  virtual CTime GetTimeStamp (void) const { return m_timeStamp; }
  virtual void SetTimeStamp (void) { m_timeStamp = CTime::GetCurrentTime (); }

  virtual void Serialize (CArchive & ar);

protected:
  explicit CPlateDefinition (void);

private:
  CString m_identifier;
  CArray <double> m_xAxis;
  CArray <double> m_yAxis;
  CArray <double> m_zAxis;
  double m_rowSpacing;
  double m_columnSpacing;
  double m_wellDepth;
  double m_minPlateHeight;
  double m_maxPlateHeight;
  int m_numRows;
  int m_numColumns;
  CString m_comment;
  CString m_barcodeRegex;
  EPosition m_referencePosition;
  CArray <bool> m_isTaught;
  CTime m_timeStamp;

  // copy construction and assignment not allowed for this class

  CPlateDefinition (const CPlateDefinition &);
  CPlateDefinition & operator = (const CPlateDefinition &);
};
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  03/17/2010  MCC     initial revision
//  03/19/2010  MCC     implemented additional integrity checks
//  03/19/2010  TAN     added plate definition
//  03/22/2010  MCC     baselined edit plate definitions dialog
//  03/25/2010  MCC     moved plate definitions to PDCLib project
//  04/01/2010  MCC     implemented support for user defined plate types
//  05/07/2010  MCC     added time stamp to plate definition
//  06/11/2010  MCC     modified to backup location file when storing
//  06/29/2010  MCC     generalized plate definition for all tip heads
//  05/11/2011  MCC     added barcode regular expression to plate definition
//  07/02/2012  MCC     added support for variable specific plate definitions
//  11/01/2013  MCC     refactored plate definitions implementation
//  11/05/2013  MCC     added destroy interface to plate definition
//  11/05/2013  MCC     generalized plate definition exception constructor
//  12/11/2013  MEG     added well depth
//  01/18/2018  MCC     implemented further support for interoperability
//
// ============================================================================

#endif
