// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: DispenserBeckhoff.cpp
//
//     Description: Dispenser Beckhoff class definition
//
//          Author: Tommy Nguyen
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: dispenserbeckhoff.cpp %
//        %version: 20.1.2 %
//          %state: %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "StdAfx.h"
#include "DispenserBeckhoff.h"
#include "DeviceVisitor.h"
#include "HaspAdapter.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace
{
  using PDCLib::CDispenserBeckhoff;

  const CString                          DEFAULT_CALIBRATION_FILE_NAME      (_T ("\\Calibration\\DispenserBeckhoff\\caldev0.csv"));
  const CString                          DEFAULT_TEMPLATE_FILE_NAME         (_T ("\\Calibration\\DispenserBeckhoff\\template0.pdt"));
  const CString                          DEFAULT_LOCATIONS_FILE_NAME        (_T ("\\Calibration\\DispenserBeckhoff\\locations0.loc"));
  const CString                          DEFAULT_PLATE_DEFINTIONS_FILE_NAME (_T ("\\Calibration\\DispenserBeckhoff\\plateDefinitions.plt"));
  const int                              DEFAULT_PORT_NUMBER                (1);
  const int                              DEFAULT_NUM_TIP_HEADS              (1);
  const int                              DEFAULT_ADS_CONTROLLER_ID          (0);
  const WORD                             DEFAULT_ANALOG_PORT_NUMBER         (302);
  const WORD                             DEFAULT_DISCRETE_PORT_NUMBER       (301);
  const bool                             DEFAULT_HAS_BALANCE                (false);
  const bool                             DEFAULT_HAS_VENTED_ENCLOSURE       (false);

  const CDispenserBeckhoff::EOrientation DEFAULT_ORIENTATION                (CDispenserBeckhoff::RightHanded);

  const int                              NUMBER_PORTS                       (3);
}

namespace PDCLib
{
IMPLEMENT_SERIAL (CDispenserBeckhoff, CDevice, VERSIONABLE_SCHEMA | TOPOLOGY_SCHEMA_VERSION_26)

CDispenserBeckhoff::CDispenserBeckhoff (void)
  : CDevice (DEFAULT_CALIBRATION_FILE_NAME, DEFAULT_TEMPLATE_FILE_NAME)
  , m_locationsFileName (GetAppDataFileName (DEFAULT_LOCATIONS_FILE_NAME))
  , m_plateDefinitionsFileName (GetAppDataFileName (DEFAULT_PLATE_DEFINTIONS_FILE_NAME))
  , m_numTipHeads (DEFAULT_NUM_TIP_HEADS)
  , m_adsControllerID (DEFAULT_ADS_CONTROLLER_ID)
  , m_analogPortNumber (DEFAULT_ANALOG_PORT_NUMBER)
  , m_discretePortNumber (DEFAULT_DISCRETE_PORT_NUMBER)
  , m_hasBalance (DEFAULT_HAS_BALANCE)
  , m_hasVentedEnclosure (DEFAULT_HAS_VENTED_ENCLOSURE)
  , m_orientation (DEFAULT_ORIENTATION)
{
  m_portNumber.SetSize (NUMBER_PORTS);

  m_portNumber[0] = DEFAULT_PORT_NUMBER;
  m_portNumber[1] = DEFAULT_PORT_NUMBER + 1;
  m_portNumber[2] = DEFAULT_PORT_NUMBER + 2;
}

CDispenserBeckhoff::~CDispenserBeckhoff ()
{
}

bool
CDispenserBeckhoff::IsFeaturePresent (void) const
{
  // ============================================================================
  //  CRITICAL SECURITY NOTE:
  //
  //  All new devices must have a unique HASP feature ID associated with them.
  //  The default feature ID is to maintain backward compatibility with legacy
  //  devices. The obsolete feature ID is to deprecate devices which no longer
  //  exist. Moving forward we must have device-specific HASP keys in order to
  //  have a finer level of control over how the software is used.
  //
  // ============================================================================

  return CHaspAdapter::HasFeature (CHaspAdapter::EHaspFeature::haspDefaultFeature);
}

void
CDispenserBeckhoff::Accept (CDeviceVisitor & deviceVisitor)
{
  deviceVisitor.Visit (this);
}

void
CDispenserBeckhoff::Serialize (CArchive & ar)
{
  CDevice::Serialize (ar);

  if (ar.IsLoading ())
    {
      switch (GetObjectSchema ())
        {
          case TOPOLOGY_SCHEMA_VERSION_0:
          case TOPOLOGY_SCHEMA_VERSION_1:
          case TOPOLOGY_SCHEMA_VERSION_2:
          case TOPOLOGY_SCHEMA_VERSION_3:
          case TOPOLOGY_SCHEMA_VERSION_4:
          case TOPOLOGY_SCHEMA_VERSION_5:
          case TOPOLOGY_SCHEMA_VERSION_6:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
          case TOPOLOGY_SCHEMA_VERSION_7:
          case TOPOLOGY_SCHEMA_VERSION_8:
            {
              ar >> m_locationsFileName;
              ar >> m_plateDefinitionsFileName;
              ar >> m_numTipHeads;
              ar >> m_adsControllerID;
              ar >> m_analogPortNumber;
              ar >> m_discretePortNumber;

              m_portNumber.Serialize (ar);
              m_portNumber.SetSize (NUMBER_PORTS);

              break;
            }
          case TOPOLOGY_SCHEMA_VERSION_9:
          case TOPOLOGY_SCHEMA_VERSION_10:
          case TOPOLOGY_SCHEMA_VERSION_11:
            {
              bool l_inhibitReferenceTeaching;

              ar >> m_locationsFileName;
              ar >> m_plateDefinitionsFileName;
              ar >> m_numTipHeads;
              ar >> m_adsControllerID;
              ar >> m_analogPortNumber;
              ar >> m_discretePortNumber;
              ar >> l_inhibitReferenceTeaching;

              m_portNumber.Serialize (ar);
              m_portNumber.SetSize (NUMBER_PORTS);

              SetLimitedAccess (l_inhibitReferenceTeaching);

              break;
            }
          case TOPOLOGY_SCHEMA_VERSION_12:
          case TOPOLOGY_SCHEMA_VERSION_13:
            {
              ar >> m_locationsFileName;
              ar >> m_plateDefinitionsFileName;
              ar >> m_numTipHeads;
              ar >> m_adsControllerID;
              ar >> m_analogPortNumber;
              ar >> m_discretePortNumber;

              m_portNumber.Serialize (ar);
              m_portNumber.SetSize (NUMBER_PORTS);

              break;
            }
          case TOPOLOGY_SCHEMA_VERSION_14:
          case TOPOLOGY_SCHEMA_VERSION_15:
          case TOPOLOGY_SCHEMA_VERSION_16:
            {
              ar >> m_locationsFileName;
              ar >> m_plateDefinitionsFileName;
              ar >> m_numTipHeads;
              ar >> m_adsControllerID;
              ar >> m_analogPortNumber;
              ar >> m_discretePortNumber;

              m_portNumber.Serialize (ar);
              m_portNumber.SetSize (NUMBER_PORTS);

              break;
            }
          case TOPOLOGY_SCHEMA_VERSION_17:
          case TOPOLOGY_SCHEMA_VERSION_18:
            {
              bool l_hasStacker;

              ar >> m_locationsFileName;
              ar >> m_plateDefinitionsFileName;
              ar >> m_numTipHeads;
              ar >> m_adsControllerID;
              ar >> m_analogPortNumber;
              ar >> m_discretePortNumber;
              ar >> l_hasStacker;
              ar >> m_hasBalance;

              m_portNumber.Serialize (ar);

              break;
            }
          case TOPOLOGY_SCHEMA_VERSION_19:
          case TOPOLOGY_SCHEMA_VERSION_20:
          case TOPOLOGY_SCHEMA_VERSION_21:
          case TOPOLOGY_SCHEMA_VERSION_22:
            {
              bool l_hasStacker;

              ar >> m_locationsFileName;
              ar >> m_plateDefinitionsFileName;
              ar >> m_numTipHeads;
              ar >> m_adsControllerID;
              ar >> m_analogPortNumber;
              ar >> m_discretePortNumber;
              ar >> l_hasStacker;
              ar >> m_hasBalance;
              ar >> m_orientation;

              m_portNumber.Serialize (ar);

              break;
            }
          case TOPOLOGY_SCHEMA_VERSION_23:
          case TOPOLOGY_SCHEMA_VERSION_24:
            {
              ar >> m_locationsFileName;
              ar >> m_plateDefinitionsFileName;
              ar >> m_numTipHeads;
              ar >> m_adsControllerID;
              ar >> m_analogPortNumber;
              ar >> m_discretePortNumber;
              ar >> m_hasBalance;
              ar >> m_orientation;

              m_portNumber.Serialize(ar);

              break;
            }
          case TOPOLOGY_SCHEMA_VERSION_25:
          case TOPOLOGY_SCHEMA_VERSION_26:
            {
              ar >> m_locationsFileName;
              ar >> m_plateDefinitionsFileName;
              ar >> m_numTipHeads;
              ar >> m_adsControllerID;
              ar >> m_analogPortNumber;
              ar >> m_discretePortNumber;
              ar >> m_hasBalance;
              ar >> m_hasVentedEnclosure;
              ar >> m_orientation;

              m_portNumber.Serialize(ar);

              break;
            }
          default:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
        }
    }
  else
    {
      ar << m_locationsFileName;
      ar << m_plateDefinitionsFileName;
      ar << m_numTipHeads;
      ar << m_adsControllerID;
      ar << m_analogPortNumber;
      ar << m_discretePortNumber;
      ar << m_hasBalance;
      ar << m_hasVentedEnclosure;
      ar << m_orientation;

      m_portNumber.Serialize (ar);
    }
}

CArchive & operator>> (CArchive & ar, CDispenserBeckhoff::EOrientation & rhs)
{
  ar.Read (&rhs, sizeof (rhs));

  return ar;
}

CArchive & operator<< (CArchive & ar, CDispenserBeckhoff::EOrientation const & rhs)
{
  ar.Write (&rhs, sizeof (rhs));

  return ar;
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  03/26/2010  TAN     initial revision
//  05/24/2010  MCC     implemented support for conditional reference teaching
//  09/13/2010  MCC     implemented infrastructure for head definition access
//  10/11/2010  MEG     implemented support for Raven barcode reader
//  11/05/2010  MCC     implemented support for limited application access
//  06/03/2011  MEG     implemented support for Libra6 plate definitions
//  07/06/2011  TAN     implemented support for G2 Dispenser barcode reader
//  09/22/2011  MCC     added Summit message handler to Device Editor
//  10/10/2011  MEG     added shaker simulation
//  01/11/2012  MCC     implemented support for device specific feature IDs
//  03/13/2012  TAN     implemented support for stacker and balance on G2 Dispenser
//  05/22/2012  MCC     implemented support for pin-tool plate definition
//  07/30/2012  MCC     implemented support for left/right handed dispensers
//  08/03/2012  TAN     modified to make right-handed orientation defaulted
//  10/04/2012  MCC     implemented support for optional high speed centrifuge
//  03/27/2014  MEG     implemented optional barcode reader for lumi reader
//  11/24/2014  MEG     added second generation lumi reader
//  01/22/2015  MCC     added stacker feature
//  01/27/2015  TAN     removed hasStacker boolean on G2Dispenser
//  11/04/2015  MCC     implemented support for simulation mode at device level
//  10/20/2017  MCC     implemented support for optional Celdyne presence
//  10/20/2017  MCC     implemented support for optional Celdyne presence
//  07/12/2018  MCC     implemented support for encrypted calibrations
//
// ============================================================================
