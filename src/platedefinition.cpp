// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: PlateDefinition.cpp
//
//     Description: Plate Definition class definition
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: platedefinition.cpp %
//        %version: 16 %
//          %state: %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "StdAfx.h"
#include "PlateDefinition.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace
{
  const UINT PLATE_DEFINITION_SCHEMA_VERSION_1 (1);
  const UINT PLATE_DEFINITION_SCHEMA_VERSION_2 (2);
  const UINT PLATE_DEFINITION_SCHEMA_VERSION_3 (3);
  const UINT PLATE_DEFINITION_SCHEMA_VERSION_4 (4);
  const UINT PLATE_DEFINITION_SCHEMA_VERSION_5 (5);
  const UINT PLATE_DEFINITION_SCHEMA_VERSION_6 (6);
  const UINT PLATE_DEFINITION_SCHEMA_VERSION_7 (7);
}

namespace PDCLib
{
IMPLEMENT_SERIAL (CPlateDefinition, IPlateDefinition, VERSIONABLE_SCHEMA | PLATE_DEFINITION_SCHEMA_VERSION_7)

CPlateDefinition::CPlateDefinition (void)
  : m_rowSpacing (2.25)
  , m_columnSpacing (2.25)
  , m_wellDepth (5.0)
  , m_minPlateHeight (1.0)
  , m_maxPlateHeight (4.0)
  , m_numRows (32)
  , m_numColumns (48)
  , m_barcodeRegex (_T (".*"))
  , m_referencePosition (EPosition::PositionNone)
  , m_timeStamp (CTime::GetCurrentTime ())
{
}

CPlateDefinition::CPlateDefinition (CString const & identifier, int numTipHeads)
  : m_identifier (identifier)
  , m_rowSpacing (2.25)
  , m_columnSpacing (2.25)
  , m_wellDepth (5.0)
  , m_minPlateHeight (1.0)
  , m_maxPlateHeight (4.0)
  , m_numRows (32)
  , m_numColumns (48)
  , m_barcodeRegex (_T (".*"))
  , m_referencePosition (EPosition::PositionNone)
  , m_timeStamp (CTime::GetCurrentTime ())
{
  m_xAxis.SetSize (numTipHeads);
  m_yAxis.SetSize (numTipHeads);
  m_zAxis.SetSize (numTipHeads);
  m_isTaught.SetSize (numTipHeads);
}

CPlateDefinition::~CPlateDefinition ()
{
}

double
CPlateDefinition::GetXAxis (int tipHead) const
{
  if (GetIsTaught (tipHead))
    {
      return m_xAxis[tipHead];
    }

  throw CPlateDefinitionException (_T ("plate definition is not taught"));
}

double
CPlateDefinition::GetYAxis (int tipHead) const
{
  if (GetIsTaught (tipHead))
    {
      return m_yAxis[tipHead];
    }

  throw CPlateDefinitionException (_T ("plate definition is not taught"));
}

double
CPlateDefinition::GetZAxis (int tipHead) const
{
  if (GetIsTaught (tipHead))
    {
      return m_zAxis[tipHead];
    }

  throw CPlateDefinitionException (_T ("plate definition is not taught"));
}

bool
CPlateDefinition::GetIsTaught (void) const
{
  for (int l_tipHead (0); l_tipHead < GetNumTipHeads (); ++l_tipHead)
    {
      if (GetIsTaught (l_tipHead))
        {
          return true;
        }
    }

  return false;
}

bool
CPlateDefinition::GetIsTaught (int tipHead) const
{
  if (tipHead < GetNumTipHeads ())
    {
      return m_isTaught[tipHead];
    }

  return false;
}

void
CPlateDefinition::Serialize (CArchive & ar)
{
  IPlateDefinition::Serialize (ar);

  if (ar.IsLoading ())
    {
      switch (ar.GetObjectSchema ())
        {
          case PLATE_DEFINITION_SCHEMA_VERSION_1:
            {
              double l_xAxis (0.0);
              double l_yAxis (0.0);
              double l_zAxis (0.0);
              int l_tipHead (0);
              bool l_isTaught (false);

              ar >> m_identifier;
              ar >> l_xAxis;
              ar >> l_yAxis;
              ar >> l_zAxis;
              ar >> m_numRows;
              ar >> m_numColumns;
              ar >> l_tipHead;
              ar >> m_comment;
              ar >> l_isTaught;

              m_xAxis.SetSize (l_tipHead + 1);
              m_yAxis.SetSize (l_tipHead + 1);
              m_zAxis.SetSize (l_tipHead + 1);
              m_isTaught.SetSize (l_tipHead + 1);

              m_xAxis[l_tipHead] = l_xAxis;
              m_yAxis[l_tipHead] = l_yAxis;
              m_zAxis[l_tipHead] = l_zAxis;
              m_isTaught[l_tipHead] = l_isTaught;

              break;
            }
          case PLATE_DEFINITION_SCHEMA_VERSION_2:
            {
              double l_xAxis (0.0);
              double l_yAxis (0.0);
              double l_zAxis (0.0);
              int l_tipHead (0);
              bool l_isTaught (false);

              ar >> m_identifier;
              ar >> l_xAxis;
              ar >> l_yAxis;
              ar >> l_zAxis;
              ar >> m_rowSpacing;
              ar >> m_columnSpacing;
              ar >> m_minPlateHeight;
              ar >> m_maxPlateHeight;
              ar >> m_numRows;
              ar >> m_numColumns;
              ar >> l_tipHead;
              ar >> m_comment;
              ar >> l_isTaught;

              m_xAxis.SetSize (l_tipHead + 1);
              m_yAxis.SetSize (l_tipHead + 1);
              m_zAxis.SetSize (l_tipHead + 1);
              m_isTaught.SetSize (l_tipHead + 1);

              m_xAxis[l_tipHead] = l_xAxis;
              m_yAxis[l_tipHead] = l_yAxis;
              m_zAxis[l_tipHead] = l_zAxis;
              m_isTaught[l_tipHead] = l_isTaught;

              break;
            }
          case PLATE_DEFINITION_SCHEMA_VERSION_3:
            {
              double l_xAxis (0.0);
              double l_yAxis (0.0);
              double l_zAxis (0.0);
              int l_tipHead (0);
              bool l_isTaught (false);

              ar >> m_identifier;
              ar >> l_xAxis;
              ar >> l_yAxis;
              ar >> l_zAxis;
              ar >> m_rowSpacing;
              ar >> m_columnSpacing;
              ar >> m_minPlateHeight;
              ar >> m_maxPlateHeight;
              ar >> m_numRows;
              ar >> m_numColumns;
              ar >> l_tipHead;
              ar >> m_comment;
              ar >> l_isTaught;
              ar >> m_timeStamp;

              m_xAxis.SetSize (l_tipHead + 1);
              m_yAxis.SetSize (l_tipHead + 1);
              m_zAxis.SetSize (l_tipHead + 1);
              m_isTaught.SetSize (l_tipHead + 1);

              m_xAxis[l_tipHead] = l_xAxis;
              m_yAxis[l_tipHead] = l_yAxis;
              m_zAxis[l_tipHead] = l_zAxis;
              m_isTaught[l_tipHead] = l_isTaught;

              break;
            }
          case PLATE_DEFINITION_SCHEMA_VERSION_4:
            {
              ar >> m_identifier;
              m_xAxis.Serialize (ar);
              m_yAxis.Serialize (ar);
              m_zAxis.Serialize (ar);
              ar >> m_rowSpacing;
              ar >> m_columnSpacing;
              ar >> m_minPlateHeight;
              ar >> m_maxPlateHeight;
              ar >> m_numRows;
              ar >> m_numColumns;
              ar >> m_comment;
              m_isTaught.Serialize (ar);
              ar >> m_timeStamp;

              break;
            }
          case PLATE_DEFINITION_SCHEMA_VERSION_5:
            {
              ar >> m_identifier;
              m_xAxis.Serialize (ar);
              m_yAxis.Serialize (ar);
              m_zAxis.Serialize (ar);
              ar >> m_rowSpacing;
              ar >> m_columnSpacing;
              ar >> m_minPlateHeight;
              ar >> m_maxPlateHeight;
              ar >> m_numRows;
              ar >> m_numColumns;
              ar >> m_comment;
              ar >> m_barcodeRegex;
              m_isTaught.Serialize (ar);
              ar >> m_timeStamp;

              break;
            }
          case PLATE_DEFINITION_SCHEMA_VERSION_6:
            {
              ar >> m_identifier;
              m_xAxis.Serialize (ar);
              m_yAxis.Serialize (ar);
              m_zAxis.Serialize (ar);
              ar >> m_rowSpacing;
              ar >> m_columnSpacing;
              ar >> m_minPlateHeight;
              ar >> m_maxPlateHeight;
              ar >> m_numRows;
              ar >> m_numColumns;
              ar >> m_comment;
              ar >> m_barcodeRegex;
              ar >> m_referencePosition;
              m_isTaught.Serialize (ar);
              ar >> m_timeStamp;

              break;
            }
          case PLATE_DEFINITION_SCHEMA_VERSION_7:
            {
              ar >> m_identifier;
              m_xAxis.Serialize (ar);
              m_yAxis.Serialize (ar);
              m_zAxis.Serialize (ar);
              ar >> m_rowSpacing;
              ar >> m_columnSpacing;
              ar >> m_wellDepth;
              ar >> m_minPlateHeight;
              ar >> m_maxPlateHeight;
              ar >> m_numRows;
              ar >> m_numColumns;
              ar >> m_comment;
              ar >> m_barcodeRegex;
              ar >> m_referencePosition;
              m_isTaught.Serialize (ar);
              ar >> m_timeStamp;

              break;
            }
          default:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
        }
    }
  else
    {
      ar << m_identifier;
      m_xAxis.Serialize (ar);
      m_yAxis.Serialize (ar);
      m_zAxis.Serialize (ar);
      ar << m_rowSpacing;
      ar << m_columnSpacing;
      ar << m_wellDepth;
      ar << m_minPlateHeight;
      ar << m_maxPlateHeight;
      ar << m_numRows;
      ar << m_numColumns;
      ar << m_comment;
      ar << m_barcodeRegex;
      ar << m_referencePosition;
      m_isTaught.Serialize (ar);
      ar << m_timeStamp;
    }
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  03/17/2010  MCC     initial revision
//  03/19/2010  MCC     implemented additional integrity checks
//  03/19/2010  TAN     added plate definition
//  03/22/2010  MCC     baselined edit plate definitions dialog
//  03/24/2010  MCC     corrected problem with Z-axis accessor
//  03/25/2010  MCC     moved plate definitions to PDCLib project
//  04/01/2010  MCC     implemented support for user defined plate types
//  05/07/2010  MCC     added time stamp to plate definition
//  06/11/2010  MCC     modified to backup location file when storing
//  06/29/2010  MCC     generalized plate definition for all tip heads
//  07/01/2010  MCC     improved check for whether plate definition is taught
//  05/11/2011  MCC     added barcode regular expression to plate definition
//  07/03/2012  MCC     added support for variable specific plate definitions
//  11/01/2013  MCC     refactored plate definitions implementation
//  12/11/2013  MEG     added well depth
//  01/18/2018  MCC     implemented further support for interoperability
//
// ============================================================================
