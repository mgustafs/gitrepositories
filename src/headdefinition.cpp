// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: HeadDefinition.cpp
//
//     Description: Head Definition class definition
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: headdefinition.cpp %
//        %version: 3 %
//          %state: %
//         %cvtype: c++ %
//     %derived_by: mgustafs %
//  %date_modified: %
//
// ============================================================================

#include "StdAfx.h"
#include "HeadDefinition.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace
{
  const UINT HEAD_DEFINITION_SCHEMA_VERSION_1 (1);
  const UINT HEAD_DEFINITION_SCHEMA_VERSION_2 (2);
}

namespace PDCLib
{
IMPLEMENT_SERIAL (CHeadDefinition, CObject, VERSIONABLE_SCHEMA | HEAD_DEFINITION_SCHEMA_VERSION_2)

CHeadDefinition::CHeadDefinition (void)
  : m_numWells (0)
  , m_offset (0.0)
  , m_stroke (0.0)
  , m_home (0.0)
  , m_dryer (0.0)
  , m_purge (0.0)
  , m_diameter (0.0)
  , m_loadPressure (0.0)
  , m_unloadPressure (0.0)
  , m_timeStamp (CTime::GetCurrentTime ())
{
}

CHeadDefinition::CHeadDefinition (CString const & identifier)
  : m_identifier (identifier)
  , m_numWells (1536)
  , m_offset (0.0)
  , m_stroke (0.0)
  , m_home (0.0)
  , m_dryer (0.0)
  , m_purge (0.0)
  , m_diameter (0.0)
  , m_loadPressure (0.0)
  , m_unloadPressure (0.0)
  , m_timeStamp (CTime::GetCurrentTime ())
{
}

CHeadDefinition::~CHeadDefinition ()
{
}

void
CHeadDefinition::Serialize (CArchive & ar)
{
  CObject::Serialize (ar);

  if (ar.IsLoading ())
    {
      switch (ar.GetObjectSchema ())
        {
          case HEAD_DEFINITION_SCHEMA_VERSION_1:
            {
              ar >> m_identifier;
              ar >> m_numWells;
              ar >> m_offset;
              ar >> m_stroke;
              ar >> m_diameter;
              ar >> m_loadPressure;
              ar >> m_unloadPressure;
              ar >> m_comment;
              ar >> m_timeStamp;

              break;
            }
          case HEAD_DEFINITION_SCHEMA_VERSION_2:
            {
              ar >> m_identifier;
              ar >> m_numWells;
              ar >> m_offset;
              ar >> m_stroke;
              ar >> m_home;
              ar >> m_dryer;
              ar >> m_purge;
              ar >> m_diameter;
              ar >> m_loadPressure;
              ar >> m_unloadPressure;
              ar >> m_comment;
              ar >> m_timeStamp;

              break;
            }
          default:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
        }
    }
  else
    {
      ar << m_identifier;
      ar << m_numWells;
      ar << m_offset;
      ar << m_stroke;
      ar << m_home;
      ar << m_dryer;
      ar << m_purge;
      ar << m_diameter;
      ar << m_loadPressure;
      ar << m_unloadPressure;
      ar << m_comment;
      ar << m_timeStamp;
    }
}

CHeadDefinitionException::CHeadDefinitionException (void)
  : m_errorMessage (PDCLib::GetErrorMessage (::GetLastError ()))
{
}

CHeadDefinitionException::CHeadDefinitionException (CException * e, bool autoDelete)
  : m_errorMessage (PDCLib::GetErrorMessage (e, autoDelete))
{
}

CHeadDefinitionException::CHeadDefinitionException (CString const & errorMessage)
  : m_errorMessage (errorMessage)
{
}

CHeadDefinitionException::CHeadDefinitionException (CHeadDefinitionException const & other)
  : m_errorMessage (other.m_errorMessage)
{
}

CHeadDefinitionException::~CHeadDefinitionException ()
{
}

CString
CHeadDefinitionException::GetErrorMessage (void) const
{
  return m_errorMessage;
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  09/13/2010  MCC     initial revision
//  09/16/2010  MCC     baseline head definition dialog classes
//  10/14/2010  MEG     updated head definition with more positioning values
//
// ============================================================================
