// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: PlateDefinitions.cpp
//
//     Description: Plate Definitions class definition
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: platedefinitions.cpp %
//        %version: 23 %
//          %state: %
//         %cvtype: c++ %
//     %derived_by: mgustafs %
//  %date_modified: %
//
// ============================================================================

#include "StdAfx.h"
#include "PlateDefinitions.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace
{
  const UINT PLATE_DEFINITIONS_SCHEMA_VERSION_1 (1);

  const UINT FILE_OPEN_R_FLAGS (CFile::modeRead       |
                                CFile::modeNoInherit  |
                                CFile::modeCreate     |
                                CFile::modeNoTruncate |
                                CFile::shareExclusive);

  const UINT FILE_OPEN_W_FLAGS (CFile::modeWrite     |
                                CFile::modeNoInherit |
                                CFile::shareExclusive);
}

namespace PDCLib
{
CArchive &
operator>> (CArchive & ar, IPlateDefinitions::ESortCriterion & rhs)
{
  ar.Read (&rhs, sizeof (rhs));

  return ar;
}

CArchive &
operator<< (CArchive & ar, IPlateDefinitions::ESortCriterion & rhs)
{
  ar.Write (&rhs, sizeof (rhs));

  return ar;
}

IMPLEMENT_SERIAL (CPlateDefinitions, IPlateDefinitions, VERSIONABLE_SCHEMA | PLATE_DEFINITIONS_SCHEMA_VERSION_1)

CPlateDefinitions::CPlateDefinitions (void)
  : m_sortCriterion (None)
  , m_plateDefinitions (new CPlateDefinitionList)
{
}

CPlateDefinitions::CPlateDefinitions (CString const & fileName)
  : m_fileName (fileName)
  , m_sortCriterion (None)
  , m_plateDefinitions (new CPlateDefinitionList)
{
}

CPlateDefinitions::~CPlateDefinitions ()
{
  Destroy (m_plateDefinitions);
}

IPlateDefinitions *
CPlateDefinitions::Load (CString const & fileName)
{
  // !!! THIS MUST BE IN THE SAME CONTEXT IN WHICH SERIALIZATION OCCURS !!!

  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  std::unique_ptr <CPlateDefinitions> l_plateDefinitions;

  try
    {
      CFile l_file (fileName, FILE_OPEN_R_FLAGS);

      if (l_file.GetLength ())
        {
          CArchive l_archive (&l_file, CArchive::load);

          l_plateDefinitions.reset (dynamic_cast <CPlateDefinitions *> (l_archive.ReadObject (RUNTIME_CLASS (CPlateDefinitions))));

          if (l_plateDefinitions.get ())
            {
              l_plateDefinitions->m_fileName = fileName;
            }
          else
            {
              throw CPlateDefinitionException (_T ("plate definitions file is corrupt: %s"), (LPCTSTR) fileName);
            }
        }
      else
        {
          l_plateDefinitions.reset (new CPlateDefinitions (fileName));
        }
    }
  catch (CArchiveException * oe)
    {
      try
        {
          CFile l_file (fileName, FILE_OPEN_R_FLAGS);

          CArchive l_archive (&l_file, CArchive::load);

          l_plateDefinitions.reset (new CPlateDefinitions (fileName));

          l_plateDefinitions->m_plateDefinitions->Serialize (l_archive);

          oe->Delete ();
        }
      catch (CException * ie)
        {
          ie->Delete ();

          throw CPlateDefinitionException (oe);
        }
    }
  catch (CException * e)
    {
      throw CPlateDefinitionException (e);
    }

  return l_plateDefinitions.release ();
}

void
CPlateDefinitions::Reload (void)
{
  // !!! THIS MUST BE IN THE SAME CONTEXT IN WHICH SERIALIZATION OCCURS !!!

  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  try
    {
      CFile l_file (m_fileName, FILE_OPEN_R_FLAGS);

      if (l_file.GetLength ())
        {
          CArchive l_archive (&l_file, CArchive::load);

          std::shared_ptr <CPlateDefinitions> l_plateDefinitions (dynamic_cast <CPlateDefinitions *> (l_archive.ReadObject (RUNTIME_CLASS (CPlateDefinitions))));

          if (l_plateDefinitions)
            {
              Destroy (m_plateDefinitions);

              m_plateDefinitions = l_plateDefinitions->m_plateDefinitions;

              l_plateDefinitions->m_plateDefinitions.reset ();
            }
          else
            {
              throw CPlateDefinitionException (_T ("plate definitions file is corrupt: %s"), (LPCTSTR) m_fileName);
            }
        }
      else
        {
          Destroy (m_plateDefinitions);
        }
    }
  catch (CArchiveException * oe)
    {
      try
        {
          CFile l_file (m_fileName, FILE_OPEN_R_FLAGS);

          CArchive l_archive (&l_file, CArchive::load);

          CPlateDefinitionListPtr l_plateDefinitions (new CPlateDefinitionList);

          l_plateDefinitions->Serialize (l_archive);

          Destroy (m_plateDefinitions);

          m_plateDefinitions = l_plateDefinitions;

          oe->Delete ();
        }
      catch (CException * ie)
        {
          ie->Delete ();

          throw CPlateDefinitionException (oe);
        }
    }
  catch (CException * e)
    {
      throw CPlateDefinitionException (e);
    }
}

void
CPlateDefinitions::Store (void)
{
  // !!! THIS MUST BE IN THE SAME CONTEXT IN WHICH SERIALIZATION OCCURS !!!

  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  if (::PathFileExists (m_fileName))
    {
      CString l_fileName (m_fileName);

      ::PathRemoveExtension (l_fileName.GetBuffer (0));

      l_fileName.ReleaseBuffer ();

      l_fileName += CTime::GetCurrentTime ().Format (_T ("-%Y%m%d%H%M%S"));

      l_fileName += CString (::PathFindExtension (m_fileName));

      if (::CopyFile (m_fileName, l_fileName, TRUE) == FALSE)
        {
          throw CPlateDefinitionException ();
        }
    }

  try
    {
      CFile l_file (m_fileName, FILE_OPEN_W_FLAGS);

      CArchive l_archive (&l_file, CArchive::store);

      l_archive.WriteObject (this);
    }
  catch (CException * e)
    {
      throw CPlateDefinitionException (e);
    }
}

POSITION
CPlateDefinitions::GetHeadPosition (void) const
{
  return m_plateDefinitions->GetHeadPosition ();
}

IPlateDefinition *
CPlateDefinitions::GetNext (POSITION & pos)
{
  return m_plateDefinitions->GetNext (pos);
}

IPlateDefinition *
CPlateDefinitions::GetAt (POSITION pos)
{
  return m_plateDefinitions->GetAt (pos);
}

IPlateDefinition *
CPlateDefinitions::Find (CString const & identifier)
{
  POSITION l_pos (GetHeadPosition ());

  while (l_pos)
    {
      IPlateDefinition * const l_plateDefinition (GetNext (l_pos));

      if (l_plateDefinition->GetIdentifier () == identifier)
        {
          return l_plateDefinition;
        }
    }

  throw CPlateDefinitionException (_T ("undefined plate type: %s"), (LPCTSTR) identifier);
}

POSITION
CPlateDefinitions::AddTail (CString const & identifier, int numTipHeads)
{
  if (identifier.IsEmpty ())
    {
      throw CPlateDefinitionException (_T ("plate definition identifier must be nonempty"));
    }
  else if (std::regex_match (static_cast <LPCTSTR> (identifier), CRegex (_T ("[a-zA-Z0-9_]*"))))
    {
      POSITION l_pos (GetHeadPosition ());

      while (l_pos)
        {
          IPlateDefinition * const l_plateDefinition (GetNext (l_pos));

          if (l_plateDefinition->GetIdentifier () == identifier)
            {
              throw CPlateDefinitionException (_T ("plate definition identifier must be unique: %s"), (LPCTSTR) identifier);
            }
        }
    }
  else
    {
      throw CPlateDefinitionException (_T ("invalid plate definition identifier: %s"), (LPCTSTR) identifier);
    }

  return m_plateDefinitions->AddTail (new CPlateDefinition (identifier, numTipHeads));
}

void
CPlateDefinitions::RemoveAt (POSITION pos)
{
  delete GetAt (pos);

  m_plateDefinitions->RemoveAt (pos);
}

void
CPlateDefinitions::Sort (ESortCriterion sortCriterion)
{
  if (sortCriterion > None)
    {
      std::vector <IPlateDefinition *> l_tmp;

      POSITION l_pos (GetHeadPosition ());

      while (l_pos)
        {
          l_tmp.push_back (GetNext (l_pos));
        }

      switch (sortCriterion)
        {
          case FormatAscending:
            {
              std::stable_sort (l_tmp.begin (),
                                l_tmp.end (),
                                [] (IPlateDefinition const * lhs, IPlateDefinition const * rhs) -> bool
                                {
                                  return (lhs->GetNumRows () * lhs->GetNumColumns ()) < (rhs->GetNumRows () * rhs->GetNumColumns ());
                                });

              break;
            }
          case FormatDescending:
            {
              std::stable_sort (l_tmp.begin (),
                                l_tmp.end (),
                                [] (IPlateDefinition const * lhs, IPlateDefinition const * rhs) -> bool
                                {
                                  return (lhs->GetNumRows () * lhs->GetNumColumns ()) > (rhs->GetNumRows () * rhs->GetNumColumns ());
                                });

              break;
            }
          case IdentifierAscending:
            {
              std::sort (l_tmp.begin (),
                         l_tmp.end (),
                         [] (IPlateDefinition const * lhs, IPlateDefinition const * rhs) -> bool { return lhs->GetIdentifier ().Compare (rhs->GetIdentifier ()) < 0; });

              break;
            }
          case IdentifierDescending:
            {
              std::sort (l_tmp.begin (),
                         l_tmp.end (),
                         [] (IPlateDefinition const * lhs, IPlateDefinition const * rhs) -> bool { return lhs->GetIdentifier ().Compare (rhs->GetIdentifier ()) > 0; });

              break;
            }
          default:
            {
              break;
            }
        }

      l_pos = GetHeadPosition ();

      for (auto&& l_i : l_tmp)
        {
          m_plateDefinitions->SetAt (l_pos, dynamic_cast <CPlateDefinition *> (l_i));

          GetNext (l_pos);
        }

      m_sortCriterion = sortCriterion;
    }
}

void
CPlateDefinitions::Serialize (CArchive & ar)
{
  IPlateDefinitions::Serialize (ar);

  if (ar.IsLoading ())
    {
      switch (ar.GetObjectSchema ())
        {
          case PLATE_DEFINITIONS_SCHEMA_VERSION_1:
            {
              ar >> m_sortCriterion;

              m_plateDefinitions->Serialize (ar);

              break;
            }
          default:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
        }
    }
  else
    {
      ar << m_sortCriterion;

      m_plateDefinitions->Serialize (ar);
    }
}

void
CPlateDefinitions::Destroy (CPlateDefinitionListPtr const & plateDefinitionList)
{
  if (plateDefinitionList)
    {
      POSITION l_pos (plateDefinitionList->GetHeadPosition ());

      while (l_pos)
        {
          delete plateDefinitionList->GetNext (l_pos);
        }

      plateDefinitionList->RemoveAll ();
    }
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  03/17/2010  MCC     initial revision
//  03/18/2010  MCC     implemented infrastructure for plate definition archive
//  03/18/2010  MCC     implemented support for empty plate definition archive
//  03/18/2010  MCC     implemented support for plate definition variable type
//  03/18/2010  MCC     modified to throw exception for undefined plate type
//  03/19/2010  MCC     implemented additional integrity checks
//  03/23/2010  MCC     baselined new plate definition dialog
//  03/24/2010  MCC     modified to check for taught plate definitions
//  03/25/2010  MCC     moved plate definitions to PDCLib project
//  04/01/2010  MCC     implemented support for user defined plate types
//  06/11/2010  MCC     modified to backup location file when storing
//  06/29/2010  MCC     generalized plate definition for all tip heads
//  07/29/2010  MCC     implemented support for sorting plate definitions
//  07/30/2010  MCC     refined support for sorting plate definitions
//  02/03/2011  MCC     implemented infrastructure for continuous jogging
//  06/22/2011  MCC     added buttons to edit definitions dialogs
//  11/01/2013  MCC     refactored plate definitions implementation
//  11/05/2013  MCC     added destroy interface to plate definition
//  11/05/2013  MCC     generalized plate definition exception constructor
//  12/17/2014  MCC     replaced auto pointers with C++11 unique pointers
//  01/14/2015  MCC     corrected for-range syntax
//  01/14/2015  MCC     modified for-range to use universal references
//  04/10/2017  MEG     added cast to CString for resolving conversion warning
//
// ============================================================================
