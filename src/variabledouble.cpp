// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: VariableDouble.cpp
//
//     Description: Variable double class definition
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: variabledouble.cpp %
//        %version: 25 %
//          %state: working %
//         %cvtype: c++ %
//     %derived_by: mgustafs %
//  %date_modified: Monday, October 07, 2002 1:00:29 PM %
//
// ============================================================================

#include "StdAfx.h"
#include "VariableDouble.h"
#include "VariableVisitor.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace PDCLib
{

IMPLEMENT_SERIAL (CVariableDouble, CVariable, VERSIONABLE_SCHEMA | METHOD_SCHEMA_VERSION_13)

CVariableDouble::CVariableDouble (double value)
  : m_value (value),
    m_minValue (NULL),
    m_maxValue (NULL)
{
}

CVariableDouble::CVariableDouble (const CVariableDouble & other)
  : CVariable (other),
    m_value (other.m_value),
    m_minValue ((other.m_minValue == NULL) ? NULL : dynamic_cast <CVariableDouble *> (other.m_minValue->Duplicate ())),
    m_maxValue ((other.m_maxValue == NULL) ? NULL : dynamic_cast <CVariableDouble *> (other.m_maxValue->Duplicate ()))
{
}

CVariableDouble::~CVariableDouble ()
{
  if (m_minValue != NULL)
    {
      m_minValue->Release ();
    }

  if (m_maxValue != NULL)
    {
      m_maxValue->Release ();
    }
}

void
CVariableDouble::SetMinValue (CVariableDouble * minValue)
{
  if (m_minValue != NULL)
    {
      m_minValue->Release ();
    }

  if ((m_minValue = minValue) != NULL)
    {
      m_minValue->AddRef ();
    }
}

void
CVariableDouble::SetMaxValue (CVariableDouble * maxValue)
{
  if (m_maxValue != NULL)
    {
      m_maxValue->Release ();
    }

  if ((m_maxValue = maxValue) != NULL)
    {
      m_maxValue->AddRef ();
    }
}

void
CVariableDouble::Serialize (CArchive & ar)
{
  if (ar.IsLoading ())
    {
      CVariable::Serialize (ar);

      switch (GetObjectSchema ())
        {
          case METHOD_SCHEMA_VERSION_0:
          case METHOD_SCHEMA_VERSION_1:
          case METHOD_SCHEMA_VERSION_2:
          case METHOD_SCHEMA_VERSION_3:
          case METHOD_SCHEMA_VERSION_4:
          case METHOD_SCHEMA_VERSION_5:
          case METHOD_SCHEMA_VERSION_6:
          case METHOD_SCHEMA_VERSION_7:
          case METHOD_SCHEMA_VERSION_8:
          case METHOD_SCHEMA_VERSION_9:
          case METHOD_SCHEMA_VERSION_10:
          case METHOD_SCHEMA_VERSION_11:
          case METHOD_SCHEMA_VERSION_12:
          case METHOD_SCHEMA_VERSION_13:
            {
              ar >> m_value;
              ar >> m_minValue;
              ar >> m_maxValue;

              break;
            }
          default:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
        }
    }
  else
    {
      CVariable::Serialize (ar);

      ar << m_value;
      ar << m_minValue;
      ar << m_maxValue;
    }
}

CString
CVariableDouble::GetFormattedVariable (void) const
{
  CString retval;

  retval.Format (_T ("%.3f"), m_value);

  return retval;
}

CString
CVariableDouble::GetPrintFormattedVariable (bool isTemplateView) const
{
  CString retval;

  retval.Format (_T ("%s %s (%s)"), (LPCTSTR) GetPrintFormattedLVal (isTemplateView), (LPCTSTR) GetFormattedVariable (), (LPCTSTR) GetUnits ());

  CString l_minValue (_T ("-"));
  CString l_maxValue (_T ("-"));

  if (m_minValue != NULL)
    {
      l_minValue.Format (_T ("%.3f"), m_minValue->GetValue ());
    }

  if (m_maxValue != NULL)
    {
      l_maxValue.Format (_T ("%.3f"), m_maxValue->GetValue ());
    }

  return retval + GetPrintFormattedModifiers (l_minValue, l_maxValue, isTemplateView);
}

void
CVariableDouble::Accept (CVariableVisitor & variableVisitor)
{
  variableVisitor.Visit (this);
}

}  // end namespace PDCLib

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  08/05/2002  MCC     initial revision
//  07/21/2003  MCC     updated version schema
//  10/29/2003  MCC     implemented step interator feature
//  04/01/2004  MCC     implemented support for cut/copy/paste operations
//  04/15/2004  MCC     modified paste variable behavior
//  07/19/2005  MCC     replaced all double-underscores
//  01/11/2006  MCC     removed workaround for MFC serialization memory leak
//  01/30/2006  MCC     commonized document and print views
//  03/22/2006  MEG     updated version schema
//  04/14/2006  MEG     modified to use namespace PDCLib
//  12/06/2006  MCC     decoupled method and topology version schema constants
//  07/03/2006  MCC     implemented skip step feature
//  05/11/2009  MCC     implemented support for new document creation strategy
//  03/18/2010  MCC     implemented support for plate definition variable type
//  08/04/2010  MCC     implemented support for tree view icons
//  08/09/2010  MCC     enhanced functionality of step iterator
//  08/10/2010  MCC     implemented support for variable grouping
//  11/05/2010  MCC     implemented support for limited application access
//  02/18/2011  MCC     implemented support for method variable logging
//  03/22/2011  MCC     implemented support modifying enumeration keys
//  07/03/2012  MCC     added support for variable specific plate definitions
//  07/20/2012  MCC     implemented support for iterator comment
//  05/09/2013  MCC     refactored variable processing
//  07/17/2014  MCC     implemented support for executing shell commands
//  04/10/2017  MEG     added cast to CString for resolving conversion warning
//
// ============================================================================
