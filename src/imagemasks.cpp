// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: ImageMasks.cpp
//
//     Description: Image Masks class definition
//
//          Author: Marc Gustafson
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: imagemasks.cpp %
//        %version: 3 %
//          %state: %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "StdAfx.h"
#include "ImageMasks.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace
{
  const UINT IMAGE_MASKS_SCHEMA_VERSION_1 (1);

  const UINT FILE_OPEN_R_FLAGS (CFile::modeRead       |
                                CFile::modeNoInherit  |
                                CFile::modeCreate     |
                                CFile::modeNoTruncate |
                                CFile::shareExclusive);

  const UINT FILE_OPEN_W_FLAGS (CFile::modeWrite     |
                                CFile::modeNoInherit |
                                CFile::shareExclusive);
}

namespace PDCLib
{
IMPLEMENT_SERIAL (CImageMasks, CObject, VERSIONABLE_SCHEMA | IMAGE_MASKS_SCHEMA_VERSION_1)

CImageMasks::CImageMasks (void)
  : m_imageMasks (new CImageMaskList)
{
}

CImageMasks::CImageMasks (CString const & fileName)
  : m_fileName (fileName)
  , m_imageMasks (new CImageMaskList)
{
}

CImageMasks::~CImageMasks ()
{
  Destroy (m_imageMasks);
}

CImageMasks *
CImageMasks::Load (CString const & fileName)
{
  // !!! THIS MUST BE IN THE SAME CONTEXT IN WHICH SERIALIZATION OCCURS !!!

  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  std::unique_ptr <CImageMasks> l_imageMasks;

  try
    {
      CFile l_file (fileName, FILE_OPEN_R_FLAGS);

      if (l_file.GetLength ())
        {
          CArchive l_archive (&l_file, CArchive::load);

          l_imageMasks.reset (dynamic_cast <CImageMasks *> (l_archive.ReadObject (RUNTIME_CLASS (CImageMasks))));

          l_imageMasks->m_fileName = fileName;
        }
      else
        {
          l_imageMasks.reset (new CImageMasks (fileName));
        }
    }
  catch (CException * e)
    {
      throw CImageMaskException (e);
    }

  return l_imageMasks.release ();
}

void
CImageMasks::Reload (void)
{
  // !!! THIS MUST BE IN THE SAME CONTEXT IN WHICH SERIALIZATION OCCURS !!!

  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  try
    {
      CFile l_file (m_fileName, FILE_OPEN_R_FLAGS);

      if (l_file.GetLength ())
        {
          CArchive l_archive (&l_file, CArchive::load);

          std::shared_ptr <CImageMasks> l_imageMasks (dynamic_cast <CImageMasks *> (l_archive.ReadObject (RUNTIME_CLASS (CImageMasks))));

          if (l_imageMasks)
            {
              Destroy (m_imageMasks);

              m_imageMasks = l_imageMasks->m_imageMasks;

              l_imageMasks->m_imageMasks.reset ();
            }
          else
            {
              throw CImageMaskException (_T ("image definitions file is corrupt: ") + m_fileName);
            }
        }
      else
        {
          Destroy (m_imageMasks);
        }
    }
  catch (CArchiveException * oe)
    {
      try
        {
          CFile l_file (m_fileName, FILE_OPEN_R_FLAGS);

          CArchive l_archive (&l_file, CArchive::load);

          CImageMaskListPtr l_imageMasks (new CImageMaskList);

          l_imageMasks->Serialize (l_archive);

          Destroy (m_imageMasks);

          m_imageMasks = l_imageMasks;

          oe->Delete ();
        }
      catch (CException * ie)
        {
          ie->Delete ();

          throw CImageMaskException (oe);
        }
    }
  catch (CException * e)
    {
      throw CImageMaskException (e);
    }
}

void
CImageMasks::Store (void)
{
  // !!! THIS MUST BE IN THE SAME CONTEXT IN WHICH SERIALIZATION OCCURS !!!

  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  if (::PathFileExists (m_fileName))
    {
      CString l_fileName (m_fileName);

      ::PathRemoveExtension (l_fileName.GetBuffer (0));

      l_fileName.ReleaseBuffer ();

      l_fileName += CTime::GetCurrentTime ().Format (_T ("-%Y%m%d%H%M%S"));

      l_fileName += CString (::PathFindExtension (m_fileName));

      if (::CopyFile (m_fileName, l_fileName, TRUE) == FALSE)
        {
          throw CImageMaskException ();
        }
    }

  try
    {
      CFile l_file (m_fileName, FILE_OPEN_W_FLAGS);

      CArchive l_archive (&l_file, CArchive::store);

      l_archive.WriteObject (this);
    }
  catch (CException * e)
    {
      throw CImageMaskException (e);
    }
}

POSITION
CImageMasks::GetHeadPosition (void) const
{
  return m_imageMasks->GetHeadPosition ();
}

CImageMask *
CImageMasks::GetNext (POSITION & pos)
{
  return m_imageMasks->GetNext (pos);
}

CImageMask *
CImageMasks::GetAt (POSITION pos)
{
  return m_imageMasks->GetAt (pos);
}

CImageMask *
CImageMasks::Find (CString const & identifier)
{
  POSITION l_pos (GetHeadPosition ());

  while (l_pos)
    {
      CImageMask * const l_imageMask (GetNext (l_pos));

      if (l_imageMask->GetIdentifier () == identifier)
        {
          return l_imageMask;
        }
    }

  throw CImageMaskException (_T ("undefined image mask: ") + identifier);
}

POSITION
CImageMasks::AddTail (CString const & identifier)
{
  if (identifier.IsEmpty ())
    {
      throw CImageMaskException (_T ("image mask identifier must be nonempty"));
    }
  else if (std::regex_match (static_cast <LPCTSTR> (identifier), CRegex (_T ("[a-zA-Z0-9_]*"))))
    {
      POSITION l_pos (GetHeadPosition ());

      while (l_pos)
        {
          CImageMask * const l_imageMask (GetNext (l_pos));

          if (l_imageMask->GetIdentifier () == identifier)
            {
              throw CImageMaskException (_T ("image mask identifier must be unique: ") + identifier);
            }
        }
    }
  else
    {
      throw CImageMaskException (_T ("invalid image mask identifier: ") + identifier);
    }

  return m_imageMasks->AddTail (new CImageMask (identifier));
}

void
CImageMasks::RemoveAt (POSITION pos)
{
  delete GetAt (pos);

  m_imageMasks->RemoveAt (pos);
}

void
CImageMasks::Serialize (CArchive & ar)
{
  CObject::Serialize (ar);

  if (ar.IsLoading ())
    {
      switch (ar.GetObjectSchema ())
        {
          case IMAGE_MASKS_SCHEMA_VERSION_1:
            {
              m_imageMasks->Serialize (ar);

              break;
            }
          default:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
        }
    }
  else
    {
      m_imageMasks->Serialize (ar);
    }
}

void
CImageMasks::Destroy (CImageMaskListPtr const & imageMaskList)
{
  if (imageMaskList)
    {
      POSITION l_pos (imageMaskList->GetHeadPosition ());

      while (l_pos)
        {
          delete imageMaskList->GetNext (l_pos);
        }

      imageMaskList->RemoveAll ();
    }
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  09/14/2010  MEG     initial revision
//  06/22/2011  MCC     added buttons to edit definitions dialogs
//  12/17/2014  MCC     replaced auto pointers with C++11 unique pointers
//
// ============================================================================
