// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2001.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: Utility.cpp
//
//     Description: Utility definitions
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: utility.cpp %
//        %version: 53 %
//          %state: %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "StdAfx.h"
#include "Utility.h"
#include "Messages.h"
#include "VersionInfo.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace
{
  class CTickCount
  {
  public:
    explicit CTickCount (void);
    virtual ~CTickCount () { ; }

    ULONGLONG operator() (void) const;

  private:
    bool const m_usePerformanceCounter;
    LARGE_INTEGER m_frequency;

    // copy construction and assignment not allowed for this class

    static LONGLONG const MSEC_PER_SEC;
    static LONGLONG const MIN_FREQUENCY;
    static LONGLONG const MAX_FREQUENCY;

    CTickCount (const CTickCount &);
    CTickCount & operator = (const CTickCount &);
  };

  LONGLONG const CTickCount::MSEC_PER_SEC  (1000);
  LONGLONG const CTickCount::MIN_FREQUENCY (1000);
  LONGLONG const CTickCount::MAX_FREQUENCY (std::numeric_limits <LONGLONG>::max () / (10 * 365 * 24 * 60 * 60 * 1000i64));

  CTickCount::CTickCount (void)
    : m_usePerformanceCounter (::QueryPerformanceFrequency (&m_frequency) && (m_frequency.QuadPart > MIN_FREQUENCY) && (m_frequency.QuadPart < MAX_FREQUENCY))
  {
    ATLTRACE2 (_T ("[DEBUG] performance counter frequency: %I64d Hz\n"), m_frequency.QuadPart);
    ATLTRACE2 (m_usePerformanceCounter ? _T ("[DEBUG] performance counter enabled\n") : _T ("[DEBUG] performance counter disabled\n"));
  }

  ULONGLONG
  CTickCount::operator() (void) const
  {
    if (m_usePerformanceCounter)
      {
        LARGE_INTEGER l_reference;

        if (::QueryPerformanceCounter (&l_reference))
          {
            return static_cast <ULONGLONG> ((l_reference.QuadPart * MSEC_PER_SEC) / m_frequency.QuadPart);
          }
      }

    return ::GetTickCount64 ();
  }

#ifdef _DEBUG
  template <typename _Fn> inline void TryExcept (_Fn _Fx) { _Fx (); }
#else
  template <typename _Fn> inline void TryExcept (_Fn _Fx) { __try { _Fx (); } __except (PDCLib::GenerateDump (GetExceptionInformation ())) { throw; } }
#endif

  CString    const EVENT_SOURCE        (_T ("PDCApp Event Logger"));

  CString    const REG_KEY             (_T ("GNF Systems"));

  CString    const REG_DIRECTORY_NAME  (_T ("DirectoryName"));

  CString    const TEMP_FILE_PREFIX    (_T ("5D264B43-FA0C-476F-807A-974BCC31A89E"));

  size_t     const MIN_PASSWORD_LENGTH (8);

  CTickCount const g_tickCount;

  GUID       const g_entropy = { 0x58d96dab, 0x9c8d, 0x4b43, { 0xa9, 0xf9, 0x7c, 0x97, 0x32, 0x96, 0xd5, 0x20 } };

  // CRC-CCITT generator polynomial: x^16 + x^12 + x^5 + 1

  WORD       const g_ccitt[256] =
    {
      0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50A5, 0x60C6, 0x70E7,
      0x8108, 0x9129, 0xA14A, 0xB16B, 0xC18C, 0xD1AD, 0xE1CE, 0xF1EF,
      0x1231, 0x0210, 0x3273, 0x2252, 0x52B5, 0x4294, 0x72F7, 0x62D6,
      0x9339, 0x8318, 0xB37B, 0xA35A, 0xD3BD, 0xC39C, 0xF3FF, 0xE3DE,
      0x2462, 0x3443, 0x0420, 0x1401, 0x64E6, 0x74C7, 0x44A4, 0x5485,
      0xA56A, 0xB54B, 0x8528, 0x9509, 0xE5EE, 0xF5CF, 0xC5AC, 0xD58D,
      0x3653, 0x2672, 0x1611, 0x0630, 0x76D7, 0x66F6, 0x5695, 0x46B4,
      0xB75B, 0xA77A, 0x9719, 0x8738, 0xF7DF, 0xE7FE, 0xD79D, 0xC7BC,
      0x48C4, 0x58E5, 0x6886, 0x78A7, 0x0840, 0x1861, 0x2802, 0x3823,
      0xC9CC, 0xD9ED, 0xE98E, 0xF9AF, 0x8948, 0x9969, 0xA90A, 0xB92B,
      0x5AF5, 0x4AD4, 0x7AB7, 0x6A96, 0x1A71, 0x0A50, 0x3A33, 0x2A12,
      0xDBFD, 0xCBDC, 0xFBBF, 0xEB9E, 0x9B79, 0x8B58, 0xBB3B, 0xAB1A,
      0x6CA6, 0x7C87, 0x4CE4, 0x5CC5, 0x2C22, 0x3C03, 0x0C60, 0x1C41,
      0xEDAE, 0xFD8F, 0xCDEC, 0xDDCD, 0xAD2A, 0xBD0B, 0x8D68, 0x9D49,
      0x7E97, 0x6EB6, 0x5ED5, 0x4EF4, 0x3E13, 0x2E32, 0x1E51, 0x0E70,
      0xFF9F, 0xEFBE, 0xDFDD, 0xCFFC, 0xBF1B, 0xAF3A, 0x9F59, 0x8F78,
      0x9188, 0x81A9, 0xB1CA, 0xA1EB, 0xD10C, 0xC12D, 0xF14E, 0xE16F,
      0x1080, 0x00A1, 0x30C2, 0x20E3, 0x5004, 0x4025, 0x7046, 0x6067,
      0x83B9, 0x9398, 0xA3FB, 0xB3DA, 0xC33D, 0xD31C, 0xE37F, 0xF35E,
      0x02B1, 0x1290, 0x22F3, 0x32D2, 0x4235, 0x5214, 0x6277, 0x7256,
      0xB5EA, 0xA5CB, 0x95A8, 0x8589, 0xF56E, 0xE54F, 0xD52C, 0xC50D,
      0x34E2, 0x24C3, 0x14A0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
      0xA7DB, 0xB7FA, 0x8799, 0x97B8, 0xE75F, 0xF77E, 0xC71D, 0xD73C,
      0x26D3, 0x36F2, 0x0691, 0x16B0, 0x6657, 0x7676, 0x4615, 0x5634,
      0xD94C, 0xC96D, 0xF90E, 0xE92F, 0x99C8, 0x89E9, 0xB98A, 0xA9AB,
      0x5844, 0x4865, 0x7806, 0x6827, 0x18C0, 0x08E1, 0x3882, 0x28A3,
      0xCB7D, 0xDB5C, 0xEB3F, 0xFB1E, 0x8BF9, 0x9BD8, 0xABBB, 0xBB9A,
      0x4A75, 0x5A54, 0x6A37, 0x7A16, 0x0AF1, 0x1AD0, 0x2AB3, 0x3A92,
      0xFD2E, 0xED0F, 0xDD6C, 0xCD4D, 0xBDAA, 0xAD8B, 0x9DE8, 0x8DC9,
      0x7C26, 0x6C07, 0x5C64, 0x4C45, 0x3CA2, 0x2C83, 0x1CE0, 0x0CC1,
      0xEF1F, 0xFF3E, 0xCF5D, 0xDF7C, 0xAF9B, 0xBFBA, 0x8FD9, 0x9FF8,
      0x6E17, 0x7E36, 0x4E55, 0x5E74, 0x2E93, 0x3EB2, 0x0ED1, 0x1EF0
    };
}

namespace PDCLib
{
CLSID const CLSID_DOMDocument60    (__uuidof (MSXML2::DOMDocument60));
CLSID const CLSID_XMLSchemaCache60 (__uuidof (MSXML2::XMLSchemaCache60));

void
DDV_FileName (CDataExchange * pDX, CString const & fileName, bool inhibitFileNotFound)
{
  if (pDX->m_bSaveAndValidate)
    {
      DWORD const l_fileAttributes (::GetFileAttributes (fileName));

      if (inhibitFileNotFound)
        {
          DWORD const l_lastError (::GetLastError ());

          if (((l_fileAttributes == INVALID_FILE_ATTRIBUTES) &&
               (l_lastError != ERROR_FILE_NOT_FOUND)) ||
              ((l_fileAttributes != INVALID_FILE_ATTRIBUTES) &&
               (l_fileAttributes & FILE_ATTRIBUTE_DIRECTORY)))
           {
             MessageBoxOk (_T ("Please enter a valid file name."));

             pDX->Fail ();
           }
        }
      else if ((l_fileAttributes == INVALID_FILE_ATTRIBUTES) ||
               (l_fileAttributes & FILE_ATTRIBUTE_DIRECTORY))
        {
          MessageBoxOk (_T ("Please enter a valid file name."));

          pDX->Fail ();
        }
    }
}

void
DDV_DirectoryName (CDataExchange * pDX, CString const & dirName)
{
  if (pDX->m_bSaveAndValidate)
    {
      DWORD const l_fileAttributes (::GetFileAttributes (dirName));

      if ((l_fileAttributes == INVALID_FILE_ATTRIBUTES) ||
          !(l_fileAttributes & FILE_ATTRIBUTE_DIRECTORY))
       {
         MessageBoxOk (_T ("Please enter a valid directory name."));

         pDX->Fail ();
       }
    }
}

void
DDV_NonEmptyText (CDataExchange * pDX, CString const & text)
{
  if (pDX->m_bSaveAndValidate && text.IsEmpty ())
    {
      MessageBoxOk (_T ("Please enter data in all required fields."));

      pDX->Fail ();
    }
}

void
DDV_HostName (CDataExchange * pDX, CString const & hostName)
{
  if (hostName.IsEmpty ())
    {
      MessageBoxOk (_T ("Please enter a valid host name."));

      pDX->Fail ();
    }
  else
    {
      try
        {
          std::unique_ptr <CWaitCursor> l_waitCursor (new CWaitCursor);

          SOCKADDR_IN l_host;

          InitHost (l_host, hostName, 0);
        }
      catch (CString const & errorMessage)
        {
          MessageBoxOk (_T ("Invalid host name: %s"), (LPCTSTR) errorMessage);

          pDX->Fail ();
        }
    }
}

void
DDV_Regex (CDataExchange * pDX, CString const & text, CString const & regex)
{
  if (pDX->m_bSaveAndValidate && !std::regex_match (static_cast <LPCTSTR> (text), CRegex (regex)))
    {
      MessageBoxOk (_T ("Please enter valid data in all required fields."));

      pDX->Fail ();
    }
}

void
DDV_Password (CDataExchange * pDX, CString const & password)
{
  std::vector <TCHAR> l_password;

  StringToVector (password, l_password);

  if (l_password.size () < MIN_PASSWORD_LENGTH)
    {
      MessageBoxOk (_T ("Password must be at least %ld characters in length."), MIN_PASSWORD_LENGTH);

      pDX->Fail ();
    }

  if (std::find_if (l_password.begin (), l_password.end (), [] (auto c) { return std::islower (c); }) == l_password.end ())
    {
      MessageBoxOk (_T ("Password must contain at least one lower case character."));

      pDX->Fail ();
    }

  if (std::find_if (l_password.begin (), l_password.end (), [] (auto c) { return std::isupper (c); }) == l_password.end ())
    {
      MessageBoxOk (_T ("Password must contain at least one upper case character."));

      pDX->Fail ();
    }

  if (std::find_if (l_password.begin (), l_password.end (), [] (auto c) { return std::ispunct (c); }) == l_password.end ())
    {
      MessageBoxOk (_T ("Password must contain at least one punctuation character."));

      pDX->Fail ();
    }

  if (std::find_if (l_password.begin (), l_password.end (), [] (auto c) { return std::isdigit (c); }) == l_password.end ())
    {
      MessageBoxOk (_T ("Password must contain at least one numeric character."));

      pDX->Fail ();
    }
}

void
DDX_IPAddress (CDataExchange * pDX, int nIDC, CString & ipAddressVal)
{
  auto const l_ipAddressCtrl (reinterpret_cast <CIPAddressCtrl *> (CWnd::FromHandle (pDX->PrepareCtrl (nIDC))));

  if (pDX->m_bSaveAndValidate)
    {
      std::array <BYTE, 4> l_ipAddressVal;

      l_ipAddressCtrl->GetAddress (l_ipAddressVal[0], l_ipAddressVal[1], l_ipAddressVal[2], l_ipAddressVal[3]);

      if (std::any_of (begin (l_ipAddressVal), end (l_ipAddressVal), [] (auto value) { return value != 0; }))
        {
          ipAddressVal.Format (_T ("%d.%d.%d.%d"), l_ipAddressVal[0], l_ipAddressVal[1], l_ipAddressVal[2], l_ipAddressVal[3]);
        }
      else
        {
          MessageBoxOk (_T ("Please enter a valid IP address."));

          pDX->Fail ();
        }
    }
  else
    {
      try
        {
          std::vector <BYTE> l_ipAddressVal;

          Tokenize <UInt8Converter> (ipAddressVal, l_ipAddressVal);

          if (size (l_ipAddressVal) == 4)
            {
              l_ipAddressCtrl->SetAddress (l_ipAddressVal[0], l_ipAddressVal[1], l_ipAddressVal[2], l_ipAddressVal[3]);
            }
        }
      catch (...)
        {
        }
    }
}

CString
GetErrorMessage (DWORD messageId)
{
  CString retval;

  LPTSTR buf (NULL);
  if (::FormatMessage (FORMAT_MESSAGE_ALLOCATE_BUFFER |
                       FORMAT_MESSAGE_FROM_SYSTEM     |
                       FORMAT_MESSAGE_IGNORE_INSERTS,
                       NULL,
                       messageId,
                       0,
                       reinterpret_cast <LPTSTR> (&buf),
                       0,
                       NULL))
    {
      retval = buf;

      retval.TrimRight ();

      ::LocalFree (buf);
    }
  else
    {
      retval.Format (_T ("[%ld]"), messageId);
    }

  return retval;
}

CString
GetErrorMessage (CException * e, bool autoDelete)
{
  CString l_errorMessage;

  std::vector <TCHAR> l_cause (256);

  if (e->GetErrorMessage (&l_cause[0], l_cause.size ()))
    {
      CString l_cause_ (&l_cause[0]);

      l_cause_.TrimRight ();

      l_errorMessage.Format (_T ("handled %s cause: %s"),
                             (LPCTSTR) CString (e->GetRuntimeClass ()->m_lpszClassName),
                             (LPCTSTR) l_cause_);
    }
  else
    {
      l_errorMessage.Format (_T ("handled %s cause: unknown"),
                             (LPCTSTR) CString (e->GetRuntimeClass ()->m_lpszClassName));
    }

  if (autoDelete)
    {
      e->Delete ();
    }

  return l_errorMessage;
}

CString
GetErrorMessage (_com_error const & e)
{
  CString l_errorMessage;

  if (LPCTSTR const l_description = e.Description ())
    {
      l_errorMessage = l_description;
    }
  else
    {
      l_errorMessage = GetErrorMessage (e.Error ());
    }

  return l_errorMessage;
}

CString
StringWithFormat (LPCTSTR format, ...)
{
  CString l_string;

  va_list args;

  va_start (args, format);

  l_string.FormatV (format, args);

  va_end (args);

  return l_string;
}

void
ThrowStringException (LPCTSTR format, ...)
{
  CString l_message;

  va_list args;

  va_start (args, format);

  l_message.FormatV (format, args);

  va_end (args);

  throw l_message;
}

void
TESTHR (HRESULT x)
{
  if (FAILED (x))
    {
      _com_issue_error (x);
    }
}

HRESULT
CoInitialize (LPVOID pvReserved)
{
  HRESULT const l_hResult (::CoInitialize (pvReserved));

  return (l_hResult == RPC_E_CHANGED_MODE) ? S_OK : l_hResult;
}

void
MessageBoxStop (LPCTSTR format, ...)
{
  CString l_message;

  va_list args;

  va_start (args, format);

  l_message.FormatV (format, args);

  va_end (args);

  ::AfxMessageBox (l_message, MB_OK | MB_ICONSTOP);
}

void
MessageBoxOk (LPCTSTR format, ...)
{
  CString l_message;

  va_list args;

  va_start (args, format);

  l_message.FormatV (format, args);

  va_end (args);

  ::AfxMessageBox (l_message, MB_OK | MB_ICONEXCLAMATION);
}

bool
MessageBoxOkCancel (LPCTSTR format, ...)
{
  CString l_message;

  va_list args;

  va_start (args, format);

  l_message.FormatV (format, args);

  va_end (args);

  return ::AfxMessageBox (l_message, MB_OKCANCEL | MB_ICONQUESTION) == IDOK;
}

bool
MessageBoxYesNo (LPCTSTR format, ...)
{
  CString l_message;

  va_list args;

  va_start (args, format);

  l_message.FormatV (format, args);

  va_end (args);

  return ::AfxMessageBox (l_message, MB_YESNO | MB_ICONQUESTION) == IDYES;
}

CString
TerminatePath (CString const & pathName)
{
  CString l_pathName (pathName);

  if (!l_pathName.IsEmpty () && (l_pathName.ReverseFind (_T ('\\')) < (l_pathName.GetLength () - 1)))
    {
      l_pathName += _T ("\\");
    }

  return l_pathName;
}

void
InitHost (SOCKADDR_IN & host, const CString & hostAddress, UINT hostPort)
{
  ::memset (&host, 0, sizeof (host));

  host.sin_family = AF_INET;
  host.sin_port   = ::htons (static_cast <WORD> (hostPort));

  ADDRINFOT * l_addrInfo (NULL);

  if (::GetAddrInfo (hostAddress, NULL, NULL, &l_addrInfo))
    {
      ThrowStringException (_T ("unable to resolve host name %s: %s"), (LPCTSTR) hostAddress, (LPCTSTR) GetErrorMessage (::WSAGetLastError ()));
    }
  else
    {
      for (auto l_i (l_addrInfo); l_i; l_i = l_i->ai_next)
        {
          if (l_i->ai_family == AF_INET)
            {
              host.sin_addr = reinterpret_cast <SOCKADDR_IN const *> (l_i->ai_addr)->sin_addr;

              break;
            }
        }

      ::FreeAddrInfo (l_addrInfo);

      if (host.sin_addr.s_addr == 0)
        {
          ThrowStringException (_T ("unable to resolve host name %s"), (LPCTSTR) hostAddress);
        }
    }
}

CString
GetHostAddress (SOCKADDR_IN const & host)
{
  std::vector <TCHAR> l_addr (16, _T ('\0'));

  if (host.sin_family == AF_INET)
    {
      ::InetNtop (AF_INET, const_cast <IN_ADDR *> (&host.sin_addr), &l_addr[0], l_addr.size ());
    }

  return CString (&l_addr[0]);
}

CString
GetAppDataFolder (void)
{
  CString l_appDataFolder;

  try
    {
      std::vector <TCHAR> l_pszPath (MAX_PATH);

      TESTHR (::SHGetFolderPath (NULL, CSIDL_PERSONAL, NULL, 0, &l_pszPath[0]));

      l_appDataFolder = CString (&l_pszPath[0]);
    }
  catch (...)
    {
    }

  l_appDataFolder = TerminatePath (l_appDataFolder) + REG_KEY;

  ::CreateDirectory (l_appDataFolder, NULL);

  l_appDataFolder = TerminatePath (l_appDataFolder) + _T ("PDCApp");

  ::CreateDirectory (l_appDataFolder, NULL);

  return l_appDataFolder;
}

CString
GetAppProgFolder (void)
{
  CString l_appProgFolder;

  try
    {
      std::vector <TCHAR> l_pszPath (MAX_PATH);

      TESTHR (::SHGetFolderPath (NULL, CSIDL_PROGRAM_FILES, NULL, 0, &l_pszPath[0]));

      l_appProgFolder = CString (&l_pszPath[0]);
    }
  catch (...)
    {
    }

  return TerminatePath (l_appProgFolder) + REG_KEY + _T ("\\PDCApp");
}

CString
GetTempFileName (void)
{
  CString l_tempFileName;

  CString l_pathName;

  DWORD const l_nBufferLength (::GetTempPath (MAX_PATH, l_pathName.GetBuffer (MAX_PATH)));

  if (l_nBufferLength > MAX_PATH)
    {
      ThrowStringException (_T ("unable to get temporary file path: path name is too long"));
    }
  else if (l_nBufferLength)
    {
      l_pathName.ReleaseBuffer ();

      if (::GetTempFileName (l_pathName, TEMP_FILE_PREFIX, 0, l_tempFileName.GetBuffer (MAX_PATH)) == 0)
        {
          ThrowStringException (_T ("unable to create temporary file name: %s"), (LPCTSTR) GetErrorMessage (::GetLastError ()));
        }
    }
  else
    {
      ThrowStringException (_T ("unable to get temporary file path: %s"), (LPCTSTR) GetErrorMessage (::GetLastError ()));
    }

  l_tempFileName.ReleaseBuffer ();

  return l_tempFileName;
}

CString
GetRegKey (void)
{
  return REG_KEY;
}

CString
GetRegSection (void)
{
  return ::AfxGetAppName ();
}

WORD
GetCRC (void const * buf, size_t len, WORD crc)
{
  BYTE const * l_buf (reinterpret_cast <BYTE const *> (buf));
  WORD l_crc (crc);

  for (size_t l_i (0); l_i < len; ++l_i)
    {
      l_crc = static_cast <WORD> ((l_crc << 8) ^ g_ccitt[static_cast <BYTE> ((l_crc >> 8) ^ *l_buf++)]);
    }

  return l_crc;
}

CTime
GetFileWriteTime (HANDLE hFile)
{
  FILETIME l_ftCreate;
  FILETIME l_ftAccess;
  FILETIME l_ftWrite;

  if (::GetFileTime (hFile, &l_ftCreate, &l_ftAccess, &l_ftWrite))
    {
      SYSTEMTIME l_stWrite;

      if (::FileTimeToSystemTime (&l_ftWrite, &l_stWrite))
        {
          SYSTEMTIME l_ltWrite;

          if (::SystemTimeToTzSpecificLocalTime (NULL, &l_stWrite, &l_ltWrite))
            {
              return CTime (l_ltWrite);
            }
        }
    }

  return CTime (0);
}

int
GenerateDump (PEXCEPTION_POINTERS pExceptionPointers)
{
  CString const l_path (TerminatePath (GetAppDataFolder ()) + _T ("Log"));

  ::CreateDirectory (l_path, NULL);

  CVersionInfo l_versionInfo;
  CString l_version;
  CString l_fileName;

  if (l_versionInfo.Create (::AfxGetApp ()->m_hInstance) && l_versionInfo.GetFileVersion (l_version))
    {
      l_fileName.Format (_T ("%s%s-%s-%s-%ld-%ld.dmp"),
                         (LPCTSTR) TerminatePath (l_path),
                         ::AfxGetApp ()->m_pszAppName,
                         (LPCTSTR) l_version,
                         (LPCTSTR) CTime::GetCurrentTime ().Format (_T ("%Y-%j-%H-%M-%S")),
                         ::GetCurrentProcessId (),
                         ::GetCurrentThreadId ());
    }
  else
    {
      l_fileName.Format (_T ("%s%s-%s-%ld-%ld.dmp"),
                         (LPCTSTR) TerminatePath (l_path),
                         ::AfxGetApp ()->m_pszAppName,
                         (LPCTSTR) CTime::GetCurrentTime ().Format (_T ("%Y-%j-%H-%M-%S")),
                         ::GetCurrentProcessId (),
                         ::GetCurrentThreadId ());
    }

  CHandle const l_handle (l_fileName, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_WRITE | FILE_SHARE_READ, CREATE_ALWAYS);

  if (l_handle.IsInvalid ())
    {
      ReportEvent (EVENTLOG_ERROR_TYPE, _T ("The application has unexpectedly terminated.  Unable to generate dump file: %s"), (LPCTSTR) GetErrorMessage (::GetLastError ()));
    }
  else
    {
      MINIDUMP_EXCEPTION_INFORMATION l_exceptionParam;
      l_exceptionParam.ThreadId          = ::GetCurrentThreadId ();
      l_exceptionParam.ExceptionPointers = pExceptionPointers;
      l_exceptionParam.ClientPointers    = TRUE;

      if (::MiniDumpWriteDump (::GetCurrentProcess (),
                               ::GetCurrentProcessId (),
                               l_handle,
                               MiniDumpWithDataSegs,
                               &l_exceptionParam,
                               NULL,
                               NULL))
        {
          ReportEvent (EVENTLOG_ERROR_TYPE,
                       _T ("The application has unexpectedly terminated.  Dump file %s has been generated.  Please send this file to service@gnfsystems.com and contact GNF customer support at (858) 812-1600"),
                       (LPCTSTR) l_fileName);
        }
      else
        {
          ReportEvent (EVENTLOG_ERROR_TYPE, _T ("The application has unexpectedly terminated.  Unable to generate dump file: %s"), (LPCTSTR) GetErrorMessage (::GetLastError ()));
        }
    }

  return EXCEPTION_CONTINUE_SEARCH;
}

void
ReportEvent (WORD eventType, LPCTSTR format, ...)
{
  CString l_eventSource (EVENT_SOURCE);

  OSVERSIONINFOEX l_osVersionInfo;

  ::memset (&l_osVersionInfo, 0, sizeof (l_osVersionInfo));

  l_osVersionInfo.dwOSVersionInfoSize = sizeof (l_osVersionInfo);

#pragma warning (push)
#pragma warning (disable:4996)
#pragma warning (suppress:28159)
  if (::GetVersionEx (reinterpret_cast <LPOSVERSIONINFO> (&l_osVersionInfo)))
#pragma warning (pop)
    {
      if (l_osVersionInfo.dwMajorVersion > 5)
        {
          l_eventSource += _T (" (x86)");
        }
    }

  if (HANDLE const l_hEventLog = ::RegisterEventSource (NULL, l_eventSource))
    {
      CString l_message;

      va_list l_args;

      va_start (l_args, format);

      l_message.FormatV (format, l_args);

      va_end (l_args);

      std::vector <LPCTSTR> l_pInsertStrings;

      l_message = _T ("[") + CString (::AfxGetApp ()->m_pszAppName) + _T ("] ") + l_message;

      l_pInsertStrings.push_back (l_message);

      switch (eventType)
        {
          case EVENTLOG_ERROR_TYPE:
            {
              VERIFY (::ReportEvent (l_hEventLog,
                                     EVENTLOG_ERROR_TYPE,
                                     CATEGORY_NONE,
                                     MSG_EVENTLOG_ERROR_TYPE,
                                     NULL,
                                     static_cast <WORD> (l_pInsertStrings.size ()),
                                     0,
                                     &l_pInsertStrings[0],
                                     NULL));

              break;
            }
          case EVENTLOG_WARNING_TYPE:
            {
              VERIFY (::ReportEvent (l_hEventLog,
                                     EVENTLOG_WARNING_TYPE,
                                     CATEGORY_NONE,
                                     MSG_EVENTLOG_WARNING_TYPE,
                                     NULL,
                                     static_cast <WORD> (l_pInsertStrings.size ()),
                                     0,
                                     &l_pInsertStrings[0],
                                     NULL));

              break;
            }
          case EVENTLOG_INFORMATION_TYPE:
            {
              VERIFY (::ReportEvent (l_hEventLog,
                                     EVENTLOG_INFORMATION_TYPE,
                                     CATEGORY_NONE,
                                     MSG_EVENTLOG_INFORMATION_TYPE,
                                     NULL,
                                     static_cast <WORD> (l_pInsertStrings.size ()),
                                     0,
                                     &l_pInsertStrings[0],
                                     NULL));

              break;
            }
          case EVENTLOG_SUCCESS:
          case EVENTLOG_AUDIT_SUCCESS:
          case EVENTLOG_AUDIT_FAILURE:
          default:
            {
              ASSERT (false); // unsupported event type (should never happen)

              break;
            }
        }

      ::DeregisterEventSource (l_hEventLog);
    }
}

IXMLDOMDocumentPtr
LoadXML (WORD xmlSchema, CString const & xmlSource, bool loadIndirect, HMODULE hModule)
{
  IXMLDOMDocumentPtr l_xmlDocument;

  try
    {
      l_xmlDocument.CreateInstance (CLSID_DOMDocument60);

      if (HRSRC const l_hResInfo = ::FindResource (hModule, MAKEINTRESOURCE (xmlSchema), RT_RCDATA))
        {
          if (HGLOBAL const l_hResData = ::LoadResource (hModule, l_hResInfo))
            {
              if (LPCSTR const l_pRes = reinterpret_cast <LPCSTR> (::LockResource (l_hResData)))
                {
                  std::vector <char> l_xmlSchema;

                  std::copy (l_pRes, l_pRes + size_t (::SizeofResource (hModule, l_hResInfo)), std::back_inserter (l_xmlSchema));

                  l_xmlSchema.push_back ('\0');

                  // load schema from resource string...

                  IXMLDOMDocumentPtr l_xdsDocument (CLSID_DOMDocument60);

                  VARIANT_BOOL l_isSuccessful;

                  TESTHR (l_xdsDocument->loadXML (_bstr_t (&l_xmlSchema[0]), &l_isSuccessful));

                  if (l_isSuccessful)
                    {
                      MSXML2::IXMLDOMSchemaCollectionPtr l_schemaCache (CLSID_XMLSchemaCache60);

                      // add schema cache to XML document...

                      MSXML2::IXMLDOMDocument2Ptr l_xmlDocument2 (l_xmlDocument);

                      l_xmlDocument2->schemas         = l_schemaCache.GetInterfacePtr ();
                      l_xmlDocument2->validateOnParse = VARIANT_FALSE;
                      l_xmlDocument2->async           = VARIANT_FALSE;

                      // load XML document...

                      if (loadIndirect)
                        {
                          TESTHR (l_xmlDocument->load (CComVariant (xmlSource), &l_isSuccessful));
                        }
                      else
                        {
                          TESTHR (l_xmlDocument->loadXML (bstr_t (xmlSource), &l_isSuccessful));
                        }

                      if (l_isSuccessful)
                        {
                          // get document element...

                          MSXML2::IXMLDOMElementPtr l_element;

                          TESTHR (l_xmlDocument2->get_documentElement (&l_element));

                          // get XML namespace attribute from document element...

                          _variant_t const l_namespaceURI (l_element->getAttribute (_bstr_t ("xmlns")));

                          if (l_namespaceURI.vt == VT_BSTR)
                            {
                              // add schema document to schema cache with XML namespace...

                              TESTHR (l_schemaCache->add (_bstr_t (l_namespaceURI), l_xdsDocument.GetInterfacePtr ()));
                            }
                          else
                            {
                              // add schema document to schema cache without XML namespace...

                              TESTHR (l_schemaCache->add (_bstr_t (), l_xdsDocument.GetInterfacePtr ()));
                            }

                          // validate XML document against XML schema...

                          MSXML2::IXMLDOMParseErrorPtr l_parseError (l_xmlDocument2->validate ());

                          if (l_parseError->errorCode)
                            {
                              ThrowStringException (_T ("unable to validate XML source against schema: %s"), static_cast <LPCTSTR> (l_parseError->reason));
                            }
                        }
                      else
                        {
                          ThrowStringException (_T ("unable to load XML source"));
                        }
                    }
                  else
                    {
                      ThrowStringException (_T ("unable to load XML schema"));
                    }
                }
              else
                {
                  ThrowStringException (_T ("unable to lock resource: %s"), (LPCTSTR) GetErrorMessage (::GetLastError ()));
                }
            }
          else
            {
              ThrowStringException (_T ("unable to load resource: %s"), (LPCTSTR) GetErrorMessage (::GetLastError ()));
            }
        }
      else
        {
          ThrowStringException (_T ("unable to find resource: %s"), (LPCTSTR) GetErrorMessage (::GetLastError ()));
        }
    }
  catch (_com_error const & e)
    {
      ThrowStringException (_T ("%s"), (LPCTSTR) GetErrorMessage (e));
    }

  return l_xmlDocument;
}

ULONGLONG
GetTickCount (void)
{
  return g_tickCount ();
}

CString
ReadProfileEncrypted (CString const & entry, CString const & value)
{
  if (HKEY const l_hSecKey = ::AfxGetApp ()->GetSectionKey (GetRegSection ()))
    {
      DWORD l_dwType (REG_NONE);
      DWORD l_dwCount (0);

      if (::RegQueryValueEx (l_hSecKey, entry, nullptr, &l_dwType, nullptr, &l_dwCount) == ERROR_SUCCESS)
        {
          if (l_dwType == REG_SZ)
            {
              CString l_password;

              if (::RegQueryValueEx (l_hSecKey,
                                     entry,
                                     nullptr,
                                     &l_dwType,
                                     reinterpret_cast <LPBYTE> (l_password.GetBuffer (l_dwCount / sizeof (TCHAR))),
                                     &l_dwCount) == ERROR_SUCCESS)
                {
                  l_password.ReleaseBuffer ();

                  if (::RegDeleteValue (l_hSecKey, entry) == ERROR_SUCCESS)
                    {
                      WriteProfileEncrypted (entry, l_password);
                    }
                }
            }
        }

      ::RegCloseKey (l_hSecKey);
    }

  CString l_value (value);

  std::vector <DATA_BLOB> l_blob (3);

  if (::AfxGetApp ()->GetProfileBinary (GetRegSection (),
                                        entry,
                                        &l_blob[1].pbData,
                                        reinterpret_cast <UINT *> (&l_blob[1].cbData)))
    {
      l_blob[2].pbData = (LPBYTE) (&g_entropy);
      l_blob[2].cbData = sizeof (g_entropy);

      if (::CryptUnprotectData (&l_blob[1], nullptr, &l_blob[2], nullptr, nullptr, 0, &l_blob[0]))
        {
          l_value = CString (l_blob[0].pbData);

          ::LocalFree (l_blob[0].pbData);
        }

      delete [] l_blob[1].pbData;
    }

  return l_value;
}

bool
WriteProfileEncrypted (CString const & entry, CString const & value)
{
  std::vector <BYTE> l_value;

  StringToVector (value, l_value);

  std::vector <DATA_BLOB> l_blob (3);

  l_blob[0].pbData = &l_value[0];
  l_blob[0].cbData = l_value.size () + 1;

  l_blob[2].pbData = (LPBYTE) (&g_entropy);
  l_blob[2].cbData = sizeof (g_entropy);

  if (::CryptProtectData (&l_blob[0], nullptr, &l_blob[2], nullptr, nullptr, 0, &l_blob[1]))
    {
      bool const retval (::AfxGetApp ()->WriteProfileBinary (GetRegSection (),
                                                             entry,
                                                             l_blob[1].pbData,
                                                             l_blob[1].cbData) ? true : false);

      ::LocalFree (l_blob[1].pbData);

      return retval;
    }

  return false;
}

LONGLONG
GetFileSize (CString const & fileName)
{
  CHandle const l_handle (fileName, FILE_READ_ATTRIBUTES, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL);

  if (!l_handle.IsInvalid ())
    {
      LARGE_INTEGER l_fileSize;

      if (::GetFileSizeEx (l_handle, &l_fileSize))
        {
          return l_fileSize.QuadPart;
        }
    }

  return 0;
}

#ifdef _DEBUG
void
Trace (LPCTSTR format, ...)
{
  CString l_string;

  va_list args;

  va_start (args, format);

  l_string.FormatV (format, args);

  va_end (args);

  static ULONGLONG l_tickCount (g_tickCount ());

  ATLTRACE2 (_T ("[DEBUG]: %.3fs %s\n"), static_cast <double> (g_tickCount () - l_tickCount) / 1000.0, (LPCTSTR) l_string);
}
#endif

IMPLEMENT_DYNAMIC (IIsRepeat, CObject)

IMPLEMENT_DYNAMIC (CIsRepeat, IIsRepeat)

IMPLEMENT_DYNAMIC (IIsMarker, CObject)

IMPLEMENT_DYNAMIC (CIsMarker, IIsMarker)

struct CWorkerThread::SWorkerThreadImpl
{
  enum class EState { Terminated, Suspended, Running, Terminating };

  std::unique_ptr <std::thread> m_thread;
  std::atomic <EState> m_state;
  std::mutex m_mutex;
  std::condition_variable m_event;
};

CWorkerThread::CWorkerThread (IWorkerThread & workerThread, DWORD loopRate, bool pumpMessages)
  : m_workerThread (workerThread)
  , m_loopRate (loopRate)
  , m_pumpMessages (pumpMessages)
  , m_workerThreadImpl (std::make_unique <SWorkerThreadImpl> ())
{
  m_workerThreadImpl->m_state = SWorkerThreadImpl::EState::Terminated;
}

CWorkerThread::~CWorkerThread ()
{
  Terminate ();
}

bool
CWorkerThread::Create (bool workerSuspended)
{
  auto l_expected (SWorkerThreadImpl::EState::Terminated);

  if (m_workerThreadImpl->m_state.compare_exchange_strong (l_expected, workerSuspended ? SWorkerThreadImpl::EState::Suspended : SWorkerThreadImpl::EState::Running))
    {
      m_workerThreadImpl->m_thread = std::make_unique <std::thread> (WorkerThread_, this);

      return true;
    }

  return false;
}

void
CWorkerThread::Resume (void)
{
  auto l_expected (SWorkerThreadImpl::EState::Suspended);

  if (m_workerThreadImpl->m_state.compare_exchange_strong (l_expected, SWorkerThreadImpl::EState::Running))
    {
      m_workerThreadImpl->m_event.notify_one ();
    }
}

void
CWorkerThread::Terminate (void)
{
  Resume ();

  auto l_expected (SWorkerThreadImpl::EState::Running);

  if (m_workerThreadImpl->m_state.compare_exchange_strong (l_expected, SWorkerThreadImpl::EState::Terminating))
    {
      if (m_pumpMessages)
        {
          CWinApp * const l_winApp (::AfxGetApp ());

          ASSERT_VALID (l_winApp);

          if (l_winApp->m_nThreadID == ::GetCurrentThreadId ())
            {
              while (m_workerThreadImpl->m_state == SWorkerThreadImpl::EState::Terminating)
                {
                  MSG l_msg;

                  while (::PeekMessage (&l_msg, nullptr, 0, WM_APP - 1, PM_NOREMOVE))
                    {
                      VERIFY (l_winApp->PumpMessage ());
                    }

                  LONG lIdle (0);

                  while (l_winApp->OnIdle (lIdle++))
                    {
                      ;
                    }
                }
            }
        }

      m_workerThreadImpl->m_thread->join ();

      m_workerThreadImpl->m_thread.reset ();
    }
  else if (l_expected == SWorkerThreadImpl::EState::Terminated)
    {
      if (m_workerThreadImpl->m_thread)
        {
          m_workerThreadImpl->m_thread->join ();

          m_workerThreadImpl->m_thread.reset ();
        }
    }
}

int
CWorkerThread::GetPriority (void) const
{
  return ::GetThreadPriority (m_workerThreadImpl->m_thread->native_handle ());
}

void
CWorkerThread::SetPriority (int priority)
{
  VERIFY (::SetThreadPriority (m_workerThreadImpl->m_thread->native_handle (), priority));
}

DWORD
CWorkerThread::GetThreadId (void) const
{
  return ::GetThreadId (m_workerThreadImpl->m_thread->native_handle ());
}

bool
CWorkerThread::IsCreated (void) const
{
  return m_workerThreadImpl->m_thread ? true : false;
}

bool
CWorkerThread::IsTerminating (void) const
{
  return m_workerThreadImpl->m_state == SWorkerThreadImpl::EState::Terminating;
}

bool
CWorkerThread::IsTerminated (void) const
{
  return m_workerThreadImpl->m_state == SWorkerThreadImpl::EState::Terminated;
}

void
CWorkerThread::WorkerThread (void)
{
  {
    std::unique_lock <std::mutex> l_mutex (m_workerThreadImpl->m_mutex);

    m_workerThreadImpl->m_event.wait (l_mutex, [this] () { return m_workerThreadImpl->m_state > SWorkerThreadImpl::EState::Suspended; });
  }

  if (SUCCEEDED (CoInitialize ()))
    {
      if (m_workerThread.OnStartup ())
        {
          while (m_workerThreadImpl->m_state == SWorkerThreadImpl::EState::Running)
            {
              if (m_workerThread.OnRun ())
                {
                  if (m_loopRate > 0)
                    {
                      Sleep (m_loopRate);
                    }
                }
              else
                {
                  break;
                }
            }
        }

      m_workerThread.OnShutdown ();
    }

  m_workerThreadImpl->m_state = SWorkerThreadImpl::EState::Terminated;
}

void
CWorkerThread::WorkerThread_ (CWorkerThread * workerThread)
{
  TryExcept ([workerThread] () { workerThread->WorkerThread (); });
}

void
Sleep (DWORD duration)
{
  std::this_thread::sleep_for (std::chrono::milliseconds (duration));
}

UINT const CApplicationMutex::WM_TASKBARCREATED (::RegisterWindowMessage (_T ("TaskbarCreated")));
UINT const CApplicationMutex::WM_NOTIFYTRAYICON (::RegisterWindowMessage (_T ("70067CC5-5CAC-469C-9DE5-98A736B1AE83")));

CApplicationMutex::CApplicationMutex (CString const & mutexName, bool showWindow, UINT cursorMenu)
  : m_mutexName (mutexName)
  , m_showWindow (showWindow)
  , m_cursorMenu (cursorMenu)
  , m_wmContextSwitch  (::RegisterWindowMessage (mutexName))
{
}

bool
CApplicationMutex::InitInstanceHook (void) const
{
  if (::CreateMutex (nullptr, FALSE, m_mutexName) == nullptr)
    {
      ThrowStringException (_T ("unable to create application mutex"));
    }
  else if (::GetLastError () == ERROR_ALREADY_EXISTS)
    {
      if (::PostMessage (HWND_BROADCAST, m_wmContextSwitch, 0, 0))
        {
          return false;
        }
      else
        {
          ThrowStringException (_T ("only one instance of this application is allowed"));
        }
    }

  return true;
}

void
CApplicationMutex::WindowProcHook (CWnd * pWnd, UINT dwMessage, WPARAM wParam, LPARAM lParam, UINT nIDResource, CString const & tip, bool minimizeToTray) const
{
  if (dwMessage == WM_NOTIFYTRAYICON)
    {
      if (wParam == nIDResource)
        {
          if (lParam == WM_LBUTTONDBLCLK)
            {
              if (TrayDelete (pWnd, nIDResource))
                {
                  ShowWindow (pWnd);
                }
            }
          else if ((lParam == WM_RBUTTONDOWN) && m_cursorMenu)
            {
              CMenu l_menu;

              l_menu.LoadMenu (m_cursorMenu);

              CMenu * const l_subMenu (l_menu.GetSubMenu (0));

              CPoint l_cursorPos;

              ::GetCursorPos (&l_cursorPos);

              pWnd->SetForegroundWindow ();

              l_subMenu->TrackPopupMenu (TPM_RIGHTALIGN | TPM_BOTTOMALIGN | TPM_RIGHTBUTTON,
                                         static_cast <int> (l_cursorPos.x),
                                         static_cast <int> (l_cursorPos.y),
                                         pWnd);

              pWnd->PostMessage (WM_NULL, 0, 0);
            }
        }
    }
  else if (dwMessage == m_wmContextSwitch)
    {
      if (pWnd->IsWindowVisible ())
        {
          DWORD const l_currentThreadId (::GetCurrentThreadId ());
          DWORD const l_windowThreadProcessId (::GetWindowThreadProcessId (::GetForegroundWindow (), NULL));

          ::AttachThreadInput (l_windowThreadProcessId, l_currentThreadId, TRUE);
          ::SetWindowPos (pWnd->m_hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
          ::SetWindowPos (pWnd->m_hWnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
          pWnd->SetForegroundWindow ();
          ::AttachThreadInput (l_windowThreadProcessId, l_currentThreadId, FALSE);
          pWnd->SetFocus ();
          pWnd->SetActiveWindow ();
        }
      else if (m_showWindow && TrayDelete (pWnd, nIDResource))
        {
          ShowWindow (pWnd);
        }
    }
  else if ((dwMessage == WM_TASKBARCREATED) && !(pWnd->IsWindowVisible () || TrayAdd (pWnd, nIDResource, tip, minimizeToTray)))
    {
      ShowWindow (pWnd);
    }
}

bool
CApplicationMutex::TrayAdd (CWnd * pWnd, UINT nIDResource, CString const & tip, bool minimizeToTray) const
{
  return TrayMessage (pWnd, NIM_ADD, nIDResource, tip, minimizeToTray);
}

bool
CApplicationMutex::TrayModify (CWnd * pWnd, UINT nIDResource, CString const & tip) const
{
  return TrayMessage (pWnd, NIM_MODIFY, nIDResource, tip, false);
}

bool
CApplicationMutex::TrayDelete (CWnd * pWnd, UINT nIDResource) const
{
  return TrayMessage (pWnd, NIM_DELETE, nIDResource, CString (), false);
}

bool
CApplicationMutex::TrayMessage (CWnd * pWnd, DWORD dwMessage, UINT nIDResource, CString const & tip, bool minimizeToTray)
{
  if (((dwMessage == NIM_ADD) && minimizeToTray) ||
      ((dwMessage == NIM_MODIFY) && !pWnd->IsWindowVisible ()) ||
      (dwMessage == NIM_DELETE))
    {
      NOTIFYICONDATA tnd;

      ::memset (&tnd, 0, sizeof (NOTIFYICONDATA));

      tnd.cbSize           = sizeof (NOTIFYICONDATA);
      tnd.hWnd             = pWnd->m_hWnd;
      tnd.uID              = nIDResource;
      tnd.uCallbackMessage = WM_NOTIFYTRAYICON;
      tnd.uFlags           = NIF_MESSAGE | NIF_ICON | NIF_TIP;
      tnd.hIcon            = ::LoadIcon (::AfxGetInstanceHandle (), MAKEINTRESOURCE (nIDResource));

      ::_tcsncpy_s (tnd.szTip, tip, sizeof (tnd.szTip));

      return tnd.hIcon && ::Shell_NotifyIcon (dwMessage, &tnd);
    }

  return false;
}

void
CApplicationMutex::ShowWindow (CWnd * pWnd)
{
  pWnd->ShowWindow (SW_NORMAL);
  pWnd->SetForegroundWindow ();
  pWnd->SetFocus ();
  pWnd->SetActiveWindow ();
}

CEditBrowseFileCtrl::CEditBrowseFileCtrl (bool openFileDialog)

  : m_openFileDialog (openFileDialog)
  , m_mode (EMode::PathName)
{
}

CEditBrowseFileCtrl::CEditBrowseFileCtrl (bool            openFileDialog,
                                          CString const & defExt,
                                          CString const & filter)

  : m_openFileDialog (openFileDialog)
  , m_mode (EMode::PathName)
  , m_defExt (defExt)
  , m_filter (filter)
{
}

CEditBrowseFileCtrl::CEditBrowseFileCtrl (bool            openFileDialog,
                                          EMode           mode,
                                          CString const & defExt,
                                          CString const & filter)

  : m_openFileDialog (openFileDialog)
  , m_mode (mode)
  , m_defExt (defExt)
  , m_filter (filter)
{
}

CEditBrowseFileCtrl::~CEditBrowseFileCtrl ()
{
}

void
CEditBrowseFileCtrl::OnBrowse (void)
{
  if (CMFCEditBrowseCtrl::GetMode () == BrowseMode_File)
    {
      CString const l_regApp (StringWithFormat (_T ("Workspace\\MFCEditBrowseCtrl-%ld"), GetDlgCtrlID ()));

      if (m_dirName.IsEmpty ())
        {
          m_dirName = ::AfxGetApp ()->GetProfileString (l_regApp, REG_DIRECTORY_NAME, GetAppDataFolder ());
        }

      std::shared_ptr <CFileDialog> l_fileDialog;

      if (m_openFileDialog)
        {
          l_fileDialog.reset (new CFileDialog (TRUE,
                                               m_defExt,
                                               GetDefaultPath (),
                                               OFN_FILEMUSTEXIST |
                                               OFN_HIDEREADONLY |
                                               OFN_LONGNAMES |
                                               OFN_NOCHANGEDIR |
                                               OFN_NONETWORKBUTTON,
                                               m_filter,
                                               this));
        }
      else
        {
          l_fileDialog.reset (new CFileDialog (FALSE,
                                               m_defExt,
                                               GetDefaultPath (),
                                               OFN_HIDEREADONLY |
                                               OFN_LONGNAMES |
                                               OFN_NOCHANGEDIR |
                                               OFN_NONETWORKBUTTON,
                                               m_filter,
                                               this));
        }

      if (!m_dirName.IsEmpty ())
        {
          DWORD const l_fileAttributes (::GetFileAttributes (m_dirName));

          if ((l_fileAttributes != INVALID_FILE_ATTRIBUTES) &&
              (l_fileAttributes & FILE_ATTRIBUTE_DIRECTORY))
            {
              l_fileDialog->m_ofn.lpstrInitialDir = m_dirName;
            }
        }

      if (l_fileDialog->DoModal () == IDOK)
        {
          CString const l_fileName (l_fileDialog->GetPathName ());

          std::vector <TCHAR> l_dirName;

          StringToVector (l_fileName, l_dirName);

          if (::PathRemoveFileSpec (&l_dirName[0]))
            {
              m_dirName = CString (&l_dirName[0]);

              if (!m_dirName.IsEmpty ())
                {
                  ::AfxGetApp ()->WriteProfileString (l_regApp, REG_DIRECTORY_NAME, m_dirName);
                }
            }

          if (m_mode == EMode::PathName)
            {
              SetWindowText (l_fileName);
            }
          else if (m_mode == EMode::FileName)
            {
              SetWindowText (l_fileDialog->GetFileName ());
            }
          else
            {
              SetWindowText (l_fileDialog->GetFileTitle ());
            }

          SetModify (TRUE);
          OnAfterUpdate ();
          SetFocus ();
        }
      else
        {
          m_dirName.Empty ();
        }
    }
  else
    {
      CMFCEditBrowseCtrl::OnBrowse ();
    }
}

CString
CEditBrowseFileCtrl::GetDefaultPath (void)
{
  CString l_defaultPath;

  GetWindowText (l_defaultPath);

  if (!l_defaultPath.IsEmpty ())
    {
      if (m_mode == EMode::FileName)
        {
          l_defaultPath = TerminatePath (m_dirName) + l_defaultPath;
        }
      else if (m_mode == EMode::FileTitle)
        {
          l_defaultPath = TerminatePath (m_dirName) + l_defaultPath + m_defExt;
        }
      else
        {
          DWORD const l_fileAttributes (::GetFileAttributes (l_defaultPath));

          if (l_fileAttributes == INVALID_FILE_ATTRIBUTES)
            {
              l_defaultPath.Empty ();
            }
          else if (l_fileAttributes & FILE_ATTRIBUTE_DIRECTORY)
            {
              m_dirName = l_defaultPath;

              l_defaultPath.Empty ();
            }
          else
            {
              std::vector <TCHAR> l_dirName;

              StringToVector (l_defaultPath, l_dirName);

              if (::PathRemoveFileSpec (&l_dirName[0]))
                {
                  m_dirName = CString (&l_dirName[0]);
                }
            }
        }
    }

  return l_defaultPath;
}

CHandle::CHandle (CHandle const & handle)
  : m_handle (INVALID_HANDLE_VALUE)
{
  std::swap (m_handle, handle.m_handle);
}

CHandle::CHandle (HANDLE handle)
  : m_handle (handle)
{
}

CHandle::CHandle (CString const & fileName,
                  DWORD           dwDesiredAccess,
                  DWORD           dwShareMode,
                  DWORD           dwCreationDisposition,
                  DWORD           dwFlagsAndAttributes)
  : m_handle (::CreateFile (fileName,
                            dwDesiredAccess,
                            dwShareMode,
                            nullptr,
                            dwCreationDisposition,
                            dwFlagsAndAttributes,
                            nullptr))
{
}

CHandle::~CHandle ()
{
  VERIFY (Close ());
}

void
CHandle::Attach (HANDLE handle)
{
  ASSERT (m_handle == INVALID_HANDLE_VALUE);

  std::swap (m_handle, handle);
}

HANDLE
CHandle::Detach (void)
{
  auto l_handle (INVALID_HANDLE_VALUE);

  std::swap (m_handle, l_handle);

  return l_handle;
}

bool
CHandle::Close (void)
{
  if (m_handle != INVALID_HANDLE_VALUE)
    {
      if (::CloseHandle (m_handle))
        {
          Detach ();
        }
      else
        {
          return false;
        }
    }

  return true;
}

CHandle &
CHandle::operator= (CHandle const & handle)
{
  if (this != &handle)
    {
      auto const l_handle (Detach ());

      VERIFY ((l_handle == INVALID_HANDLE_VALUE) || ::CloseHandle (l_handle));

      std::swap (m_handle, handle.m_handle);
    }

  return *this;
}

struct CEncryptedFile::SBFImpl
{
  std::unique_ptr <CFile> m_file;
  std::vector <BYTE> m_buffer;
  std::vector <BYTE>::iterator m_pos;
  BF_KEY m_bfKey;
  std::vector <BYTE> m_ivec;
  int m_num;
};

CEncryptedFile::CEncryptedFile (CString const & fileName, UINT flags, CString const & password)
  : m_bfImpl (std::make_unique <SBFImpl> ())
{
  try
    {
      m_bfImpl->m_file = std::make_unique <CFile> (fileName, flags);

      m_bfImpl->m_buffer.resize (BUFSIZ);

      m_bfImpl->m_pos = m_bfImpl->m_buffer.end ();

      if (password.IsEmpty ())
        {
          return;
        }

      std::vector <BYTE> l_bfKey;

      StringToVector (password, l_bfKey);

      ::BF_set_key (&m_bfImpl->m_bfKey, l_bfKey.size (), &l_bfKey[0]);

      m_bfImpl->m_ivec.assign (8, 0);

      m_bfImpl->m_num = 0;
    }
  catch (CException * e)
    {
      ThrowStringException (_T ("%s"), (LPCTSTR) GetErrorMessage (e, true));
    }
}

CEncryptedFile::~CEncryptedFile ()
{
  try
    {
      m_bfImpl->m_file.reset ();
    }
  catch (CException * e)
    {
      ThrowStringException (_T ("%s"), (LPCTSTR) GetErrorMessage (e, true));
    }
}

void
CEncryptedFile::Read (std::vector <BYTE> & buffer)
{
  try
    {
      if (!buffer.empty ())
        {
          std::vector <BYTE> l_cypher (buffer.size ());

          auto const l_count (m_bfImpl->m_file->Read (&l_cypher[0], l_cypher.size ()));

          if (l_count > 0)
            {
              if (m_bfImpl->m_ivec.empty ())
                {
                  std::swap (buffer, l_cypher);
                }
              else
                {
                  buffer.resize (l_count);

                  ::BF_cfb64_encrypt (&l_cypher[0], &buffer[0], buffer.size (), &m_bfImpl->m_bfKey, &m_bfImpl->m_ivec[0], &m_bfImpl->m_num, BF_DECRYPT);
                }
            }
          else
            {
              buffer.clear ();
            }
        }
    }
  catch (CException * e)
    {
      ThrowStringException (_T ("%s"), (LPCTSTR) GetErrorMessage (e, true));
    }
}

bool
CEncryptedFile::ReadString (CString & buffer)
{
  buffer.Empty ();

  while (true)
    {
      while (m_bfImpl->m_pos != m_bfImpl->m_buffer.end ())
        {
          if (auto const l_tmp (*m_bfImpl->m_pos++); l_tmp)
            {
              if (std::isprint (l_tmp) || std::isspace (l_tmp))
                {
                  if (l_tmp == '\n')
                    {
                      return true;
                    }

                  if (l_tmp != '\r')
                    {
                      buffer += l_tmp;
                    }
                }
              else
                {
                  ThrowStringException (_T ("invalid file format"));
                }
            }
        }

      Read (m_bfImpl->m_buffer);

      if (m_bfImpl->m_buffer.empty ())
        {
          m_bfImpl->m_pos = m_bfImpl->m_buffer.end ();

          break;
        }

      m_bfImpl->m_pos = m_bfImpl->m_buffer.begin ();
    }

  return !buffer.IsEmpty ();
}

void
CEncryptedFile::Write (std::vector <BYTE> const & buffer)
{
  try
    {
      if (m_bfImpl->m_ivec.empty ())
        {
          m_bfImpl->m_file->Write (&buffer[0], buffer.size ());
        }
      else
        {
          std::vector <BYTE> l_cypher (buffer.size ());

          ::BF_cfb64_encrypt (&buffer[0], &l_cypher[0], l_cypher.size (), &m_bfImpl->m_bfKey, &m_bfImpl->m_ivec[0], &m_bfImpl->m_num, BF_ENCRYPT);

          m_bfImpl->m_file->Write (&l_cypher[0], l_cypher.size ());
        }
    }
  catch (CException * e)
    {
      ThrowStringException (_T ("%s"), (LPCTSTR) GetErrorMessage (e, true));
    }
}

void
CEncryptedFile::WriteString (CString const & buffer)
{
  std::vector <BYTE> l_buffer;

  l_buffer.reserve (buffer.GetLength ());

  for (LPCTSTR l_p (buffer); *l_p; ++l_p)
    {
      l_buffer.emplace_back (static_cast <BYTE> (*l_p));
    }

  Write (l_buffer);
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  12/18/2009  MCC     initial revision
//  01/05/2010  MCC     implemented support for application data folder
//  05/12/2010  MCC     refactored CRC subroutine into utility library
//  06/18/2010  MCC     modified to generate dump file on abnormal termination
//  07/08/2010  MCC     modified to continue search on exception
//  08/03/2010  MCC     added method last modified attribute
//  08/05/2010  MCC     pre-release code cleanup for last round of changes
//  09/28/2010  MCC     added mini dump logging to system event log
//  09/29/2010  MCC     modified event source to be application specific
//  09/29/2010  MCC     implemented support for directory name subtype
//  03/25/2011  MCC     added function for generating temporary file name
//  05/13/2011  MCC     added barcode regular expression to plate definition
//  09/16/2011  MCC     generalized event logging interface
//  09/21/2011  MCC     baselined message handler class
//  09/22/2011  MCC     added Summit message handler to Device Editor
//  03/21/2013  MCC     implemented workaround for Windows 7 tick count problem
//  08/15/2013  MCC     corrected problem with event reporting
//  08/15/2013  MCC     corrected event source identifier
//  08/16/2013  MCC     added diagnostics to dump file generation
//  08/16/2013  MCC     modified to ensure application data folder exists
//  08/16/2013  MCC     implemented workaround for event id error message
//  11/25/2013  TAN     implemented stacker process
//  12/04/2013  MCC     reimplemented support for repeating iterators
//  04/22/2014  MCC     updated for Visual Studio 2013 upgrade
//  08/06/2014  MCC     updated for Visual Studio 2013 update
//  08/15/2014  MCC     refined host initialization function
//  09/09/2014  MCC     corrected problem with CoInitialize workaround
//  09/26/2014  MCC     implemented support for e-mail notification feature
//  10/08/2014  MCC     implemented e-mail notification test feature
//  10/09/2014  MCC     modified to use SSL CLSID instead of class string
//  11/19/2014  MCC     corrected code analysis issues
//  12/17/2014  MCC     replaced auto pointers with C++11 unique pointers
//  01/09/2015  MCC     templatized number of elements macro
//  07/02/2015  MCC     added method editor to remote control interface
//  07/15/2015  MCC     default browse controls to unique MRU directory
//  07/16/2015  MCC     refactored downstack/upstack into separate iterators
//  08/28/2015  MCC     corrected problems with load XML function
//  08/28/2015  MCC     corrected potential problem with load XML function
//  10/05/2016  MCC     implemented support for application context switching
//  03/31/2017  MCC     added reset method to remote control interface
//  04/04/2017  MCC     refined worker thread implementation
//  04/05/2017  MCC     updated worker thread implementation
//  04/10/2017  MEG     added cast to CString for resolving conversion warning
//  11/20/2017  MCC     baselined handle wrapper class
//  12/01/2017  MCC     updated worker thread implementation
//  12/01/2017  MCC     minor cleanup of worker thread class
//  02/28/2018  MCC     removed dependency on EZMail SMTP components
//  06/06/2018  MCC     implemented support for TwinCAT 3 ADS interface
//  07/11/2018  MCC     implemented support for encrypted calibrations
//  07/24/2018  MCC     corrected problem with reconciling calibrations
//  08/08/2018  MCC     implemented more robust TwinCAT 3 ADS detection
//  08/10/2018  MCC     refactored numeric converstion templates
//  08/14/2018  MCC     updated numeric conversion templates
//
// ============================================================================
