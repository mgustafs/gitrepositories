// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: VariableList.cpp
//
//     Description: Variable list class definition
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: variablelist.cpp %
//        %version: 22 %
//          %state: working %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: Monday, October 07, 2002 1:00:35 PM %
//
// ============================================================================

#include "StdAfx.h"
#include "VariableList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace PDCLib
{

IMPLEMENT_SERIAL (CVariableList, CObject, VERSIONABLE_SCHEMA | METHOD_SCHEMA_VERSION_13)

CVariableList::CVariableList (void)
  : m_objectSchema (METHOD_SCHEMA_VERSION_0)
  , m_isCollapsed (false)
{
}

CVariableList::CVariableList (const CString & identifier)
  : m_objectSchema (METHOD_SCHEMA_VERSION_0)
  , m_isCollapsed (false)
  , m_identifier (identifier)
{
}

CVariableList::CVariableList (const CVariableList & other)
  : m_objectSchema (other.m_objectSchema)
  , m_isCollapsed (other.m_isCollapsed)
  , m_identifier (other.m_identifier)
  , m_comment (other.m_comment)
{
}

CVariableList::~CVariableList ()
{
  while (!m_varList.IsEmpty ())
    {
      CVariable * const variable (dynamic_cast <CVariable *> (m_varList.RemoveHead ()));

      ASSERT_VALID (variable);

      variable->Release ();
    }
}

CVariable *
CVariableList::GetVariable (const CString & alias) const
{
  CVariable * variable (NULL);

  POSITION pos (GetFirstVariable ());

  while (pos != NULL)
    {
      CVariable * const l_variable (GetNextVariable (pos));

      if (l_variable->GetAlias () == alias)
        {
          variable = l_variable;

          break;
        }
    }

  return variable;
}

POSITION
CVariableList::AddVariable (CVariable * variable)
{
  ASSERT_VALID (variable);

  variable->AddRef ();

  return m_varList.AddTail (variable);
}

POSITION
CVariableList::InsertVariableAfter (POSITION pos, CVariable * variable)
{
  ASSERT_VALID (variable);

  variable->AddRef ();

  return m_varList.InsertAfter (pos, variable);
}

POSITION
CVariableList::InsertVariableBefore (POSITION pos, CVariable * variable)
{
  ASSERT_VALID (variable);

  variable->AddRef ();

  return m_varList.InsertBefore (pos, variable);
}

void
CVariableList::RemoveVariable (POSITION pos)
{
  CVariable * const variable (dynamic_cast <CVariable *> (m_varList.GetAt (pos)));

  ASSERT_VALID (variable);

  m_varList.RemoveAt (pos);

  variable->Release ();
}

CVariableList *
CVariableList::Duplicate (bool copyVariables) const
{
  CVariableList * l_varList (new CVariableList (*this));

  if (copyVariables)
    {
      POSITION l_pos (GetFirstVariable ());

      while (l_pos)
        {
          CVariable * const l_variable (GetNextVariable (l_pos)->Duplicate ());

          l_varList->AddVariable (l_variable);

          l_variable->Release ();
        }
    }

  return l_varList;
}

void
CVariableList::Serialize (CArchive & ar)
{
  CObject::Serialize (ar);

  if (ar.IsLoading ())
    {
      m_objectSchema = ar.GetObjectSchema ();

      switch (m_objectSchema)
        {
          case METHOD_SCHEMA_VERSION_0:
          case METHOD_SCHEMA_VERSION_1:
          case METHOD_SCHEMA_VERSION_2:
          case METHOD_SCHEMA_VERSION_3:
          case METHOD_SCHEMA_VERSION_4:
          case METHOD_SCHEMA_VERSION_5:
          case METHOD_SCHEMA_VERSION_6:
          case METHOD_SCHEMA_VERSION_7:
            {
              m_varList.Serialize (ar);

              break;
            }
          case METHOD_SCHEMA_VERSION_8:
          case METHOD_SCHEMA_VERSION_9:
          case METHOD_SCHEMA_VERSION_10:
          case METHOD_SCHEMA_VERSION_11:
          case METHOD_SCHEMA_VERSION_12:
          case METHOD_SCHEMA_VERSION_13:
            {
              ar >> m_isCollapsed;
              ar >> m_identifier;
              ar >> m_comment;

              m_varList.Serialize (ar);

              break;
            }
          default:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
        }
    }
  else
    {
      ar << m_isCollapsed;
      ar << m_identifier;
      ar << m_comment;

      m_varList.Serialize (ar);
    }
}
}  // end namespace PDCLib

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  08/16/2002  MCC     initial revision
//  07/21/2003  MCC     updated version schema
//  10/29/2003  MCC     implemented step interator feature
//  07/21/2004  MCC     added method for getting variable by identifier
//  08/19/2004  MCC     modified to search for variables by alias
//  07/19/2005  MCC     replaced all double-underscores
//  03/22/2006  MEG     updated version schema
//  04/14/2006  MEG     modified to use namespace PDCLib
//  11/28/2006  MCC     baselined serializable topology class hierarchy
//  12/06/2006  MCC     decoupled method and topology version schema constants
//  07/03/2006  MCC     implemented skip step feature
//  03/18/2010  MCC     implemented support for plate definition variable type
//  08/09/2010  MCC     enhanced functionality of step iterator
//  08/10/2010  MCC     implemented support for variable grouping
//  02/18/2011  MCC     implemented support for method variable logging
//  03/22/2011  MCC     implemented support modifying enumeration keys
//  07/03/2012  MCC     added support for variable specific plate definitions
//  07/20/2012  MCC     implemented support for iterator comment
//  04/30/2013  MCC     removed save before run method constraint
//  05/03/2013  MCC     corrected removal of save before run method constraint
//  06/05/2014  MCC     modified to propagate collapsed property on duplicate
//  07/17/2014  MCC     implemented support for executing shell commands
//
// ============================================================================
