// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: Device.cpp
//
//     Description: Device class definition
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: device.cpp %
//        %version: 27.1.2 %
//          %state: %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "StdAfx.h"
#include "Device.h"
#include "DeviceVisitor.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace
{
  const bool  DEFAULT_LIMITED_ACCESS  (false);
  const bool  DEFAULT_SIMULATION_MODE (false);
  const DWORD DEFAULT_RESET_TIMEOUT   (30000);
  const DWORD DEFAULT_RESET_DELAY     (3000);
  const DWORD DEFAULT_LOOP_RATE       (1000);
}

namespace PDCLib
{
IMPLEMENT_SERIAL(CDevice, CObject, VERSIONABLE_SCHEMA | TOPOLOGY_SCHEMA_VERSION_26)

CDevice::CDevice (CString const & calibrationFileName,
                  CString const & templateFileName,
                  CString const & encryptedCalibrationFileName)
  : m_objectSchema (TOPOLOGY_SCHEMA_VERSION_0)
  , m_calibrationFileName (GetAppDataFileName (calibrationFileName))
  , m_templateFileName (GetAppProgFileName (templateFileName))
  , m_encryptedCalibrationFileName (GetAppDataFileName (encryptedCalibrationFileName))
  , m_limitedAccess (DEFAULT_LIMITED_ACCESS)
  , m_simulationMode (DEFAULT_SIMULATION_MODE)
  , m_resetTimeout (DEFAULT_RESET_TIMEOUT)
  , m_resetDelay (DEFAULT_RESET_DELAY)
  , m_loopRate (DEFAULT_LOOP_RATE)
{
}

CDevice::~CDevice ()
{
}

void
CDevice::Accept (CDeviceVisitor & deviceVisitor)
{
  UNUSED_ALWAYS (deviceVisitor);
}

void
CDevice::Serialize (CArchive & ar)
{
  CObject::Serialize (ar);

  if (ar.IsLoading ())
    {
      m_objectSchema = ar.GetObjectSchema ();

      switch (m_objectSchema)
        {
          case TOPOLOGY_SCHEMA_VERSION_0:
          case TOPOLOGY_SCHEMA_VERSION_1:
          case TOPOLOGY_SCHEMA_VERSION_2:
          case TOPOLOGY_SCHEMA_VERSION_3:
          case TOPOLOGY_SCHEMA_VERSION_4:
          case TOPOLOGY_SCHEMA_VERSION_5:
            {
              ar >> m_calibrationFileName;
              ar >> m_resetTimeout;
              ar >> m_resetDelay;
              ar >> m_loopRate;

              break;
            }
          case TOPOLOGY_SCHEMA_VERSION_6:
          case TOPOLOGY_SCHEMA_VERSION_7:
          case TOPOLOGY_SCHEMA_VERSION_8:
          case TOPOLOGY_SCHEMA_VERSION_9:
          case TOPOLOGY_SCHEMA_VERSION_10:
          case TOPOLOGY_SCHEMA_VERSION_11:
            {
              ar >> m_calibrationFileName;
              ar >> m_templateFileName;
              ar >> m_resetTimeout;
              ar >> m_resetDelay;
              ar >> m_loopRate;

              break;
            }
          case TOPOLOGY_SCHEMA_VERSION_12:
          case TOPOLOGY_SCHEMA_VERSION_13:
          case TOPOLOGY_SCHEMA_VERSION_14:
          case TOPOLOGY_SCHEMA_VERSION_15:
          case TOPOLOGY_SCHEMA_VERSION_16:
          case TOPOLOGY_SCHEMA_VERSION_17:
          case TOPOLOGY_SCHEMA_VERSION_18:
          case TOPOLOGY_SCHEMA_VERSION_19:
          case TOPOLOGY_SCHEMA_VERSION_20:
          case TOPOLOGY_SCHEMA_VERSION_21:
          case TOPOLOGY_SCHEMA_VERSION_22:
          case TOPOLOGY_SCHEMA_VERSION_23:
            {
              ar >> m_calibrationFileName;
              ar >> m_templateFileName;
              ar >> m_limitedAccess;
              ar >> m_resetTimeout;
              ar >> m_resetDelay;
              ar >> m_loopRate;

              break;
            }
          case TOPOLOGY_SCHEMA_VERSION_24:
          case TOPOLOGY_SCHEMA_VERSION_25:
            {
              ar >> m_calibrationFileName;
              ar >> m_templateFileName;
              ar >> m_limitedAccess;
              ar >> m_simulationMode;
              ar >> m_resetTimeout;
              ar >> m_resetDelay;
              ar >> m_loopRate;

              break;
            }
          case TOPOLOGY_SCHEMA_VERSION_26:
            {
              ar >> m_calibrationFileName;
              ar >> m_templateFileName;
              ar >> m_encryptedCalibrationFileName;
              ar >> m_limitedAccess;
              ar >> m_simulationMode;
              ar >> m_resetTimeout;
              ar >> m_resetDelay;
              ar >> m_loopRate;

              break;
            }
          default:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
        }
    }
  else
    {
      ar << m_calibrationFileName;
      ar << m_templateFileName;
      ar << m_encryptedCalibrationFileName;
      ar << m_limitedAccess;
      ar << m_simulationMode;
      ar << m_resetTimeout;
      ar << m_resetDelay;
      ar << m_loopRate;
    }
}

CString
CDevice::GetAppDataFileName (CString const & fileName)
{
  return fileName.IsEmpty () ? CString () : (GetAppDataFolder () + fileName);
}

CString
CDevice::GetAppProgFileName (CString const & fileName)
{
  return fileName.IsEmpty () ? CString () : (GetAppProgFolder () + fileName);
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  11/28/2006  MCC     initial revision
//  12/05/2006  MCC     configured location of calibration and location files
//  12/06/2006  MCC     decoupled method and topology version schema constants
//  09/06/2007  MCC     implemented purge valve feature
//  10/30/2007  MCC     implemented pintool barcode reader support
//  01/11/2008  TAN     added TOPOLOGY_SCHEMA_VERSION_4
//  04/04/2008  TAN     added TOPOLOGY_SCHEMA_VERSION_5
//  05/06/2009  MCC     added template file name to topology
//  06/23/2009  MEG     added TOPOLOGY_SCHEMA_VERSION_7
//  01/04/2010  MCC     implemented support for application data folder
//  03/29/2010  MCC     implemented support for user defined plate types
//  05/24/2010  MCC     implemented support for conditional reference teaching
//  09/13/2010  MCC     implemented infrastructure for head definition access
//  10/11/2010  MEG     implemented support for Raven barcode reader
//  11/05/2010  MCC     implemented support for limited application access
//  06/03/2011  MEG     implemented support for Libra6 plate definitions
//  07/06/2011  TAN     implemented support for G2 Dispenser barcode reader
//  09/22/2011  MCC     added Summit message handler to Device Editor
//  10/10/2011  MEG     added shaker simulation
//  03/13/2012  TAN     implemented support for stacker and balance on G2 Dispenser
//  05/22/2012  MCC     implemented support for pin-tool plate definition
//  07/30/2012  MCC     implemented support for left/right handed dispensers
//  10/04/2012  MCC     implemented support for optional high speed centrifuge
//  03/27/2014  MEG     implemented optional barcode reader for lumi reader
//  11/24/2014  MEG     added second generation lumi reader
//  01/27/2015  TAN     removed hasStacker boolean on G2Dispenser
//  11/04/2015  MCC     implemented support for simulation mode at device level
//  10/20/2017  MCC     implemented support for optional Celdyne presence
//  07/12/2018  MCC     implemented support for encrypted calibrations
//
// ============================================================================
