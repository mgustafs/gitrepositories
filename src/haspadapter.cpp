// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2008.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: HaspAdapter.cpp
//
//     Description: Hasp Adapter class definition
//
//          Author: Marc Gustafson
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: haspadapter.cpp %
//        %version: 9.1.7 %
//          %state: %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "StdAfx.h"
#include "HaspAdapter.h"
#include "hasp_api_cpp.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace
{
  std::string const g_vendorCode
  {
    "oxKt3/I78yoSFgjcfqeEJ/s5u5c7aOlX3eA1/pHgt4B3kidhd5LJd1miIsbqfPPyvJZBuZvS90PqBdav"
    "pmxdSS2nKnS/Pf6wrTF5ZvOmn5xudrSBvU5zDq3MU2mvRX9P9zzLpreoIUbPgTn/YO8FOmFqlX9Uowxe"
    "CiLGbzY5YpGVnRiPQ8wmBkDtz5EHaQ79i1LU3U2isbwJJlxlSugctLV3wyYOL8v8338o+pWLefdhoxMg"
    "hpveBtJO9Ao3GSgimQFB9G1CLn6HFURUjHy+mZ75AB6JZApMIjm+QCFcS2i9SDp9KhMI+3D+7UYk2IC1"
    "Rz7UbAN4ijG11Pm0cKLNtWEtjBm1JPZIjxhR6oOK8Qo0GlO4W5+BP/zahc52O5nqQ07S8CNblFiJTL0M"
    "KQvt+43dmaxsUmS3OthjGU212DMoEk35HjZsV3HdbNO9NT8hvEXQscMVqF1Ysh2PRMqf4c915HPV798v"
    "/aMGnAFVmriiQuznQLYWyxbd1YjkYNqrjo4Edo4H5SBSjP50UcKFyH33tWDFrnTyevSqbqyDcgvPr4f8"
    "bOjcHcgod7lT0mnF1vgwMP2fL3//hAv3UT9mM2bjSlgYouq8VhH/gHcuzAw5hljl8XKwutGhE8ckDhG0"
    "atH3zmaGVTfEDcsY9WDiwzvzVf8mMOesKd2nm4EXE7AKPAV/YHZb3w4Cup99bkFHW+EmkDcJunarKfX/"
    "010yY9c+YIbWc2X3xk0L3/p+JdzHGPuhj1TTAx8HkMsIJTONYJcqduTtuh5I/d+QJCx4LwdJTq9D5QRd"
    "Cd0jAYZNVsw5zgNvH+PX1W5v4s9FKDSdojaESH0ZJJKSNXXOVkIfxyjlA3v52rMOfaLCySx4SdVJHahM"
    "MLH/zO4ED9CwOBAxj+KWcU2ryxoj2D7mLY5675+SbgIrGJRrx0xqajMqD0w="
  };

  std::array <CString, 49> const g_feature
  {
    _T ("ObsoleteFeature"),
    _T ("DefaultFeature"),
    _T ("PDCApp"),
    _T ("RunTimeEnv"),
    _T ("RADSAC"),
    _T ("MethodEditor"),
    _T ("EnVisionBridge"),
    _T ("EvowareBridge"),
    _T ("ExplorerBridge"),
    _T ("MinitrakBridge"),
    _T ("SchedulerBridge"),
    _T ("ViluBridge"),
    _T ("VWorksBridge"),
    _T ("RTELib"),
    _T ("TopologyEditor"),
    _T ("Galil"),
    _T ("Beckhoff"),
    _T ("PDCLib"),
    _T ("DeviceEditor"),
    _T ("HC12Emu"),
    _T ("MagellanBridge"),
    _T ("BiomekBridge"),
    _T ("BenchWorksBridge"),
    _T ("UberBridge"),
    _T ("Simulator"),
    _T ("GNFCommon"),
    _T ("EchoBridge"),
    _T ("GNFDeveloper"),
    _T ("CyanBridge"),
    _T ("G2Incubator"),
    _T ("PHERAstarBridge"),
    _T ("SummitAgent"),
    _T ("RemoteControl"),
    _T ("SSStation"),
    _T ("ADTool"),
    _T ("PlateDBViewer"),
    _T ("SingleTipDispenser"),
    _T ("LumiReader"),
    _T ("Celdyne"),
    _T ("CeligoBridge"),
    _T ("FFStation"),
    _T ("GNFService"),
    _T ("Stacker"),
    _T ("G3Incubator"),
    _T ("CentralStation"),
    _T ("SimulationMode"),
	  _T ("Delidder"),
	  _T ("AspirateSingleTip"),
    _T ("InvalidFeature")
  };

  std::array <CString, 64> const g_errorMessage
  {
    _T ("request successfully completed"),
    _T ("request exceeds memory range of a HASP file"),
    _T ("legacy HASP HL runtime API: unknown or invalid feature id option"),
    _T ("system is out of memory"),
    _T ("too many open features or login sessions"),
    _T ("access to feature, HASP protection key or functionality denied"),
    _T ("legacy decryption function cannot work on feature"),
    _T ("HASP SRM protection key not available"),
    _T ("encrypted or decrypted data length too short to execute function call"),
    _T ("invalid login handle passed to function"),
    _T ("specified file id not recognized by API"),
    _T ("installed driver or daemon too old to execute function"),
    _T ("realtime clock not available"),
    _T ("generic error from host system call"),
    _T ("required driver not installed"),
    _T ("unrecognized file format for update"),
    _T ("unable to execute function in this context"),
    _T ("binary data passed to function does not contain valid update"),
    _T ("HASP protection key not found"),
    _T ("required XML tags not found: contents in binary data are missing or invalid"),
    _T ("update request not supported by HASP SRM protection key"),
    _T ("update counter set incorrectly"),
    _T ("invalid vendor code passed"),
    _T ("HASP SRM protection key does not support encryption type"),
    _T ("passed time value outside supported value range"),
    _T ("realtime clock battery out of power"),
    _T ("acknowledge data requested by update but acknowledge data parameter is null"),
    _T ("program running on a terminal server"),
    _T ("requested feature type not implemented"),
    _T ("unknown algorithm used in H2R or V2C file"),
    _T ("signature verification operation failed"),
    _T ("requested feature not available"),
    _T ("access log not enabled"),
    _T ("communication error between API and local HASP license manager"),
    _T ("vendor code not recognized by API"),
    _T ("invalid XML specification"),
    _T ("invalid XML scope"),
    _T ("too many HASP SRM protection keys currently connected"),
    _T ("too many concurrent user sessions currently connected"),
    _T ("session been interrupted"),
    _T ("communication error between local and remote HASP license managers"),
    _T ("feature expired"),
    _T ("HASP license manager version too old"),
    _T ("I/O error occured in secure storage area of HASP SL key or a USB error occured when communicating with a HASP HL key"),
    _T ("update installation not permitted: this update was already applied"),
    _T ("system time has been tampered with"),
    _T ("communication error occurred in secure channel"),
    _T ("corrupt data exists in secure storage area of HASP SL protection key"),
    _T ("unable to find vendor library"),
    _T ("unable to load vendor library"),
    _T ("unable to locate any feature matching scope"),
    _T ("program running on a virtual machine"),
    _T ("HASP SL key incompatible with machine hardware"),
    _T ("login denied because of user restrictions"),
    _T ("the update counter value in the V2C file is lower than the value in HASP SRM protection key"),
    _T ("the update counter value in the V2C file is greater than the value in the HASP SRM protection key"),
    _T ("vendor library version too old"),
    _T ("upload via ACC failed possibly because of illegal format"),
    _T ("invalid XML recipient parameter"),
    _T ("invalid XML action parameter"),
    _T ("scope does not specify a unique product"),
    _T ("invalid product information"),
    _T ("update can only be applied to the specified recipient and not to this computer"),
    _T ("invalid duration")
  };
}

namespace PDCLib
{
CHaspAdapter::CHaspAdapter (void)
  : m_hasp (std::make_unique <Chasp> (ChaspFeature (HASP_PROGNUM_DEFAULT_FID | HASP_PROGNUM_OPT_TS)))
{
  haspStatus const l_status (m_hasp->login (g_vendorCode.c_str ()));

  if (!HASP_SUCCEEDED (l_status))
    {
      if (static_cast <size_t> (l_status) < g_errorMessage.size ())
        {
          ThrowStringException (_T ("HASP login failed - %s"), (LPCTSTR) g_errorMessage[l_status]);
        }
      else
        {
          ThrowStringException (_T ("HASP login failed - status code: %ld"), l_status);
        }
    }
}

CHaspAdapter::~CHaspAdapter ()
{
  m_hasp->logout ();
}

void
CHaspAdapter::GetAllFeatures (CHaspFeatureSet & features)
{
  GetFeatures (features, false);
}

void
CHaspAdapter::GetUsableFeatures (CHaspFeatureSet & features)
{
  GetFeatures (features, true);
}

bool
CHaspAdapter::HasFeature (EHaspFeature feature)
{
  CHaspFeatureSet l_features;

  CHaspAdapter ().GetUsableFeatures (l_features);

  return l_features.find (feature) != l_features.end ();
}

void
CHaspAdapter::GetValidFeatures (CHaspFeatureSet & features)
{
  for (int l_feature (static_cast <int> (EHaspFeature::haspDefaultFeature));
       l_feature < static_cast <int> (EHaspFeature::haspInvalidFeature);
       ++l_feature)
    {
      features.insert (static_cast <EHaspFeature> (l_feature));
    }
}

CString
CHaspAdapter::GetFeature (EHaspFeature feature)
{
  if ((feature > EHaspFeature::haspObsoleteFeature) && (feature < EHaspFeature::haspInvalidFeature))
    {
      return g_feature[static_cast <int> (feature) + 1];
    }

  return _T ("UnrecognizedFeature");
}

void
CHaspAdapter::GetFeatures (CHaspFeatureSet & features, bool usableOnly)
{
  std::string const l_scope ("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
                             "<haspscope/>");

  std::string const l_format ("<haspformat root=\"hasp_info\">"
                              "    <feature>"
                              "       <attribute name=\"id\" />"
                              "       <attribute name=\"usable\" />"
                              "    </feature>"
                              "</haspformat>");

  std::string l_info;

  haspStatus const l_status (Chasp::getInfo (l_scope.c_str (), l_format.c_str (), g_vendorCode.c_str (), l_info));

  if (HASP_SUCCEEDED (l_status))
    {
      try
        {
          IXMLDOMDocumentPtr document;

          TESTHR (document.CreateInstance (CLSID_DOMDocument60));

          VARIANT_BOOL isSuccessful (FALSE);

          TESTHR (document->loadXML (_bstr_t (l_info.c_str ()), &isSuccessful));

          if (isSuccessful)
            {
              IXMLDOMElementPtr element;

              TESTHR (document->get_documentElement (&element));

              GetFeatureIDs (element, features, usableOnly);
            }
          else
            {
              ThrowStringException (_T ("IXMLDOMDocument::loadXML () failed"));
            }
        }
      catch (_com_error const & e)
        {
          ThrowStringException ((LPCTSTR) GetErrorMessage (e));
        }
    }
  else if (static_cast <size_t> (l_status) < g_errorMessage.size ())
    {
      ThrowStringException (_T ("HASP getInfo failed - %s"), (LPCTSTR) g_errorMessage[l_status]);
    }
  else
    {
      ThrowStringException (_T ("HASP getInfo failed - status code: %ld"), l_status);
    }
}

void
CHaspAdapter::GetFeatureIDs (IXMLDOMElementPtr & element, CHaspFeatureSet & features, bool usableOnly)
{
  features.clear ();

  IXMLDOMNodeListPtr featureIDList;

  TESTHR (element->getElementsByTagName (_bstr_t ("feature"), &featureIDList));

  long numFeatureIDs (0);

  TESTHR (featureIDList->get_length (&numFeatureIDs));

  for (int l_i (0); l_i < numFeatureIDs; ++l_i)
    {
      IXMLDOMNodePtr featureID;

      TESTHR (featureIDList->get_item (l_i, &featureID));

      auto const l_feature (static_cast <EHaspFeature> (Convert <Int32Converter> (GetNodeValue (featureID, _T ("id")))));

      if ((l_feature < EHaspFeature::haspDefaultFeature) || (l_feature >= EHaspFeature::haspInvalidFeature))
        {
          ThrowStringException (_T ("invalid feature ID value"));
        }
      else if ((usableOnly && (GetNodeValue (featureID, _T ("usable")) == _T ("true"))) || !usableOnly)
        {
          features.insert (l_feature);
        }
    }
}

CString
CHaspAdapter::GetNodeValue (IXMLDOMNodePtr & item, CString const & name)
{
  CString retval;

  IXMLDOMNamedNodeMapPtr attributes;

  TESTHR (item->get_attributes (&attributes));

  IXMLDOMNodePtr namedItem;

  TESTHR (attributes->getNamedItem (_bstr_t (name), &namedItem));

  if (namedItem != nullptr)
    {
      VARIANT nodeValue;

      TESTHR (namedItem->get_nodeValue (&nodeValue));

      retval = CString (static_cast <LPCTSTR> (_bstr_t (nodeValue)));
    }

  return retval;
}

CArchive &
operator>> (CArchive & ar, CHaspAdapter::EHaspFeature & rhs)
{
  ar.Read (&rhs, sizeof (rhs));

  return ar;
}

CArchive &
operator<< (CArchive & ar, CHaspAdapter::EHaspFeature const & rhs)
{
  ar.Write (&rhs, sizeof (rhs));

  return ar;
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  07/16/2008  MEG     initial revision
//  12/09/2009  MCC     modified to reference MSXML6 explicitly
//  06/24/2011  MCC     added HASP error message table
//  07/29/2011  MCC     disabled HASP API check for terminal server presence
//  07/17/2014  MCC     implemented support for executing shell commands
//  09/11/2014  MCC     implemented support for developer and service features
//  01/09/2015  MCC     templatized number of elements macro
//  01/22/2015  MCC     added stacker feature
//  07/02/2015  MCC     added G3 incubator feature
//  10/30/2015  MCC     added central station feature
//  03/03/2016  MCC     added simulation mode feature
//  04/10/2017  MEG     added cast to CString for resolving conversion warning
//  06/05/2017  TAN     added delidder feature
//  06/25/2018  MCC     changed aspirate duration to floating point
//  08/14/2018  MCC     updated numeric conversion templates
//
// ============================================================================
