// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: ControllerType.cpp
//
//     Description: EControllerType enumeration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: controllertype.cpp %
//        %version: 4 %
//          %state: %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "StdAfx.h"
#include "ControllerType.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace PDCLib
{
std::array <CString, 4> const g_controllerType
  {
    _T ("Galil"),
    _T ("Parker"),
    _T ("Beckhoff"),
    _T ("Xilinx")
  };

CString
GetControllerType (EControllerType controllerType)
{
  CString l_controllerType (_T ("UNKNOWN"));

  if ((controllerType >= 0) && (static_cast <size_t> (controllerType) < g_controllerType.size ()))
    {
      l_controllerType = g_controllerType[controllerType];
    }

  return l_controllerType;
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  11/28/2006  MCC     initial revision
//  01/12/2007  MCC     implemented support for Beckhoff controller type
//  05/07/2013  MCC     implemented support for Xilinx controller type
//  01/09/2015  MCC     templatized number of elements macro
//
// ============================================================================
