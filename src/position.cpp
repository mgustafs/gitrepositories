// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: Position.cpp
//
//     Description: Position definitions
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: position.cpp %
//        %version: 46 %
//        %version: 46 %
//          %state: %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "StdAfx.h"
#include "Position.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace PDCLib
{
  std::array <CString, 344> const g_position
    {
      _T ("None"),
      _T ("Idle"),
      _T ("TipA1536"),
      _T ("TipA384"),
      _T ("TipB1536"),
      _T ("TipB384"),
      _T ("TipAClean"),
      _T ("TipBClean"),
      _T ("TipAPrime"),
      _T ("TipBPrime"),
      _T ("Maintenance"),
      _T ("SafeHeight"),
      _T ("TipAReference"),
      _T ("TipBReference"),
      _T ("TipCReference"),
      _T ("TipCClean"),
      _T ("TipCPrime"),
      _T ("TipABeadRecovery1"),
      _T ("TipABeadRecovery2"),
      _T ("TipATipCleaning"),
      _T ("TipASonicate"),
      _T ("TipBSonicate"),
      _T ("TipCSonicate"),
      _T ("TipAPrimeAux"),
      _T ("TipBPrimeAux"),
      _T ("TipCPrimeAux"),
      _T ("LDIdle"),
      _T ("LDMaintenance"),
      _T ("LDAspirateSample"),
      _T ("LDAspirateTipClean"),
      _T ("LDAspirateTipDrying"),
      _T ("LDTiltAspirateSample"),
      _T ("CCMaintenance"),
      _T ("CCTransferSample"),
      _T ("CCTipAPrime"),
      _T ("CCTipAClean"),
      _T ("CCAspirateSample"),
      _T ("CCAspirateTipClean"),
      _T ("CCAspirateTipDrying"),
      _T ("Dryer"),
      _T ("ReferenceQuadrant1"),
      _T ("ReferenceQuadrant2"),
      _T ("ReferenceQuadrant3"),
      _T ("ReferenceQuadrant4"),
      _T ("SafeHeight"),
      _T ("Wash1"),
      _T ("Wash2"),
      _T ("Wash3"),
      _T ("PinToolSrcRef"),
      _T ("PinToolDstRef"),
      _T ("PinToolNstRef"),
      _T ("PinToolStaticNstRef"),
      _T ("FlaskLocator1Idle"),
      _T ("FlaskLocator2Idle"),
      _T ("FlaskLocator1RetractedIdle"),
      _T ("FlaskLocator2RetractedIdle"),
      _T ("CleanStationIdle"),
      _T ("SystemMaintenance"),
      _T ("TipHead1Idle"),
      _T ("TipHead1Prime"),
      _T ("TipHead1CleanStation"),
      _T ("TipHead1TipDrying"),
      _T ("TipHead1FlaskLocator1"),
      _T ("TipHead1FlaskLocator2"),
      _T ("TipHead2Idle"),
      _T ("TipHead2Prime"),
      _T ("TipHead2CleanStation"),
      _T ("TipHead2TipDrying"),
      _T ("TipHead2Waste"),
      _T ("TipHead2FlaskLocator1"),
      _T ("TipHead2FlaskLocator2"),
      _T ("TipHead3Idle"),
      _T ("TipHead3Prime"),
      _T ("TipHead3CleanStation"),
      _T ("TipHead3TipDrying"),
      _T ("TipHead3FlaskLocator1"),
      _T ("TipHead3FlaskLocator2"),
      _T ("SafeHeight"),
      _T ("CleaningStation"),
      _T ("Rack12SampleA1"),
      _T ("Rack12SampleC4"),
      _T ("Rack96SampleA1"),
      _T ("Rack96SampleH12"),
      _T ("Rack384SampleA1"),
      _T ("Rack384SampleP24"),
      _T ("Rack1536SampleA1"),
      _T ("Rack1536SampleAF48"),
      _T ("ACTCSystemMaintenance"),
      _T ("TransferTipFlaskPos"),
      _T ("ReagentTipFlaskPos"),
      _T ("ReagentRemovalTipFlaskPos"),
      _T ("CleanStation"),
      _T ("CleanStation2"),
      _T ("Prime"),
      _T ("TransferTipTiltFlaskPos"),
      _T ("CleanSeptumPos"),
      _T ("ReagentRemovalTiltFlaskPos"),
      _T ("CellCounterDispenserIdle"),
      _T ("ReagentRemovalTipSampleWellPos"),
      _T ("ReagentTipSampleWellPos"),
      _T ("CellCounterCleanStation"),
      _T ("CellCounterPrime"),
      _T ("FCSystemMaintenance"),
      _T ("FCWashPos"),
      _T ("FCPrimePos"),
      _T ("FCInFlaskAspiratePos"),
      _T ("FCPostTiltAspiratePos"),
      _T ("FCInFlaskDispensePos"),
      _T ("Valve1Port1"),
      _T ("Valve2Port1"),
      _T ("FCIdle"),
      _T ("FCMaintenance"),
      _T ("FCWash"),
      _T ("FCPrime"),
      _T ("FCCalibrate"),
      _T ("FCDispenseHigh"),
      _T ("FCDispenseMedium"),
      _T ("FCDispenseLow"),
      _T ("PPSLDIdle"),
      _T ("PPSLDMaintenance"),
      _T ("PPSLDWash"),
      _T ("PPSLDPrime"),
      _T ("PPSLDCalibrate"),
      _T ("PPSLDDispense"),
      _T ("PPSLDAspirate"),
      _T ("PPSLDAspirateTilt"),
      _T ("PPSHSCIdle"),
      _T ("PPSHSCMaintenance"),
      _T ("PPSHSCCalibrate"),
      _T ("PPSHSCAspirate"),
      _T ("PPSHSCTriturate"),
      _T ("PPSHSCSonicate"),
      _T ("PPSHSCLDDispense"),
      _T ("PPSHSCBufferDispense"),
      _T ("PPSHSCLDPrime"),
      _T ("PPSHSCBufferPrime"),
      _T ("PPSHSCAspirateWash"),
      _T ("PPSHSCSonicateWash"),
      _T ("PPSHSCRotorIdle"),
      _T ("ACSSystemMaintenance"),
      _T ("ACSWashPos"),
      _T ("ACSPrimePos"),
      _T ("ACSInFlaskAspiratePos"),
      _T ("ACSPostTiltAspiratePos"),
      _T ("ACSInFlaskDispensePos"),
      _T ("ACSValve1Port1"),
      _T ("ACSValve2Port1"),
      _T ("ACSValve3Port1"),
      _T ("ACSTriturateDispensePos"),
      _T ("ACSTipDrying"),
      _T ("CCSIdle"),
      _T ("CCSSystemMaintenance"),
      _T ("CCSAspirateSample"),
      _T ("CCSDispenseSample"),
      _T ("CCSPrime"),
      _T ("CCSCalibrate"),
      _T ("CCSTipDrying"),
      _T ("CCSQuickCleanWells"),
      _T ("TCSSystemMaintenance"),
      _T ("TCSHead1WashPos"),
      _T ("TCSHead1PrimePos"),
      _T ("TCSHead1DropRemovePos"),
      _T ("TCSConicalPos"),
      _T ("TCSInSrcFlaskHead1DispensePos"),
      _T ("TCSInSrcFlaskHead1AspiratePos"),
      _T ("TCSPostTiltSrcHead1FlaskAspPos"),
      _T ("TCSTipHead2Idle"),
      _T ("TCSHead2WashPos"),
      _T ("TCSHead2PrimePos"),
      _T ("TCSHead2DropRemovePos"),
      _T ("TCSInDestFlaskDispensePos"),
      _T ("TCSInDestFlaskAspiratePos"),
      _T ("TCSPostTiltDestFlaskAspPos"),
      _T ("TCSInSrcFlaskHead2DispensePos"),
      _T ("TCSInSrcFlaskHead2AspiratePos"),
      _T ("TCSPostTiltSrcFlaskHead2AspPos"),
      _T ("TCSValve1Port1"),
      _T ("TCSValve2Port1"),
      _T ("TCSValve3Port1"),
      _T ("TCSInSrcTriturateDispensePos"),
      _T ("TCSInDestTriturateDispensePos"),
      _T ("F2PLDMaintenance"),
      _T ("F2PLDWash"),
      _T ("F2PLDCalibrate"),
      _T ("F2PLDAspirate"),
      _T ("F2PLDAspirateTilt"),
      _T ("F2PLDValvePort1"),
      _T ("F2PLDTriturateDispense"),
      _T ("CFlaskHome"),
      _T ("CPlateHome"),
      _T ("PipettorDryer"),
      _T ("SafeHeight"),
      _T ("PipettorGreinerSrc1_1536"),
      _T ("PipettorUserDefinedSrc1_1536"),
      _T ("PipettorGreinerDst1_1536"),
      _T ("PipettorUserDefinedDst1_1536"),
      _T ("PipettorGreinerDst2_1536"),
      _T ("PipettorUserDefinedDst2_1536"),
      _T ("PipettorWash1"),
      _T ("PipettorWash2"),
      _T ("PipettorNestSrc_1536"),
      _T ("PipettorWaste"),
      _T ("PipettorAssemble"),
      _T ("PipettorGuideLock"),
      _T ("PipettorBlot"),
      _T ("PipettorHome"),
      _T ("Libra6Dryer"),
      _T ("SafeHeight"),
      _T ("Libra6SrcReference"),
      _T ("Libra6DstReference"),
      _T ("Libra6Wash1"),
      _T ("Libra6Wash2"),
      _T ("Libra6NestSrc_1536"),
      _T ("Libra6NestSrc_384"),
      _T ("Libra6TipLoad"),
      _T ("Libra6TipApproach"),
      _T ("Libra6TipEject"),
      _T ("Libra6Home"),
      _T ("Libra6TipStrokeLimited"),
      _T ("Libra6ReferenceQuadrant1"),
      _T ("Libra6ReferenceQuadrant2"),
      _T ("Libra6ReferenceQuadrant3"),
      _T ("Libra6ReferenceQuadrant4"),
      _T ("Maintenance"),
      _T ("SafeHeight"),
      _T ("PipetteHeadReference"),
      _T ("ReferenceQuadrant1Head1"),
      _T ("ReferenceQuadrant2Head1"),
      _T ("ReferenceQuadrant3Head1"),
      _T ("ReferenceQuadrant4Head1"),
      _T ("ReferenceQuadrant1Head2"),
      _T ("ReferenceQuadrant2Head2"),
      _T ("ReferenceQuadrant3Head2"),
      _T ("ReferenceQuadrant4Head2"),
      _T ("GripperRobotAccess"),
      _T ("GripperCameraAccess"),
      _T ("FilterWheelRef"),
      _T ("SrcRefHead1"),
      _T ("DstRefHead1"),
      _T ("SrcRefHead2"),
      _T ("DstRefHead2"),
      _T ("BoatHead1"),
      _T ("TroughHead1"),
      _T ("BoatHead2"),
      _T ("TroughHead2"),
      _T ("Wash1Head1"),
      _T ("Wash2Head1"),
      _T ("Wash3Head1"),
      _T ("DryerHead1"),
      _T ("BlotHead1"),
      _T ("QuadrantSafeHead1"),
      _T ("Wash1Head2"),
      _T ("Wash2Head2"),
      _T ("Wash3Head2"),
      _T ("DryerHead2"),
      _T ("BlotHead2"),
      _T ("QuadrantSafeHead2"),
      _T ("TipApproach"),
      _T ("TipLoad"),
      _T ("SafeHeight"),
      _T ("Maintenance"),
      _T ("TipAPlateReference"),
      _T ("TipAClean"),
      _T ("TipAManualClean"),
      _T ("TipBPlateReference"),
      _T ("TipBVacuum"),
      _T ("TipBPrime"),
      _T ("TipBPrimeAux"),
      _T ("TipBSonicate"),
      _T ("TipCPlateReference"),
      _T ("TipCVacuum"),
      _T ("TipCPrime"),
      _T ("TipCPrimeAux"),
      _T ("TipCSonicate"),
      _T ("Syringe1Idle"),
      _T ("Syringe2Idle"),
      _T ("Syringe3Idle"),
      _T ("Syringe4Idle"),
      _T ("Syringe5Idle"),
      _T ("Syringe6Idle"),
      _T ("Syringe7Idle"),
      _T ("SuspensionPumpIdle"),
      _T ("ValveIdle"),
      _T ("SafeHeight"),
      _T ("Maintenance"),
      _T ("ReferenceTopLeft1536"),
      _T ("ReferenceTopRight1536"),
      _T ("ReferenceBottomRight1536"),
      _T ("PlateClearanceHeight"),
      _T ("Tube 1"),
      _T ("Tube 2"),
      _T ("Wash 1"),
      _T ("Wash 2"),
      _T ("Wash 3"),
      _T ("Wash 4"),
      _T ("ReferenceTopLeft384"),
      _T ("ReferenceTopRight384"),
      _T ("ReferenceBottomRight384"),
      _T ("ReferenceTopLeft96"),
      _T ("ReferenceTopRight96"),
      _T ("ReferenceBottomRight96"),
      _T ("Reference"),
      _T ("X2Safe"),
      _T ("Maintenance"),
      _T ("Reference"),
      _T ("InternalIdle"),
      _T ("Maintenance"),
      _T ("Safe"),
      _T ("PlateReference"),
      _T ("CameraIdle"),
      _T ("CameraMaintenance"),
      _T ("CameraReference"),
      _T ("DoorOpen"),
      _T ("DoorPartOpen"),
      _T ("DoorClosed"),
      _T ("X2Safe"),
      _T ("SafeHeight"),
      _T ("Maintenance"),
      _T ("ReferenceTopLeft1536"),
      _T ("ReferenceTopRight1536"),
      _T ("ReferenceBottomRight1536"),
      _T ("ReferenceTopLeft384"),
      _T ("ReferenceTopRight384"),
      _T ("ReferenceBottomRight384"),
      _T ("ReferenceTopLeft96"),
      _T ("ReferenceTopRight96"),
      _T ("ReferenceBottomRight96"),
      _T ("Reference"),
      _T ("Tube1"),
      _T ("Tube2"),
      _T ("Wash1"),
      _T ("Wash2"),
      _T ("Wash3"),
      _T ("Wash4"),
      _T ("SystemMaintenance"),
      _T ("CleanTipPos"),
      _T ("PrimePos"),
      _T ("InFlaskAspiratePos"),
      _T ("PostTiltAspiratePos"),
      _T ("InFlaskDispensePos"),
      _T ("Nest"),
      _T ("Tray"),
      _T ("Maintenance"),
      _T ("Last")
    };

CString
GetPosition (EPosition position)
{
  return (position < EPosition::PositionLast) ? g_position[static_cast <int> (position)] : CString ("INVALID POSITION");
}

EPosition
GetFirstPosition (void)
{
  return EPosition::PositionNone;
}

void
GetNextPosition (EPosition & position)
{
  if (position < EPosition::PositionLast)
    {
      position = static_cast <EPosition> (static_cast <int> (position) + 1);
    }
}

EPosition
GetLastPosition (void)
{
  return EPosition::PositionLast;
}

CArchive &
operator>> (CArchive & ar, EPosition & rhs)
{
  ar.Read (&rhs, sizeof (rhs));

  return ar;
}

CArchive &
operator<< (CArchive & ar, EPosition & rhs)
{
  ar.Write (&rhs, sizeof (rhs));

  return ar;
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  10/12/2007  MCC     initial revision
//  01/07/2008  TAN     added Centrifuge locations
//  02/05/2008  TAN     removed sensor location on centrifuge
//  05/06/2008  MCC     implemented aspirate/dispense velocity interfaces
//  06/26/2008  MCC     implemented transfer sample quick clean feature
//  07/01/2008  MCC     added AC-Station tip drying location
//  10/06/2008  MCC     implemented support for sonicator locations
//  01/06/2009  MEG     added new prime positions for dispenser
//  03/04/2009  MEG     added Pipettor locations
//  03/27/2009  MEG     added Pipettor Home location
//  06/30/2009  MCC     added Libra6 locations
//  07/23/2009  MEG     removed unused Libra6 locations
//  08/18/2009  MEG     added Libra6 stroke limited location
//  10/07/2009  MEG     added Libra6 384 locations
//  10/15/2009  MEG     added Libra6 reference quadrant locations
//  03/23/2010  MEG     added initial  locations
//  03/25/2010  MCC     implemented support for user defined plate types
//  04/02/2010  TAN     added support for Dispenser Beckhoff device type
//  06/07/2010  TAN     added Dispenser Beckhoff positions
//  06/09/2010  MCC     baselined  filter wheel state machine class
//  06/14/2010  TAN     merged parallel version
//  06/25/2010  MEG     updated with current  positions
//  06/28/2010  MEG     added new  positions for wash and birdbath
//  10/13/2010  MEG     added reference quadrant positions for both heads
//  01/11/2011  MEG     added new taught positions for tip handling
//  02/08/2011  MCC     removed Raven prefix from taught locations
//  06/06/2011  MEG     modified libra6 positions for user defined plate types
//  07/20/2011  MCC     added support for ISMAC device type
//  07/21/2011  MCC     baselined ISMAC I/O class
//  07/22/2011  MCC     implemented ISMAC device configuration dialog
//  07/27/2011  MEG     added taught positions for quadrant safe moves
//  08/25/2011  TAN     added support for FACSCyan device type
//  11/09/2011  MEG     added plate reference positions for plate types
//  05/21/2012  MCC     implemented support for pin-tool plate definition
//  06/20/2012  TAN     added FACSCyanX2Safe position for G2 FACSCyan
//  07/03/2012  MCC     added support for variable specific plate definitions
//  07/23/2013  MCC     baselined single tip dispenser device config dialog
//  08/27/2013  MEG     added basic lumi reader positions
//  04/16/2014  TAN     refactored single tip dispenser device config dialog
//  05/07/2014  MCC     added support for Celdyne device type
//  05/08/2014  MCC     implemented Celdyne gripper and shaker options
//  05/21/2014  MCC     implemented Celdyne plate sample
//  07/29/2014  MCC     added support for FFStation device type
//  07/30/2014  MCC     removed unused FFStation locations
//  08/04/2014  MCC     updated FFStation wash position
//  11/05/2015  MCC     added Central Station placeholder position
//  01/28/2016  TAN     added Central Station locations
//  01/18/2018  MCC     implemented further support for interoperability
//
// ============================================================================
