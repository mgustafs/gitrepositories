// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: DispenserGalil.cpp
//
//     Description: DispenserGalil class definition
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: dispensergalil.cpp %
//        %version: 32.1.2 %
//          %state: %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "StdAfx.h"
#include "DispenserGalil.h"
#include "DeviceVisitor.h"
#include "HaspAdapter.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace
{
  const CString DEFAULT_CALIBRATION_FILE_NAME      (_T ("\\Calibration\\DispenserGalil\\caldev0.csv"));
  const CString DEFAULT_TEMPLATE_FILE_NAME         (_T ("\\Calibration\\DispenserGalil\\template0.pdt"));
  const CString DEFAULT_LOCATIONS_FILE_NAME        (_T ("\\Calibration\\DispenserGalil\\locations0.loc"));
  const CString DEFAULT_PLATE_DEFINTIONS_FILE_NAME (_T ("\\Calibration\\DispenserGalil\\plateDefinitions.plt"));
  const bool    DEFAULT_HAS_LOADING_DOCK           (false);
  const bool    DEFAULT_HAS_AUTOMATION_INTERFACE   (false);
  const bool    DEFAULT_HAS_PURGE_VALVE            (false);
  const int     DEFAULT_PORT_NUMBER                (1);
  const int     DEFAULT_NUM_TIP_HEADS              (1);
  const WORD    DEFAULT_ANALOG_PORT_NUMBER         (302);
  const WORD    DEFAULT_DISCRETE_PORT_NUMBER       (301);
  const int     DEFAULT_CONTROLLER_ID              (1);
}

namespace PDCLib
{
IMPLEMENT_SERIAL (CDispenserGalil, CDevice, VERSIONABLE_SCHEMA | TOPOLOGY_SCHEMA_VERSION_26)

CDispenserGalil::CDispenserGalil (void)
  : CDevice (DEFAULT_CALIBRATION_FILE_NAME, DEFAULT_TEMPLATE_FILE_NAME)
  , m_locationsFileName (GetAppDataFileName (DEFAULT_LOCATIONS_FILE_NAME))
  , m_plateDefinitionsFileName (GetAppDataFileName (DEFAULT_PLATE_DEFINTIONS_FILE_NAME))
  , m_hasLoadingDock (DEFAULT_HAS_LOADING_DOCK)
  , m_hasAutomationInterface (DEFAULT_HAS_AUTOMATION_INTERFACE)
  , m_hasPurgeValve (DEFAULT_HAS_PURGE_VALVE)
  , m_portNumber (DEFAULT_PORT_NUMBER)
  , m_numTipHeads (DEFAULT_NUM_TIP_HEADS)
  , m_analogPortNumber (DEFAULT_ANALOG_PORT_NUMBER)
  , m_discretePortNumber (DEFAULT_DISCRETE_PORT_NUMBER)
{
  m_controllerID.SetSize (2);

  m_controllerID[0] = DEFAULT_CONTROLLER_ID;
  m_controllerID[1] = m_controllerID[0] + 1;
}

CDispenserGalil::~CDispenserGalil ()
{
}

bool
CDispenserGalil::IsFeaturePresent (void) const
{
  // ============================================================================
  //  CRITICAL SECURITY NOTE:
  //
  //  All new devices must have a unique HASP feature ID associated with them.
  //  The default feature ID is to maintain backward compatibility with legacy
  //  devices. The obsolete feature ID is to deprecate devices which no longer
  //  exist. Moving forward we must have device-specific HASP keys in order to
  //  have a finer level of control over how the software is used.
  //
  // ============================================================================

  return CHaspAdapter::HasFeature (CHaspAdapter::EHaspFeature::haspObsoleteFeature);
}

void
CDispenserGalil::Accept (CDeviceVisitor & deviceVisitor)
{
  deviceVisitor.Visit (this);
}

void
CDispenserGalil::Serialize (CArchive & ar)
{
  CDevice::Serialize (ar);

  if (ar.IsLoading ())
    {
      switch (GetObjectSchema ())
        {
          case TOPOLOGY_SCHEMA_VERSION_0:
          case TOPOLOGY_SCHEMA_VERSION_1:
            {
              ar >> m_locationsFileName;
              ar >> m_hasLoadingDock;
              ar >> m_hasAutomationInterface;
              ar >> m_portNumber;
              ar >> m_numTipHeads;
              ar >> m_analogPortNumber;
              ar >> m_discretePortNumber;

              m_controllerID.Serialize (ar);

              break;
            }
          case TOPOLOGY_SCHEMA_VERSION_2:
          case TOPOLOGY_SCHEMA_VERSION_3:
          case TOPOLOGY_SCHEMA_VERSION_4:
          case TOPOLOGY_SCHEMA_VERSION_5:
          case TOPOLOGY_SCHEMA_VERSION_6:
          case TOPOLOGY_SCHEMA_VERSION_7:
            {
              ar >> m_locationsFileName;
              ar >> m_hasLoadingDock;
              ar >> m_hasAutomationInterface;
              ar >> m_hasPurgeValve;
              ar >> m_portNumber;
              ar >> m_numTipHeads;
              ar >> m_analogPortNumber;
              ar >> m_discretePortNumber;

              m_controllerID.Serialize (ar);

              break;
            }
          case TOPOLOGY_SCHEMA_VERSION_8:
            {
              ar >> m_locationsFileName;
              ar >> m_plateDefinitionsFileName;
              ar >> m_hasLoadingDock;
              ar >> m_hasAutomationInterface;
              ar >> m_hasPurgeValve;
              ar >> m_portNumber;
              ar >> m_numTipHeads;
              ar >> m_analogPortNumber;
              ar >> m_discretePortNumber;

              m_controllerID.Serialize (ar);

              break;
            }
          case TOPOLOGY_SCHEMA_VERSION_9:
          case TOPOLOGY_SCHEMA_VERSION_10:
          case TOPOLOGY_SCHEMA_VERSION_11:
            {
              bool l_inhibitReferenceTeaching;

              ar >> m_locationsFileName;
              ar >> m_plateDefinitionsFileName;
              ar >> m_hasLoadingDock;
              ar >> m_hasAutomationInterface;
              ar >> m_hasPurgeValve;
              ar >> l_inhibitReferenceTeaching;
              ar >> m_portNumber;
              ar >> m_numTipHeads;
              ar >> m_analogPortNumber;
              ar >> m_discretePortNumber;

              SetLimitedAccess (l_inhibitReferenceTeaching);

              m_controllerID.Serialize (ar);

              break;
            }
          case TOPOLOGY_SCHEMA_VERSION_12:
          case TOPOLOGY_SCHEMA_VERSION_13:
          case TOPOLOGY_SCHEMA_VERSION_14:
          case TOPOLOGY_SCHEMA_VERSION_15:
          case TOPOLOGY_SCHEMA_VERSION_16:
          case TOPOLOGY_SCHEMA_VERSION_17:
          case TOPOLOGY_SCHEMA_VERSION_18:
          case TOPOLOGY_SCHEMA_VERSION_19:
          case TOPOLOGY_SCHEMA_VERSION_20:
          case TOPOLOGY_SCHEMA_VERSION_21:
          case TOPOLOGY_SCHEMA_VERSION_22:
          case TOPOLOGY_SCHEMA_VERSION_23:
          case TOPOLOGY_SCHEMA_VERSION_24:
          case TOPOLOGY_SCHEMA_VERSION_25:
          case TOPOLOGY_SCHEMA_VERSION_26:
            {
              ar >> m_locationsFileName;
              ar >> m_plateDefinitionsFileName;
              ar >> m_hasLoadingDock;
              ar >> m_hasAutomationInterface;
              ar >> m_hasPurgeValve;
              ar >> m_portNumber;
              ar >> m_numTipHeads;
              ar >> m_analogPortNumber;
              ar >> m_discretePortNumber;

              m_controllerID.Serialize (ar);

              break;
            }
          default:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
        }
    }
  else
    {
      ar << m_locationsFileName;
      ar << m_plateDefinitionsFileName;
      ar << m_hasLoadingDock;
      ar << m_hasAutomationInterface;
      ar << m_hasPurgeValve;
      ar << m_portNumber;
      ar << m_numTipHeads;
      ar << m_analogPortNumber;
      ar << m_discretePortNumber;

      m_controllerID.Serialize (ar);
    }
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  11/28/2006  MCC     initial revision
//  11/30/2006  MCC     corrected analog/discrete port number data types
//  12/05/2006  MCC     configured location of calibration and location files
//  12/06/2006  MCC     decoupled method and topology version schema constants
//  01/11/2007  MEG     changed Galil controller constant from 0 to 1
//  09/06/2007  MCC     implemented purge valve feature
//  10/30/2007  MCC     implemented pintool barcode reader support
//  01/11/2008  TAN     added TOPOLOGY_SCHEMA_VERSION_4
//  04/04/2008  TAN     added TOPOLOGY_SCHEMA_VERSION_5
//  05/06/2009  MCC     added template file name to topology
//  06/23/2009  MEG     added TOPOLOGY_SCHEMA_VERSION_7
//  01/04/2010  MCC     implemented support for application data folder
//  03/29/2010  MCC     implemented support for user defined plate types
//  05/24/2010  MCC     implemented support for conditional reference teaching
//  09/13/2010  MCC     implemented infrastructure for head definition access
//  10/11/2010  MEG     implemented support for Raven barcode reader
//  11/05/2010  MCC     implemented support for limited application access
//  06/03/2011  MEG     implemented support for Libra6 plate definitions
//  07/06/2011  TAN     implemented support for G2 Dispenser barcode reader
//  09/22/2011  MCC     added Summit message handler to Device Editor
//  10/10/2011  MEG     added shaker simulation
//  01/11/2012  MCC     implemented support for device specific feature IDs
//  03/13/2012  TAN     implemented support for stacker and balance on G2 Dispenser
//  05/22/2012  MCC     implemented support for pin-tool plate definition
//  07/30/2012  MCC     implemented support for left/right handed dispensers
//  10/04/2012  MCC     implemented support for optional high speed centrifuge
//  03/27/2014  MEG     implemented optional barcode reader for lumi reader
//  11/24/2014  MEG     added second generation lumi reader
//  01/22/2015  MCC     added stacker feature
//  01/27/2015  TAN     removed hasStacker boolean on G2Dispenser
//  06/17/2015  MCC     marked device obsolete
//  11/04/2015  MCC     implemented support for simulation mode at device level
//  10/20/2017  MCC     implemented support for optional Celdyne presence
//  07/12/2018  MCC     implemented support for encrypted calibrations
//
// ============================================================================
