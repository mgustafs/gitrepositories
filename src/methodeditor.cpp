// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2006.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: MethodEditor.cpp
//
//     Description: Method Editor class definition
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: methodeditor.cpp %
//        %version: 12 %
//          %state: %
//         %cvtype: c++ %
//     %derived_by: mgustafs %
//  %date_modified: %
//
// ============================================================================

#include "StdAfx.h"
#include "MethodEditor.h"
#include "Method.h"
#include "VariableByteArray.h"
#include "VariableDouble.h"
#include "VariableEnum.h"
#include "VariableInt.h"
#include "VariableString.h"
#include "VariableVisitor.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace
{
  const UINT    FILE_OPEN_C_FLAGS (CFile::modeWrite      |
                                   CFile::shareExclusive |
                                   CFile::modeCreate);

  const UINT    FILE_OPEN_R_FLAGS (CFile::modeRead       |
                                   CFile::shareDenyWrite |
                                   CFile::modeNoInherit);

  const UINT    FILE_OPEN_W_FLAGS (CFile::modeWrite      |
                                   CFile::shareExclusive |
                                   CFile::modeNoInherit);

  const CString GLOBAL_SCOPE      (_T ("__GLOBAL__"));
}

namespace PDCLib
{
  class CSetVariable : public CVariableVisitor
  {
  public:
    explicit CSetVariable (CString const & stepAlias,
                           CString const & variableAlias,
                           CString const & value);
    virtual ~CSetVariable() = default;

    CSetVariable & operator = (const CSetVariable &) = delete;
    CSetVariable (const CSetVariable &) = delete;

  protected:
    virtual void Visit (CVariableByteArray * variable);
    virtual void Visit (CVariableDouble * variable);
    virtual void Visit (CVariableEnum * variable);
    virtual void Visit (CVariableInt * variable);
    virtual void Visit (CVariableString * variable);

  private:
    CString const m_stepAlias;
    CString const m_variableAlias;
    CString const m_value;
  };

  class CGetVariable : public CVariableVisitor
  {
  public:
    explicit CGetVariable (CString const & stepAlias,
                           CString const & variableAlias,
                           CString       & value);
    virtual ~CGetVariable() = default;

    CGetVariable & operator = (const CGetVariable &) = delete;
    CGetVariable (const CGetVariable &) = delete;

  protected:
    virtual void Visit (CVariableByteArray * variable);
    virtual void Visit (CVariableDouble * variable);
    virtual void Visit (CVariableEnum * variable);
    virtual void Visit (CVariableInt * variable);
    virtual void Visit (CVariableString * variable);

  private:
    CString const m_stepAlias;
    CString const m_variableAlias;
    CString & m_value;
  };

CMethodEditor::CMethodEditor (CString const & fileName)
  : m_fileName (fileName)
{
  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  try
    {
      CFile l_file (m_fileName, FILE_OPEN_R_FLAGS);

      CArchive l_archive (&l_file, CArchive::load);

      m_method.reset (CMethod::ReadObject (l_archive).release ());
    }
  catch (CException * e)
    {
      ThrowStringException (_T ("unable to load method: %s"), (LPCTSTR) GetErrorMessage (e));
    }
}

CMethodEditor::CMethodEditor (std::shared_ptr <CMethod> method)
  : m_method (method)
{
}

CMethodEditor::~CMethodEditor ()
{
}

void
CMethodEditor::Save (void)
{
  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  SaveAs (m_fileName, FILE_OPEN_W_FLAGS);
}

void
CMethodEditor::SaveAs (CString const & fileName)
{
  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  SaveAs (fileName, FILE_OPEN_C_FLAGS);
}

void
CMethodEditor::SkipStep (CString const & stepAlias, bool skip)
{
  if (CStep * const l_step = m_method->GetStep (stepAlias))
    {
      if (skip && !l_step->GetSkippable ())
        {
          ThrowStringException (_T ("method step cannot be skipped: %s"), (LPCTSTR) stepAlias);
        }
      else
        {
          l_step->SetSkip (skip);
        }
    }
  else
    {
      ThrowStringException (_T ("method step is undefined: %s"), (LPCTSTR) stepAlias);
    }
}

void
CMethodEditor::SetVariable (CString const & stepAlias,
                            CString const & variableAlias,
                            CString const & value)
{
  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  if (CVariable * const l_variable = GetVariable (stepAlias, variableAlias))
    {
      if (l_variable->GetIsConstant ())
        {
          ThrowStringException (_T ("method variable is constant: %s.%s"), (LPCTSTR) stepAlias, (LPCTSTR) variableAlias);
        }
      else
        {
          CSetVariable l_setVariable (stepAlias, variableAlias, value);

          l_variable->Accept (l_setVariable);
        }
    }
  else
    {
      ThrowStringException (_T ("method variable is undefined: %s.%s"), (LPCTSTR) stepAlias, (LPCTSTR) variableAlias);
    }
}

void
CMethodEditor::SetVariable (CString const & stepAlias,
                            CString const & variableAlias,
                            int             value)
{
  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  SetVariable <CVariableInt> (stepAlias, variableAlias, value);
}

void
CMethodEditor::SetVariable (CString const & stepAlias,
                            CString const & variableAlias,
                            double          value)
{
  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  SetVariable <CVariableDouble> (stepAlias, variableAlias, value);
}

void
CMethodEditor::GetVariable (CString const & stepAlias,
                            CString const & variableAlias,
                            CString       & value) const
{
  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  if (CVariable * const l_variable = GetVariable (stepAlias, variableAlias))
    {
      CGetVariable l_getVariable (stepAlias, variableAlias, value);

      l_variable->Accept (l_getVariable);
    }
  else
    {
      ThrowStringException (_T ("method variable is undefined: %s.%s"), (LPCTSTR) stepAlias, (LPCTSTR) variableAlias);
    }
}

void
CMethodEditor::GetVariable (CString const & stepAlias,
                            CString const & variableAlias,
                            int           & value) const
{
  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  GetVariable <CVariableInt> (stepAlias, variableAlias, value);
}

void
CMethodEditor::GetVariable (CString const & stepAlias,
                            CString const & variableAlias,
                            double        & value) const
{
  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  GetVariable <CVariableDouble> (stepAlias, variableAlias, value);
}

void
CMethodEditor::SaveAs (CString const & fileName, UINT nOpenFlags)
{
  try
    {
      CFile l_file (fileName, nOpenFlags);

      CArchive l_archive (&l_file, CArchive::store);

      l_archive.WriteObject (m_method.get ());
    }
  catch (CException * e)
    {
      ThrowStringException (_T ("unable to store method: %s"), (LPCTSTR) GetErrorMessage (e));
    }
}

template <typename T, typename U> void
CMethodEditor::SetVariable (CString const & stepAlias,
                            CString const & variableAlias,
                            U       const & value)
{
  CVariable * const l_variable_ (GetVariable (stepAlias, variableAlias));

  if (l_variable_ && l_variable_->GetIsConstant ())
    {
      ThrowStringException (_T ("method variable is constant: %s.%s"), (LPCTSTR) stepAlias, (LPCTSTR) variableAlias);
    }
  else if (T * const l_variable = dynamic_cast <T *> (l_variable_))
    {
      T const * const l_minValue (l_variable->GetMinValue ());
      T const * const l_maxValue (l_variable->GetMaxValue ());

      if ((l_minValue && (value < l_minValue->GetValue ())) ||
          (l_maxValue && (value > l_maxValue->GetValue ())))
        {
          ThrowStringException (_T ("method variable is out of range: %s.%s"), (LPCTSTR) stepAlias, (LPCTSTR) variableAlias);
        }
      else
        {
          l_variable->SetValue (value);
        }
    }
  else
    {
      ThrowStringException (_T ("method variable is undefined: %s.%s"), (LPCTSTR) stepAlias, (LPCTSTR) variableAlias);
    }
}

template <typename T, typename U> void
CMethodEditor::GetVariable (CString const & stepAlias,
                            CString const & variableAlias,
                            U             & value) const
{
  if (T const * const l_variable = dynamic_cast <T const *> (GetVariable (stepAlias, variableAlias)))
    {
      value = l_variable->GetValue ();
    }
  else
    {
      ThrowStringException (_T ("method variable is undefined: %s.%s"), (LPCTSTR) stepAlias, (LPCTSTR) variableAlias);
    }
}

CVariable *
CMethodEditor::GetVariable (CString const & stepAlias,
                            CString const & variableAlias) const
{
  return (stepAlias == GLOBAL_SCOPE) ?
         m_method->CVariableList::GetVariable (variableAlias) :
         m_method->GetVariable (stepAlias, variableAlias);
}

XMethodEditor::XMethodEditor (CString const & fileName)
  : m_fileName (fileName)
{
}

XMethodEditor::~XMethodEditor ()
{
}

bool
XMethodEditor::Open (void)
{
  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  bool retval (false);

  try
    {
      m_methodEditor.reset (new CMethodEditor (m_fileName));

      retval = true;
    }
  catch (CString const & errorMessage)
    {
      m_errorMessage = errorMessage;
    }

  return retval;
}

bool
XMethodEditor::Save (void)
{
  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  bool retval (false);

  try
    {
      m_methodEditor->Save ();

      retval = true;
    }
  catch (CString const & errorMessage)
    {
      m_errorMessage = errorMessage;
    }

  return retval;
}

bool
XMethodEditor::SaveAs (CString const & fileName)
{
  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  bool retval (false);

  try
    {
      m_methodEditor->SaveAs (fileName);

      retval = true;
    }
  catch (CString const & errorMessage)
    {
      m_errorMessage = errorMessage;
    }

  return retval;
}

bool
XMethodEditor::SkipStep (CString const & stepAlias, bool skip)
{
  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  bool retval (false);

  try
    {
      m_methodEditor->SkipStep (stepAlias, skip);

      retval = true;
    }
  catch (CString const & errorMessage)
    {
      m_errorMessage = errorMessage;
    }

  return retval;
}

bool
XMethodEditor::SetVariable (CString const & stepAlias,
                            CString const & variableAlias,
                            CString const & value)
{
  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  bool retval (false);

  try
    {
      m_methodEditor->SetVariable (stepAlias, variableAlias, value);

      retval = true;
    }
  catch (CString const & errorMessage)
    {
      m_errorMessage = errorMessage;
    }

  return retval;
}

bool
XMethodEditor::SetVariable (CString const & stepAlias,
                            CString const & variableAlias,
                            int             value)
{
  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  bool retval (false);

  try
    {
      m_methodEditor->SetVariable (stepAlias, variableAlias, value);

      retval = true;
    }
  catch (CString const & errorMessage)
    {
      m_errorMessage = errorMessage;
    }

  return retval;
}

bool
XMethodEditor::SetVariable (CString const & stepAlias,
                            CString const & variableAlias,
                            double          value)
{
  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  bool retval (false);

  try
    {
      m_methodEditor->SetVariable (stepAlias, variableAlias, value);

      retval = true;
    }
  catch (CString const & errorMessage)
    {
      m_errorMessage = errorMessage;
    }

  return retval;
}

bool
XMethodEditor::GetVariable (CString const & stepAlias,
                            CString const & variableAlias,
                            CString       & value) const
{
  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  bool retval (false);

  try
    {
      m_methodEditor->GetVariable (stepAlias, variableAlias, value);

      retval = true;
    }
  catch (CString const & errorMessage)
    {
      m_errorMessage = errorMessage;
    }

  return retval;
}

bool
XMethodEditor::GetVariable (CString const & stepAlias,
                            CString const & variableAlias,
                            int           & value) const
{
  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  bool retval (false);

  try
    {
      m_methodEditor->GetVariable (stepAlias, variableAlias, value);

      retval = true;
    }
  catch (CString const & errorMessage)
    {
      m_errorMessage = errorMessage;
    }

  return retval;
}

bool
XMethodEditor::GetVariable (CString const & stepAlias,
                            CString const & variableAlias,
                            double        & value) const
{
  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  bool retval (false);

  try
    {
      m_methodEditor->GetVariable (stepAlias, variableAlias, value);

      retval = true;
    }
  catch (CString const & errorMessage)
    {
      m_errorMessage = errorMessage;
    }

  return retval;
}

CString
XMethodEditor::GetErrorMessage (void) const
{
  return m_errorMessage;
}

CSetVariable::CSetVariable (CString const & stepAlias, CString const & variableAlias, CString const & value)
  : m_stepAlias (stepAlias)
  , m_variableAlias (variableAlias)
  , m_value (value)
{
}

void
CSetVariable::Visit (CVariableByteArray * variable)
{
  int const l_length (m_value.SpanIncluding (_T ("01")).GetLength ());
  CVariableByteArray::EMaskType const l_type (variable->GetType ());

  if ((((l_length == 12) || (l_length == 24) || (l_length == 48)) && (m_value.GetLength () == l_length)) &&
      ((l_type == CVariableByteArray::EMaskType::Column8x12) ||
      (l_type == CVariableByteArray::EMaskType::Column16x24) ||
      (l_type == CVariableByteArray::EMaskType::Column32x48)))
    {
      if (l_length == 12)
        {
          variable->SetType (CVariableByteArray::EMaskType::Column8x12);
        }
      else if (l_length == 24)
        {
          variable->SetType (CVariableByteArray::EMaskType::Column16x24);
        }
      else
        {
          variable->SetType (CVariableByteArray::EMaskType::Column32x48);
        }

      CByteArray l_value;

      int const l_size (CVariableByteArray::GetSize (variable->GetType ()));

      l_value.SetSize (l_size);

      for (int l_i (0); l_i < l_size; ++l_i)
        {
          l_value[l_i] = 0x00;
        }

      for (int l_i (0); l_i < l_length; ++l_i)
        {
          if (m_value[l_i] == _T ('1'))
            {
              l_value[l_i >> 3] |= static_cast <BYTE> (0x80 >> (l_i % 8));
            }
        }

      variable->SetValue (l_value);
    }
  else
    {
      ThrowStringException (_T ("method variable is out of range: %s.%s"), (LPCTSTR) m_stepAlias, (LPCTSTR) m_variableAlias);
    }
}

void
CSetVariable::Visit (CVariableDouble * variable)
{
  UNUSED_ALWAYS (variable);

  ThrowStringException (_T ("method variable is undefined: %s.%s"), (LPCTSTR) m_stepAlias, (LPCTSTR) m_variableAlias);
}

void
CSetVariable::Visit (CVariableEnum * variable)
{
  POSITION l_pos (variable->GetFirstAssoc ());
  VARIANT l_unused;

  while (l_pos)
    {
      CString l_value;

      variable->GetNextAssoc (l_pos, l_value, l_unused);

      if (l_value == m_value)
        {
          variable->SetValue (m_value);

          return;
        }
    }

  ThrowStringException (_T ("method variable is out of range: %s.%s"), (LPCTSTR) m_stepAlias, (LPCTSTR) m_variableAlias);
}

void
CSetVariable::Visit (CVariableInt * variable)
{
  UNUSED_ALWAYS (variable);

  ThrowStringException (_T ("method variable is undefined: %s.%s"), (LPCTSTR) m_stepAlias, (LPCTSTR) m_variableAlias);
}

void
CSetVariable::Visit (CVariableString * variable)
{
  if (m_value.IsEmpty () && variable->GetInhibitEmpty ())
    {
      ThrowStringException (_T ("method variable is out of range: %s.%s"), (LPCTSTR) m_stepAlias, (LPCTSTR) m_variableAlias);
    }
  else
    {
      variable->SetValue (m_value);
    }
}

CGetVariable::CGetVariable (CString const & stepAlias, CString const & variableAlias, CString & value)
  : m_stepAlias (stepAlias)
  , m_variableAlias (variableAlias)
  , m_value (value)
{
}

void CGetVariable::Visit (CVariableByteArray * variable)
{
  CByteArray l_value;

  variable->GetValue (l_value);

  int const l_size (l_value.GetSize () << 3);

  m_value.Empty ();

  for (int l_i (0); l_i < l_size; ++l_i)
    {
      if (l_value[l_i >> 3] & static_cast <BYTE> (0x80 >> (l_i % 8)))
        {
          m_value += _T ('1');
        }
      else
        {
          m_value += _T ('0');
        }
    }
}

void
CGetVariable::Visit (CVariableDouble * variable)
{
  UNUSED_ALWAYS (variable);

  ThrowStringException (_T ("method variable is undefined: %s.%s"), (LPCTSTR) m_stepAlias, (LPCTSTR) m_variableAlias);
}

void
CGetVariable::Visit (CVariableEnum * variable)
{
  m_value = variable->GetValue ();
}

void
CGetVariable::Visit (CVariableInt * variable)
{
  UNUSED_ALWAYS (variable);

  ThrowStringException (_T ("method variable is undefined: %s.%s"), (LPCTSTR) m_stepAlias, (LPCTSTR) m_variableAlias);
}

void
CGetVariable::Visit (CVariableString * variable)
{
  m_value = variable->GetValue ();
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  08/22/2008  MCC     initial revision
//  09/03/2008  MCC     added interface for accessing variables
//  09/03/2008  MCC     modified to support string variables
//  09/09/2008  MCC     corrected problem with save as implementation
//  09/10/2008  MCC     corrected problem with exception handling
//  10/09/2008  MCC     implemented interface for skipping steps
//  10/21/2008  MCC     implemented check for constant variable
//  10/22/2008  MCC     corrected problem with previous change
//  01/09/2015  MCC     templatized number of elements macro
//  05/14/2015  MCC     implemented support for Visual Studio 2015
//  07/02/2015  MCC     added method editor to remote control interface
//  04/10/2017  MEG     added cast to CString for resolving conversion warning
//
// ============================================================================
