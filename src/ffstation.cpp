// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: FFStation.cpp
//
//     Description: AC-Station class definition
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: ffstation.cpp %
//        %version: 7.1.2 %
//          %state: %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "StdAfx.h"
#include "FFStation.h"
#include "DeviceVisitor.h"
#include "HaspAdapter.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace
{
  const int     DEFAULT_ADS_CONTROLLER_ID     (0);
  const WORD    DEFAULT_ANALOG_PORT_NUMBER    (302);
  const WORD    DEFAULT_DISCRETE_PORT_NUMBER  (301);
  const CString DEFAULT_CALIBRATION_FILE_NAME (_T ("\\Calibration\\FFStation\\caldev0.csv"));
  const CString DEFAULT_TEMPLATE_FILE_NAME    (_T ("\\Calibration\\FFStation\\template0.pdt"));
  const CString DEFAULT_LOCATIONS_FILE_NAME   (_T ("\\Calibration\\FFStation\\locations0.loc"));
}

namespace PDCLib
{
IMPLEMENT_SERIAL (CFFStation, CDevice, VERSIONABLE_SCHEMA | TOPOLOGY_SCHEMA_VERSION_26)

CFFStation::CFFStation (void)
  : CDevice (DEFAULT_CALIBRATION_FILE_NAME, DEFAULT_TEMPLATE_FILE_NAME)
  , m_adsControllerID (DEFAULT_ADS_CONTROLLER_ID)
  , m_analogPortNumber (DEFAULT_ANALOG_PORT_NUMBER)
  , m_discretePortNumber (DEFAULT_DISCRETE_PORT_NUMBER)
  , m_locationsFileName (GetAppDataFileName (DEFAULT_LOCATIONS_FILE_NAME))
{
}

CFFStation::~CFFStation ()
{
}

bool
CFFStation::IsFeaturePresent (void) const
{
  // ============================================================================
  //  CRITICAL SECURITY NOTE:
  //
  //  All new devices must have a unique HASP feature ID associated with them.
  //  The default feature ID is to maintain backward compatibility with legacy
  //  devices. The obsolete feature ID is to deprecate devices which no longer
  //  exist. Moving forward we must have device-specific HASP keys in order to
  //  have a finer level of control over how the software is used.
  //
  // ============================================================================

  return CHaspAdapter::HasFeature (CHaspAdapter::EHaspFeature::haspFFStation) ||
         CHaspAdapter::HasFeature (CHaspAdapter::EHaspFeature::haspGNFDeveloper) ||
         CHaspAdapter::HasFeature (CHaspAdapter::EHaspFeature::haspGNFService);
}

void
CFFStation::Accept (CDeviceVisitor & deviceVisitor)
{
  deviceVisitor.Visit (this);
}

void
CFFStation::Serialize (CArchive & ar)
{
  CDevice::Serialize (ar);

  if (ar.IsLoading ())
    {
      switch (GetObjectSchema ())
        {
          case TOPOLOGY_SCHEMA_VERSION_0:
          case TOPOLOGY_SCHEMA_VERSION_1:
          case TOPOLOGY_SCHEMA_VERSION_2:
          case TOPOLOGY_SCHEMA_VERSION_3:
          case TOPOLOGY_SCHEMA_VERSION_4:
          case TOPOLOGY_SCHEMA_VERSION_5:
          case TOPOLOGY_SCHEMA_VERSION_6:
          case TOPOLOGY_SCHEMA_VERSION_7:
          case TOPOLOGY_SCHEMA_VERSION_8:
          case TOPOLOGY_SCHEMA_VERSION_9:
          case TOPOLOGY_SCHEMA_VERSION_10:
          case TOPOLOGY_SCHEMA_VERSION_11:
          case TOPOLOGY_SCHEMA_VERSION_12:
          case TOPOLOGY_SCHEMA_VERSION_13:
          case TOPOLOGY_SCHEMA_VERSION_14:
          case TOPOLOGY_SCHEMA_VERSION_15:
          case TOPOLOGY_SCHEMA_VERSION_16:
          case TOPOLOGY_SCHEMA_VERSION_17:
          case TOPOLOGY_SCHEMA_VERSION_18:
          case TOPOLOGY_SCHEMA_VERSION_19:
          case TOPOLOGY_SCHEMA_VERSION_20:
          case TOPOLOGY_SCHEMA_VERSION_21:
          case TOPOLOGY_SCHEMA_VERSION_22:
          case TOPOLOGY_SCHEMA_VERSION_23:
          case TOPOLOGY_SCHEMA_VERSION_24:
          case TOPOLOGY_SCHEMA_VERSION_25:
          case TOPOLOGY_SCHEMA_VERSION_26:
            {
              ar >> m_adsControllerID;
              ar >> m_analogPortNumber;
              ar >> m_discretePortNumber;
              ar >> m_locationsFileName;

              break;
            }
          default:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
        }
    }
  else
    {
      ar << m_adsControllerID;
      ar << m_analogPortNumber;
      ar << m_discretePortNumber;
      ar << m_locationsFileName;
    }
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  07/29/2014  MCC     initial revision
//  09/11/2014  MCC     implemented support for developer and service features
//  11/24/2014  MEG     added second generation lumi reader
//  01/22/2015  MCC     added stacker feature
//  01/27/2015  TAN     removed hasStacker boolean on G2Dispenser
//  03/04/2015  MCC     added service feature to device feature present
//  11/04/2015  MCC     implemented support for simulation mode at device level
//  10/20/2017  MCC     implemented support for optional Celdyne presence
//  07/12/2018  MCC     implemented support for encrypted calibrations
//
// ============================================================================
