// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: DeviceType.cpp
//
//     Description: EDeviceType enumeration
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: devicetype.cpp %
//        %version: 17 %
//          %state: %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "StdAfx.h"
#include "DeviceType.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace PDCLib
{
std::array <CString, 25> const g_deviceType
  {
    _T ("Dispenser"),
    _T ("PinTool"),
    _T ("TCDispenser"),
    _T ("CCDispenser"),
    _T ("Microfluidizer"),
    _T ("TCStation"),
    _T ("ACStation"),
    _T ("FCDispenser"),
    _T ("CellGenerator"),
    _T ("Centrifuge"),
    _T ("ProteinPurificationSystem"),
    _T ("CCStation"),
    _T ("LoadingDock"),
    _T ("Pipettor"),
    _T ("Libra6"),
    _T ("Raven"),
    _T ("ISMAC"),
    _T ("FACSCyan"),
    _T ("SSStation"),
    _T ("SingleTipDispenser"),
    _T ("LuminescenceReader"),
    _T ("Celdyne"),
    _T ("FFStation"),
    _T ("Incubator"),
    _T ("CentralStation")
  };

CString
GetDeviceType (EDeviceType deviceType)
{
  CString l_deviceType (_T ("UNKNOWN"));

  if ((deviceType >= 0) && (static_cast <size_t> (deviceType) < g_deviceType.size ()))
    {
      l_deviceType = g_deviceType[deviceType];
    }

  return l_deviceType;
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  01/01/2005  MCC     initial revision
//  05/10/2007  MEG     implemented protein purification system device type
//  07/18/2007  MCC     added support for CC-Station device type
//  08/29/2007  MCC     added support for Loading Dock device type
//  02/16/2009  MEG     added support for Pipettor device type
//  06/29/2009  MCC     added support for Libra6 device type
//  03/10/2010  MEG     added support for Raven device type
//  07/19/2011  MCC     added support for ISMAC device type
//  08/24/2011  TAN     added support for FACSCyan device type
//  03/23/2012  MCC     added support for SSStation device type
//  05/08/2013  MCC     added support for single tip dispenser device type
//  07/25/2013  MEG     added support for lumi reader device type
//  05/06/2014  MCC     added support for Celdyne device type
//  07/29/2014  MCC     added support for FFStation device type
//  01/09/2015  MCC     templatized number of elements macro
//  07/01/2015  TAN     added support for G3 Incubator device type
//  10/30/2015  MCC     added support for Central Station device type
//
// ============================================================================
