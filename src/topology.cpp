// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: Topology.cpp
//
//     Description: Topology class definition
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: topology.cpp %
//        %version: 26.1.2 %
//          %state: %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "StdAfx.h"
#include "Topology.h"
#include "Device.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace
{
  const UINT FILE_OPEN_R_FLAGS (CFile::modeRead      |
                                CFile::modeNoInherit |
                                CFile::shareExclusive);

  const UINT FILE_OPEN_W_FLAGS (CFile::modeCreate    |
                                CFile::modeWrite     |
                                CFile::modeNoInherit |
                                CFile::shareExclusive);
}

namespace PDCLib
{
IMPLEMENT_SERIAL (CTopology, CObject, VERSIONABLE_SCHEMA | TOPOLOGY_SCHEMA_VERSION_26)

CTopology::CTopology (void)
{
}

CTopology::~CTopology ()
{
  while (!IsEmpty ())
    {
      RemoveDevice (GetFirstDevice ());
    }
}

void
CTopology::Load (CString const & fileName)
{
  CFile l_file (fileName, FILE_OPEN_R_FLAGS);

  CArchive l_archive (&l_file, CArchive::load);

  Serialize (l_archive);
}

void
CTopology::Store (CString const & fileName)
{
  CFile l_file (fileName, FILE_OPEN_W_FLAGS);

  CArchive l_archive (&l_file, CArchive::store);

  Serialize (l_archive);
}

bool
CTopology::IsEmpty (void) const
{
  return m_device.IsEmpty () ? true : false;
}

POSITION
CTopology::GetFirstDevice (void) const
{
  return m_device.GetHeadPosition ();
}

CDevice *
CTopology::GetNextDevice (POSITION & pos) const
{
  return m_device.GetNext (pos);
}

CDevice *
CTopology::GetDevice (POSITION pos) const
{
  return m_device.GetAt (pos);
}

POSITION
CTopology::AppendDevice (CDevice * device)
{
  return m_device.AddTail (device);
}

POSITION
CTopology::PrependDevice (CDevice * device)
{
  return m_device.AddHead (device);
}

POSITION
CTopology::InsertDeviceAfter (POSITION pos, CDevice * device)
{
  return m_device.InsertAfter (pos, device);
}

POSITION
CTopology::InsertDeviceBefore (POSITION pos, CDevice * device)
{
  return m_device.InsertBefore (pos, device);
}

void
CTopology::RemoveDevice (POSITION pos)
{
  delete GetDevice (pos);

  return m_device.RemoveAt (pos);
}

void
CTopology::Serialize (CArchive & ar)
{
#if defined (__QACPP_VERSION)
#pragma PRQA_MESSAGES_OFF 4208, 1808
#endif

  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

#if defined (__QACPP_VERSION)
#pragma PRQA_MESSAGES_ON
#endif

  CObject::Serialize (ar);

  if (ar.IsLoading ())
    {
      switch (ar.GetObjectSchema ())
        {
          case TOPOLOGY_SCHEMA_VERSION_0:
          case TOPOLOGY_SCHEMA_VERSION_1:
          case TOPOLOGY_SCHEMA_VERSION_2:
          case TOPOLOGY_SCHEMA_VERSION_3:
          case TOPOLOGY_SCHEMA_VERSION_4:
          case TOPOLOGY_SCHEMA_VERSION_5:
          case TOPOLOGY_SCHEMA_VERSION_6:
          case TOPOLOGY_SCHEMA_VERSION_7:
          case TOPOLOGY_SCHEMA_VERSION_8:
          case TOPOLOGY_SCHEMA_VERSION_9:
          case TOPOLOGY_SCHEMA_VERSION_10:
          case TOPOLOGY_SCHEMA_VERSION_11:
          case TOPOLOGY_SCHEMA_VERSION_12:
          case TOPOLOGY_SCHEMA_VERSION_13:
          case TOPOLOGY_SCHEMA_VERSION_14:
          case TOPOLOGY_SCHEMA_VERSION_15:
          case TOPOLOGY_SCHEMA_VERSION_16:
          case TOPOLOGY_SCHEMA_VERSION_17:
          case TOPOLOGY_SCHEMA_VERSION_18:
          case TOPOLOGY_SCHEMA_VERSION_19:
          case TOPOLOGY_SCHEMA_VERSION_20:
          case TOPOLOGY_SCHEMA_VERSION_21:
          case TOPOLOGY_SCHEMA_VERSION_22:
          case TOPOLOGY_SCHEMA_VERSION_23:
          case TOPOLOGY_SCHEMA_VERSION_24:
          case TOPOLOGY_SCHEMA_VERSION_25:
          case TOPOLOGY_SCHEMA_VERSION_26:
            {
              m_device.Serialize (ar);

              break;
            }
          default:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
        }
    }
  else
    {
      m_device.Serialize (ar);
    }
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  11/29/2006  MCC     initial revision
//  12/01/2006  MCC     exposed serialize interface
//  12/06/2006  MCC     decoupled method and topology version schema constants
//  09/06/2007  MCC     implemented purge valve feature
//  10/30/2007  MCC     implemented pintool barcode reader support
//  01/11/2008  TAN     added TOPOLOGY_SCHEMA_VERSION_4
//  04/04/2008  TAN     added TOPOLOGY_SCHEMA_VERSION_5
//  05/06/2009  MCC     added template file name to topology
//  06/23/2009  MEG     added TOPOLOGY_SCHEMA_VERSION_7
//  03/29/2010  MCC     implemented support for user defined plate types
//  05/24/2010  MCC     implemented support for conditional reference teaching
//  09/13/2010  MCC     implemented infrastructure for head definition access
//  10/11/2010  MEG     implemented support for Raven barcode reader
//  11/05/2010  MCC     implemented support for limited application access
//  06/03/2011  MEG     implemented support for Libra6 plate definitions
//  07/06/2011  TAN     implemented support for G2 Dispenser barcode reader
//  09/22/2011  MCC     added Summit message handler to Device Editor
//  10/10/2011  MEG     added shaker simulation
//  03/13/2012  TAN     implemented support for stacker and balance on G2 Dispenser
//  05/22/2012  MCC     implemented support for pin-tool plate definition
//  07/30/2012  MCC     implemented support for left/right handed dispensers
//  10/04/2012  MCC     implemented support for optional high speed centrifuge
//  03/27/2014  MEG     implemented optional barcode reader for lumi reader
//  11/24/2014  MEG     added second generation lumi reader
//  01/27/2015  TAN     removed hasStacker boolean on G2Dispenser
//  11/04/2015  MCC     implemented support for simulation mode at device level
//  10/20/2017  MCC     implemented support for optional Celdyne presence
//  07/12/2018  MCC     implemented support for encrypted calibrations
//
// ============================================================================
