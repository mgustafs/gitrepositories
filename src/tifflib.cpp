// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: TIFFLib.cpp
//
//     Description: TIFF library wrapper class definition
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: tifflib.cpp %
//        %version: 27 %
//          %state: %
//         %cvtype: c++ %
//     %derived_by: mgustafs %
//  %date_modified: %
//
// ============================================================================

#include "StdAfx.h"
#include "TIFFLib.h"
#include "tiffio.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace
{
  using CIterator = stdext::checked_array_iterator <WORD *>;
}

namespace PDCLib
{
CMutex CTIFFLib::m_apiGate;

bool
CTIFFLib::RawToTIFF (CString const & fileName,
                     void    const * rawImage,
                     DWORD           cxImage,
                     DWORD           cyImage,
                     DWORD           cxOffset,
                     DWORD           cyOffset,
                     DWORD           cxMask,
                     DWORD           cyMask,
                     ERotation       rotation,
                     bool            yFlip)
{
  bool retval (false);

  std::vector <char> l_fileName;

  PDCLib::StringToVector (fileName, l_fileName);

  CUniqueLock l_apiGate { m_apiGate };

  if (TIFF * const l_tiff = TIFFOpen (&l_fileName[0], "w"))
    {
      int const l_depth (TIFFDataWidth (TIFF_SHORT));

      ASSERT (l_depth > 0);
      ASSERT (cxImage > 0);
      ASSERT (cyImage > 0);

      if (TIFFSetField (l_tiff, TIFFTAG_BITSPERSAMPLE, static_cast <WORD> (l_depth * 8)) &&
          TIFFSetField (l_tiff, TIFFTAG_SAMPLESPERPIXEL, static_cast <WORD> (1)) &&
          TIFFSetField (l_tiff, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT) &&
          TIFFSetField (l_tiff, TIFFTAG_FILLORDER, FILLORDER_MSB2LSB) &&
          TIFFSetField (l_tiff, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG) &&
          TIFFSetField (l_tiff, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK) &&
          TIFFSetField (l_tiff, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_UINT) &&
          TIFFSetField (l_tiff, TIFFTAG_COMPRESSION, COMPRESSION_NONE))
        {
          ASSERT (::AfxIsValidAddress (rawImage, cyImage * cxImage * l_depth, FALSE));

          std::vector <WORD> l_rawImage (reinterpret_cast <WORD const *> (rawImage), reinterpret_cast <WORD const *> (rawImage) + cxImage * cyImage);

          if (CropImage (&l_rawImage[0], cxImage, cyImage, cxOffset, cyOffset, cxMask, cyMask))
            {
              switch (rotation)
                {
                  case ERotation::None:
                    {
                      retval = RawToTIFF0 (l_tiff, &l_rawImage[0], cxMask, cyMask, yFlip);

                      break;
                    }
                  case ERotation::Rotate90:
                    {
                      retval = RawToTIFF1 (l_tiff, &l_rawImage[0], cxMask, cyMask, yFlip);

                      break;
                    }
                  case ERotation::Rotate180:
                    {
                      retval = RawToTIFF2 (l_tiff, &l_rawImage[0], cxMask, cyMask, yFlip);

                      break;
                    }
                  case ERotation::Rotate270:
                    {
                      retval = RawToTIFF3 (l_tiff, &l_rawImage[0], cxMask, cyMask, yFlip);

                      break;
                    }
                  default:
                    {
                      m_errorMessage.Format (_T ("invalid rotation value"));

                      break;
                    }
                }
            }
        }
      else
        {
          m_errorMessage.Format (_T ("unable to set TIFF field"));
        }

      TIFFClose (l_tiff);
    }
  else
    {
      m_errorMessage.Format (_T ("unable to open TIFF file for writing: %s"), (LPCTSTR) fileName);
    }

  return retval;
}

bool
CTIFFLib::TIFFToRaw (CString const & fileName, std::vector <WORD> & rawImage, int & cxImage, int & cyImage)
{
  bool retval (true);

  std::vector <char> l_fileName;

  PDCLib::StringToVector (fileName, l_fileName);

  CUniqueLock l_apiGate { m_apiGate };

  if (TIFF * const l_tiff = TIFFOpen (&l_fileName[0], "r"))
    {
      if (TIFFGetField (l_tiff, TIFFTAG_IMAGEWIDTH, &cxImage) &&
          TIFFGetField (l_tiff, TIFFTAG_IMAGELENGTH, &cyImage))
        {
          if (cxImage > 0)
            {
              if (cyImage > 0)
                {
                  rawImage.resize (cyImage * cxImage);

                  for (int l_y (0); l_y < cyImage; ++l_y)
                    {
                      if (!TIFFReadScanline (l_tiff, &rawImage[l_y * cxImage], l_y))
                        {
                          m_errorMessage.Format (_T ("unable to read TIFF scan line"));

                          retval = false;

                          break;
                        }
                    }
                }
              else
                {
                  m_errorMessage.Format (_T ("invalid TIFF image length"));

                  retval = false;
                }
            }
          else
            {
              m_errorMessage.Format (_T ("invalid TIFF image width"));

              retval = false;
            }
        }
      else
        {
          m_errorMessage.Format (_T ("unable to get TIFF field"));

          retval = false;
        }

      TIFFClose (l_tiff);
    }
  else
    {
      m_errorMessage.Format (_T ("unable to open TIFF file for reading: %s"), (LPCTSTR) fileName);

      retval = false;
    }

  return retval;
}

bool
CTIFFLib::DarkFrameSubtract (CString       const & inputFileName,
                             void                * rawImage,
                             int                   cxImage,
                             int                   cyImage,
                             EMedianFilter         medianFilter,
                             int                   medianElement)
{
  std::vector <WORD> l_darkImage;
  int l_cxImage (0);
  int l_cyImage (0);

  if (TIFFToRaw (inputFileName, l_darkImage, l_cxImage, l_cyImage))
    {
      if ((l_cxImage == cxImage) && (l_cyImage == cyImage))
        {
          WORD * l_rawValue (reinterpret_cast <WORD *> (rawImage));

          for each (auto l_darkValue in l_darkImage)
            {
              *l_rawValue -= std::min (l_darkValue, *l_rawValue);

              ++l_rawValue;
            }

          MedianFilter (rawImage, cxImage, cyImage, medianFilter, medianElement);

          return true;
        }
      else
        {
          m_errorMessage.Format (_T ("image dimensions are not equal"));
        }
    }

  return false;
}

void
CTIFFLib::Transform (CRect           & imageMask,
                     CSize     const & frame,
                     ERotation         rotation,
                     bool              yFlip)
{
  int const l_xMask (imageMask.left);
  int const l_yMask (imageMask.top);
  int const l_cxMask (imageMask.Size ().cx);
  int const l_cyMask (imageMask.Size ().cy);
  int const l_cxFrame (frame.cx);
  int const l_cyFrame (frame.cy);

  switch (rotation)
    {
      case ERotation::None:
        {
          if (yFlip)
            {
              imageMask = CRect (CPoint (l_xMask, l_cyFrame - l_yMask - l_cyMask - 1), CSize (l_cxMask, l_cyMask));
            }

          break;
        }
      case ERotation::Rotate90:
        {
          if (yFlip)
            {
              imageMask = CRect (CPoint (l_cxFrame - l_yMask - l_cyMask - 1, l_cyFrame - l_xMask - l_cxMask - 1), CSize (l_cyMask, l_cxMask));
            }
          else
            {
              imageMask = CRect (CPoint (l_yMask, l_cyFrame - l_xMask - l_cxMask - 1), CSize (l_cyMask, l_cxMask));
            }

          break;
        }
      case ERotation::Rotate180:
        {
          if (yFlip)
            {
              imageMask = CRect (CPoint (l_cxFrame - l_xMask - l_cxMask - 1, l_yMask), CSize (l_cxMask, l_cyMask));
            }
          else
            {
              imageMask = CRect (CPoint (l_cxFrame - l_xMask - l_cxMask - 1, l_cyFrame - l_yMask - l_cyMask - 1), CSize (l_cxMask, l_cyMask));
            }

          break;
        }
      case ERotation::Rotate270:
        {
          if (yFlip)
            {
              imageMask = CRect (CPoint (l_yMask, l_xMask), CSize (l_cyMask, l_cxMask));
            }
          else
            {
              imageMask = CRect (CPoint (l_cxFrame - l_yMask - l_cyMask - 1, l_xMask), CSize (l_cyMask, l_cxMask));
            }

          break;
        }
      default:
        {
          ASSERT (false); // invalid rotation (should never happen)

          break;
        }
    }
}

void
CTIFFLib::MedianFilter (void * rawImage, int cxImage, int cyImage, EMedianFilter medianFilter, int medianElement)
{
  switch (medianFilter)
    {
      case EMedianFilter::MedianFilterNone:
        {
          // No median filter applied; raw image is unchanged

          break;
        }
      case EMedianFilter::MedianFilter2x2:
        {
          MedianFilter2x2 (rawImage, cxImage, cyImage, medianElement);

          break;
        }
      case EMedianFilter::MedianFilter3x3:
        {
          MedianFilter3x3 (rawImage, cxImage, cyImage, medianElement);

          break;
        }
      case EMedianFilter::MedianFilter5x5:
        {
          MedianFilter5x5 (rawImage, cxImage, cyImage, medianElement);

          break;
        }
      default:
        {
          ASSERT (false); // invalid median filter (should never happen)

          break;
        }
    }
}

void
CTIFFLib::MedianFilter2x2 (void * rawImage, int cxImage, int cyImage, int medianElement)
{
  int const l_cxImage (cxImage + 1);
  int const l_cyImage (cyImage + 1);

  std::vector <WORD> l_rawImage (l_cyImage * l_cxImage);

  for (int l_y (0); l_y < cyImage; ++l_y)
    {
      for (int l_x (0); l_x < cxImage; ++l_x)
        {
          l_rawImage[l_y * l_cxImage + l_x] = reinterpret_cast <WORD *> (rawImage)[l_y * cxImage + l_x];
        }

      l_rawImage[(l_y + 1) * l_cxImage - 1] = l_rawImage[(l_y + 1) * l_cxImage - 2];
    }

  for (int l_x (0); l_x < l_cxImage; ++l_x)
    {
      l_rawImage[(l_cyImage - 1) * l_cxImage + l_x] = l_rawImage[(l_cyImage - 2) * l_cxImage + l_x];
    }

  std::vector <WORD> l_m (4);

  for (int l_y (0); l_y < cyImage; ++l_y)
    {
      for (int l_x (0); l_x < cxImage; ++l_x)
        {
          WORD const * l_p (&l_rawImage[l_y * l_cxImage + l_x]);

          for (int l_i (0); l_i < static_cast <int> (l_m.size ()); ++l_i)
            {
              l_m[l_i]  = *(l_p + (l_i / 2) * l_cxImage + (l_i % 2));
            }

          std::nth_element (l_m.begin (), l_m.begin () + medianElement, l_m.end ());

          reinterpret_cast <WORD *> (rawImage)[l_y * cxImage + l_x] = l_m[medianElement];
        }
    }
}

void
CTIFFLib::MedianFilter3x3 (void * rawImage, int cxImage, int cyImage, int medianElement)
{
  int const l_cxImage (cxImage + 2);
  int const l_cyImage (cyImage + 2);

  std::vector <WORD> l_rawImage (l_cyImage * l_cxImage);

  for (int l_y (0); l_y < cyImage; ++l_y)
    {
      for (int l_x (0); l_x < cxImage; ++l_x)
        {
          l_rawImage[(l_y + 1) * l_cxImage + l_x + 1] = reinterpret_cast <WORD *> (rawImage)[l_y * cxImage + l_x];
        }

      l_rawImage[(l_y + 1) * l_cxImage] = l_rawImage[(l_y + 1) * l_cxImage + 1];

      l_rawImage[(l_y + 2) * l_cxImage - 1] = l_rawImage[(l_y + 2) * l_cxImage - 2];
    }

  for (int l_x (0); l_x < l_cxImage; ++l_x)
    {
      l_rawImage[l_x] = l_rawImage[l_cxImage + l_x];

      l_rawImage[(l_cyImage - 1) * l_cxImage + l_x] = l_rawImage[(l_cyImage - 2) * l_cxImage + l_x];
    }

  std::vector <WORD> l_m (9);

  for (int l_y (0); l_y < cyImage; ++l_y)
    {
      for (int l_x (0); l_x < cxImage; ++l_x)
        {
          WORD const * l_p (&l_rawImage[(l_y + 1) * l_cxImage + l_x + 1]);

          for (int l_i (0); l_i < static_cast <int> (l_m.size ()); ++l_i)
            {
              l_m[l_i]  = *(l_p + (l_i / 3 - 1) * l_cxImage + (l_i % 3 - 1));
            }

          std::nth_element (l_m.begin (), l_m.begin () + medianElement, l_m.end ());

          reinterpret_cast <WORD *> (rawImage)[l_y * cxImage + l_x] = l_m[medianElement];
        }
    }
}

void
CTIFFLib::MedianFilter5x5 (void * rawImage, int cxImage, int cyImage, int medianElement)
{
  int const l_cxImage (cxImage + 4);
  int const l_cyImage (cyImage + 4);

  std::vector <WORD> l_rawImage (l_cyImage * l_cxImage);

  for (int l_y (0); l_y < cyImage; ++l_y)
    {
      for (int l_x (0); l_x < cxImage; ++l_x)
        {
          l_rawImage[(l_y + 2) * l_cxImage + l_x + 2] = reinterpret_cast <WORD *> (rawImage)[l_y * cxImage + l_x];
        }

      l_rawImage[(l_y + 2) * l_cxImage] = l_rawImage[(l_y + 2) * l_cxImage + 1] = l_rawImage[(l_y + 2) * l_cxImage + 2];

      l_rawImage[(l_y + 3) * l_cxImage - 1] = l_rawImage[(l_y + 3) * l_cxImage - 2] = l_rawImage[(l_y + 3) * l_cxImage - 3];
    }

  for (int l_x (0); l_x < l_cxImage; ++l_x)
    {
      l_rawImage[l_x] = l_rawImage[l_cxImage + l_x] = l_rawImage[2 * l_cxImage + l_x];

      l_rawImage[(l_cyImage - 1) * l_cxImage + l_x] = l_rawImage[(l_cyImage - 2) * l_cxImage + l_x] = l_rawImage[(l_cyImage - 3) * l_cxImage + l_x];
    }

  std::vector <WORD> l_m (25);

  for (int l_y (0); l_y < cyImage; ++l_y)
    {
      for (int l_x (0); l_x < cxImage; ++l_x)
        {
          WORD const * l_p (&l_rawImage[(l_y + 2) * l_cxImage + l_x + 2]);

          for (int l_i (0); l_i < static_cast <int> (l_m.size ()); ++l_i)
            {
              l_m[l_i]  = *(l_p + (l_i / 5 - 2) * l_cxImage + (l_i % 2 - 2));
            }

          std::nth_element (l_m.begin (), l_m.begin () + medianElement, l_m.end ());

          reinterpret_cast <WORD *> (rawImage)[l_y * cxImage + l_x] = l_m[medianElement];
        }
    }
}

bool
CTIFFLib::CropImage (WORD  * rawImage,
                     DWORD   cxImage,
                     DWORD   cyImage,
                     DWORD   cxOffset,
                     DWORD   cyOffset,
                     DWORD   cxMask,
                     DWORD   cyMask)
{
  bool retval (true);

  if ((cxMask > cxImage) || (cyMask > cyImage))
    {
      m_errorMessage.Format (_T ("image mask size exceeds raw image size"));

      retval = false;
    }
  else if ((cxImage > cxMask) || (cyOffset > 0))
    {
      DWORD const l_cxDelta (cxImage - cxMask);
      DWORD const l_cyDelta (cyImage - cyMask);

      if ((cxOffset > l_cxDelta) || (cyOffset > l_cyDelta))
        {
          m_errorMessage.Format (_T ("image mask offset exceeds the raw image size difference"));

          retval = false;
        }
      else
        {
          size_t const l_size (cxMask * cyMask);

          CIterator l_dst (rawImage, l_size);

          CIterator const l_end (rawImage, l_size, l_size);

          for (CIterator l_src (rawImage, cxImage * cyImage, (cyOffset * cxImage) + cxOffset); l_dst != l_end; l_src += cxImage)
            {
              l_dst = std::copy (l_src, l_src + cxMask, l_dst);
            }
        }
    }

  return retval;
}

bool
CTIFFLib::RawToTIFF0 (TIFF * tiff, WORD * rawImage, DWORD cxImage, DWORD cyImage, bool yFlip)
{
  if (TIFFSetField (tiff, TIFFTAG_IMAGEWIDTH, cxImage) &&
      TIFFSetField (tiff, TIFFTAG_IMAGELENGTH, cyImage) &&
      TIFFSetField (tiff, TIFFTAG_ROWSPERSTRIP, std::min (TIFFDefaultStripSize (tiff, 0), cyImage)))
    {
      for (DWORD l_y (0); l_y < cyImage; ++l_y)
        {
          int const l_y_ (yFlip ? (cyImage - l_y - 1) : l_y);

          if (!TIFFWriteScanline (tiff, &rawImage[l_y_ * cxImage], l_y))
            {
              m_errorMessage.Format (_T ("unable to write TIFF scan line"));

              return false;
            }
        }
    }
  else
    {
      m_errorMessage.Format (_T ("unable to set TIFF field"));

      return false;
    }

  return true;
}

bool
CTIFFLib::RawToTIFF1 (TIFF * tiff, WORD * rawImage, DWORD cxImage, DWORD cyImage, bool yFlip)
{
  if (TIFFSetField (tiff, TIFFTAG_IMAGEWIDTH, cyImage) &&
      TIFFSetField (tiff, TIFFTAG_IMAGELENGTH, cxImage) &&
      TIFFSetField (tiff, TIFFTAG_ROWSPERSTRIP, std::min (TIFFDefaultStripSize (tiff, 0), cxImage)))
    {
      std::vector <WORD> l_scanLine (cyImage);

      for (DWORD l_y (0); l_y < cxImage; ++l_y)
        {
          int const l_y_ (yFlip ? (cxImage - l_y - 1) : l_y);

          for (DWORD l_x (0); l_x < cyImage; ++l_x)
            {
              l_scanLine[l_x] = rawImage[(cyImage - l_x - 1) * cxImage + l_y_];
            }

          if (!TIFFWriteScanline (tiff, &l_scanLine[0], l_y))
            {
              m_errorMessage.Format (_T ("unable to write TIFF scan line"));

              return false;
            }
        }
    }
  else
    {
      m_errorMessage.Format (_T ("unable to set TIFF field"));

      return false;
    }

  return true;
}

bool
CTIFFLib::RawToTIFF2 (TIFF * tiff, WORD * rawImage, DWORD cxImage, DWORD cyImage, bool yFlip)
{
  if (TIFFSetField (tiff, TIFFTAG_IMAGEWIDTH, cxImage) &&
      TIFFSetField (tiff, TIFFTAG_IMAGELENGTH, cyImage) &&
      TIFFSetField (tiff, TIFFTAG_ROWSPERSTRIP, std::min (TIFFDefaultStripSize (tiff, 0), cyImage)))
    {
      std::vector <WORD> l_scanLine (cxImage);

      for (DWORD l_y (0); l_y < cyImage; ++l_y)
        {
          int const l_y_ (yFlip ? (cyImage - l_y - 1) : l_y);

          for (DWORD l_x (0); l_x < cxImage; ++l_x)
            {
              l_scanLine[l_x] = rawImage[(cyImage - l_y_ - 1) * cxImage + cxImage - l_x - 1];
            }

          if (!TIFFWriteScanline (tiff, &l_scanLine[0], l_y))
            {
              m_errorMessage.Format (_T ("unable to write TIFF scan line"));

              return false;
            }
        }
    }
  else
    {
      m_errorMessage.Format (_T ("unable to set TIFF field"));

      return false;
    }

  return true;
}

bool
CTIFFLib::RawToTIFF3 (TIFF * tiff, WORD * rawImage, DWORD cxImage, DWORD cyImage, bool yFlip)
{
  if (TIFFSetField (tiff, TIFFTAG_IMAGEWIDTH, cyImage) &&
      TIFFSetField (tiff, TIFFTAG_IMAGELENGTH, cxImage) &&
      TIFFSetField (tiff, TIFFTAG_ROWSPERSTRIP, std::min (TIFFDefaultStripSize (tiff, 0), cxImage)))
    {
      std::vector <WORD> l_scanLine (cyImage);

      for (DWORD l_y (0); l_y < cxImage; ++l_y)
        {
          int const l_y_ (yFlip ? (cxImage - l_y - 1) : l_y);

          for (DWORD l_x (0); l_x < cyImage; ++l_x)
            {
              l_scanLine[l_x] = rawImage[l_x * cxImage + cxImage - l_y_ - 1];
            }

          if (!TIFFWriteScanline (tiff, &l_scanLine[0], l_y))
            {
              m_errorMessage.Format (_T ("unable to write TIFF scan line"));

              return false;
            }
        }
    }
  else
    {
      m_errorMessage.Format (_T ("unable to set TIFF field"));

      return false;
    }

  return true;
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  05/14/2010  MCC     initial revision
//  08/20/2010  MCC     implemented support for image analysis
//  09/16/2010  MCC     moved TIFF library class into PDCLib project
//  09/21/2010  MCC     implemented correct image mask drawing
//  09/21/2010  MCC     modified to support image rotation
//  12/13/2010  MCC     relaxed constraints on image mask size validation
//  02/23/2011  MCC     implemented support for flipping image
//  02/25/2011  MCC     fixed support for flipping image
//  02/26/2011  MCC     corrected problem with image cropping
//  02/28/2011  MCC     modified to use checked iterators
//  03/01/2011  MEG     fixed crop and rotate problems
//  03/02/2011  MCC     refactored image mask transformation code
//  12/17/2014  MCC     implemented critical sections with C++11 concurrency
//  10/12/2015  MCC     added median filter to TIFF library class
//  10/13/2015  MCC     added dark field subtraction to TIFF library class
//  10/15/2015  MCC     added 2x2 median filter to TIFF library class
//  10/20/2015  MCC     generalized median filter in TIFF library class
//  10/22/2015  MEG     removed output file from dark frame subtract
//  11/09/2015  MCC     added 5x5 median filter to TIFF library class
//  11/10/2015  MCC     minor code cleanup to 2x2 median filter
//  12/09/2015  MEG     made RawToTIFF into non-destructive operation
//  12/11/2015  MCC     optimized TIFF library modification
//  04/06/2016  MCC     refactored median filter algorithms
//  04/15/2016  MEG     modified dark frame subtract to perform median filter after
//  04/21/2016  MEG     added median filter none option
//  03/31/2017  MCC     added reset method to remote control interface
//  04/10/2017  MEG     added cast to CString for resolving conversion warning
//
// ============================================================================
