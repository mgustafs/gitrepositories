// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2006.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: Global.cpp
//
//     Description: Global definitions
//
//          Author: Marc Gustafson
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 4 %
//           %name: global.cpp %
//        %version: 6 %
//          %state: %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "StdAfx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace PDCLib
{
CArchive & operator>> (CArchive & ar, EControllerType & rhs)
{
  ar.Read (&rhs, sizeof (rhs));

  return ar;
}

CArchive & operator<< (CArchive & ar, EControllerType const & rhs)
{
  ar.Write (&rhs, sizeof (rhs));

  return ar;
}

CArchive & operator>> (CArchive & ar, EDeviceType & rhs)
{
  ar.Read (&rhs, sizeof (rhs));

  return ar;
}

CArchive & operator<< (CArchive & ar, EDeviceType const & rhs)
{
  ar.Write (&rhs, sizeof (rhs));

  return ar;
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  04/14/2006  MEG     initial revision
//  11/28/2006  MCC     baselined serializable topology class hierarchy
//  08/22/2008  MCC     added method editor class to project
//  05/06/2009  MCC     implemented support for HASP adapter
//  12/18/2009  MCC     baselined and refactored common utility support
//  01/15/2010  MCC     implemented support for application data folder
//
// ============================================================================
