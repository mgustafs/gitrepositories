// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: Variable.cpp
//
//     Description: Variable class defintion
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: variable.cpp %
//        %version: 28 %
//          %state: working %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: Monday, October 07, 2002 1:00:27 PM %
//
// ============================================================================

#include "StdAfx.h"
#include "Variable.h"
#include "VariableVisitor.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace PDCLib
{
IMPLEMENT_SERIAL (CVariable, CObject, VERSIONABLE_SCHEMA | METHOD_SCHEMA_VERSION_13)

CVariable::CVariable (void)
  : m_objectSchema (METHOD_SCHEMA_VERSION_0)
  , m_isConstant (false)
  , m_isLoggingEnabled (false)
  , m_refCount (1)
{
}

CVariable::CVariable (const CVariable & other)
  : m_objectSchema (METHOD_SCHEMA_VERSION_0)
  , m_identifier (other.m_identifier)
  , m_alias (other.m_alias)
  , m_units (other.m_units)
  , m_isConstant (other.m_isConstant)
  , m_isLoggingEnabled (other.m_isLoggingEnabled)
  , m_comment (other.m_comment)
  , m_refCount (1)
{
}

CVariable::~CVariable ()
{
}

void
CVariable::Release (void)
{
  if (--m_refCount == 0)
    {
      delete this;
    }
}

CString
CVariable::GetFormattedVariable (void) const
{
  ASSERT (false); // should be pure virtual function (constrained by serialization)

  return _T ("");
}

CString
CVariable::GetPrintFormattedVariable (bool) const
{
  ASSERT (false); // should be pure virtual function (constrained by serialization)

  return _T ("");
}

CVariable *
CVariable::Duplicate (void) const
{
  ASSERT (false); // should be pure virtual function (constrained by serialization)

  return NULL;
}

void
CVariable::Serialize (CArchive & ar)
{
  CObject::Serialize (ar);

  if (ar.IsLoading ())
    {
      m_objectSchema = ar.GetObjectSchema ();

      switch (m_objectSchema)
        {
          case METHOD_SCHEMA_VERSION_0:
          case METHOD_SCHEMA_VERSION_1:
          case METHOD_SCHEMA_VERSION_2:
          case METHOD_SCHEMA_VERSION_3:
          case METHOD_SCHEMA_VERSION_4:
          case METHOD_SCHEMA_VERSION_5:
          case METHOD_SCHEMA_VERSION_6:
          case METHOD_SCHEMA_VERSION_7:
          case METHOD_SCHEMA_VERSION_8:
            {
              ar >> m_identifier;
              ar >> m_alias;
              ar >> m_units;
              ar >> m_isConstant;
              ar >> m_comment;
              ar >> m_refCount;

              break;
            }
          case METHOD_SCHEMA_VERSION_9:
          case METHOD_SCHEMA_VERSION_10:
          case METHOD_SCHEMA_VERSION_11:
          case METHOD_SCHEMA_VERSION_12:
          case METHOD_SCHEMA_VERSION_13:
            {
              ar >> m_identifier;
              ar >> m_alias;
              ar >> m_units;
              ar >> m_isConstant;
              ar >> m_isLoggingEnabled;
              ar >> m_comment;
              ar >> m_refCount;

              break;
            }
          default:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
        }
    }
  else
    {
      ar << m_identifier;
      ar << m_alias;
      ar << m_units;
      ar << m_isConstant;
      ar << m_isLoggingEnabled;
      ar << m_comment;
      ar << m_refCount;
    }
}

void
CVariable::Accept (CVariableVisitor & variableVisitor)
{
  UNUSED_ALWAYS (variableVisitor);
}

CString
CVariable::GetPrintFormattedLVal (bool isTemplateView) const
{
  CString retval (m_alias);

  if (isTemplateView && (m_alias != m_identifier))
    {
      retval += _T (" (") + m_identifier + _T (") = ");
    }
  else
    {
      retval += _T (" = ");
    }

  return retval;
}

CString
CVariable::GetPrintFormattedModifiers (const CString   & minValue,
                                       const CString   & maxValue,
                                             bool        isTemplateView) const
{
  CString retval;

  if (isTemplateView)
    {
      retval = _T (" [") + minValue + _T (", ") + maxValue;

      if (m_isConstant)
        {
          retval += _T (", const");
        }

      if (m_isLoggingEnabled)
        {
          retval += _T (", logged");
        }

      retval += _T ("]");
    }
  else if (m_isLoggingEnabled)
    {
      retval = _T (" [logged]");
    }

  return retval;
}

CString
CVariable::GetPrintFormattedModifiers (bool isTemplateView) const
{
  CString retval;

  if (isTemplateView)
    {
      if (m_isConstant)
        {
          if (m_isLoggingEnabled)
            {
              retval = _T (" [const, logged]");
            }
          else
            {
              retval = _T (" [const]");
            }
        }
      else if (m_isLoggingEnabled)
        {
          retval = _T (" [logged]");
        }
    }
  else if (m_isLoggingEnabled)
    {
      retval = _T (" [logged]");
    }

  return retval;
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  08/05/2002  MCC     initial revision
//  07/21/2003  MCC     updated version schema
//  08/28/2003  MCC     modified code for QAC++ compliance
//  10/29/2003  MCC     implemented step interator feature
//  04/01/2004  MCC     implemented support for cut/copy/paste operations
//  04/15/2004  MCC     modified paste variable behavior
//  07/19/2005  MCC     replaced all double-underscores
//  01/30/2006  MCC     commonized document and print views
//  03/22/2006  MEG     updated version schema
//  04/14/2006  MEG     modified to use namespace PDCLib
//  11/28/2006  MCC     baselined serializable topology class hierarchy
//  12/06/2006  MCC     decoupled method and topology version schema constants
//  06/27/2007  MEG     changed VARIANT interpretation from intVal to lVal
//  07/03/2006  MCC     implemented skip step feature
//  05/11/2009  MCC     implemented support for new document creation strategy
//  08/12/2009  MCC     modified to display reference attribute
//  03/18/2010  MCC     implemented support for plate definition variable type
//  08/04/2010  MCC     implemented support for tree view icons
//  08/09/2010  MCC     enhanced functionality of step iterator
//  08/10/2010  MCC     implemented support for variable grouping
//  11/05/2010  MCC     implemented support for limited application access
//  02/18/2011  MCC     implemented support for method variable logging
//  03/22/2011  MCC     implemented support modifying enumeration keys
//  07/26/2011  MCC     corrected numerous problems with printing
//  07/03/2012  MCC     added support for variable specific plate definitions
//  07/20/2012  MCC     implemented support for iterator comment
//  05/09/2013  MCC     refactored variable processing
//  07/17/2014  MCC     implemented support for executing shell commands
//
// ============================================================================
