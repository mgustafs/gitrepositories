// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: Step.cpp
//
//     Description: Step class definition
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: step.cpp %
//        %version: 20 %
//          %state: working %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: Monday, October 07, 2002 1:00:25 PM %
//
// ============================================================================

#include "StdAfx.h"
#include "Step.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace PDCLib
{
IMPLEMENT_SERIAL (CStep, CVariableList, VERSIONABLE_SCHEMA | METHOD_SCHEMA_VERSION_13)

CStep::CStep (void)
  : m_skip (false)
  , m_skippable (false)
  , m_feature (CHaspAdapter::EHaspFeature::haspDefaultFeature)
{
}

CStep::CStep (const CStep & other)
  : CVariableList (other)
  , m_alias (other.m_alias)
  , m_skip (other.m_skip)
  , m_skippable (other.m_skippable)
  , m_feature (other.m_feature)
{
}

CStep::~CStep ()
{
  ASSERT (GetFirstVariable () == NULL);

  while (!m_varList.IsEmpty ())
    {
      delete m_varList.RemoveHead ();
    }
}

CVariable *
CStep::GetVariable (const CString & alias) const
{
  POSITION l_pos;

  l_pos = GetFirstVariableList ();

  while (l_pos)
    {
      CVariableList const * const l_varList (GetNextVariableList (l_pos));

      if (CVariable * const l_variable = l_varList->GetVariable (alias))
        {
          return l_variable;
        }
    }

  return NULL;
}

void
CStep::RemoveVariableList (POSITION pos, bool deleteVarList)
{
  CObject const * const l_object (m_varList.GetAt (pos));

  m_varList.RemoveAt (pos);

  if (deleteVarList)
    {
      delete l_object;
    }
}

CStep *
CStep::Duplicate (bool copyVariables) const
{
  CStep * const l_step (new CStep (*this));

  if (copyVariables)
    {
      POSITION l_pos (GetFirstVariableList ());

      while (l_pos)
        {
          l_step->AddVariableList (GetNextVariableList (l_pos)->Duplicate (copyVariables));
        }
    }

  return l_step;
}

void
CStep::Serialize (CArchive & ar)
{
  if (ar.IsLoading ())
    {
      CVariableList::Serialize (ar);

      switch (GetObjectSchema ())
        {
          case METHOD_SCHEMA_VERSION_0:
          case METHOD_SCHEMA_VERSION_1:
          case METHOD_SCHEMA_VERSION_2:
          case METHOD_SCHEMA_VERSION_3:
          case METHOD_SCHEMA_VERSION_4:
            {
              CString l_identifier;
              CString l_comment;

              ar >> l_identifier;
              ar >> m_alias;
              ar >> l_comment;

              SetIdentifier (l_identifier);
              SetComment (l_comment);

              CreateVariableList ();

              break;
            }
          case METHOD_SCHEMA_VERSION_5:
          case METHOD_SCHEMA_VERSION_6:
          case METHOD_SCHEMA_VERSION_7:
            {
              CString l_identifier;
              CString l_comment;

              ar >> l_identifier;
              ar >> m_alias;
              ar >> l_comment;
              ar >> m_skip;
              ar >> m_skippable;

              SetIdentifier (l_identifier);
              SetComment (l_comment);

              CreateVariableList ();

              break;
            }
          case METHOD_SCHEMA_VERSION_8:
          case METHOD_SCHEMA_VERSION_9:
          case METHOD_SCHEMA_VERSION_10:
          case METHOD_SCHEMA_VERSION_11:
          case METHOD_SCHEMA_VERSION_12:
            {
              ar >> m_alias;
              ar >> m_skip;
              ar >> m_skippable;

              m_varList.Serialize (ar);

              break;
            }
          case METHOD_SCHEMA_VERSION_13:
            {
              ar >> m_alias;
              ar >> m_skip;
              ar >> m_skippable;
              ar >> m_feature;

              m_varList.Serialize (ar);

              break;
            }
          default:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
        }
    }
  else
    {
      ASSERT (GetFirstVariable () == NULL);

      CVariableList::Serialize (ar);

      ar << m_alias;
      ar << m_skip;
      ar << m_skippable;
      ar << m_feature;

      m_varList.Serialize (ar);
    }
}

void
CStep::CreateVariableList (void)
{
  CVariableList * const l_varList (new CVariableList (_T ("Default Group")));

  std::vector <POSITION> l_pos (2);

  l_pos[1] = GetFirstVariable ();

  while ((l_pos[0] = l_pos[1]) != NULL)
    {
      l_varList->AddVariable (GetNextVariable (l_pos[1]));

      RemoveVariable (l_pos[0]);
    }

  AddVariableList (l_varList);
}
}  // end namespace PDCLib

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  08/05/2002  MCC     initial revision
//  07/21/2003  MCC     updated version schema
//  10/29/2003  MCC     implemented step interator feature
//  04/01/2004  MCC     implemented support for cut/copy/paste operations
//  07/19/2005  MCC     replaced all double-underscores
//  01/11/2006  MCC     removed workaround for MFC serialization memory leak
//  03/22/2006  MEG     updated version schema
//  04/14/2006  MEG     modified to use namespace PDCLib
//  12/06/2006  MCC     decoupled method and topology version schema constants
//  07/03/2006  MCC     implemented skip step feature
//  03/18/2010  MCC     implemented support for plate definition variable type
//  08/09/2010  MCC     enhanced functionality of step iterator
//  08/10/2010  MCC     implemented support for variable grouping
//  02/18/2011  MCC     implemented support for method variable logging
//  03/22/2011  MCC     implemented support modifying enumeration keys
//  07/03/2012  MCC     added support for variable specific plate definitions
//  07/20/2012  MCC     implemented support for iterator comment
//  06/05/2014  MCC     modified to propagate collapsed property on duplicate
//  07/17/2014  MCC     implemented support for executing shell commands
//  01/22/2015  MCC     added stacker feature
//
// ============================================================================
