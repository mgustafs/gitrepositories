// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: EnumAssoc.cpp
//
//     Description: Enumeration association class definition
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: enumassoc.cpp %
//        %version: 18 %
//          %state: working %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: Monday, October 07, 2002 1:00:19 PM %
//
// ============================================================================

#include "StdAfx.h"
#include "EnumAssoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace PDCLib
{
IMPLEMENT_SERIAL (CEnumAssoc, CObject, VERSIONABLE_SCHEMA | METHOD_SCHEMA_VERSION_13)

CEnumAssoc::CEnumAssoc (void)
{
}

CEnumAssoc::CEnumAssoc (const CString & key, VARIANT const & value)
  : m_key (key)
  , m_value (value)
{
}

CEnumAssoc::~CEnumAssoc ()
{
}

void
CEnumAssoc::Serialize (CArchive & ar)
{
  if (ar.IsLoading ())
    {
      CObject::Serialize (ar);

      switch (ar.GetObjectSchema ())
        {
          case METHOD_SCHEMA_VERSION_0:
          case METHOD_SCHEMA_VERSION_1:
          case METHOD_SCHEMA_VERSION_2:
          case METHOD_SCHEMA_VERSION_3:
          case METHOD_SCHEMA_VERSION_4:
          case METHOD_SCHEMA_VERSION_5:
          case METHOD_SCHEMA_VERSION_6:
          case METHOD_SCHEMA_VERSION_7:
          case METHOD_SCHEMA_VERSION_8:
          case METHOD_SCHEMA_VERSION_9:
          case METHOD_SCHEMA_VERSION_10:
          case METHOD_SCHEMA_VERSION_11:
          case METHOD_SCHEMA_VERSION_12:
          case METHOD_SCHEMA_VERSION_13:
            {
              ar >> m_key;
              ar >> m_value;

              break;
            }
          default:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
        }
    }
  else
    {
      CObject::Serialize (ar);

      ar << m_key;
      ar << m_value;
    }
}

CArchive & operator>> (CArchive & ar, VARIANT & rhs)
{
  ar.Read (&rhs, sizeof (rhs));

  return ar;
}

CArchive & operator<< (CArchive & ar, VARIANT const & rhs)
{
  ar.Write (&rhs, sizeof (rhs));

  return ar;
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  08/13/2002  MCC     initial revision
//  07/21/2003  MCC     updated version schema
//  10/29/2003  MCC     implemented step interator feature
//  01/11/2006  MCC     removed workaround for MFC serialization memory leak
//  03/22/2006  MEG     updated version schema
//  04/14/2006  MEG     modified to use namespace PDCLib
//  11/28/2006  MCC     baselined serializable topology class hierarchy
//  12/05/2006  MCC     modified for QAC++ compliance
//  12/06/2006  MCC     decoupled method and topology version schema constants
//  07/03/2006  MCC     implemented skip step feature
//  03/18/2010  MCC     implemented support for plate definition variable type
//  08/09/2010  MCC     enhanced functionality of step iterator
//  08/10/2010  MCC     implemented support for variable grouping
//  02/18/2011  MCC     implemented support for method variable logging
//  03/22/2011  MCC     implemented support modifying enumeration keys
//  07/03/2012  MCC     added support for variable specific plate definitions
//  07/20/2012  MCC     implemented support for iterator comment
//  07/17/2014  MCC     implemented support for executing shell commands
//
// ============================================================================
