// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: HeadDefinitions.cpp
//
//     Description: Head Definitions class definition
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: headdefinitions.cpp %
//        %version: 3 %
//          %state: %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "StdAfx.h"
#include "HeadDefinitions.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace
{
  const UINT HEAD_DEFINITIONS_SCHEMA_VERSION_1 (1);

  const UINT FILE_OPEN_R_FLAGS (CFile::modeRead       |
                                CFile::modeNoInherit  |
                                CFile::modeCreate     |
                                CFile::modeNoTruncate |
                                CFile::shareExclusive);

  const UINT FILE_OPEN_W_FLAGS (CFile::modeWrite     |
                                CFile::modeNoInherit |
                                CFile::shareExclusive);
}

namespace PDCLib
{
IMPLEMENT_SERIAL (CHeadDefinitions, CObject, VERSIONABLE_SCHEMA | HEAD_DEFINITIONS_SCHEMA_VERSION_1)

CHeadDefinitions::CHeadDefinitions (void)
  : m_headDefinitions (new CHeadDefinitionList)
{
}

CHeadDefinitions::CHeadDefinitions (CString const & fileName)
  : m_fileName (fileName)
  , m_headDefinitions (new CHeadDefinitionList)
{
}

CHeadDefinitions::~CHeadDefinitions ()
{
  Destroy (m_headDefinitions);
}

CHeadDefinitions *
CHeadDefinitions::Load (CString const & fileName)
{
  // !!! THIS MUST BE IN THE SAME CONTEXT IN WHICH SERIALIZATION OCCURS !!!

  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  std::unique_ptr <CHeadDefinitions> l_headDefinitions;

  try
    {
      CFile l_file (fileName, FILE_OPEN_R_FLAGS);

      if (l_file.GetLength ())
        {
          CArchive l_archive (&l_file, CArchive::load);

          l_headDefinitions.reset (dynamic_cast <CHeadDefinitions *> (l_archive.ReadObject (RUNTIME_CLASS (CHeadDefinitions))));

          l_headDefinitions->m_fileName = fileName;
        }
      else
        {
          l_headDefinitions.reset (new CHeadDefinitions (fileName));
        }
    }
  catch (CException * e)
    {
      throw CHeadDefinitionException (e);
    }

  return l_headDefinitions.release ();
}

void
CHeadDefinitions::Reload (void)
{
  // !!! THIS MUST BE IN THE SAME CONTEXT IN WHICH SERIALIZATION OCCURS !!!

  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  try
    {
      CFile l_file (m_fileName, FILE_OPEN_R_FLAGS);

      if (l_file.GetLength ())
        {
          CArchive l_archive (&l_file, CArchive::load);

          std::shared_ptr <CHeadDefinitions> l_headDefinitions (dynamic_cast <CHeadDefinitions *> (l_archive.ReadObject (RUNTIME_CLASS (CHeadDefinitions))));

          if (l_headDefinitions)
            {
              Destroy (m_headDefinitions);

              m_headDefinitions = l_headDefinitions->m_headDefinitions;

              l_headDefinitions->m_headDefinitions.reset ();
            }
          else
            {
              throw CHeadDefinitionException (_T ("head definitions file is corrupt: ") + m_fileName);
            }
        }
      else
        {
          Destroy (m_headDefinitions);
        }
    }
  catch (CArchiveException * oe)
    {
      try
        {
          CFile l_file (m_fileName, FILE_OPEN_R_FLAGS);

          CArchive l_archive (&l_file, CArchive::load);

          CHeadDefinitionListPtr l_headDefinitions (new CHeadDefinitionList);

          l_headDefinitions->Serialize (l_archive);

          Destroy (m_headDefinitions);

          m_headDefinitions = l_headDefinitions;

          oe->Delete ();
        }
      catch (CException * ie)
        {
          ie->Delete ();

          throw CHeadDefinitionException (oe);
        }
    }
  catch (CException * e)
    {
      throw CHeadDefinitionException (e);
    }
}

void
CHeadDefinitions::Store (void)
{
  // !!! THIS MUST BE IN THE SAME CONTEXT IN WHICH SERIALIZATION OCCURS !!!

  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  if (::PathFileExists (m_fileName))
    {
      CString l_fileName (m_fileName);

      ::PathRemoveExtension (l_fileName.GetBuffer (0));

      l_fileName.ReleaseBuffer ();

      l_fileName += CTime::GetCurrentTime ().Format (_T ("-%Y%m%d%H%M%S"));

      l_fileName += CString (::PathFindExtension (m_fileName));

      if (::CopyFile (m_fileName, l_fileName, TRUE) == FALSE)
        {
          throw CHeadDefinitionException ();
        }
    }

  try
    {
      CFile l_file (m_fileName, FILE_OPEN_W_FLAGS);

      CArchive l_archive (&l_file, CArchive::store);

      l_archive.WriteObject (this);
    }
  catch (CException * e)
    {
      throw CHeadDefinitionException (e);
    }
}

POSITION
CHeadDefinitions::GetHeadPosition (void) const
{
  return m_headDefinitions->GetHeadPosition ();
}

CHeadDefinition *
CHeadDefinitions::GetNext (POSITION & pos)
{
  return m_headDefinitions->GetNext (pos);
}

CHeadDefinition *
CHeadDefinitions::GetAt (POSITION pos)
{
  return m_headDefinitions->GetAt (pos);
}

CHeadDefinition *
CHeadDefinitions::Find (CString const & identifier)
{
  POSITION l_pos (GetHeadPosition ());

  while (l_pos)
    {
      CHeadDefinition * const l_headDefinition (GetNext (l_pos));

      if (l_headDefinition->GetIdentifier () == identifier)
        {
          return l_headDefinition;
        }
    }

  throw CHeadDefinitionException (_T ("undefined head type: ") + identifier);
}

POSITION
CHeadDefinitions::AddTail (CString const & identifier)
{
  if (identifier.IsEmpty ())
    {
      throw CHeadDefinitionException (_T ("head definition identifier must be nonempty"));
    }
  else if (std::regex_match (static_cast <LPCTSTR> (identifier), CRegex (_T ("[A-Z0-9]*"))))
    {
      POSITION l_pos (GetHeadPosition ());

      while (l_pos)
        {
          CHeadDefinition * const l_headDefinition (GetNext (l_pos));

          if (l_headDefinition->GetIdentifier () == identifier)
            {
              throw CHeadDefinitionException (_T ("head definition identifier must be unique: ") + identifier);
            }
        }
    }
  else
    {
      throw CHeadDefinitionException (_T ("invalid head definition identifier: ") + identifier);
    }

  return m_headDefinitions->AddTail (new CHeadDefinition (identifier));
}

void
CHeadDefinitions::RemoveAt (POSITION pos)
{
  delete GetAt (pos);

  m_headDefinitions->RemoveAt (pos);
}

void
CHeadDefinitions::Serialize (CArchive & ar)
{
  CObject::Serialize (ar);

  if (ar.IsLoading ())
    {
      switch (ar.GetObjectSchema ())
        {
          case HEAD_DEFINITIONS_SCHEMA_VERSION_1:
            {
              m_headDefinitions->Serialize (ar);

              break;
            }
          default:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
        }
    }
  else
    {
      m_headDefinitions->Serialize (ar);
    }
}

void
CHeadDefinitions::Destroy (CHeadDefinitionListPtr const & headDefinitionList)
{
  if (headDefinitionList)
    {
      POSITION l_pos (headDefinitionList->GetHeadPosition ());

      while (l_pos)
        {
          delete headDefinitionList->GetNext (l_pos);
        }

      headDefinitionList->RemoveAll ();
    }
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  09/13/2010  MCC     initial revision
//  06/22/2011  MCC     added buttons to edit definitions dialogs
//  12/17/2014  MCC     replaced auto pointers with C++11 unique pointers
//
// ============================================================================
