// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: Celdyne.cpp
//
//     Description: Celdyne Beckhoff class definition
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: celdyne.cpp %
//        %version: 9.1.2 %
//          %state: %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "StdAfx.h"
#include "Celdyne.h"
#include "DeviceVisitor.h"
#include "HaspAdapter.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace
{
  CString const DEFAULT_CALIBRATION_FILE_NAME      (_T ("\\Calibration\\Celdyne\\caldev0.csv"));
  CString const DEFAULT_TEMPLATE_FILE_NAME         (_T ("\\Calibration\\Celdyne\\template0.pdt"));
  CString const DEFAULT_LOCATIONS_FILE_NAME        (_T ("\\Calibration\\Celdyne\\locations0.loc"));
  CString const DEFAULT_PLATE_DEFINTIONS_FILE_NAME (_T ("\\Calibration\\Celdyne\\plateDefinitions.plt"));
  int     const DEFAULT_ADS_CONTROLLER_ID          (0);
  WORD    const DEFAULT_ANALOG_PORT_NUMBER         (302);
  WORD    const DEFAULT_DISCRETE_PORT_NUMBER       (301);
  bool    const DEFAULT_CELDYNE_PRESENT            (true);
  bool    const DEFAULT_GRIPPER_PRESENT            (false);
  bool    const DEFAULT_SHAKER_PRESENT             (false);
  int     const DEFAULT_SHAKER_PORT_NUMBER         (1);
  bool    const DEFAULT_SHAKER_SIMULATION          (false);
}

namespace PDCLib
{
IMPLEMENT_SERIAL (CCeldyne, CDevice, VERSIONABLE_SCHEMA | TOPOLOGY_SCHEMA_VERSION_26)

CCeldyne::CCeldyne (void)
  : CDevice (DEFAULT_CALIBRATION_FILE_NAME, DEFAULT_TEMPLATE_FILE_NAME)
  , m_adsControllerID (DEFAULT_ADS_CONTROLLER_ID)
  , m_analogPortNumber (DEFAULT_ANALOG_PORT_NUMBER)
  , m_discretePortNumber (DEFAULT_DISCRETE_PORT_NUMBER)
  , m_celdynePresent (DEFAULT_CELDYNE_PRESENT)
  , m_gripperPresent (DEFAULT_GRIPPER_PRESENT)
  , m_shakerPresent (DEFAULT_SHAKER_PRESENT)
  , m_shakerPortNumber (DEFAULT_SHAKER_PORT_NUMBER)
  , m_shakerSimulation (DEFAULT_SHAKER_SIMULATION)
  , m_locationsFileName (GetAppDataFileName (DEFAULT_LOCATIONS_FILE_NAME))
  , m_plateDefinitionsFileName (GetAppDataFileName (DEFAULT_PLATE_DEFINTIONS_FILE_NAME))
{
}

CCeldyne::~CCeldyne ()
{
}

bool
CCeldyne::IsFeaturePresent (void) const
{
  // ============================================================================
  //  CRITICAL SECURITY NOTE:
  //
  //  All new devices must have a unique HASP feature ID associated with them.
  //  The default feature ID is to maintain backward compatibility with legacy
  //  devices. The obsolete feature ID is to deprecate devices which no longer
  //  exist. Moving forward we must have device-specific HASP keys in order to
  //  have a finer level of control over how the software is used.
  //
  // ============================================================================

  return CHaspAdapter::HasFeature (CHaspAdapter::EHaspFeature::haspCeldyne) ||
         CHaspAdapter::HasFeature (CHaspAdapter::EHaspFeature::haspGNFDeveloper) ||
         CHaspAdapter::HasFeature (CHaspAdapter::EHaspFeature::haspGNFService);
}

void
CCeldyne::Accept (CDeviceVisitor & deviceVisitor)
{
  deviceVisitor.Visit (this);
}

void
CCeldyne::Serialize (CArchive & ar)
{
  CDevice::Serialize (ar);

  if (ar.IsLoading ())
    {
      switch (GetObjectSchema ())
        {
          case TOPOLOGY_SCHEMA_VERSION_0:
          case TOPOLOGY_SCHEMA_VERSION_1:
          case TOPOLOGY_SCHEMA_VERSION_2:
          case TOPOLOGY_SCHEMA_VERSION_3:
          case TOPOLOGY_SCHEMA_VERSION_4:
          case TOPOLOGY_SCHEMA_VERSION_5:
          case TOPOLOGY_SCHEMA_VERSION_6:
          case TOPOLOGY_SCHEMA_VERSION_7:
          case TOPOLOGY_SCHEMA_VERSION_8:
          case TOPOLOGY_SCHEMA_VERSION_9:
          case TOPOLOGY_SCHEMA_VERSION_10:
          case TOPOLOGY_SCHEMA_VERSION_11:
          case TOPOLOGY_SCHEMA_VERSION_12:
          case TOPOLOGY_SCHEMA_VERSION_13:
          case TOPOLOGY_SCHEMA_VERSION_14:
          case TOPOLOGY_SCHEMA_VERSION_16:
          case TOPOLOGY_SCHEMA_VERSION_17:
          case TOPOLOGY_SCHEMA_VERSION_18:
          case TOPOLOGY_SCHEMA_VERSION_19:
          case TOPOLOGY_SCHEMA_VERSION_20:
          case TOPOLOGY_SCHEMA_VERSION_21:
          case TOPOLOGY_SCHEMA_VERSION_22:
          case TOPOLOGY_SCHEMA_VERSION_23:
          case TOPOLOGY_SCHEMA_VERSION_24:
            {
              ar >> m_adsControllerID;
              ar >> m_analogPortNumber;
              ar >> m_discretePortNumber;
              ar >> m_gripperPresent;
              ar >> m_shakerPresent;
              ar >> m_shakerPortNumber;
              ar >> m_shakerSimulation;
              ar >> m_locationsFileName;
              ar >> m_plateDefinitionsFileName;

              break;
            }
          case TOPOLOGY_SCHEMA_VERSION_25:
          case TOPOLOGY_SCHEMA_VERSION_26:
            {
              ar >> m_adsControllerID;
              ar >> m_analogPortNumber;
              ar >> m_discretePortNumber;
              ar >> m_celdynePresent;
              ar >> m_gripperPresent;
              ar >> m_shakerPresent;
              ar >> m_shakerPortNumber;
              ar >> m_shakerSimulation;
              ar >> m_locationsFileName;
              ar >> m_plateDefinitionsFileName;

              break;
            }
          default:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
        }
    }
  else
    {
      ar << m_adsControllerID;
      ar << m_analogPortNumber;
      ar << m_discretePortNumber;
      ar << m_celdynePresent;
      ar << m_gripperPresent;
      ar << m_shakerPresent;
      ar << m_shakerPortNumber;
      ar << m_shakerSimulation;
      ar << m_locationsFileName;
      ar << m_plateDefinitionsFileName;
    }
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  05/06/2014  MCC     initial revision
//  05/08/2014  MCC     enabled Celdyne feature in debug build
//  05/08/2014  MCC     implemented Celdyne gripper and shaker options
//  09/11/2014  MCC     implemented support for developer and service features
//  11/24/2014  MEG     added second generation lumi reader
//  01/22/2015  MCC     added stacker feature
//  01/27/2015  TAN     removed hasStacker boolean on G2Dispenser
//  03/04/2015  MCC     added service feature to device feature present
//  11/04/2015  MCC     implemented support for simulation mode at device level
//  10/20/2017  MCC     implemented support for optional Celdyne presence
//  07/12/2018  MCC     implemented support for encrypted calibrations
//
// ============================================================================
