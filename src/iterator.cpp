// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: Iterator.cpp
//
//     Description: Iterator class defintion
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: iterator.cpp %
//        %version: 21 %
//          %state: %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "StdAfx.h"
#include "Iterator.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace PDCLib
{
IMPLEMENT_SERIAL (CIterator, CObject, VERSIONABLE_SCHEMA | METHOD_SCHEMA_VERSION_13)

CIterator::CIterator (void)
  : m_lowerBound (1)
  , m_upperBound (1)
  , m_increment (1)
{
}

CIterator::CIterator (const CIterator & other)
  : m_lowerBound (other.m_lowerBound)
  , m_upperBound (other.m_upperBound)
  , m_increment (other.m_increment)
  , m_comment (other.m_comment)
{
}

CIterator::~CIterator ()
{
  while (!m_stepList.IsEmpty ())
    {
      delete m_stepList.RemoveHead ();
    }
}

void
CIterator::RemoveStep (POSITION pos, bool deleteStep)
{
  if (deleteStep)
    {
      delete m_stepList.GetAt (pos);
    }

  m_stepList.RemoveAt (pos);
}

CStep *
CIterator::GetStep (const CString & stepAlias) const
{
  CStep * l_step (NULL);

  for (POSITION l_pos (GetFirstStep ()); l_pos; )
    {
      CStep * const l_step_ (GetNextStep (l_pos));

      if (l_step_->GetAlias () == stepAlias)
        {
          l_step = l_step_;

          break;
        }
    }

  return l_step;
}

CVariable *
CIterator::GetVariable (const CString & stepAlias, const CString & variableAlias) const
{
  CStep const * const l_step (GetStep (stepAlias));

  return l_step ? l_step->GetVariable (variableAlias) : NULL;
}

CIterator *
CIterator::Duplicate (bool copyVariables) const
{
  CIterator * const l_iterator (new CIterator (*this));

  POSITION l_pos (GetFirstStep ());

  while (l_pos != NULL)
    {
      l_iterator->AddStep (GetNextStep (l_pos)->Duplicate (copyVariables));
    }

  return l_iterator;
}

void
CIterator::Serialize (CArchive & ar)
{
  if (ar.IsLoading ())
    {
      CObject::Serialize (ar);

      switch (ar.GetObjectSchema ())
        {
          case METHOD_SCHEMA_VERSION_0:
          case METHOD_SCHEMA_VERSION_1:
          case METHOD_SCHEMA_VERSION_2:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
          case METHOD_SCHEMA_VERSION_3:
          case METHOD_SCHEMA_VERSION_4:
          case METHOD_SCHEMA_VERSION_5:
          case METHOD_SCHEMA_VERSION_6:
            {
              bool l_isConstant;

              ar >> m_upperBound;
              ar >> l_isConstant;

              m_stepList.Serialize (ar);

              break;
            }
          case METHOD_SCHEMA_VERSION_7:
          case METHOD_SCHEMA_VERSION_8:
          case METHOD_SCHEMA_VERSION_9:
          case METHOD_SCHEMA_VERSION_10:
          case METHOD_SCHEMA_VERSION_11:
            {
              ar >> m_lowerBound;
              ar >> m_upperBound;
              ar >> m_increment;

              m_stepList.Serialize (ar);

              break;
            }
          case METHOD_SCHEMA_VERSION_12:
          case METHOD_SCHEMA_VERSION_13:
            {
              ar >> m_lowerBound;
              ar >> m_upperBound;
              ar >> m_increment;
              ar >> m_comment;

              m_stepList.Serialize (ar);

              break;
            }
          default:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
        }
    }
  else
    {
      CObject::Serialize (ar);

      ar << m_lowerBound;
      ar << m_upperBound;
      ar << m_increment;
      ar << m_comment;

      m_stepList.Serialize (ar);
    }
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  10/23/2003  MCC     initial revision
//  10/31/2003  MCC     modified for QAC++ compliance
//  04/01/2004  MCC     implemented support for cut/copy/paste operations
//  07/19/2005  MCC     replaced all double-underscores
//  01/11/2006  MCC     removed workaround for MFC serialization memory leak
//  03/22/2006  MEG     updated version schema
//  04/14/2006  MEG     modified to use namespace PDCLib
//  11/14/2006  MCC     added fully qualified variable accessor
//  11/28/2006  MCC     baselined serializable topology class hierarchy
//  12/06/2006  MCC     decoupled method and topology version schema constants
//  07/03/2006  MCC     implemented skip step feature
//  10/09/2008  MCC     implemented interface for skipping steps
//  04/28/2009  MCC     implemented support for drag and drop template editing
//  03/18/2010  MCC     implemented support for plate definition variable type
//  08/09/2010  MCC     enhanced functionality of step iterator
//  08/10/2010  MCC     implemented support for variable grouping
//  02/18/2011  MCC     implemented support for method variable logging
//  03/22/2011  MCC     implemented support modifying enumeration keys
//  07/03/2012  MCC     added support for variable specific plate definitions
//  07/20/2012  MCC     implemented support for iterator comment
//  07/17/2014  MCC     implemented support for executing shell commands
//
// ============================================================================
