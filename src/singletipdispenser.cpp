// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: SingleTipDispenser.cpp
//
//     Description: Single Tip Dispenser class definition
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: singletipdispenser.cpp %
//        %version: 8.1.2 %
//          %state: %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "StdAfx.h"
#include "SingleTipDispenser.h"
#include "DeviceVisitor.h"
#include "HaspAdapter.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace
{
  CString const DEFAULT_CALIBRATION_FILE_NAME           (_T ("\\Calibration\\SingleTipDispenser\\caldev0.csv"));
  CString const DEFAULT_TEMPLATE_FILE_NAME              (_T ("\\Calibration\\SingleTipDispenser\\template0.pdt"));
  CString const DEFAULT_ENCRYPTED_CALIBRATION_FILE_NAME (_T ("\\Calibration\\SingleTipDispenser\\caldev0.bin"));
  CString const DEFAULT_HOST_ADDRESS                    (_T ("192.168.0.2"));
  WORD    const DEFAULT_HOST_PORT_NUMBER                (5000);
}

namespace PDCLib
{
IMPLEMENT_SERIAL (CSingleTipDispenser, CDevice, VERSIONABLE_SCHEMA | TOPOLOGY_SCHEMA_VERSION_26)

CSingleTipDispenser::CSingleTipDispenser (void)
  : CDevice (DEFAULT_CALIBRATION_FILE_NAME, DEFAULT_TEMPLATE_FILE_NAME, DEFAULT_ENCRYPTED_CALIBRATION_FILE_NAME)
  , m_hostAddress (DEFAULT_HOST_ADDRESS)
  , m_hostPortNumber (DEFAULT_HOST_PORT_NUMBER)
{
}

CSingleTipDispenser::~CSingleTipDispenser ()
{
}

bool
CSingleTipDispenser::IsFeaturePresent (void) const
{
  // ============================================================================
  //  CRITICAL SECURITY NOTE:
  //
  //  All new devices must have a unique HASP feature ID associated with them.
  //  The default feature ID is to maintain backward compatibility with legacy
  //  devices. The obsolete feature ID is to deprecate devices which no longer
  //  exist. Moving forward we must have device-specific HASP keys in order to
  //  have a finer level of control over how the software is used.
  //
  // ============================================================================

  return CHaspAdapter::HasFeature (CHaspAdapter::EHaspFeature::haspSingleTipDispenser) ||
         CHaspAdapter::HasFeature (CHaspAdapter::EHaspFeature::haspGNFDeveloper) ||
         CHaspAdapter::HasFeature (CHaspAdapter::EHaspFeature::haspGNFService);
}

void
CSingleTipDispenser::Accept (CDeviceVisitor & deviceVisitor)
{
  deviceVisitor.Visit (this);
}

void
CSingleTipDispenser::Serialize (CArchive & ar)
{
  CDevice::Serialize (ar);

  if (ar.IsLoading ())
    {
      switch (GetObjectSchema ())
        {
          case TOPOLOGY_SCHEMA_VERSION_0:
          case TOPOLOGY_SCHEMA_VERSION_1:
          case TOPOLOGY_SCHEMA_VERSION_2:
          case TOPOLOGY_SCHEMA_VERSION_3:
          case TOPOLOGY_SCHEMA_VERSION_4:
          case TOPOLOGY_SCHEMA_VERSION_5:
          case TOPOLOGY_SCHEMA_VERSION_6:
          case TOPOLOGY_SCHEMA_VERSION_7:
          case TOPOLOGY_SCHEMA_VERSION_8:
          case TOPOLOGY_SCHEMA_VERSION_9:
          case TOPOLOGY_SCHEMA_VERSION_10:
          case TOPOLOGY_SCHEMA_VERSION_11:
          case TOPOLOGY_SCHEMA_VERSION_12:
          case TOPOLOGY_SCHEMA_VERSION_13:
          case TOPOLOGY_SCHEMA_VERSION_14:
          case TOPOLOGY_SCHEMA_VERSION_15:
          case TOPOLOGY_SCHEMA_VERSION_16:
          case TOPOLOGY_SCHEMA_VERSION_17:
          case TOPOLOGY_SCHEMA_VERSION_18:
          case TOPOLOGY_SCHEMA_VERSION_19:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
          case TOPOLOGY_SCHEMA_VERSION_20:
          case TOPOLOGY_SCHEMA_VERSION_21:
          case TOPOLOGY_SCHEMA_VERSION_22:
          case TOPOLOGY_SCHEMA_VERSION_23:
          case TOPOLOGY_SCHEMA_VERSION_24:
          case TOPOLOGY_SCHEMA_VERSION_25:
          case TOPOLOGY_SCHEMA_VERSION_26:
            {
              ar >> m_hostAddress;
              ar >> m_hostPortNumber;

              break;
            }
          default:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
        }
    }
  else
    {
      ar << m_hostAddress;
      ar << m_hostPortNumber;
    }
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  05/08/2013  MCC     initial revision
//  03/27/2014  MEG     implemented optional barcode reader for lumi reader
//  09/11/2014  MCC     implemented support for developer and service features
//  11/24/2014  MEG     added second generation lumi reader
//  01/22/2015  MCC     added stacker feature
//  01/27/2015  TAN     removed hasStacker boolean on G2Dispenser
//  03/04/2015  MCC     added service feature to device feature present
//  11/04/2015  MCC     implemented support for simulation mode at device level
//  10/20/2017  MCC     implemented support for optional Celdyne presence
//  07/12/2018  MCC     implemented support for encrypted calibrations
//
// ============================================================================
