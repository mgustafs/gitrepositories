// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: IPlateDefinition.cpp
//
//     Description: Plate Definition Interface class definition
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: iplatedefinition.cpp %
//        %version: 3 %
//          %state: %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "StdAfx.h"
#include "IPlateDefinition.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace PDCLib
{
_IMPLEMENT_RUNTIMECLASS (IPlateDefinition, CObject, 0xFFFF, NULL, NULL)

IPlateDefinition::IPlateDefinition (void)
{
}

IPlateDefinition::~IPlateDefinition ()
{
}

void
IPlateDefinition::Serialize (CArchive & ar)
{
  CObject::Serialize (ar);
}

CPlateDefinitionException::CPlateDefinitionException (void)
  : m_errorMessage (PDCLib::GetErrorMessage (::GetLastError ()))
{
}

CPlateDefinitionException::CPlateDefinitionException (CException * e, bool autoDelete)
  : m_errorMessage (PDCLib::GetErrorMessage (e, autoDelete))
{
}

CPlateDefinitionException::CPlateDefinitionException (LPCTSTR format, ...)
{
  va_list args;

  va_start (args, format);

  m_errorMessage.FormatV (format, args);

  va_end (args);
}

CPlateDefinitionException::CPlateDefinitionException (CPlateDefinitionException const & other)
  : m_errorMessage (other.m_errorMessage)
{
}

CPlateDefinitionException::~CPlateDefinitionException ()
{
}

CString
CPlateDefinitionException::GetErrorMessage (void) const
{
  return m_errorMessage;
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  11/01/2013  MCC     initial revision
//  11/05/2013  MCC     generalized plate definition exception constructor
//  11/11/2013  MCC     removed unused local variable
//
// ============================================================================
