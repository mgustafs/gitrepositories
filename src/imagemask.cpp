// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: ImageMask.cpp
//
//     Description: Image Mask class definition
//
//          Author: Marc Gustafson
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: imagemask.cpp %
//        %version: 9 %
//          %state: %
//         %cvtype: c++ %
//     %derived_by: mgustafs %
//  %date_modified: %
//
// ============================================================================

#include "StdAfx.h"
#include "ImageMask.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace
{
  const UINT IMAGE_MASK_SCHEMA_VERSION_1 (1);
  const UINT IMAGE_MASK_SCHEMA_VERSION_2 (2);

  const int  MAX_IMAGE_SIZE              (2100);
}

namespace PDCLib
{
IMPLEMENT_SERIAL (CImageMask, CObject, VERSIONABLE_SCHEMA | IMAGE_MASK_SCHEMA_VERSION_2)

CImageMask::CImageMask (void)
  : m_pixelsPerWell (1)
  , m_plateOrigin (0, 0)
  , m_maskOrigin (0, 0)
  , m_pixelsPerMM (0.1)
  , m_timeStamp (CTime::GetCurrentTime ())
{
}

CImageMask::CImageMask (CString const & identifier)
  : m_identifier (identifier)
  , m_pixelsPerWell (1)
  , m_plateOrigin (0, 0)
  , m_maskOrigin (0, 0)
  , m_pixelsPerMM (0.1)
  , m_timeStamp (CTime::GetCurrentTime ())
{
}

CImageMask::CImageMask (int pixelsPerWell, CPoint const & plateOrigin, CPoint const & maskOrigin, double pixelsPerMM)
  : m_pixelsPerWell (pixelsPerWell)
  , m_plateOrigin (plateOrigin)
  , m_maskOrigin (maskOrigin)
  , m_pixelsPerMM (pixelsPerMM)
{
}

CImageMask::~CImageMask ()
{
}

void
CImageMask::SetImageMask (std::vector <WORD> & mask,
                          int                & horizontalSize,
                          int                & verticalSize,
                          int                  numRows,
                          int                  numColumns,
                          double               rowSpacing,
                          double               columnSpacing) const
{
  const int l_plateHorizontalMargin (std::max (0L, m_maskOrigin.x - m_plateOrigin.x));
  const int l_plateVerticalMargin (std::max (0L, m_maskOrigin.y - m_plateOrigin.y));
  const double l_initialHorizontalOffset (l_plateHorizontalMargin + std::max (0.0, columnSpacing / 2.0 * m_pixelsPerMM - m_pixelsPerWell));
  const double l_initialVerticalOffset (l_plateVerticalMargin + std::max (0.0, rowSpacing / 2.0 * m_pixelsPerMM - m_pixelsPerWell));
  const double l_horizontalWellWidth (columnSpacing * m_pixelsPerMM);
  const double l_verticalWellWidth (rowSpacing * m_pixelsPerMM);
  const int l_numWells (numColumns * numRows);
  horizontalSize = static_cast <int> (l_initialHorizontalOffset + l_verticalWellWidth * numColumns + l_plateHorizontalMargin);
  verticalSize = static_cast <int> (l_initialVerticalOffset + l_horizontalWellWidth * numRows + l_plateVerticalMargin);

  if ((horizontalSize > 0) &&
      (horizontalSize <= MAX_IMAGE_SIZE) &&
      (verticalSize > 0) &&
      (verticalSize <= MAX_IMAGE_SIZE))
    {
      mask.resize (horizontalSize * verticalSize);

      if (((horizontalSize * (l_initialVerticalOffset + (l_verticalWellWidth * (numRows - 1)))) + 
            l_initialHorizontalOffset + (l_horizontalWellWidth * (numColumns - 1)) +            
            m_pixelsPerWell + (horizontalSize * m_pixelsPerWell)) < mask.size ())
        {
          for (int l_i (0); l_i < l_numWells; ++l_i)
            {
              const int l_rowOfWell (l_i / numColumns);
              const int l_columnOfWell (l_i % numColumns);
              const int l_verticalOffset (horizontalSize * static_cast <int> (l_rowOfWell * l_verticalWellWidth + l_initialVerticalOffset));
              const int l_horizontalOffset (static_cast <int> (l_columnOfWell * l_horizontalWellWidth + l_initialHorizontalOffset));
              const int l_wellOffset (l_verticalOffset + l_horizontalOffset);

              for (int l_j (0); l_j < m_pixelsPerWell; ++l_j)
                {
                  for (int l_k (0); l_k < m_pixelsPerWell; ++l_k)
                    {
                      mask[l_wellOffset + l_k + horizontalSize * l_j] = static_cast <WORD> (l_i + 1);
                    }
                }
            }
        }
      else
        {
          throw CImageMaskException (_T ("Pixels per well is too large to fit within a well of image mask this size"));
        }
    }
  else
    {
      CString errMsg;
      errMsg.Format (_T ("Image mask must fit within total image size between 0 and %d"), MAX_IMAGE_SIZE);
      throw CImageMaskException (errMsg);
    }
}

void
CImageMask::Serialize (CArchive & ar)
{
  CObject::Serialize (ar);

  if (ar.IsLoading ())
    {
      switch (ar.GetObjectSchema ())
        {
          case IMAGE_MASK_SCHEMA_VERSION_1:
            {
              ar >> m_identifier;
              ar >> m_pixelsPerWell;
              ar >> m_maskOrigin;
              ar >> m_pixelsPerMM;
              ar >> m_comment;
              ar >> m_timeStamp;

              break;
            }
          case IMAGE_MASK_SCHEMA_VERSION_2:
            {
              ar >> m_identifier;
              ar >> m_pixelsPerWell;
              ar >> m_plateOrigin;
              ar >> m_maskOrigin;
              ar >> m_pixelsPerMM;
              ar >> m_comment;
              ar >> m_timeStamp;

              break;
            }
          default:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
        }
    }
  else
    {
      ar << m_identifier;
      ar << m_pixelsPerWell;
      ar << m_plateOrigin;
      ar << m_maskOrigin;
      ar << m_pixelsPerMM;
      ar << m_comment;
      ar << m_timeStamp;
    }
}

CImageMaskException::CImageMaskException (void)
  : m_errorMessage (PDCLib::GetErrorMessage (::GetLastError ()))
{
}

CImageMaskException::CImageMaskException (CException * e, bool autoDelete)
  : m_errorMessage (PDCLib::GetErrorMessage (e, autoDelete))
{
}

CImageMaskException::CImageMaskException (CString const & errorMessage)
  : m_errorMessage (errorMessage)
{
}

CImageMaskException::CImageMaskException (CImageMaskException const & other)
  : m_errorMessage (other.m_errorMessage)
{
}

CImageMaskException::~CImageMaskException ()
{
}

CString
CImageMaskException::GetErrorMessage (void) const
{
  return m_errorMessage;
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  09/09/2010  MEG     initial revision
//  09/16/2010  MEG     added horizontal and vertical size setting to mask
//  09/20/2010  MCC     implemented support for image mask visualization
//  09/21/2010  MEG     fixed bug with image mask calculation
//  09/22/2010  MCC     fixed bug with image mask calculation
//  09/22/2010  MEG     add error checking to image mask calculation
//  03/21/2014  MEG     updated max image size for lumi reader
//  01/09/2015  MEG     updated max image size for 2nd gen lumi reader
//  03/22/2016  MEG     added plate origin to mask to support wider image view
//
// ============================================================================
