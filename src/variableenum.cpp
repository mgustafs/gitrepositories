// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: VariableEnum.cpp
//
//     Description: Variable enumeration class definition
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: variableenum.cpp %
//        %version: 29 %
//          %state: working %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: Monday, October 07, 2002 1:00:31 PM %
//
// ============================================================================

#include "StdAfx.h"
#include "VariableEnum.h"
#include "EnumAssoc.h"
#include "VariableVisitor.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace PDCLib
{

IMPLEMENT_SERIAL (CVariableEnum, CVariable, VERSIONABLE_SCHEMA | METHOD_SCHEMA_VERSION_13)

CVariableEnum::CVariableEnum (void)
  : m_inhibitKeyModify (true)
{
}

CVariableEnum::CVariableEnum (const CVariableEnum & other)
  : CVariable (other)
  , m_inhibitKeyModify (other.m_inhibitKeyModify)
  , m_value (other.m_value)
{
  POSITION l_pos (other.GetFirstAssoc ());

  while (l_pos)
    {
      CString l_key;
      VARIANT l_value;

      other.GetNextAssoc (l_pos, l_key, l_value);

      RegisterAssoc (l_key, l_value);
    }
}

CVariableEnum::~CVariableEnum ()
{
  RemoveAllAssoc ();
}

void
CVariableEnum::RegisterAssoc (const CString & key, VARIANT const & value)
{
  VARIANT l_value;

  if (Lookup (key, l_value))
    {
      ASSERT (false);
    }
  else
    {
      m_assocList.AddTail (new CEnumAssoc (key, value));
    }
}

void
CVariableEnum::GetNextAssoc (POSITION & pos, CString & key, VARIANT & value) const
{
  ASSERT (pos != NULL);

  CEnumAssoc const * const l_enumAssoc (dynamic_cast <const CEnumAssoc *> (m_assocList.GetNext (pos)));

  ASSERT_VALID (l_enumAssoc);

  key   = l_enumAssoc->GetKey ();
  value = l_enumAssoc->GetValue ();
}

void
CVariableEnum::RemoveAllAssoc (void)
{
  while (!m_assocList.IsEmpty ())
    {
      delete m_assocList.RemoveHead ();
    }
}

void
CVariableEnum::Serialize (CArchive & ar)
{
  if (ar.IsLoading ())
    {
      CVariable::Serialize (ar);

      switch (GetObjectSchema ())
        {
          case METHOD_SCHEMA_VERSION_0:
          case METHOD_SCHEMA_VERSION_1:
          case METHOD_SCHEMA_VERSION_2:
          case METHOD_SCHEMA_VERSION_3:
          case METHOD_SCHEMA_VERSION_4:
          case METHOD_SCHEMA_VERSION_5:
          case METHOD_SCHEMA_VERSION_6:
          case METHOD_SCHEMA_VERSION_7:
          case METHOD_SCHEMA_VERSION_8:
          case METHOD_SCHEMA_VERSION_9:
            {
              ar >> m_value;

              m_assocList.Serialize (ar);

              break;
            }
          case METHOD_SCHEMA_VERSION_10:
          case METHOD_SCHEMA_VERSION_11:
          case METHOD_SCHEMA_VERSION_12:
          case METHOD_SCHEMA_VERSION_13:
            {
              ar >> m_inhibitKeyModify;
              ar >> m_value;

              m_assocList.Serialize (ar);

              break;
            }
          default:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
        }
    }
  else
    {
      CVariable::Serialize (ar);

      ar << m_inhibitKeyModify;
      ar << m_value;

      m_assocList.Serialize (ar);
    }
}

CString
CVariableEnum::GetFormattedVariable (void) const
{
  return m_value;
}

CString
CVariableEnum::GetPrintFormattedVariable (bool isTemplateView) const
{
  return GetPrintFormattedLVal (isTemplateView) + GetFormattedVariable () + GetPrintFormattedModifiers (isTemplateView);
}

_variant_t
CVariableEnum::GetValue_ (void) const
{
  _variant_t l_value;

  VERIFY (Lookup (m_value, l_value) && ((l_value.vt == VT_I4) || (l_value.vt == VT_R8)));

  return l_value;
}

bool
CVariableEnum::Lookup (const CString & key, VARIANT & value) const
{
  bool retval (false);

  POSITION l_pos (GetFirstAssoc ());

  while (l_pos != NULL)
    {
      CString l_key;

      GetNextAssoc (l_pos, l_key, value);

      if (l_key == key)
        {
          retval = true;

          break;
        }
    }

  return retval;
}

void
CVariableEnum::Accept (CVariableVisitor & variableVisitor)
{
  variableVisitor.Visit (this);
}

}  // end namespace PDCLib

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  08/05/2002  MCC     initial revision
//  12/11/2002  MCC     removed const qualifiers
//  07/21/2003  MCC     updated version schema
//  07/30/2003  MCC     improved memory mangement with STL smart pointers
//  10/29/2003  MCC     implemented step interator feature
//  04/01/2004  MCC     implemented support for cut/copy/paste operations
//  04/15/2004  MCC     modified paste variable behavior
//  07/19/2005  MCC     replaced all double-underscores
//  01/11/2006  MCC     removed workaround for MFC serialization memory leak
//  01/30/2006  MCC     commonized document and print views
//  03/22/2006  MEG     updated version schema
//  04/14/2006  MEG     modified to use namespace PDCLib
//  12/05/2006  MCC     modified for QAC++ compliance
//  12/06/2006  MCC     decoupled method and topology version schema constants
//  06/27/2007  MEG     changed VARIANT interpretation from intVal to lVal
//  07/03/2006  MCC     implemented skip step feature
//  05/11/2009  MCC     implemented support for new document creation strategy
//  03/18/2010  MCC     implemented support for plate definition variable type
//  08/04/2010  MCC     implemented support for tree view icons
//  08/09/2010  MCC     enhanced functionality of step iterator
//  08/10/2010  MCC     implemented support for variable grouping
//  11/05/2010  MCC     implemented support for limited application access
//  02/18/2011  MCC     implemented support for method variable logging
//  03/22/2011  MCC     implemented support modifying enumeration keys
//  07/25/2011  MCC     modified to allow floating point enumerations
//  07/03/2012  MCC     added support for variable specific plate definitions
//  07/20/2012  MCC     implemented support for iterator comment
//  05/09/2013  MCC     refactored variable processing
//  07/17/2014  MCC     implemented support for executing shell commands
//
// ============================================================================
