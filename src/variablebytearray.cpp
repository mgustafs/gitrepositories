// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: VariableByteArray.cpp
//
//     Description: Variable byte array class definition
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: variablebytearray.cpp %
//        %version: 30 %
//          %state: %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "StdAfx.h"
#include "Bit.h"
#include "VariableByteArray.h"
#include "VariableVisitor.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace
{
  std::array <int, 9> const g_size
    {
        2, // Column8x12  (  96 well)
        3, // Column16x24 ( 384 well)
        6, // Column32x48 (1536 well)
        2, // Grid3x4     (  12 well, deprecated)
       12, // Grid8x12    (  96 well)
       48, // Grid16x32   ( 384 well)
      192, // Grid32x48   (1536 well)
        3, // Grid4x6     (  24 well)
        1  // Column4x6   (  24 well)
    };
}

namespace PDCLib
{
IMPLEMENT_SERIAL (CVariableByteArray, CVariable, VERSIONABLE_SCHEMA | METHOD_SCHEMA_VERSION_13)

CVariableByteArray::CVariableByteArray (EMaskType maskType)
  : m_maskType (maskType)
{
  m_value.SetSize (GetSize (maskType));
}

CVariableByteArray::CVariableByteArray (const CVariableByteArray & other)
  : CVariable (other)
  , m_maskType (other.m_maskType)
{
  m_value.Copy (other.m_value);
}

CVariableByteArray::~CVariableByteArray ()
{
}

void
CVariableByteArray::SetType (EMaskType maskType)
{
  m_maskType = maskType;

  m_value.SetSize (GetSize (maskType));
}

void
CVariableByteArray::SetValue (const CByteArray & value)
{
  ASSERT (m_value.GetSize () == value.GetSize ());
  ASSERT (GetSize (m_maskType) == m_value.GetSize ());

  m_value.Copy (value);
}

void
CVariableByteArray::GetValue (CByteArray & value) const
{
  value.SetSize (m_value.GetSize ());

  value.Copy (m_value);
}

int
CVariableByteArray::GetSize (EMaskType maskType)
{
  return g_size[static_cast <int> (maskType)];
}

void
CVariableByteArray::Serialize (CArchive & ar)
{
  if (ar.IsLoading ())
    {
      CVariable::Serialize (ar);

      switch (GetObjectSchema ())
        {
          case METHOD_SCHEMA_VERSION_0:
          case METHOD_SCHEMA_VERSION_1:
          case METHOD_SCHEMA_VERSION_2:
          case METHOD_SCHEMA_VERSION_3:
            {
              m_value.Serialize (ar);

              // set mask type once only to first match in size list...

              for (int l_i (0); static_cast <size_t> (l_i) < g_size.size (); ++l_i)
                {
                  if (g_size[l_i] == m_value.GetSize ())
                    {
                      m_maskType = static_cast <EMaskType> (l_i);

                      break;
                    }
                }

              break;
            }
          case METHOD_SCHEMA_VERSION_4:
          case METHOD_SCHEMA_VERSION_5:
          case METHOD_SCHEMA_VERSION_6:
          case METHOD_SCHEMA_VERSION_7:
          case METHOD_SCHEMA_VERSION_8:
          case METHOD_SCHEMA_VERSION_9:
          case METHOD_SCHEMA_VERSION_10:
          case METHOD_SCHEMA_VERSION_11:
          case METHOD_SCHEMA_VERSION_12:
          case METHOD_SCHEMA_VERSION_13:
            {
              ar >> m_maskType;

              m_value.Serialize (ar);

              break;
            }
          default:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
        }
    }
  else
    {
      CVariable::Serialize (ar);

      ar << m_maskType;

      m_value.Serialize (ar);
    }
}

CString
CVariableByteArray::GetFormattedVariable (void) const
{
  CString l_value;

  int const l_numBits (m_value.GetSize () * 8);
  int l_bitCount (0);

  for (int l_bit (1); l_bit <= l_numBits; ++l_bit)
    {
      if (l_bitCount > 0)
        {
          if (IsBitSet (m_value, l_bit - 1))
            {
              if (l_bit < l_numBits)
                {
                  ++l_bitCount;
                }
              else
                {
                  CString l_tmp;

                  l_tmp.Format (_T ("-%d"), l_bit);

                  l_value += l_tmp;
                }
            }
          else
            {
              if (l_bitCount > 1)
                {
                  CString l_tmp;

                  l_tmp.Format (_T ("-%d"), l_bit - 1);

                  l_value += l_tmp;
                }

              l_bitCount = 0;
            }
        }
      else if (IsBitSet (m_value, l_bit - 1))
        {
          if (l_value.IsEmpty ())
            {
              l_value.Format (_T ("%d"), l_bit);
            }
          else
            {
              CString l_tmp;

              l_tmp.Format (_T (",%d"), l_bit);

              l_value += l_tmp;
            }

          ++l_bitCount;
        }
    }

  return l_value.IsEmpty () ? _T ("--") : l_value;
}

CString
CVariableByteArray::GetPrintFormattedVariable (bool isTemplateView) const
{
  return GetPrintFormattedLVal (isTemplateView) + GetFormattedVariable () + GetPrintFormattedModifiers (isTemplateView);
}

void
CVariableByteArray::Accept (CVariableVisitor & variableVisitor)
{
  variableVisitor.Visit (this);
}

CArchive & operator>> (CArchive & ar, CVariableByteArray::EMaskType & rhs)
{
  ar.Read (&rhs, sizeof (rhs));

  return ar;
}

CArchive & operator<< (CArchive & ar, CVariableByteArray::EMaskType const & rhs)
{
  ar.Write (&rhs, sizeof (rhs));

  return ar;
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  05/30/2003  MCC     initial revision
//  07/21/2003  MCC     updated version schema
//  10/29/2003  MCC     implemented step interator feature
//  04/01/2004  MCC     implemented support for cut/copy/paste operations
//  04/15/2004  MCC     modified paste variable behavior
//  07/19/2005  MCC     replaced all double-underscores
//  09/22/2005  MEG     added size parameter to default constructor
//  01/11/2006  MCC     removed workaround for MFC serialization memory leak
//  01/30/2006  MCC     commonized document and print views
//  03/21/2006  MEG     modified interface to support more generic mask types
//  04/14/2006  MEG     modified to use namespace PDCLib
//  11/28/2006  MCC     baselined serializable topology class hierarchy
//  12/06/2006  MCC     decoupled method and topology version schema constants
//  07/03/2006  MCC     implemented skip step feature
//  05/11/2009  MCC     implemented support for new document creation strategy
//  03/18/2010  MCC     implemented support for plate definition variable type
//  08/04/2010  MCC     implemented support for tree view icons
//  08/09/2010  MCC     enhanced functionality of step iterator
//  08/10/2010  MCC     implemented support for variable grouping
//  11/05/2010  MCC     implemented support for limited application access
//  02/18/2011  MCC     implemented support for method variable logging
//  03/22/2011  MCC     implemented support modifying enumeration keys
//  05/19/2011  MCC     corrected problems with variable logging
//  07/03/2012  MCC     added support for variable specific plate definitions
//  07/20/2012  MCC     implemented support for iterator comment
//  05/09/2013  MCC     refactored variable processing
//  07/17/2014  MCC     implemented support for executing shell commands
//  01/09/2015  MCC     templatized number of elements macro
//  10/27/2015  MCC     implemented support for 24 well grid and column masks
//
// ============================================================================
