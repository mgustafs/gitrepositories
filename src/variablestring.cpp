// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: VariableString.cpp
//
//     Description: Variable string class definition
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: variablestring.cpp %
//        %version: 27 %
//          %state: %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: %
//
// ============================================================================

#include "StdAfx.h"
#include "VariableString.h"
#include "VariableVisitor.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace PDCLib
{
IMPLEMENT_SERIAL (CVariableString, CVariable, VERSIONABLE_SCHEMA | METHOD_SCHEMA_VERSION_13)

CVariableString::CVariableString (ESubtype subtype)
  : m_subtype (subtype)
  , m_referencePosition (EPosition::PositionNone)
  , m_inhibitEmpty ((subtype == PlateDefinition) || (subtype == ImageMask))
{
  if ((subtype == PlateDefinition) || (subtype == ImageMask) || (m_subtype == DirectoryName))
    {
      SetUnits (_T ("N/A"));
    }
}

CVariableString::CVariableString (const CVariableString & other)
  : CVariable (other)
  , m_value (other.m_value)
  , m_subtype (other.m_subtype)
  , m_referencePosition (other.m_referencePosition)
  , m_inhibitEmpty (other.m_inhibitEmpty)
{
}

CVariableString::~CVariableString ()
{
}

void
CVariableString::Serialize (CArchive & ar)
{
  if (ar.IsLoading ())
    {
      CVariable::Serialize (ar);

      switch (GetObjectSchema ())
        {
          case METHOD_SCHEMA_VERSION_0:
          case METHOD_SCHEMA_VERSION_1:
          case METHOD_SCHEMA_VERSION_2:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
          case METHOD_SCHEMA_VERSION_3:
          case METHOD_SCHEMA_VERSION_4:
          case METHOD_SCHEMA_VERSION_5:
            {
              bool l_isFilename (false);

              ar >> m_value;
              ar >> l_isFilename;
              ar >> m_inhibitEmpty;

              m_subtype = l_isFilename ? FileOpen : String;

              break;
            }
          case METHOD_SCHEMA_VERSION_6:
          case METHOD_SCHEMA_VERSION_7:
          case METHOD_SCHEMA_VERSION_8:
          case METHOD_SCHEMA_VERSION_9:
          case METHOD_SCHEMA_VERSION_10:
            {
              ar >> m_value;
              ar >> m_subtype;
              ar >> m_inhibitEmpty;

              break;
            }
          case METHOD_SCHEMA_VERSION_11:
          case METHOD_SCHEMA_VERSION_12:
          case METHOD_SCHEMA_VERSION_13:
            {
              ar >> m_value;
              ar >> m_subtype;
              ar >> m_referencePosition;
              ar >> m_inhibitEmpty;

              break;
            }
          default:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
        }
    }
  else
    {
      CVariable::Serialize (ar);

      ar << m_value;
      ar << m_subtype;
      ar << m_referencePosition;
      ar << m_inhibitEmpty;
    }
}

CString
CVariableString::GetFormattedVariable (void) const
{
  return m_value;
}

CString
CVariableString::GetPrintFormattedVariable (bool isTemplateView) const
{
  CString retval;

  if ((m_subtype == PlateDefinition) || (m_subtype == ImageMask) || (m_subtype == DirectoryName))
    {
      retval.Format (_T ("%s %s"), (LPCTSTR) GetPrintFormattedLVal (isTemplateView), (LPCTSTR) GetFormattedVariable ());
    }
  else
    {
      retval.Format (_T ("%s %s (%s)"), (LPCTSTR) GetPrintFormattedLVal (isTemplateView), (LPCTSTR) GetFormattedVariable (), (LPCTSTR) GetUnits ());
    }

  return retval + GetPrintFormattedModifiers (isTemplateView);
}

void
CVariableString::Accept (CVariableVisitor & variableVisitor)
{
  variableVisitor.Visit (this);
}

CArchive & operator>> (CArchive & ar, CVariableString::ESubtype & rhs)
{
  ar.Read (&rhs, sizeof (rhs));

  return ar;
}

CArchive & operator<< (CArchive & ar, CVariableString::ESubtype const & rhs)
{
  ar.Write (&rhs, sizeof (rhs));

  return ar;
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  04/29/2004  MCC     initial revision
//  04/29/2004  MCC     modified for QAC++ compliance
//  07/19/2005  MCC     replaced all double-underscores
//  01/11/2006  MCC     removed workaround for MFC serialization memory leak
//  01/30/2006  MCC     commonized document and print views
//  03/22/2006  MEG     updated version schema
//  04/14/2006  MEG     modified to use namespace PDCLib
//  11/28/2006  MCC     baselined serializable topology class hierarchy
//  12/06/2006  MCC     decoupled method and topology version schema constants
//  07/03/2006  MCC     implemented skip step feature
//  05/11/2009  MCC     implemented support for new document creation strategy
//  03/18/2010  MCC     implemented support for plate definition variable type
//  03/19/2010  MCC     corrected problem with display of units
//  08/04/2010  MCC     implemented support for tree view icons
//  08/09/2010  MCC     enhanced functionality of step iterator
//  08/10/2010  MCC     implemented support for variable grouping
//  09/14/2010  MCC     implemented user interface support for image mask
//  09/29/2010  MCC     implemented support for directory name subtype
//  11/05/2010  MCC     implemented support for limited application access
//  02/18/2011  MCC     implemented support for method variable logging
//  03/22/2011  MCC     implemented support modifying enumeration keys
//  07/03/2012  MCC     added support for variable specific plate definitions
//  07/20/2012  MCC     implemented support for iterator comment
//  05/09/2013  MCC     refactored variable processing
//  07/17/2014  MCC     implemented support for executing shell commands
//  04/10/2017  MEG     added cast to CString for resolving conversion warning
//  01/18/2018  MCC     implemented further support for interoperability
//
// ============================================================================
