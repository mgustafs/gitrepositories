// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2001.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: VersionInfo.cpp
//
//     Description: Version Information class definition
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: versioninfo.cpp %
//        %version: 6 %
//          %state: working %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: Monday, October 07, 2002 1:00:37 PM %
//
// ============================================================================

#include "StdAfx.h"
#include "VersionInfo.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace
{
  struct CTranslation
    {
      WORD m_language;
      WORD m_codePage;
    };
}

namespace PDCLib
{
WORD const CVersionInfo::CP_ANSI    (0x04E4);
WORD const CVersionInfo::CP_UNICODE (0x04B0);

CVersionInfo::CVersionInfo (void)
{
}

CVersionInfo::~CVersionInfo (void)
{
}

bool
CVersionInfo::Create (CString const & fileName)
{
  return Attach (fileName);
}

bool
CVersionInfo::Create (HMODULE hModule)
{
  CString l_fileName;

  if (::GetModuleFileName (hModule, l_fileName.GetBuffer (MAX_PATH), MAX_PATH) > 0)
    {
      l_fileName.ReleaseBuffer ();

      return Attach (l_fileName);
    }

  return false;
}

bool
CVersionInfo::GetCompanyName (CString & companyName, WORD codePage) const
{
  return FindResource (companyName, codePage, _T ("CompanyName"));
}

bool
CVersionInfo::GetFileDescription (CString & fileDescription, WORD codePage) const
{
  return FindResource (fileDescription, codePage, _T ("FileDescription"));
}

bool
CVersionInfo::GetFileVersion (CString & fileVersion, WORD codePage) const
{
  return FindResource (fileVersion, codePage, _T ("FileVersion"));
}

bool
CVersionInfo::GetInternalName (CString & internalName, WORD codePage) const
{
  return FindResource (internalName, codePage, _T ("InternalName"));
}

bool
CVersionInfo::GetLegalCopyright (CString & legalCopyright, WORD codePage) const
{
  return FindResource (legalCopyright, codePage, _T ("LegalCopyright"));
}

bool
CVersionInfo::GetOriginalFilename (CString & originalFilename, WORD codePage) const
{
  return FindResource (originalFilename, codePage, _T ("OriginalFilename"));
}

bool
CVersionInfo::GetProductName (CString & productName, WORD codePage) const
{
  return FindResource (productName, codePage, _T ("ProductName"));
}

bool
CVersionInfo::GetProductVersion (CString & productVersion, WORD codePage) const
{
  return FindResource (productVersion, codePage, _T ("ProductVersion"));
}

bool
CVersionInfo::GetComments (CString & comments, WORD codePage) const
{
  return FindResource (comments, codePage, _T ("Comments"));
}

bool
CVersionInfo::GetLegalTrademarks (CString & legalTrademarks, WORD codePage) const
{
  return FindResource (legalTrademarks, codePage, _T ("LegalTrademarks"));
}

bool
CVersionInfo::GetPrivateBuild (CString & privateBuild, WORD codePage) const
{
  return FindResource (privateBuild, codePage, _T ("PrivateBuild"));
}

bool
CVersionInfo::GetSpecialBuild (CString & specialBuild, WORD codePage) const
{
  return FindResource (specialBuild, codePage, _T ("SpecialBuild"));
}

bool
CVersionInfo::GetFileVersion (std::vector <DWORD> & fileVersion, WORD codePage) const
{
  CString l_fileVersion;

  if (GetFileVersion (l_fileVersion, codePage))
    {
      try
        {
          PDCLib::Tokenize (l_fileVersion, _T (".,"), [&fileVersion] (auto token) { fileVersion.emplace_back (Convert <UInt32Converter> (token)); });

          return true;
        }
      catch (...)
        {
        }
    }

  return false;
}

bool
CVersionInfo::Attach (CString const & fileName)
{
  DWORD l_handle (0);
  
  if (auto const l_size (::GetFileVersionInfoSize ((LPTSTR) (LPCTSTR) fileName, &l_handle)); l_size > 0)
    {
      m_versionInfo.resize (l_size);

      return ::GetFileVersionInfo ((LPTSTR) (LPCTSTR) fileName, 0, m_versionInfo.size (), &m_versionInfo[0]) == TRUE;
    }

  return false;
}

bool
CVersionInfo::FindResource (CString & resourceValue, WORD codePage, CString const & resourceName) const
{
  UINT l_len (0);

  CTranslation * l_begin (nullptr);

  if (::VerQueryValue ((LPCVOID) &m_versionInfo[0], _T ("\\VarFileInfo\\Translation"), reinterpret_cast <LPVOID *> (&l_begin), &l_len))
    {
      auto const l_end (l_begin + (l_len / sizeof (CTranslation)));

      if (auto const l_i (std::find_if (l_begin, l_end, [codePage] (auto translation) { return translation.m_codePage == codePage; })) ;l_i != l_end)
        {
          CString l_subBlock;

          l_subBlock.Format (_T ("\\StringFileInfo\\%04x%04x\\%s"), (*l_i).m_language, (*l_i).m_codePage, (LPCTSTR) resourceName);

          LPTSTR l_buf (nullptr);

          if (::VerQueryValue ((LPCVOID) &m_versionInfo[0], (LPTSTR) (LPCTSTR) l_subBlock, reinterpret_cast <LPVOID *> (&l_buf), &l_len))
            {
              resourceValue = l_buf;

              return true;
            }
        }
    }

  return false;
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  08/06/2002  MCC     initial revision
//  07/19/2005  MCC     replaced all double-underscores
//  12/04/2006  MCC     relocated class into PDCLib DLL
//  11/19/2014  MCC     corrected code analysis issues
//  08/08/2018  MCC     implemented more robust TwinCAT 3 ADS detection
//  08/10/2018  MCC     refactored numeric converstion templates
//
// ============================================================================
