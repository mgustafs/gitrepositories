// ============================================================================
//
//                              CONFIDENTIAL
//
//        GENOMICS INSTITUTE OF THE NOVARTIS RESEARCH FOUNDATION (GNF)
//
//  This is an unpublished work of authorship, which contains trade secrets,
//  created in 2002.  GNF owns all rights to this work and intends to maintain
//  it in confidence to preserve its trade secret status.  GNF reserves the
//  right, under the copyright laws of the United States or those of any other
//  country that may have jurisdiction, to protect this work as an unpublished
//  work, in the event of an inadvertent or deliberate unauthorized publication.
//  GNF also reserves its rights under all copyright laws to protect this work
//  as a published work, when appropriate.  Those having access to this work
//  may not copy it, use it, modify it or disclose the information contained
//  in it without the written authorization of GNF.
//
// ============================================================================

// ============================================================================
//
//            Name: Method.cpp
//
//     Description: Method class defintion
//
//          Author: Mike Conner
//
// ============================================================================

// ============================================================================
//
//      %subsystem: 1 %
//           %name: method.cpp %
//        %version: 40 %
//          %state: working %
//         %cvtype: c++ %
//     %derived_by: mconner %
//  %date_modified: Tuesday, October 08, 2002 8:34:29 AM %
//
// ============================================================================

#include "StdAfx.h"
#include "Method.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace
{
  const UINT FILE_OPEN_R_FLAGS (CFile::modeRead       |
                                CFile::shareDenyWrite |
                                CFile::modeNoInherit);

  const UINT FILE_OPEN_W_FLAGS (CFile::modeWrite      |
                                CFile::shareExclusive |
                                CFile::modeCreate);
}

namespace PDCLib
{
IMPLEMENT_SERIAL (CMethod, CVariableList, VERSIONABLE_SCHEMA | METHOD_SCHEMA_VERSION_13)

CMethod::CMethod (void)
  : m_deviceType (Dispenser)
  , m_controllerType (Galil)
  , m_deviceId (0)
  , m_isTemplate (true)
  , m_refCount (1)
{
}

CMethod::CMethod (CMethod const & other)
  : CVariableList (other)
  , m_deviceType (other.m_deviceType)
  , m_controllerType (other.m_controllerType)
  , m_deviceId (other.m_deviceId)
  , m_calibrationFile (other.m_calibrationFile)
  , m_isTemplate (other.m_isTemplate)
  , m_refCount (1)
{
  POSITION l_pos (other.GetFirstIterator ());

  while (l_pos)
    {
      AddIterator (other.GetNextIterator (l_pos)->Duplicate (true));
    }
}

CMethod::~CMethod ()
{
  while (!m_iteratorList.IsEmpty ())
    {
      delete m_iteratorList.RemoveHead ();
    }
}

void
CMethod::RemoveIterator (POSITION pos, bool deleteIterator)
{
  if (deleteIterator)
    {
      delete m_iteratorList.GetAt (pos);
    }

  m_iteratorList.RemoveAt (pos);
}

CStep *
CMethod::GetStep (const CString & stepAlias) const
{
  CStep * l_step (NULL);

  POSITION l_pos (GetFirstIterator ());

  while (l_pos != NULL)
    {
      CIterator const * const l_iterator (GetNextIterator (l_pos));

      if ((l_step = l_iterator->GetStep (stepAlias)) != NULL)
        {
          break;
        }
    }

  return l_step;
}

CVariable *
CMethod::GetVariable (const CString & stepAlias, const CString & variableAlias) const
{
  CVariable * l_variable (NULL);

  POSITION l_pos (GetFirstIterator ());

  while (l_pos != NULL)
    {
      CIterator const * const l_iterator (GetNextIterator (l_pos));

      if ((l_variable = l_iterator->GetVariable (stepAlias, variableAlias)) != NULL)
        {
          break;
        }
    }

  return l_variable;
}

void
CMethod::Release (void)
{
  if (::InterlockedDecrement (&m_refCount) == 0)
    {
      delete this;
    }
}

void
CMethod::Serialize (CArchive & ar)
{
  CVariableList::Serialize (ar);

  if (ar.IsLoading ())
    {
      try
        {
          switch (GetObjectSchema ())
            {
              case METHOD_SCHEMA_VERSION_0:
              case METHOD_SCHEMA_VERSION_1:
                {
                  CString l_comment;

                  ar >> m_deviceType;
                  ar >> m_controllerType;
                  ar >> m_calibrationFile;
                  ar >> l_comment;
                  ar >> m_isTemplate;

                  SetComment (l_comment);

                  SerializeIteratorList (ar);

                  break;
                }
              case METHOD_SCHEMA_VERSION_2:
              case METHOD_SCHEMA_VERSION_3:
              case METHOD_SCHEMA_VERSION_4:
              case METHOD_SCHEMA_VERSION_5:
              case METHOD_SCHEMA_VERSION_6:
              case METHOD_SCHEMA_VERSION_7:
                {
                  CString l_comment;

                  ar >> m_deviceType;
                  ar >> m_controllerType;
                  ar >> m_deviceId;
                  ar >> m_calibrationFile;
                  ar >> l_comment;
                  ar >> m_isTemplate;

                  SetComment (l_comment);

                  SerializeIteratorList (ar);

                  break;
                }
              case METHOD_SCHEMA_VERSION_8:
              case METHOD_SCHEMA_VERSION_9:
              case METHOD_SCHEMA_VERSION_10:
              case METHOD_SCHEMA_VERSION_11:
              case METHOD_SCHEMA_VERSION_12:
              case METHOD_SCHEMA_VERSION_13:
                {
                  ar >> m_deviceType;
                  ar >> m_controllerType;
                  ar >> m_deviceId;
                  ar >> m_calibrationFile;
                  ar >> m_isTemplate;

                  SerializeIteratorList (ar);

                  break;
                }
              default:
                {
                  ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

                  break;
                }
            }
        }
      catch (CArchiveException *)
        {
          // Destroy all variables regardless of reference count.

          CArray <POSITION, POSITION const &> l_pos;

          l_pos.SetSize (2);

          l_pos[0] = GetFirstVariable ();

          while ((l_pos[1] = l_pos[0]) != NULL)
            {
              CVariable * l_variable (GetNextVariable (l_pos[0]));

              l_variable->AddRef ();

              RemoveVariable (l_pos[1]);

              delete l_variable;
            }

          throw;
        }
    }
  else
    {
      ar << m_deviceType;
      ar << m_controllerType;
      ar << m_deviceId;
      ar << m_calibrationFile;
      ar << m_isTemplate;

      SerializeIteratorList (ar);
    }
}

std::unique_ptr <CMethod>
CMethod::ReadObject (CArchive & ar)
{
  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  std::unique_ptr <CMethod> l_method;

  try
    {
      std::unique_ptr <CObject> l_object (ar.ReadObject (NULL));

      l_method.reset (dynamic_cast <CMethod *> (l_object.get ()));

      if (l_method.get () == NULL)
        {
          ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());
        }
      else
        {
          l_object.release ();
        }
    }
  catch (CArchiveException * e)
    {
      e->Delete ();

      CFile * const l_file (ar.GetFile ());

      ar.Close ();

      l_file->SeekToBegin ();

      CArchive l_ar (l_file, CArchive::load);

      l_method.reset (new CMethod);

      l_method->Serialize (l_ar);

      l_ar.Close ();
    }

  return l_method;
}

std::unique_ptr <CMethod>
CMethod::ReadObject (CString const & fileName)
{
  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  CFile l_file (fileName, FILE_OPEN_R_FLAGS);

  CArchive l_archive (&l_file, CArchive::load);

  return ReadObject (l_archive);
}

void
CMethod::WriteObject (CArchive & ar, CMethod & method)
{
  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  ar.WriteObject (&method);
}

void
CMethod::WriteObject (CString const & fileName, CMethod & method)
{
  AFX_MANAGE_STATE (AfxGetStaticModuleState ());

  CFile l_file (fileName, FILE_OPEN_W_FLAGS);

  CArchive l_archive (&l_file, CArchive::store);

  WriteObject (l_archive, method);
}

void
CMethod::SerializeIteratorList (CArchive & ar)
{
  if (ar.IsLoading ())
    {
      switch (GetObjectSchema ())
        {
          case METHOD_SCHEMA_VERSION_0:
          case METHOD_SCHEMA_VERSION_1:
          case METHOD_SCHEMA_VERSION_2:
            {
              std::unique_ptr <CIterator> l_iterator (new CIterator);

              CObList l_stepList;

              l_stepList.Serialize (ar);

              POSITION l_pos (l_stepList.GetHeadPosition ());

              while (l_pos != NULL)
                {
                  l_iterator->AddStep (dynamic_cast <CStep *> (l_stepList.GetNext (l_pos)));
                }

              m_iteratorList.AddTail (l_iterator.release ());

              break;
            }
          case METHOD_SCHEMA_VERSION_3:
          case METHOD_SCHEMA_VERSION_4:
          case METHOD_SCHEMA_VERSION_5:
          case METHOD_SCHEMA_VERSION_6:
          case METHOD_SCHEMA_VERSION_7:
          case METHOD_SCHEMA_VERSION_8:
          case METHOD_SCHEMA_VERSION_9:
          case METHOD_SCHEMA_VERSION_10:
          case METHOD_SCHEMA_VERSION_11:
          case METHOD_SCHEMA_VERSION_12:
          case METHOD_SCHEMA_VERSION_13:
            {
              m_iteratorList.Serialize (ar);

              break;
            }
          default:
            {
              ::AfxThrowArchiveException (CArchiveException::badSchema, ar.GetFile ()->GetFilePath ());

              break;
            }
        }
    }
  else
    {
      m_iteratorList.Serialize (ar);
    }
}
}

// ============================================================================
//  R E V I S I O N    N O T E S
// ============================================================================
//
//  For each change to this file, record the following:
//
//   1. who made the change and when the change was made
//   2. why the change was made and the intended result
//
// ============================================================================
//
//  Date        Author  Description
// ----------------------------------------------------------------------------
//  08/05/2002  MCC     initial revision
//  10/08/2002  MCC     added reference counting
//  07/21/2003  MCC     updated version schema
//  07/30/2003  MCC     improved memory mangement with STL smart pointers
//  08/28/2003  MCC     modified code for QAC++ compliance
//  10/29/2003  MCC     implemented step interator feature
//  01/27/2004  MEG     used global enumeration EDeviceType
//  07/21/2004  MCC     modified to initialize pointers on construction
//  08/12/2004  MCC     implemented support for tissue culture dispenser
//  09/13/2004  MCC     implemented support for cell counter dispenser
//  07/19/2005  MCC     replaced all double-underscores
//  08/09/2005  MCC     implemented support for microfluidizer device type
//  12/13/2005  MEG     implemented support for ac/tc station device types
//  01/11/2006  MCC     removed workaround for MFC serialization memory leak
//  03/22/2006  MEG     updated version schema
//  04/14/2006  MEG     modified to use namespace PDCLib
//  04/27/2006  MEG     removed EDeviceType from global namespace
//  09/28/2006  TAN     add FCDispenser device type
//  10/03/2006  MCC     implemented support for cell generator device type
//  10/03/2006  MEG     implemented support for centrifuge device type
//  10/11/2006  TAN     reversed order for AC and TC stations
//  11/14/2006  MCC     added fully qualified variable accessor
//  11/28/2006  MCC     baselined serializable topology class hierarchy
//  12/06/2006  MCC     decoupled method and topology version schema constants
//  07/03/2006  MCC     implemented skip step feature
//  10/09/2008  MCC     implemented interface for skipping steps
//  04/28/2009  MCC     implemented support for drag and drop template editing
//  03/18/2010  MCC     implemented support for plate definition variable type
//  08/09/2010  MCC     enhanced functionality of step iterator
//  08/10/2010  MCC     implemented support for variable grouping
//  02/18/2011  MCC     implemented support for method variable logging
//  03/22/2011  MCC     implemented support modifying enumeration keys
//  07/03/2012  MCC     added support for variable specific plate definitions
//  07/20/2012  MCC     implemented support for iterator comment
//  04/30/2013  MCC     removed save before run method constraint
//  05/03/2013  MCC     corrected removal of save before run method constraint
//  07/17/2014  MCC     implemented support for executing shell commands
//  12/17/2014  MCC     replaced auto pointers with C++11 unique pointers
//  07/02/2015  MCC     added method editor to remote control interface
//
// ============================================================================
